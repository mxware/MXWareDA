#include "DummyHierarchy.hh"

#include "MXWare/Extensions/Plugins.hh"

class DerivedPlugin : public DummyInterface
{
public:
    DerivedPlugin() = default;

    DerivedPlugin(int x) : m_i{ x } {};

    int
    GetInt() const override
    {
        return m_i;
    }

    int m_i = 10;
};

MXWARE_DEFINE_PLUGIN(TestPlugin, 1)

MXWare::Extensions::Registry
TestPlugin::GetExtensions() const
{
    MXWare::Extensions::Registry ans{};
    ans.RegisterExtension<DerivedPlugin, DummyInterface>("DerivedPlugin");
    ans.RegisterExtension<DerivedPlugin, DummyInterface, int>("DerivedPlugin");
    ans.RegisterExtension<DerivedA, DummyInterface>("DerivedA");
    return ans;
}
