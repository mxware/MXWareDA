#include "catch2/catch.hpp"

#include "MXWare/Exceptions.hh"
#include "MXWare/Core/MXCourier.hh"
#include <iostream>
#include <boost/core/demangle.hpp>
#include <boost/current_function.hpp>

template <class E, class Base = MXWare::GeneralException>
void
TestMXException()
{
    std::cout << "Testing the exception " << boost::core::demangle(typeid(E).name()) << std::endl;
    // Simple Construction
    std::string SampleMessage{ "Sample message" };

    REQUIRE_NOTHROW(([&]() { E MyException(SampleMessage); }));

    REQUIRE_NOTHROW(([&]() { E MyException2("Class", BOOST_CURRENT_FUNCTION, SampleMessage); }));

    auto SimpleThrow = [&]() { throw E(SampleMessage); };
    REQUIRE_THROWS_AS(SimpleThrow(), E);

    auto SimpleRaise = [&]() {
        E MyException(SampleMessage);
        MyException.raise();
    };
    REQUIRE_THROWS_AS(SimpleRaise(), E);
    REQUIRE_THROWS_AS(SimpleRaise(), Base);
}

TEST_CASE("Testing the framework exception types", "[exceptions]")
{
    TestMXException<MXWare::GeneralException>();
    TestMXException<MXWare::NullPointerException, MXWare::GeneralException>();
    TestMXException<MXWare::ObjectNotFound, MXWare::GeneralException>();
    TestMXException<MXWare::DuplicateObject, MXWare::GeneralException>();
    TestMXException<MXWare::InvalidArgument, MXWare::GeneralException>();
    TestMXException<MXWare::OutOfRange, MXWare::InvalidArgument>();
    TestMXException<MXWare::RequirementFailure, MXWare::GeneralException>();
    TestMXException<MXWare::FileError, MXWare::GeneralException>();
    TestMXException<MXWare::FileNotFound, MXWare::GeneralException>();
    TestMXException<MXWare::FileAccessError, MXWare::GeneralException>();
}

TEST_CASE("Testing the courier exception types", "[exceptions]")
{
    TestMXException<MXWare::Core::MXCourierException, MXWare::GeneralException>();
    TestMXException<MXWare::Core::Courier::InvalidSocket, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::InvalidEndpoint, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::EndpointNotConnected, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::UnsupportedProtocol, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::IncompatibleProtocol, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::AddressInUse, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::AddressNotAvailable, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::NoDevices, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::IOServiceTerminated, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::IOServiceInvalid, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::MissingThread, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::MaxSupportedSockets, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::MessageDecodingError, MXWare::Core::MXCourierException>();
    TestMXException<MXWare::Core::Courier::SocketDisconnected, MXWare::Core::MXCourierException>();
}

