#include "catch2/catch.hpp"

#include "MXWare/Configuration.hh"

#include "MXWare/Execution/IElement.hh"
#include "MXWare/Execution/Node.hh"
#include "MXWare/Configuration/ICodec.hh"
#include "MXWare/Configuration/Specializations.hh"
#include "MXWare/Configuration/SmartParameterList.hh"
#include "MXWare/Configuration/SmartConfigurable.hh"

#include <chrono>
#include <ctime>
#include <sstream>
#include <string_view>
#include <algorithm>

TEST_CASE("Using a property tree", "[configuration]")
{
    using MXWare::Configuration::PropertyTree;
    PropertyTree MyTree{};
    REQUIRE(MyTree.empty());

    SECTION("How to input and access data")
    {
        MyTree.put("a.path.to.float.value", 3.14f);
        // Overwrites the value
        MyTree.put("a.path.to.float.value", 2.72f);
        // Adds a second node with the new value.
        // Returns the subtree that was modified
        auto ChildTree = MyTree.add("a.path.to.float.value", 3.14f);
        REQUIRE(!MyTree.empty());
        // get_value retrieves the data from a given node
        REQUIRE(ChildTree.get_value<float>() == Approx(3.14f));
        // get_child retrieves the tree at a given path
        auto InnerTree = MyTree.get_child("a.path.to.float");
        REQUIRE(InnerTree.count("value") == 2);
        // get_child_optional can be used to check if a path exists
        REQUIRE(!MyTree.get_child_optional("invalid.path"));
        // The get function returns the firts object found at a path
        REQUIRE(MyTree.get<float>("a.path.to.float.value") == Approx(2.72f));
        // The equal range function provides an iterator range of child with the same key
        auto ValueItRange = InnerTree.equal_range("value");
        // The first element of the associative iterator is the key
        REQUIRE(ValueItRange.first->first == "value");
        // the second is the tree connected to that key
        REQUIRE(ValueItRange.first->second.get_value<float>() == Approx(2.72f));
        REQUIRE(std::next(ValueItRange.first)->second.get_value<float>() == Approx(3.14f));
        // One can also check if a certain path contains something
        REQUIRE(!MyTree.get_optional<float>("invalid.path"));
    }

    SECTION("Boolean can be inserted as true/false or yes/no")
    {
        // true/false yes/no strings are correctly converted to booleans
        MyTree.put("logic.truth", "true");
        REQUIRE(MyTree.get<bool>("logic.truth"));
        MyTree.put("lie", "false");
        REQUIRE(!MyTree.get<bool>("lie"));
        MyTree.put("is.it.ok", "yes");
        REQUIRE(MyTree.get<bool>("is.it.ok"));
        MyTree.put("Something.wrong", "no");
        REQUIRE(!MyTree.get<bool>("Something.wrong"));

        // The automatic translator always uses true/false
        MyTree.put("auto.logic.truth", true);
        REQUIRE(MyTree.get_child("auto.logic.truth").data() == "true");
        MyTree.put("auto.logic.lie", false);
        REQUIRE(MyTree.get_child("auto.logic.lie").data() == "false");
    }

    SECTION("Time point can be input using the ISO convention <Date>T<Time>,<Fraction>")
    {
        auto Now = std::chrono::high_resolution_clock::now();
        MyTree.put("date.now", Now);
        std::cout << "An ISO date:\n" << MyTree.get_child("date.now").data() << '\n';
        auto Date = MyTree.get<decltype(Now)>("date.now");
        REQUIRE(Date == Now);
        MyTree.put("date", "20190219T091254.933");
        Date = MyTree.get<decltype(Date)>("date");
        auto TT = std::chrono::system_clock::to_time_t(Date);
        auto TM = *(std::gmtime(&TT));
        REQUIRE(TM.tm_year == (2019 - 1900));
        // TODO Use the date.h library
    }

    SECTION("Formatting a tree to XML")
    {
        MyTree.put("custom.string", "Something to talk about");
        MyTree.put("integer", 12);
        MyTree.put("logic", true);
        MyTree.put("date.now", std::chrono::system_clock::now());
        // The tree can be written to an existing stream using a custom codec
        REQUIRE_NOTHROW(
            MXWare::Configuration::StreamTree(std::cout, MyTree, "application/mxware+xml"));
        std::cout << '\n';
        REQUIRE_THROWS_AS(MXWare::Configuration::StreamTree(std::cout, MyTree, "invalid"),
            MXWare::ExtensionNotFound);
        // The tree can also output on a new stream
        // by default it uses the XML format
        std::stringstream NewOut = MXWare::Configuration::StreamTree(MyTree);
        // And a new tree can then be parsed back from the stream
        auto NewTree = MXWare::Configuration::ParseTree(NewOut, "application/mxware+xml");
        REQUIRE(MyTree.get<std::string>("custom.string") == "Something to talk about");
        REQUIRE(MyTree.get<int>("integer") == 12);
        REQUIRE(MyTree.get<bool>("logic") == true);
    }

    SECTION("Merging trees")
    {
        // TODO
    }
}

template <typename T>
void
TestSimpleObjectsConfiguration(const T& Obj)
{
    using MXWare::Configuration::PropertyTree;
    using MXWare::Configuration::WriteToTree;
    using MXWare::Configuration::UpdateParameter;

    // Writes the object configuration to a tree
    PropertyTree TestTree{};
    WriteToTree(Obj, TestTree, "object");
    REQUIRE((TestTree.find("object") != TestTree.not_found()));

    // Reconfigures the object from a tree
    T DefaultObj;
    REQUIRE(UpdateParameter(DefaultObj, TestTree, "object"));
    REQUIRE(!UpdateParameter(DefaultObj, TestTree, "not_object"));
    REQUIRE((Obj == DefaultObj));
}

template <typename T>
void
TestPointerObjectConfiguration(const T& Obj)
{
    using MXWare::Configuration::PropertyTree;
    using MXWare::Configuration::WriteToTree;
    using MXWare::Configuration::UpdateParameter;

    // Writes the object configuration to a tree
    PropertyTree TestTree{};
    WriteToTree(Obj, TestTree, "object");
    REQUIRE((TestTree.find("object") != TestTree.not_found()));

    auto DefaultObj = std::make_unique<typename std::pointer_traits<T>::element_type>();
    REQUIRE(UpdateParameter(DefaultObj, TestTree, "object"));
    REQUIRE(DefaultObj);
    REQUIRE(!UpdateParameter(DefaultObj, TestTree, "not_object"));
    REQUIRE(DefaultObj);
    REQUIRE((*Obj == *DefaultObj));
}

template <typename Interface, typename... Args>
std::unique_ptr<Interface>
TestExtensionObjectConfiguration(std::string_view TypeId, Args&&... args)
{
    using MXWare::Configuration::PropertyTree;
    using MXWare::Configuration::WriteToTree;
    using MXWare::Configuration::UpdateParameter;

    PropertyTree TestTree{};
    TestTree.put("object.type", std::string{ TypeId });
    std::unique_ptr<Interface> ans;
    REQUIRE(UpdateParameter(ans, TestTree, "object", std::forward<Args>(args)...));
    REQUIRE(ans);

    TestTree.clear();
    WriteToTree(ans, TestTree, "object");
    REQUIRE(TestTree.get<std::string>("object.type") == TypeId);
    return ans;
}

// A complex objects with method of the configurable signature
struct ConfigurableClass
{
    using PropertyTree = MXWare::Configuration::PropertyTree;

public:
    double x;
    std::string s;
    int i;

    void
    Configure(const PropertyTree& ConfTree)
    {
        using MXWare::Configuration::UpdateParameter;
        UpdateParameter(x, ConfTree, "x");
        UpdateParameter(s, ConfTree, "s");
        UpdateParameter(i, ConfTree, "i");
    }

    PropertyTree
    CurrentConfiguration() const
    {
        using MXWare::Configuration::WriteToTree;
        PropertyTree ans;
        WriteToTree(x, ans, "x");
        WriteToTree(s, ans, "s");
        WriteToTree(i, ans, "i");
        return ans;
    }

    friend bool
    operator==(const ConfigurableClass& lhs, const ConfigurableClass& rhs)
    {
        return (lhs.x == rhs.x && lhs.s == rhs.s && lhs.i == rhs.i);
    }

    friend bool
    operator<(const ConfigurableClass& lhs, const ConfigurableClass& rhs)
    {
        return (lhs.x < rhs.x || (lhs.x == rhs.x && lhs.i < rhs.i));
    }
};

TEST_CASE("Automatic configuration of custom objects", "[configuration]")
{
    SECTION("Simple objects can be configured if they can be streamed")
    {
        int A = 12;
        double X = 1.3;
        float F = 19.2f;
        bool B = false;
        std::string S = "Hello World";
        ConfigurableClass C{ X, S, A };

        TestSimpleObjectsConfiguration(A);
        TestSimpleObjectsConfiguration(X);
        TestSimpleObjectsConfiguration(F);
        TestSimpleObjectsConfiguration(B);
        TestSimpleObjectsConfiguration(S);
        TestSimpleObjectsConfiguration(C);
    }

    SECTION("Pointers to simple object can be configured in a similar way")
    {
        int A = 12;
        double X = 1.3;
        float F = 19.2f;
        bool B = false;
        std::string S = "Hello World";
        ConfigurableClass C{ X, S, A };

        TestPointerObjectConfiguration(&A);
        TestPointerObjectConfiguration(&X);
        TestPointerObjectConfiguration(&F);
        TestPointerObjectConfiguration(&B);
        TestPointerObjectConfiguration(&S);
        TestPointerObjectConfiguration(&C);
    }

    SECTION("Extensions can be configured using their type id")
    {
        using MXWare::Execution::Node;
        using MXWare::Execution::IElement;
        using MXWare::Configuration::ICodec;

        Node MyNode;
        auto Element = TestExtensionObjectConfiguration<IElement>("DummyElement", MyNode);
        // If the interface is NOT configurable but registered among the extensions
        // only the "type" attribute will be written
        auto Codec = TestExtensionObjectConfiguration<ICodec>(MXWare::Configuration::XMLFormatTag);
    }
}

TEMPLATE_PRODUCT_TEST_CASE(
    "Sequential containers can also be configured automatically with simple objects",
    "[configuration]", (std::vector, std::list, std::deque, std::set, std::multiset),
    (int, double, std::string, ConfigurableClass))
{
    static constexpr std::size_t N = 10;
    using ValueT = typename TestType::value_type;
    TestType Container{};
    std::fill_n(std::inserter(Container, Container.end()), N, ValueT{});
    auto InitialSize = Container.size();

    using MXWare::Configuration::PropertyTree;
    using MXWare::Configuration::WriteToTree;
    using MXWare::Configuration::UpdateParameter;

    // Writes the object configuration to a tree
    PropertyTree TestTree{};
    WriteToTree(Container, TestTree, "object");
    auto& SubTree = TestTree.get_child("object");
    REQUIRE(SubTree.count("val") == InitialSize);

    // Reconfigures the object from a tree
    TestType DefaultObj{};
    REQUIRE(UpdateParameter(DefaultObj, TestTree, "object"));
    REQUIRE(!UpdateParameter(DefaultObj, TestTree, "not_object"));
    PropertyTree RoundTripTree{};
    WriteToTree(DefaultObj, RoundTripTree, "object");
    REQUIRE((Container == DefaultObj));
}

TEMPLATE_TEST_CASE("Associative containers can be configured automatically", "[configuration]",
    (std::map<std::string, int>), (std::unordered_map<std::string, int>))
{
    using MXWare::Configuration::PropertyTree;
    using MXWare::Configuration::WriteToTree;
    using MXWare::Configuration::UpdateParameter;

    PropertyTree InputTree{};

    PropertyTree TmpChild{};
    TmpChild.put("key", "first");
    TmpChild.put("val", 1);
    InputTree.add_child("map", TmpChild);

    TmpChild.clear();
    TmpChild.put("val", 2);
    TmpChild.put("key", "second");
    InputTree.add_child("map", TmpChild);

    TmpChild.clear();
    TmpChild.put("val", 3);
    TmpChild.put("key", "first");
    auto& MapChild = InputTree.add_child("map", PropertyTree{});
    MapChild.add_child("rec", TmpChild);

    TestType Container{};
    UpdateParameter(Container, InputTree, "map");
    REQUIRE(Container.size() == 2);
    WriteToTree(Container, InputTree, "object");
    auto SubTree = InputTree.get_child("object");
    REQUIRE(SubTree.count("rec") == 2);
}

TEST_CASE("Sequences of pointers can be configured with the extension system", "[configuration]")
{
    using MXWare::Configuration::PropertyTree;
    using MXWare::Configuration::WriteToTree;
    using MXWare::Configuration::UpdateParameter;

    SECTION("Filling a vector of IElements")
    {
        using MXWare::Execution::IElement;
        using MXWare::Execution::Node;
        Node MyNode;
        PropertyTree Tree{};
        PropertyTree ElementTypeTree{};
        ElementTypeTree.put("type", "DummyElement");
        Tree.add_child("element", ElementTypeTree);
        Tree.add_child("element", ElementTypeTree);
        Tree.add_child("element", ElementTypeTree);
        // MXWare::Configuration::StreamTree(std::cout, Tree);
        std::vector<std::unique_ptr<IElement>> ElementVec{};
        REQUIRE(UpdateParameter(ElementVec, Tree, "element", MyNode));
        REQUIRE(ElementVec.size() == 3);
        PropertyTree RoundTripTree{};
        WriteToTree(ElementVec, RoundTripTree, "element");
        // MXWare::Configuration::StreamTree(std::cout, RoundTripTree);
    }
}

// TODO Review
TEST_CASE("Smart Parameter list test", "[configuration]")
{
    using MXWare::Configuration::detail::ParameterMetadata;
    using MXWare::Configuration::SmartParameterList;
    ParameterMetadata P1{ "Something", "Else" };
    REQUIRE(P1 == P1);
    ParameterMetadata P2{ "Another", "One" };
    REQUIRE(P2 < P1);

    int A = 12;
    double X = 1.3;
    float F = 19.2f;
    bool B = false;
    std::string S = "Hello World";
    ConfigurableClass I{ 12.1, "Internal", 1 };

    SmartParameterList List{};
    REQUIRE(List.empty());
    REQUIRE((List.AddParameter("a", "integer", A) && List.size() == 1 && !List.find("a")->IsSet()));
    REQUIRE((List.AddParameter("X", "double", X) && List.size() == 2));
    REQUIRE((!List.AddParameter("X", "duplicate", X) && List.size() == 2));
    REQUIRE((List.AddParameter("F", "float", F) && List.size() == 3));
    REQUIRE((List.AddParameter("B", "bool", B) && List.size() == 4));
    REQUIRE((List.AddParameter("S", "string", S) && List.size() == 5));
    REQUIRE((List.AddParameter("I", "Configurable", I) && List.size() == 6));

    auto OutTree = List.CreateTree();
    std::cout << OutTree << std::endl;

    SECTION("Iteration test")
    {
        auto count = List.size();
        for (auto& Par : List)
        {
            Par.IsSet();
            --count;
        }
        REQUIRE(count == 0U);

        for (auto it = List.rbegin(); it != List.rend(); ++it)
        {
            ++count;
        }
        REQUIRE(count == List.size());
    }

    SECTION("Dependency test")
    {
        double energy = 10;
        REQUIRE(
            (List.AddPhysicalParameter("MeV", "Energy in MeV", energy, 0.001) && List.size() == 7));
        REQUIRE(
            (List.AddPhysicalParameter("GeV", "Energy in GeV", energy, 1.0) && List.size() == 8));
        List.SetParameterDependency("GeV", "MeV");

        auto Tree = List.CreateTree();
        List.UpdateAll(Tree);
        REQUIRE(energy == 10);
    }
}

class AutoConfigured : public MXWare::Configuration::SmartConfigurable
{
public:
    AutoConfigured(double D = 0.0, const std::string& S = "", int Z = 0) :
        MXWare::Configuration::SmartConfigurable{}, x{ D }, s{ S }, i{ Z }
    {}

    friend bool
    operator==(const AutoConfigured& lhs, const AutoConfigured& rhs)
    {
        return (lhs.x == rhs.x && lhs.s == rhs.s && lhs.i == rhs.i);
    }

    friend bool
    operator<(const AutoConfigured& lhs, const AutoConfigured& rhs)
    {
        return (lhs.x < rhs.x || (lhs.x == rhs.x && lhs.i < rhs.i));
    }

private:
    double x;
    std::string s;
    int i;

    /// Registers the configurable parameters in the parameter list
    void
    RegisterParameters(MXWare::Configuration::SmartParameterList& Pars) const override
    {
        Pars.AddParameter("double", "A floating point number", x);
        Pars.AddParameter("string", "A string", s);
        Pars.AddParameter("integer", "An integer", i);
    }
};

TEST_CASE("Smart configurable classes", "[configuration]")
{
    AutoConfigured AC{ 12.1, "AutoConfigured", 1 };
    static_assert(MXWare::Configuration::detail::has_currentconfiguration<AutoConfigured>::value,
        "Internal class doesn't have current configuration");

    TestSimpleObjectsConfiguration(AC);
}

// TODO Review and test the parser