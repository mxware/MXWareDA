#pragma once

#include <memory>

class DummyInterface
{
public:
    virtual ~DummyInterface() = default;

    virtual int
    GetInt() const = 0;
};

class DerivedA : public DummyInterface
{
public:
    DerivedA() = default;

    DerivedA(int x):m_i{x}{};

    int
    GetInt() const override 
    { return m_i; }

    int m_i = 0;
};

class DerivedB : public DummyInterface
{
public:
    DerivedB() = default;

    DerivedB(int x):m_i{x + 1}{};

    int
    GetInt() const override 
    { return m_i; }

    int m_i = 1;
};

class DerivedC : public DummyInterface
{
public:
    DerivedC() = default;

    DerivedC(int x):m_i{x + 2}{};

    int
    GetInt() const override 
    { return m_i; }

    int m_i = 2;
};

template <class Derived>
std::unique_ptr<DummyInterface>
CreateDerivedDummy()
{
    return std::make_unique<Derived>();
}

template <class Derived>
std::unique_ptr<DummyInterface>
CreateDerivedDummyI(int a)
{
    return std::make_unique<Derived>(a);
}

class SecondDummyInterface
{
    virtual ~SecondDummyInterface() = default;
};

template <class Derived>
std::unique_ptr<SecondDummyInterface>
CreateSecondDummy()
{
    return std::make_unique<Derived>();
}