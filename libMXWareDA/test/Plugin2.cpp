#include "MXWare/Extensions/Plugins.hh"

#include "DummyHierarchy.hh"

class DerivedPlugin20 : public DummyInterface
{
public:
    DerivedPlugin20() = default;

    DerivedPlugin20(int x) : m_i{ x } {};

    int
    GetInt() const override
    {
        return m_i;
    }

    int m_i = 20;
};

MXWARE_DEFINE_PLUGIN(Plugin2, 1)

MXWare::Extensions::Registry
Plugin2::GetExtensions() const
{
    MXWare::Extensions::Registry ans{};
    ans.RegisterExtension<DerivedPlugin20, DummyInterface>("DerivedPlugin20");
    ans.RegisterExtension<DerivedPlugin20, DummyInterface, int>("DerivedPlugin20");
    ans.RegisterExtension<DerivedA, DummyInterface>("DerivedA");
    return ans;
}
