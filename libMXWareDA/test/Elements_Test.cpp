/*
 * ElementsTest.cpp
 *
 *  Created on: 1 Jan 2020
 *      Author: caiazza
 */

#include "catch2/catch.hpp"

#include "MXWare/Execution.hh"
#include "MXWare/Log.hh"
#include "MXWare/Extensions.hh"

#include <memory>
#include <iostream>

using MXWare::Execution::IElement;
using MXWare::Execution::ElementList;
using MXWare::Execution::Node;
using MXWare::Configuration::PropertyTree;

bool
TestElementTransitions(std::unique_ptr<IElement>& Element)
{
    REQUIRE(Element);

    PropertyTree ans{};

    REQUIRE(Element->GetState() == IElement::State::Loaded);
    REQUIRE(Element->Enable() == IElement::State::Ready);
    REQUIRE(Element->Configure(ans) == IElement::State::Ready);
    REQUIRE(Element->Start(ans) == IElement::State::Running);
    REQUIRE(Element->Pause() == IElement::State::Suspended);
    REQUIRE(Element->Continue() == IElement::State::Running);
    REQUIRE(Element->Stop(ans) == IElement::State::Ready);
    REQUIRE(Element->Reset() == IElement::State::Ready);
    REQUIRE(Element->Disable() == IElement::State::Loaded);

    return true;
}

TEST_CASE("Testing the dummy element", "[Elements]")
{    
    using MXWare::Extensions::CreateObject;

    Node N{};
    N.GetLoggingService().SetLevel(MXWare::Log::Level::Trace);

    auto DummyElement = CreateObject<IElement>("DummyElement", N);
    REQUIRE(DummyElement);
    REQUIRE(MXWare::Extensions::GetRegistration(*DummyElement) == "DummyElement");
    DummyElement->SetName("LittleDummy");

    SECTION("Test Element Transitions") { TestElementTransitions(DummyElement); }

    SECTION("Auto configuration")
    {
        std::cout << "Testing auto configuration\n";
        PropertyTree TestTree{};
        REQUIRE(TestTree.empty());
        MXWare::Configuration::WriteToTree(DummyElement, TestTree, "");
        std::cout << TestTree << std::endl;
        REQUIRE(!TestTree.empty());

        std::unique_ptr<IElement> E0{};
        REQUIRE(!E0);
        MXWare::Configuration::UpdateParameter(E0, TestTree, "", N);
        REQUIRE(E0);
        REQUIRE(E0->GetName() == DummyElement->GetName());
    }
}

TEST_CASE("Test of the elements list", "[Elements]")
{
    using MXWare::Extensions::CreateObject;  
    
    Node N{};
    N.GetLoggingService().SetLevel(MXWare::Log::Level::Trace);
    // auto CurrentImplementations = MXWare::Core::GetBuiltinImplementations();

    ElementList EL{};
    REQUIRE(EL.empty());
    EL.push_back(CreateObject<IElement>("DummyElement", N));
    REQUIRE(EL.size() == 1);
    EL.begin()->SetName("LittlePony");

    SECTION("Autoconfiguration Test")
    {
        MXWare::Configuration::PropertyTree TestTree{};
        REQUIRE(TestTree.empty());
        MXWare::Configuration::WriteToTree(EL, TestTree, "");
        REQUIRE(!TestTree.empty());
        REQUIRE(TestTree.size() == 1);
        std::cout << TestTree << std::endl;

        MXWare::Configuration::PropertyTree Subtree;
        REQUIRE_NOTHROW(Subtree = TestTree.get_child("val"));
        REQUIRE(!Subtree.empty());
        std::cout << "Subtree" << std::endl;
        std::cout << Subtree << std::endl;

        TestTree.add_child("val", Subtree);
        TestTree.add_child("val", Subtree);
        std::cout << TestTree << std::endl;
        MXWare::Configuration::UpdateParameter(EL, TestTree, "", N);
        REQUIRE(EL.size() == 3);
        MXWare::Configuration::WriteToTree(EL, TestTree, "");
        std::cout << TestTree << std::endl;
    }
}
