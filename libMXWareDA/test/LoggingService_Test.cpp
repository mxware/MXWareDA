#include "catch2/catch.hpp"

#include "MXWare/Log.hh"
namespace MXLog = MXWare::Log;

#include "spdlog/sinks/base_sink.h"
#include <mutex>

#include "MXWare/Extensions.hh"

// class test_sink : public spdlog::sinks::base_sink<std::mutex>
// {
// public:
//     bool
//     SinkedMsg() const
//     {
//         return m_SinkedMsg;
//     }

// private:
//     using spdlog::sinks::base_sink<std::mutex>::base_sink;

//     void
//     sink_it_(const spdlog::details::log_msg&)
//     {
//         m_SinkedMsg = true;
//     }
//     void
//     flush_()
//     {
//         m_SinkedMsg = false;
//     }

//     bool m_SinkedMsg{ false };
// };

TEST_CASE("Standalone loggers tests", "[MXLogger]")
{
    using MXWare::Log::ISink;
    MXLog::Logger TestLogger{ "test" };
    REQUIRE(TestLogger.GetLevel() == MXLog::Level::Info);
    auto ConsoleSink = std::make_unique<MXLog::Sinks::StdoutColor>();
    REQUIRE(TestLogger.AddSink(*ConsoleSink));
    TestLogger.Info("Logging on console");
    TestLogger.Warn("Warning on console");
    REQUIRE(!TestLogger.AddSink(*ConsoleSink));
    auto ExtSink = MXWare::Extensions::CreateObject<ISink>(MXLog::Sinks::StdoutColor::TypeId());
    REQUIRE(ExtSink);
    REQUIRE(TestLogger.AddSink(*ExtSink));
    TestLogger.Info("Logging again");

    // REQUIRE(TestLogger.sinks().empty());
    // auto TestSink = std::make_shared<test_sink>();
    // REQUIRE(!TestSink->SinkedMsg());
    // TestLogger.sinks().push_back(TestSink);
    // REQUIRE(TestLogger.sinks().size() == 1);

    // TestLogger.Debug("Test");
    // REQUIRE(TestSink->SinkedMsg());
    // TestLogger.flush();
    // REQUIRE(!TestSink->SinkedMsg());
}


TEST_CASE("Logging service test", "[MXLogger]") { MXLog::LoggingService TestService{}; }
