#include "catch2/catch.hpp"

#include <thread>

#include "MXWare/Core/MXCourier.hh"
namespace MXCore = MXWare::Core;

#include "MXWare/Log.hh"
namespace MXLog = MXWare::Log;

#include <zmq.hpp>
#include <iostream>

TEST_CASE("zmq simple test", "[]")
{
    auto ctx = std::make_unique<zmq::context_t>(1);
    auto Router = zmq::socket_t{ *ctx, zmq::socket_type::router };
    Router.bind("inproc://Test");

    auto Dealer = zmq::socket_t{ *ctx, zmq::socket_type::dealer };
    Dealer.connect("inproc://Test");

    Dealer.send(zmq::message_t{ std::string{ "TestHeader" } }, zmq::send_flags::sndmore);
    Dealer.send(zmq::message_t{ "TestDealer", 10 }, zmq::send_flags::dontwait);

    zmq::message_t Identity{};
    auto Res = Router.recv(Identity, zmq::recv_flags::dontwait);
    if (Res)
    {
        std::cout << "Identity received: " << Identity.str() << "\n"
                  << std::string{ static_cast<char*>(Identity.data()), Identity.size() }
                  << std::endl;
    }

    REQUIRE(Identity.more());

    zmq::message_t TheRest{};
    Res = Router.recv(TheRest, zmq::recv_flags::dontwait);
    std::cout << "Frame received: " << TheRest.str() << "\n"
              << std::string{ static_cast<char*>(TheRest.data()), TheRest.size() } << std::endl;
    while (TheRest.more())
    {
        Res = Router.recv(TheRest, zmq::recv_flags::dontwait);
        std::cout << "Frame received: " << TheRest.str() << "\n"
                  << std::string{ static_cast<char*>(TheRest.data()), TheRest.size() } << std::endl;
    }

    Router.send(Identity, zmq::send_flags::sndmore);
    Router.send(zmq::message_t{ "TestDealer" }, zmq::send_flags::dontwait);

    std::vector<zmq::pollitem_t> Sockets2Poll{};
    Sockets2Poll.push_back(zmq::pollitem_t{ static_cast<void*>(Dealer), 0, ZMQ_POLLIN, 0 });
    zmq::poll(Sockets2Poll, std::chrono::milliseconds{ 1 });

    if (Sockets2Poll[0].revents & (ZMQ_POLLIN))
    {
        Res = Dealer.recv(TheRest, zmq::recv_flags::dontwait);
        if (Res)
        {
            std::cout << "The rest received: " << TheRest.str() << "\n"
                      << std::string{ static_cast<char*>(TheRest.data()), TheRest.size() }
                      << std::endl;
        }
    }
}

TEST_CASE("Create and connect services", "[messaging]")
{
    auto Courier = MXCore::MXCourier::NewCourier();
    Courier->Log().SetLevel(MXLog::Level::Trace);
    REQUIRE(static_cast<bool>(Courier));
    auto ClientInternal = Courier->CreateClient();
    REQUIRE(ClientInternal);
    auto Server = Courier->CreateServer();
    REQUIRE(Server);
    auto ClientRemote = Courier->CreateClient();
    REQUIRE(ClientRemote);

    const std::string InternalEndpoint{ "TestInternal" };
    REQUIRE_NOTHROW(Server->BindInternal(InternalEndpoint));
    REQUIRE_NOTHROW(Server->BindNetworkInterface());
    REQUIRE(Server->GetNBoundEndpoints() == 2);

    REQUIRE_NOTHROW(ClientInternal->ConnectInternal(InternalEndpoint));

    const std::string NetworkEndpoint{ "127.0.0.1" };
    REQUIRE_NOTHROW(ClientRemote->ConnectRemote(NetworkEndpoint));
    // std::this_thread::sleep_for(std::chrono::seconds{ 2 });

    auto Req1 = MXCore::Courier::Request{ "NodeControl", "Testing 1" };
    auto Req2 = MXCore::Courier::Request{ "NodeQuery", "Testing 2" };

    auto Rep1 = ClientInternal->SendRequest(std::move(Req1));
    auto Rep2 = ClientRemote->SendRequest(std::move(Req2));

    MXCore::Courier::Reply R;
    REQUIRE_NOTHROW(R = Rep1.get());
    REQUIRE(R.GetCategory() == "501");
    Courier->Log().Info("Reply received with category: {}", R.GetCategory());
    REQUIRE_NOTHROW(R = Rep2.get());
    REQUIRE(R.GetCategory() == "501");
    Courier->Log().Info("Reply received with category: {}", R.GetCategory());

    std::this_thread::sleep_for(std::chrono::milliseconds{ 500 });

    Courier->Log().Info("Disconnecting");
    REQUIRE_NOTHROW(ClientInternal->Disconnect());
    REQUIRE_NOTHROW(ClientRemote->Disconnect());
}
