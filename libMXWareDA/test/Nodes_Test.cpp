#include "catch2/catch.hpp"

#include "MXWare/Execution/Node.hh"

#include <memory>
#include <iostream>

using MXWare::Execution::Node;

bool
TestNodeOperations(Node& MyNode)
{
    auto StandardConf = MyNode.CurrentConfiguration();
    REQUIRE(!StandardConf.empty());

    return true;
}

TEST_CASE("Testing the node creation", "[Nodes]")
{
    Node MyNode{};
    TestNodeOperations(MyNode);
}
