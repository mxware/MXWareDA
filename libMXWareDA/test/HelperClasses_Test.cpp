#include "catch2/catch.hpp"

#include "MXWare/Utilities/PolymorphicConstructible.hh"
#include "MXWare/Utilities/Cloneable.hh"
#include "MXWare/Utilities/Comparable.hh"

class DefaultInterface :
    public MXWare::Utilities::DefaultConstructible<DefaultInterface>,
    public MXWare::Utilities::PolymorphicConstructible<DefaultInterface, int>,
    public MXWare::Utilities::Cloneable<DefaultInterface>,
    public MXWare::Utilities::Comparable<DefaultInterface>
{
public:
    virtual ~DefaultInterface() = default;

    virtual int
    GetN() const = 0;

    virtual void
    SetN(int a) = 0;

    using MXWare::Utilities::DefaultConstructible<DefaultInterface>::Create;
    using MXWare::Utilities::PolymorphicConstructible<DefaultInterface, int>::Create;
    using MXWare::Utilities::Cloneable<DefaultInterface>::Clone;

private:
    virtual DefaultInterface*
    RawCreate() const override = 0;

    virtual DefaultInterface*
    RawCreate(int&& a) const override = 0;

    virtual DefaultInterface*
    RawClone() const override = 0;
};

class DefaultA : public DefaultInterface
{
public:
    DefaultA() : m_a{ 0 } {}

    DefaultA(int a) : m_a(a) {}

    virtual int
    GetN() const override
    {
        return m_a;
    }

    virtual void
    SetN(int a) override
    {
        m_a = a;
    }

private:
    int m_a;

    DefaultA*
    RawCreate() const override
    {
        return new DefaultA();
    }

    DefaultA*
    RawCreate(int&& a) const override
    {
        return new DefaultA(a);
    }

    DefaultA*
    RawClone() const override
    {
        return new DefaultA{ *this };
    }

    friend bool
    operator==(const DefaultA& lhs, const DefaultA& rhs)
    {
        return lhs.m_a == rhs.m_a;
    }

    friend bool
    operator!=(const DefaultA& lhs, const DefaultA& rhs)
    {
        return !(lhs == rhs);
    }

    bool
    IsEqual(const compare_base& rhs) const override
    {
        // return static_cast<const DefaultA&>(rhs).m_a == m_a;
        return (*this == static_cast<const DefaultA&>(rhs));
    }
};

TEST_CASE("Default Constructible class creation", "[creation]")
{
    std::unique_ptr<DefaultInterface> Source{ new DefaultA() };
    Source->SetN(12);
    REQUIRE(Source->GetN() == 12);
    auto Child = Source->Create();
    REQUIRE(Child->GetN() == 0);

    auto Child2 = Source->Create(9);
    REQUIRE(Child2->GetN() == 9);

    auto ChildClone = Source->Clone();
    REQUIRE((ChildClone->GetN() == Source->GetN()));
    REQUIRE((*ChildClone == *Source));
}
