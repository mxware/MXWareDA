#include "catch2/catch.hpp"

#include "MXWare/Extensions/GeneralizedFactory.hh"
#include <memory>
#include <iostream>

#include "DummyHierarchy.hh"

using DummyFactory =
    MXWare::Extensions::GeneralizedFactory<std::unique_ptr<DummyInterface>(), std::string>;

TEST_CASE("Class Registration names", "[traits]")
{
    std::cout << "Dummy factory registration name:\n" <<
        DummyFactory::UniqueFactoryId() << '\n';
}

TEST_CASE("Concrete factory tests", "[factory]")
{
    DummyFactory DF{};
    REQUIRE(DF.empty());
    REQUIRE(DF.size() == 0);
    REQUIRE(DF.begin() == DF.end());
    REQUIRE(DF.cbegin() == DF.cend());

    REQUIRE(DF.RegisterImplementation(CreateDerivedDummy<DerivedA>,
        "DerivedA", typeid(DerivedA)));

    REQUIRE(DF.count("DerivedA"));
    REQUIRE(DF.find("DerivedA") != DF.end());
    REQUIRE(DF.FindRegistration<DerivedA>() != DF.end());

    REQUIRE(DF.RegisterImplementation(CreateDerivedDummy<DerivedB>, "DerivedB", typeid(DerivedB)));

    REQUIRE(DF.size() == 2);

    SECTION("IFactory functions")
    {
        auto IFactoryPtr = static_cast<MXWare::Extensions::IFactory<std::string>*>(&DF);
        REQUIRE(IFactoryPtr->GetUniqueFactoryId() == DummyFactory::UniqueFactoryId());
        REQUIRE(IFactoryPtr->size() == DF.size());
        REQUIRE(IFactoryPtr->CountRegistrations(typeid(DerivedA)) == 1);
        REQUIRE(IFactoryPtr->CountRegistrations("DerivedA") == 1);
        REQUIRE(IFactoryPtr->FindRegistrationKey(typeid(DerivedA)));
    }

    SECTION("Unregistration")
    {
        REQUIRE(DF.UnregisterImplementation("DerivedA"));
        REQUIRE(DF.size() == 1);

        REQUIRE_FALSE(DF.UnregisterImplementation("DerivedA"));
        REQUIRE(DF.size() == 1);

        REQUIRE(DF.UnregisterImplementation(typeid(DerivedB)));
        REQUIRE(DF.empty());
    }

    SECTION("Registration errors")
    {
        REQUIRE_FALSE(
            DF.RegisterImplementation(CreateDerivedDummy<DerivedA>, "DerivedA", typeid(DerivedA)));
        REQUIRE(DF.size() == 2);

        REQUIRE_FALSE(DF.UnregisterImplementation("DerivedFalse"));
        REQUIRE(DF.size() == 2);
    }


    SECTION("Copy and move tests")
    {
        DummyFactory DF2{ DF };
        REQUIRE(DF.size() == 2);
        REQUIRE(DF == DF2);

        DummyFactory DF3{};
        REQUIRE(DF3.empty());
        REQUIRE(DF3 != DF);

        DF3 = DF2;
        REQUIRE(DF3 == DF);

        DummyFactory DF4{ std::move(DF2) };
        REQUIRE(DF4 == DF);
    }

    SECTION("Creation test")
    {
        auto DA_ptr = DF.CreateObject("DerivedA");
        REQUIRE(DA_ptr);
        auto& DA_ref = *DA_ptr;
        REQUIRE(typeid(DA_ref) == typeid(DerivedA));
        REQUIRE(DF.FindRegistration(*DA_ptr) != DF.end());
    }

    SECTION("Factory Merging")
    {
        DummyFactory DF_Merge{};
        DF_Merge.RegisterImplementation(CreateDerivedDummy<DerivedA>, "DerivedA", typeid(DerivedA));

        DF_Merge.RegisterImplementation(CreateDerivedDummy<DerivedC>, "DerivedC", typeid(DerivedC));

        REQUIRE(DF_Merge.size() == 2);

        auto MergedKeys = DF.Merge(DF_Merge);
        REQUIRE(MergedKeys.size() == 1);
        REQUIRE(DF.size() == 3);
    }

    SECTION("Factory Cloning")
    {
        auto DFCloned = DF.Clone();
        REQUIRE(DFCloned->size() == DF.size());
    }
}