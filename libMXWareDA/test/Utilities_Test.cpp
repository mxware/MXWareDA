#include "catch2/catch.hpp"

#include "MXWare/Random/RandomString.hh"
#include "MXWare/Utilities/FlatLookupTable.hh"
#include "MXWare/Log/Level.hh"

#include <iostream>
#include <string_view>

TEST_CASE("Generating a random string", "[utility]")
{
    using namespace MXWare::Utilities;
    using namespace MXWare::Random;

    auto RandomString = GenerateRandomString(16);
    for (auto& C : RandomString)
    {
        CHECK(std::count(std::begin(CharacterSets::Alphanumeric),
                  std::end(CharacterSets::Alphanumeric), C) > 0);
    }
    std::cout << "Random string:\n" << RandomString << '\n';
}

TEST_CASE("Using the flat lookup table")
{
    using namespace MXWare::Utilities;
    using namespace std::literals::string_view_literals;
    using MXWare::Log::Level;
    static constexpr auto LevelLookup = FlatLookupTable<std::string_view, Level, 7>{
        std::array<std::pair<std::string_view, Level>, 7>{
            { { "trace"sv, Level::Trace }, { "debug"sv, Level::Debug }, { "info"sv, Level::Info },
                { "warning"sv, Level::Warning }, { "error"sv, Level::Error },
                { "critical"sv, Level::Critical }, { "off"sv, Level::Off } } }
    };
    REQUIRE(LevelLookup.at("trace"sv) == Level::Trace);
}