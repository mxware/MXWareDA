#include "catch2/catch.hpp"

#include <filesystem>
#include <iostream>

#include "MXWare/Extensions.hh"
#include "MXWare/Extensions/Registry.hh"
#include "MXWare/Extensions/PluginManager.hh"
#include "MXWare/Exceptions.hh"
#include "DummyHierarchy.hh"

#include <boost/core/demangle.hpp>

TEST_CASE("Checking the uniqueness of demangling")
{
    std::cout << "typeid hash:\n" << typeid(MXWare::Extensions::GlobalPluginManager).hash_code() << '\n';
    std::cout << "mangled name:\n" << typeid(MXWare::Extensions::GlobalPluginManager).name() << '\n';
    std::cout << "demangled name:\n"
              << boost::core::demangle(typeid(MXWare::Extensions::GlobalPluginManager).name()) << std::endl;
}

TEST_CASE("Testing the extension registry", "[core][extensions]")
{
    std::cerr << "Testing the extensions registry\n";
    MXWare::Extensions::Registry CustomRegistry{};
    REQUIRE(CustomRegistry.empty());
    REQUIRE(CustomRegistry.CountRegisteredExtensions() == 0);

    REQUIRE(CustomRegistry.RegisterExtension<DerivedA, DummyInterface>("DerivedA"));
    REQUIRE(CustomRegistry.CountRegisteredExtensions() == 1);
    REQUIRE(CustomRegistry.RegisterExtension<DerivedB, DummyInterface>("DerivedB"));
    REQUIRE(CustomRegistry.CountRegisteredExtensions() == 2);
    REQUIRE(CustomRegistry.RegisterExtension<DerivedA, DummyInterface, int>("DerivedA"));
    REQUIRE(CustomRegistry.CountRegisteredExtensions() == 3);

    MXWare::Extensions::Registry BackupReg{};
    REQUIRE(BackupReg.empty());
    BackupReg = CustomRegistry;    
    REQUIRE(BackupReg.CountRegisteredExtensions() == 3);
    MXWare::Extensions::Registry BackupCopy{CustomRegistry};
    REQUIRE(BackupCopy.CountRegisteredExtensions() == 3);
    BackupReg.clear();    
    REQUIRE(BackupReg.empty());
    swap(BackupReg, BackupCopy);
    REQUIRE(BackupCopy.empty());
    BackupCopy.Merge(CustomRegistry);
    REQUIRE(BackupCopy.CountRegisteredExtensions() == 3);
    BackupReg.Merge(CustomRegistry);
    REQUIRE(BackupReg.CountRegisteredExtensions() == 3);

    auto DA = CustomRegistry.CreateObject<DummyInterface>("DerivedA");
    REQUIRE(DA);
    std::cout << "DA Registration " << CustomRegistry.GetExtensionRegistration(*DA).value() << '\n';
    auto DAI = CustomRegistry.CreateObject<DummyInterface>("DerivedA", 5);
    REQUIRE(DAI);
    REQUIRE(DAI->GetInt() == 5);
    auto DC = CustomRegistry.CreateObject<DummyInterface>("DerivedC");
    REQUIRE(!DC);

    REQUIRE(CustomRegistry.CountRegistrations<DummyInterface>("DerivedA") == 1);
    REQUIRE(CustomRegistry.UnregisterExtension<DummyInterface>("DerivedA"));
    REQUIRE(!CustomRegistry.UnregisterExtension<DummyInterface>("DerivedA"));
    REQUIRE(CustomRegistry.CountRegistrations<DummyInterface>("DerivedA") == 0);
    
    REQUIRE(MXWare::Extensions::Builtins.CountRegisteredExtensions() == 4);
}

TEST_CASE("Plugin manager operations", "[core][extensions]")
{
    std::cout << "Current working directory: " << std::filesystem::current_path().string() << '\n';

    // MXWare::Extensions::Registry CustomReg{};
    MXWare::Extensions::PluginManager PM{};
    REQUIRE(PM.AddSearchFolder(std::filesystem::current_path()));
    REQUIRE(!PM.AddSearchFolder(std::filesystem::current_path()));
    REQUIRE_THROWS_AS((PM.AddSearchFolder(std::filesystem::current_path() / "libTestPlugin.so")),
        MXWare::FileNotFound);
    
    PM.AddSearchFolder(std::filesystem::current_path() / "..");
    
    SECTION("Configuration test")
    {
    auto SearchFoldersConfiguration = PM.CurrentConfiguration();
    std::cout << "Current configuration (with search folders):\n" << SearchFoldersConfiguration;

    MXWare::Extensions::PluginManager PM2{};
    PM2.Configure(SearchFoldersConfiguration);
    std::cout << "Copied through configuration:\n" << PM2.CurrentConfiguration();
    }

    SECTION("Plugin searching test")
    {
        PM.LoadValidPlugins();
        auto LoadedPlugins = PM.CountLoadedPlugins();
        REQUIRE(LoadedPlugins == 2);
        PM.LoadValidPlugins();
        REQUIRE(PM.CountLoadedPlugins() == 2);        
        auto LoadedExtensions =  PM.CountRegisteredExtensions();
        std::cout << "Number of loaded extensions: " << LoadedExtensions << '\n';
        REQUIRE(LoadedExtensions == 6);
    }

    SECTION("Extension Query and creation")
    {
        PM.LoadValidPlugins();
        REQUIRE(PM.CountLoadedPlugins() == 2);
        REQUIRE(PM.CountRegisteredExtensions<DummyInterface>("DerivedA") == 2);
        REQUIRE(PM.CountRegisteredExtensions<DummyInterface, int>("DerivedPlugin") == 1);
        auto DerA = PM.CreateExtensionObject<DummyInterface>("DerivedPlugin");
        REQUIRE(DerA);
        REQUIRE( DerA->GetInt() == 10);
        auto DerAInt = PM.CreateExtensionObject<DummyInterface>("DerivedPlugin", 5);
        REQUIRE((DerAInt && DerAInt->GetInt() == 5));
    }
}

TEST_CASE("Global pluginb manager operations", "[core][extensions]")
{
    MXWare::Extensions::GlobalPluginManager.AddSearchFolder(std::filesystem::current_path());
    MXWare::Extensions::GlobalPluginManager.AddSearchFolder(std::filesystem::current_path() / "..");
    MXWare::Extensions::GlobalPluginManager.LoadValidPlugins();

    auto DerA = MXWare::Extensions::CreateObject<DummyInterface>("DerivedA");
    REQUIRE(DerA);
    REQUIRE( DerA->GetInt() == 0);

    REQUIRE(MXWare::Extensions::GetRegistration(*DerA) == "DerivedA");
}