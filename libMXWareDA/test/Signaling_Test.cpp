#include "catch2/catch.hpp"

#include "MXWare/Signals.hh"

#include <iostream>
#include <sstream>

MXWare::Signals::Signal<void()> MySignal;
std::stringstream msg;

auto Hello = []() { msg << "Hello"; };

auto Hi = []() { msg << "Hi! "; };

auto World = []() { msg << ", World!"; };

TEST_CASE("Signal Creation", "[Signals]")
{
    msg.str("");
    auto Connection = MySignal.connect(Hello);
    auto Connection2 = MySignal.connect_front(Hi);
    auto Connection3 = MySignal.connect(World);
    MySignal.emit();

    std::cout << msg.str() << std::endl;
    REQUIRE((msg.str() == "Hi! Hello, World!"));

    msg.str("");
    Connection2.Disconnect();
    MySignal.emit();
    std::cout << msg.str() << std::endl;
    REQUIRE((msg.str() == "Hello, World!"));
}

MXWare::Signals::Signal<void(int)> IntSignal;
auto Number = [](int i) { msg << " Number " << i; };

TEST_CASE("Argument Passing", "[Signals]")
{
    msg.str("");
    auto Connection = MySignal.connect(Hello);
    auto Connection2 = MySignal.connect_front(Hi);
    auto Block = Connection2.GetBlock();
    REQUIRE(Connection2.Blocked());
    auto Connection3 = MySignal.connect(World);
    auto Connection4 = IntSignal.connect(Number);
    MySignal.emit();

    int n = 1;
    IntSignal.emit(n);

    std::cout << msg.str() << std::endl;

    REQUIRE((msg.str() == "Hello, World! Number 1"));
}
