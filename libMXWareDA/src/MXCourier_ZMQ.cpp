
#include "MXCourier_ZMQ.hh"
#include "Client_ZMQ.hh"
#include "Server_ZMQ.hh"
#include <boost/current_function.hpp>
#include "spdlog/sinks/stdout_color_sinks.h"

namespace MXWare
{
namespace Core
{

/* **********************************************************************

                Courier implementation

*********************************************************************** */

MXCourier_ZMQ::MXCourier_ZMQ(int io_threads) :
    std::enable_shared_from_this<MXCourier_ZMQ>{},
    m_Context{ std::make_unique<zmq::context_t>(io_threads) },
    m_DefaultCourierLogger{Log::GlobalService.GetOrCreateLogger(MXCourier::DefaultLoggerName)}
{
    m_DefaultCourierLogger.Info("ZMQ-based courier service initialized");
}

void
swap(MXCourier_ZMQ& lhs, MXCourier_ZMQ& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_Context, rhs.m_Context);
}

MXCourier_ZMQ::MXCourier_ZMQ(MXCourier_ZMQ&& other) noexcept : MXCourier_ZMQ{}
{
    swap(*this, other);
}

MXCourier_ZMQ&
MXCourier_ZMQ::operator=(MXCourier_ZMQ&& other) noexcept
{
    swap(*this, other);
    return *this;
}

zmq::context_t&
MXCourier_ZMQ::ZMQ_Context()
{
    return *m_Context;
}

std::unique_ptr<Courier::Client>
MXCourier_ZMQ::CreateClient()
{
    Log().Debug("Creating a new Courier client");
    return std::make_unique<Courier::Client_ZMQ>(shared_from_this());
}

std::unique_ptr<Courier::Server>
MXCourier_ZMQ::CreateServer()
{
    Log().Debug("Creating a new Courier server");
    return std::make_unique<Courier::Server_ZMQ>(shared_from_this());
}

Log::Logger&
MXCourier_ZMQ::Log()
{
    return m_DefaultCourierLogger;
}

void
MXCourier_ZMQ::Configure(const Configuration::PropertyTree&)
{}

Configuration::PropertyTree
MXCourier_ZMQ::CurrentConfiguration() const
{
    return Configuration::PropertyTree{};
}

std::shared_ptr<MXCourier>
MXCourier::NewCourier()
{
    return std::make_shared<MXCourier_ZMQ>(1);
}

} // namespace Core
} // namespace MXWare
