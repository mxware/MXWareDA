/*
 * MXCourier.cpp
 * Implementation independent parts of MXCourier
 *  Created on: Nov 12, 2019
 *      Author: caiazza
 */

#include "MXWare/Core/MXCourier.hh"

namespace MXWare
{
namespace Core
{

MXWARE_IMPLEMENT_EXCEPTION(MXCourierException, GeneralException)

namespace Courier
{
MXWARE_IMPLEMENT_EXCEPTION(InvalidEndpoint, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(EndpointNotConnected, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(UnsupportedProtocol, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(IncompatibleProtocol, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(AddressInUse, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(AddressNotAvailable, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(NoDevices, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(IOServiceTerminated, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(InvalidSocket, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(IOServiceInvalid, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(MissingThread, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(MaxSupportedSockets, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(MessageDecodingError, MXCourierException)
MXWARE_IMPLEMENT_EXCEPTION(SocketDisconnected, InvalidSocket)

Request::Request() : m_Category{ "" }, m_Payload{} {}

Request::Request(const std::string& Category) : m_Category{ Category }, m_Payload{} {}

Request::Request(const std::string& Category, RawPayload&& Payload) :
    m_Category{ Category }, m_Payload(std::move(Payload))
{}


Request::Request(const std::string& Category, const std::string& Payload) :
    m_Category{ Category }, m_Payload{ Payload.begin(), Payload.end() }
{}

void
swap(Request& lhs, Request& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_Category, rhs.m_Category);
    swap(lhs.m_Payload, rhs.m_Payload);
}

Request::Request(Request&& other) : Request() { swap(*this, other); }

Request&
Request::operator=(Request other)
{
    swap(*this, other);
    return *this;
}

bool
Request::Valid() const
{
    return !m_Category.empty();
}


Reply::Reply() : m_Category{ "" }, m_Payload{} {}

Reply::Reply(const std::string& Category) : m_Category{ Category }, m_Payload{} {}

Reply::Reply(const std::string& Category, RawPayload&& Payload) :
    m_Category{ Category }, m_Payload(std::move(Payload))
{}

void
swap(Reply& lhs, Reply& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_Category, rhs.m_Category);
    swap(lhs.m_Payload, rhs.m_Payload);
}

Reply::Reply(Reply&& other) : Reply() { swap(*this, other); }

Reply&
Reply::operator=(Reply other)
{
    swap(*this, other);
    return *this;
}

bool
Reply::Valid() const
{
    return !m_Category.empty();
}

} // namespace Courier

} // namespace Core


} // namespace MXWare
