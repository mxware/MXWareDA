#include "MXWare/Extensions/Builtins.hh"

#include "PropertyTreeCodecs.hh"
#include "MXWare/Log/Sinks/StdoutColor.hh"
#include "MXWare/Log/Sinks/SingleFileSink.hh"

#include "MXWare/Execution/ElementBase.hh"
#include "MXWare/Log.hh"

namespace MXWare::Execution
{

class DummyElement : public ElementBase<DummyElement>
{
public:
    DummyElement(Node& N) : crtp_base{ N } { MXWare::Log::Info("Creating a dummy element"); }

    void
    ExecutionTask(const MXWare::Configuration::PropertyTree&) override
    {
        MXWare::Log::Info("I'm your dummy");
    }

private:
};

} // namespace MXWare::Execution

namespace MXWare::Extensions
{
Registry
CreateBuiltinsRegistry() noexcept
{
    Registry ans{};
    ans.RegisterExtension<Configuration::XML_ConfTreeCodec, Configuration::ICodec>(
        Configuration::XMLFormatTag);
    ans.RegisterExtension<Execution::DummyElement, Execution::IElement, Execution::Node&>(
        "DummyElement");
    ans.RegisterExtension<Log::Sinks::StdoutColor, Log::ISink>(Log::Sinks::StdoutColor::TypeId());
    ans.RegisterExtension<Log::Sinks::SingleFileSink, Log::ISink>(Log::Sinks::SingleFileSink::TypeId());
    return ans;
}

// const Registry&
// GetBuiltinsRegistry() noexcept
// {
//     static auto Builtins = CreateBuiltinsRegistry();
//     return Builtins;
// }
} // namespace MXWare::Extensions