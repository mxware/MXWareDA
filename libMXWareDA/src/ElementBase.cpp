/*
 * ElementBase.cpp
 *
 *  Created on: 29 Dec 2019
 *      Author: caiazza
 */

#include "MXWare/Execution/ElementBase.hh"

#include "MXWare/Execution/Node.hh"
#include "MXWare/Log.hh"
#include "MXWare/Utilities/real_async.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"
#include "MXWare/Configuration/SmartParameterList.hh" //<- TODO Replace with SmartCOnfigurable?

#include <atomic>
#include <functional>
#include <mutex>
#include <utility>
#include <future>
#include <cassert>

namespace MXWare::Execution::detail
{
/// Implementation class of the ElementStableBase class
/// It is just a container of the private members of the mother class
class ElementBase_Complete::Impl
{
    friend class ElementBase_Complete;

public:
    ///@{
    /// The gang of five
    /// Move and copy are not necessary because the base class can just move
    /// the implementation pointer around
    Impl(Node& N) :
        m_Node{ N },
        m_Name{ "" },
        m_State{ IElement::State::Loaded },
        m_TransitionAccess{},
        m_ExecutionHandle{},
        m_MonitoringHandle{},
        m_StopMonitoring{ true },
        m_StopExecution{ true },
        m_PauseExecution{ false },
        m_Log{ N.GetLoggingService().GeneralLogger() },
        m_Pars{}
    {}

    ~Impl() = default;

    Impl(const Impl& other) = delete;
    Impl&
    operator=(const Impl& other) = delete;

    Impl(Impl&& other) = delete;
    Impl&
    operator=(Impl&& other) = delete;
    ///@}

    auto
    Enable(ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        try
        {
            if (m_State.load() == State::Loaded)
            {
                // The monitoring handle should not be valid in LOADED mode
                assert(!m_MonitoringHandle.valid());
                m_StopMonitoring.store(false);
                m_MonitoringHandle = real_async([&]() { MonitorElement(E); });
                E.doEnable();
                m_Log.get().Debug("Element enabled");
                m_State.store(State::Ready);
            }
        }
        catch (...)
        {
            HandleTransitionExceptions(E);
        }
        return m_State.load();
    }

    auto
    Configure(const Configuration::PropertyTree& Data, ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        try
        {
            switch (m_State.load())
            {
            case State::Loaded: {
                Enable(E);
            }
                [[fallthrough]];
            case State::Ready: {
                if (!m_Pars.IsInitialized())
                {
                    E.RegisterParameters(m_Pars);
                }
                m_Pars.UpdateAll(Data);
                E.doConfigure(Data);
                break;
            }
            default: {
            }
            }
        }
        catch (...)
        {
            HandleTransitionExceptions(E);
        }
        return m_State.load();
    }

    auto
    Start(const Configuration::PropertyTree& Data, ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        try
        {
            if (m_State.load() == State::Ready)
            {
                E.doStart(Data);
                assert(!m_ExecutionHandle.valid());
                m_StopExecution.store(false);
                m_PauseExecution.store(false);
                m_ExecutionHandle = real_async([&]() { E.ExecutionTask(Data); });
                m_Log.get().Debug("Element named {} is running", m_Name);
                m_State.store(State::Running);
            }
        }
        catch (...)
        {
            HandleTransitionExceptions(E);
        }
        return m_State.load();
    }

    auto
    Pause(ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        try
        {
            if (m_State.load() == State::Running)
            {
                m_PauseExecution.store(true);
                E.doPause();
                m_Log.get().Debug("Element named {} has been paused", m_Name);
                m_State.store(State::Suspended);
            }
        }
        catch (...)
        {
            HandleTransitionExceptions(E);
        }
        return m_State.load();
    }

    auto
    Continue(ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        if (m_State.load() == State::Suspended)
        {
            try
            {
                m_PauseExecution.store(false);
                E.doContinue();
                m_Log.get().Debug("Element named {} has been continued", m_Name);
                m_State.store(State::Running);
            }
            catch (...)
            {
                HandleTransitionExceptions(E);
            }
        }
        return m_State.load();
    }

    auto
    Stop(const Configuration::PropertyTree& Data, ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        Continue(E);

        if (m_State.load() == State::Running)
        {
            try
            {
                m_StopExecution.store(true);
                CollectExecutionFuture();
                E.doStop(Data);
                m_Log.get().Debug("Element named {} has been stopped", m_Name);
                m_State.store(State::Ready);
            }
            catch (...)
            {
                HandleTransitionExceptions(E);
            }
        }
        return m_State.load();
    }

    auto
    Reset(ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        if (m_State.load() == State::Ready)
        {
            try
            {
                E.doReset();
                m_Log.get().Debug("Element named {} has been reset", m_Name);
            }
            catch (...)
            {
                HandleTransitionExceptions(E);
            }
        }
        return m_State.load();
    }

    auto
    Disable(ElementBase_Complete& E) noexcept -> State
    {
        std::lock_guard Lock(m_TransitionAccess);
        m_PauseExecution.store(false);
        m_StopExecution.store(true);
        CollectExecutionFuture();
        // A try catch here it's useless because doDisable is a noexcept function
        // therefore it will terminate anyway if it throws which it SHOULD NOT
        E.doDisable();

        m_StopMonitoring.store(true);
        CollectMonitoringFuture();

        m_State.store(State::Loaded);
        return m_State.load();
    }

private:
    /// The node connected
    std::reference_wrapper<Node> m_Node;

    /// The string containing the name of the element
    std::string m_Name;

    /// The current state of the element
    std::atomic<IElement::State> m_State;

    /// The mutex synchronizing the transitions between the states of the element
    mutable std::recursive_mutex m_TransitionAccess;

    /// The future to handle the execution task
    std::future<void> m_ExecutionHandle;

    /// The future to handle the monitoring of the element
    std::future<void> m_MonitoringHandle;

    /// A signal for stopping the monitoring
    std::atomic<bool> m_StopMonitoring;

    /// Flag to stop the task execution
    std::atomic<bool> m_StopExecution;

    /// Flag to pause the task execution
    std::atomic<bool> m_PauseExecution;

    /// The logger of this element
    std::reference_wrapper<Log::Logger> m_Log;

    /// The list of the configurable parameters of the class
    /// This member is mutable to be initialized also in the CurrentConfiguration function
    mutable Configuration::SmartParameterList m_Pars;

    void
    MonitorElement(ElementBase_Complete& E)
    {
        while (!m_StopMonitoring)
        {
            using std::chrono::milliseconds;
            if (m_ExecutionHandle.valid())
            {
                auto status = m_ExecutionHandle.wait_for(milliseconds{ 100 });
                if (status == std::future_status::ready)
                {
                    E.Stop();
                }
            }
            else
            {
                std::this_thread::sleep_for(milliseconds{ 1 });
            }
        }
    }

    /// Lippincott function to manage transition errors
    void
    HandleTransitionExceptions(ElementBase_Complete&)
    {
        try
        {
            throw;
        }
        catch (std::exception& e)
        {
            m_Log.get().Warn("Exception caught during a transition of element {}.\n Details:\n{}", m_Name, e.what());
            m_State.store(State::Error);
        }
        catch (...)
        {
            m_Log.get().Warn("Unrecognized exception during a transition");
            m_State.store(State::Error);
        }
    }

    /// Retrieves the future of the execution task
    void
    CollectExecutionFuture() noexcept
    {
        try
        {
            if (m_ExecutionHandle.valid())
            {
                m_ExecutionHandle.get();
            }
        }
        catch (std::exception& e)
        {
            m_Log.get().Warn("Exception caught during the task execution.\n Details:\n{}", e.what());
            m_State.store(State::Error);
        }
        catch (...)
        {
            m_Log.get().Warn("Exception caught during the task execution.");
            m_State.store(State::Error);
        }
    }

    /// Retrieves the future of the monitoring task
    void
    CollectMonitoringFuture() noexcept
    {
        try
        {
            if (m_MonitoringHandle.valid())
            {
                m_MonitoringHandle.get();
            }
        }
        catch (std::exception& e)
        {
            m_Log.get().Warn("Exception caught during the monitoring.\n Details:\n{}", e.what());
            m_State.store(State::Error);
        }
        catch (...)
        {
            m_Log.get().Warn("Exception caught during the monitoring.");
            m_State.store(State::Error);
        }
    }
};

ElementBase_Complete::ElementBase_Complete(Node& N) : m_impl{ new ElementBase_Complete::Impl{ N } }
{}

ElementBase_Complete::~ElementBase_Complete() noexcept { Disable(); }

ElementBase_Complete::ElementBase_Complete(ElementBase_Complete&& other) noexcept = default;

ElementBase_Complete&
ElementBase_Complete::operator=(ElementBase_Complete&& other) noexcept = default;

void
swap(ElementBase_Complete& lhs, ElementBase_Complete& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_impl, rhs.m_impl);
}


IElement::State
ElementBase_Complete::GetState() const
{
    return m_impl->m_State.load();
}

const std::string&
ElementBase_Complete::GetName() const
{
    return m_impl->m_Name;
}

void
ElementBase_Complete::SetName(const std::string& Name)
{
    m_impl->m_Name = Name;
}

const Node&
ElementBase_Complete::GetNode() const
{
    return m_impl->m_Node;
}

Node&
ElementBase_Complete::GetNode()
{
    return m_impl->m_Node;
}

IElement::State
ElementBase_Complete::Enable()
{
    return m_impl->Enable(*this);
}

IElement::State
ElementBase_Complete::Configure(const Configuration::PropertyTree& Data)
{
    return m_impl->Configure(Data, *this);
}

IElement::State
ElementBase_Complete::Start(const Configuration::PropertyTree& Data)
{
    return m_impl->Start(Data, *this);
}

IElement::State
ElementBase_Complete::Pause()
{
    return m_impl->Pause(*this);
}

IElement::State
ElementBase_Complete::Continue()
{
    return m_impl->Continue(*this);
}

IElement::State
ElementBase_Complete::Stop(const Configuration::PropertyTree& Data)
{
    return m_impl->Stop(Data, *this);
}

IElement::State
ElementBase_Complete::Reset()
{
    return m_impl->Reset(*this);
}

IElement::State
ElementBase_Complete::Disable() noexcept
{
    return m_impl->Disable(*this);
}

bool
ElementBase_Complete::ShouldStopExecution() const noexcept
{
    return m_impl->m_StopExecution.load();
}

bool
ElementBase_Complete::ShouldPauseExecution() const noexcept
{
    return m_impl->m_PauseExecution.load();
}

Configuration::PropertyTree
ElementBase_Complete::CurrentConfiguration() const
{
    std::lock_guard Lock(m_impl->m_TransitionAccess);
    if (!m_impl->m_Pars.IsInitialized())
    {
        RegisterParameters(m_impl->m_Pars);
    }

    return m_impl->m_Pars.CreateTree();
}

void
ElementBase_Complete::RegisterParameters(Configuration::SmartParameterList& Pars) const
{
    Pars.AddParameter("name", "Identifying name of the element", m_impl->m_Name);
}
} // namespace MXWare::Execution::detail
