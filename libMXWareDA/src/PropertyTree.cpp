#include "MXWare/Configuration/PropertyTree.hh"
#include "MXWare/Log.hh"
#include "MXWare/Extensions.hh"
#include "MXWare/Exceptions.hh"

#include "fmt/format.h"

#include <boost/version.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "PropertyTreeCodecs.hh"

#include <set>
#include <sstream>

namespace MXWare::Configuration
{

std::ostream&
StreamTree(std::ostream& Out, const PropertyTree& Tree, std::string_view FormatTag)
{
    auto Codec = Extensions::CreateObject<ICodec>(std::string{FormatTag});
    if(!Codec)
    {
        MXWare::ExtensionNotFound e{fmt::format("Codec with the tag {} not found", FormatTag)};
        e.raise();
    }    
    return Codec->Write(Out, Tree);
}

std::stringstream
StreamTree(const PropertyTree& Tree, std::string_view FormatTag)
{
    std::stringstream out;
    StreamTree(out, Tree, FormatTag);
    return out;
}

std::istream&
ParseTree(std::istream& In, PropertyTree& Tree, std::string_view FormatTag)
{
    auto Codec = Extensions::CreateObject<ICodec>(std::string{FormatTag});
    if(!Codec)
    {
        MXWare::ExtensionNotFound e{fmt::format("Codec with the tag {} not found", FormatTag)};
        e.raise();
    }
    return Codec->Read(In, Tree);
}

PropertyTree
ParseTree(std::istream& In, std::string_view FormatTag)
{
    PropertyTree ans{};
    ParseTree(In, ans, FormatTag);
    return ans;
}

namespace detail
{
boost::optional<BoolTranslator::external_type>
BoolTranslator::get_value(const BoolTranslator::internal_type& str)
{
    if (!str.empty())
    {
        auto LowerStr = str;
        std::transform(str.begin(), str.end(), LowerStr.begin(),
            [](unsigned char c) { return static_cast<char>(std::tolower(c)); });
        using boost::algorithm::iequals;

        if (iequals(LowerStr, "true") || iequals(LowerStr, "yes") || LowerStr == "1")
            return boost::optional<external_type>(true);
        else
            return boost::optional<external_type>(false);
    }
    else
        return boost::optional<external_type>(boost::none);
}
} // namespace detail

void
MergeConfigurationTrees(PropertyTree& Sink, const PropertyTree& Source, bool Overwrite)
{
    // If overwrite and the source data is not empty then do the overwrite
    if (Overwrite && (Source.data() != PropertyTree::data_type{}))
    {
        Sink.data() = Source.data();
    }
    else
    {
        // if the sink is empty write data from the source
        if (Sink.data() == PropertyTree::data_type{})
        {
            Sink.data() = Source.data();
        }
    }

    std::set<std::string> MergedElements{};
    // The comments are never considered in the merging
    // NOTE The comments are going to be eliminated by the parser
    // const std::string CommentNodeName = "<xmlcomment>";
    // MergedElements.insert(CommentNodeName);

    // We loop through the children of the source tree
    for (auto& SrcChild : Source)
    {
        // We already consider an element with this name and possibly merged it already
        if (MergedElements.find(SrcChild.first) != MergedElements.end())
        {
            continue;
        }

        // and we look for matches of the node name in the sink
        auto SinkMatch = Sink.count(SrcChild.first);

        // If there are no matches we just copy the all the children in the source with the same
        // identifier in the sink
        if (SinkMatch == 0)
        {
            for (auto& Element : Source)
            {
                if (Element.first == SrcChild.first)
                {
                    Sink.insert(Sink.end(), Element);
                }
            }
            // Marking that this element has been already merged
            MergedElements.insert(SrcChild.first);
        }
        // otherwise we have to decide if we have to merge the children
        // or add the source as further member of a list
        else
        {
            auto CheckIsNamed = [](const PropertyTree& Tree) -> bool {
                return (Tree.count("name") > 0);
            };
            /*
             * if the child in the source is named
             */
            if (CheckIsNamed(SrcChild.second))
            {
                auto SrcName = SrcChild.second.get<std::string>("name");

                // if there is a single matching item in the sink we
                // just match them directly
                if (SinkMatch == 1)
                {
                    auto& SinkFound = Sink.find(SrcChild.first)->second;

                    // If the sink object is named and has a different name we add
                    if (CheckIsNamed(SinkFound) && SrcName != SinkFound.get<std::string>("name"))
                    {
                        Sink.insert(Sink.end(), SrcChild);
                    }
                    // otherwise we merge
                    else
                    {
                        MergeConfigurationTrees(SinkFound, SrcChild.second, Overwrite);
                    }
                }
                // in this case we need to look among all objects in the sink
                // if any has a matching name
                else
                {
                    bool MatchFound = false;
                    for (auto& SinkChild : Sink)
                    {
                        auto SinkName = SinkChild.second.get<std::string>("name");
                        if (SinkName == SrcName)
                        {
                            MatchFound = true;
                            MergeConfigurationTrees(SinkChild.second, SrcChild.second, Overwrite);
                            break;
                        }
                    }
                    if (!MatchFound)
                    {
                        Sink.insert(Sink.end(), SrcChild);
                    }
                }
            }
            // if the child in the source is unnamed
            else
            {
                /*
                 * If there is more than one object under the same node tag in
                 * either the sink or the source or if the single sink object
                 * is named, considering that the source object is unnamed then
                 * we just add a new child with the same node name as member of
                 * of a list
                 */
                if (SinkMatch > 1 || Source.count(SrcChild.first) > 1 ||
                    CheckIsNamed(Sink.find(SrcChild.first)->second))
                {
                    Sink.insert(Sink.end(), SrcChild);
                }
                /*
                 * At this point the sink we know the sink has exactly one unnamed object
                 * just as the source object so we can merge them together
                 */
                else
                {
                    MergeConfigurationTrees(
                        Sink.find(SrcChild.first)->second, SrcChild.second, Overwrite);
                }
            }
        }
    }
}


std::istream&
XML_ConfTreeCodec::Read(std::istream& In, PropertyTree& Tree) const
{
    namespace pt = boost::property_tree;
    try
    {
        pt::read_xml(In, Tree, pt::xml_parser::trim_whitespace);
    }
    catch (pt::xml_parser_error& e)
    {
        Log::Framework::Warn("Unable to decode the input message as XML. Error: {}", e.what());
        In.setstate(std::istream::failbit);
        return In;
    }
    RemoveXMLComments(Tree);
    FlattenXMLAttributes(Tree);

    return In;
}

std::ostream&
XML_ConfTreeCodec::Write(std::ostream& Out, const PropertyTree& Tree) const
{
    namespace pt = boost::property_tree;
#if BOOST_VERSION >= 105600
    pt::xml_writer_settings<std::string> settings('\t', 1);
#else
    pt::xml_writer_settings<char> settings('\t', 1);
#endif

    try
    {
        pt::write_xml(Out, Tree, settings);
    }
    catch (pt::xml_parser_error& e)
    {
        Log::Framework::Warn(
            "Unable to encode the input configuration tree in the outgoing stream. Error: {}",
            e.what());
        Out.setstate(std::istream::failbit);
        return Out;
    }

    return Out;
}

void
XML_ConfTreeCodec::RemoveXMLComments(PropertyTree& Tree)
{
    /// node name for the XML comments in a property tree
    constexpr char g_CommentNodeName[] = "<xmlcomment>";

    PropertyTree::assoc_iterator itComment;
    while ((itComment = Tree.find(g_CommentNodeName)) != Tree.not_found())
    {
        Tree.erase(Tree.to_iterator(itComment));
    }

    for (auto& Node : Tree)
    {
        RemoveXMLComments(Node.second);
    }
}

void
XML_ConfTreeCodec::FlattenXMLAttributes(PropertyTree& Tree)
{
    constexpr char g_AttribNodeName[] = "<xmlattr>";

    auto itAttrib = Tree.find(g_AttribNodeName);
    if (itAttrib != Tree.not_found())
    {
        for (auto& Node : itAttrib->second)
        {
            if (Tree.count(Node.first) > 0)
            {
                auto itDuplicate = Tree.find(Node.first);
                while (itDuplicate != Tree.not_found())
                {
                    Tree.erase(Tree.to_iterator(itDuplicate));
                    itDuplicate = Tree.find(Node.first);
                }
            }

            // Inserts the new node before the position of <xmlattrib> element (typically the
            // beginning)
            Tree.insert(Tree.to_iterator(itAttrib), Node);
        }
        Tree.erase(Tree.to_iterator(itAttrib));
    }

    for (auto& Node : Tree)
    {
        FlattenXMLAttributes(Node.second);
    }
}

} // namespace MXWare::Configuration

namespace boost::property_tree
{
std::ostream&
operator<<(std::ostream& os, const ptree& Tree)
{
    MXWare::Configuration::XML_ConfTreeCodec Codec{};
    return Codec.Write(os, Tree);
}
} // namespace boost::property_tree