/*
 * MessageSerializer.cpp
 *
 *  Created on: Sep 2, 2019
 *      Author: caiazza
 */

#include "MXWare/Configuration/ConfigFileParser.hh"
#include "MXWare/Extensions.hh"
#include "MXWare/Log.hh"

#include <unordered_map>
#include <fstream>

#include <boost/version.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
namespace bfs = boost::filesystem;

#include <filesystem>
#include <fmt/ostream.h>

namespace MXWare::Configuration
{
class ConfigFileParser::Impl
{
public:
    Impl() {}

    std::istream&
    Read(std::istream& In, PropertyTree& Tree, const std::string FormatTag) const
    {
        auto& res = SimpleRead(In, Tree, FormatTag);
        if (!res.good())
        {
            return res;
        }

        // Now I decoded the raw version of the tree. Now I have to resolve the inclusions and
        // replace the common blocks
        ExpandIncludeBlocks(Tree, 0);
        auto CommonBlocks = ExtractCommonBlocks(Tree);
        for (auto& Block : CommonBlocks)
        {
            ReplaceIncludes(Block.second, CommonBlocks, 0);
        }

        ReplaceIncludes(Tree, CommonBlocks, 0);

        return res;
    }

    std::ostream&
    Write(std::ostream& Out, const PropertyTree& Tree, const std::string& FormatTag) const
    {
        auto Codec = Extensions::CreateObject<ICodec>(FormatTag);
        if (Codec)
        {
            return Codec->Write(Out, Tree);
        }
        else
        {
            Log::Framework::Warn(
                "Unknown format required to encode the configuration tree: {}", FormatTag);
            return Out;
        }
    }

private:
    friend class ConfigFileParser;

    /// The maximum number of nested inclusion allowed
    static constexpr uint32_t s_MaxNestingLevel = 50;

    /// The keyword used to include other blocks or files
    static constexpr char s_Include_Keyword[] = "_include";

    static constexpr char s_CommonBlock_Keyword[] = "common";

    /// A map of the common blocks
    using CommonBlockMap = std::unordered_map<std::string, PropertyTree>;

    /// This function simply reads the input stream without performing any additional elaboration of
    /// the message
    std::istream&
    SimpleRead(std::istream& In, PropertyTree& Tree, const std::string FormatTag) const
    {
        auto Codec = Extensions::CreateObject<ICodec>(FormatTag);
        if (Codec)
        {
            return Codec->Read(In, Tree);
        }
        else
        {
            Log::Framework::Warn(
                "Unknown format required to decode the incoming message: {}", FormatTag);
            return In;
        }
    }

    bool
    ExpandIncludeBlocks(PropertyTree& Tree, uint32_t Lvl) const
    {
        // Local keyword for the include nodes to specify at which path one has to locate the file
        // to include
        constexpr auto PathKeyword = "path";

        // Local keyword for the include nodes to specify the format to use for the decoding
        constexpr auto FormatKeyword = "format";

        if (Lvl > s_MaxNestingLevel)
        {
            Log::Framework::Warn(
                "Maximum nesting level ({}) reached while decoding an input message",
                s_MaxNestingLevel);
            return false;
        }

        // DEVNOTE I use the while loop because if I find an include I don't want to increment
        // the iterator but erase it and the erase function will return the element after that which
        // I just deleted
        auto IncludeRange = Tree.equal_range(s_Include_Keyword);
        std::vector<PropertyTree::assoc_iterator> ToErase{};
        auto ans = false;
        for (auto itInclude = IncludeRange.first; itInclude != IncludeRange.second; ++itInclude)
        {
            if (auto OptPath = itInclude->second.get_optional<std::string>(PathKeyword))
            {
                if (std::filesystem::is_regular_file(*OptPath))
                {
                    try
                    {
                        std::ifstream InputFile{};
                        InputFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
                        InputFile.open(*OptPath);
                        auto MyFormat =
                            itInclude->second.get<std::string>(FormatKeyword, XMLFormatTag);
                        PropertyTree SubTree{};
                        // If no format is specified we will use the XML format
                        this->SimpleRead(InputFile, SubTree, MyFormat);
                        // Iterative expansion of the new block
                        ExpandIncludeBlocks(SubTree, Lvl + 1);
                        Tree.insert(Tree.to_iterator(itInclude), SubTree.begin(), SubTree.end());
                        Log::Framework::Debug("Included file at {}", *OptPath);
                        ans = true;
                    }
                    catch (std::ifstream::failure&)
                    {
                        Log::Framework::Error("Unable to open the file at the path:\n{}", *OptPath);
                    }                       
                }
                else
                {
                    Log::Framework::Error("The specified include path:\n{}\n"
                                          "is not valid or does not match a regular file",
                        *OptPath);
                }
                ToErase.push_back(itInclude); 
            }
        }

        for(auto& it: ToErase)
        {
            Tree.erase(Tree.to_iterator(it));
        }
        return ans;
    }

    /// Extracts all the common blocks from the input tree and puts them in a separate map so that
    /// they can be replaced further
    CommonBlockMap
    ExtractCommonBlocks(PropertyTree& Tree) const
    {
        constexpr auto NameKeyword = "name";
        constexpr auto ContentKeyword = "content";
        CommonBlockMap ans;

        auto BlockRange = Tree.equal_range(s_CommonBlock_Keyword);
        for(auto itBlock = BlockRange.first; itBlock != BlockRange.second; ++itBlock)
        {
            auto OptName = itBlock->second.get_optional<std::string>(NameKeyword);
            auto itContent = itBlock->second.find(ContentKeyword);
            if(!OptName)
            {
                Log::Framework::Warn("Unnamed common block in the input message");
            }
            else if(ans.count(*OptName) != 0)
            {
                Log::Framework::Warn("Duplicate common block with the name {}", *OptName);
            }
            else if(itContent == itBlock->second.not_found())
            {
                Log::Framework::Warn(
                    "Missing content element in the common block named {}", *OptName);
            }
            else
            {
                ans.insert(std::make_pair(*OptName, itContent->second));
                Log::Framework::Debug("Common block named {} loaded", *OptName);
            }
        }
        for(auto itBlock = BlockRange.first; itBlock != BlockRange.second; ++itBlock)
        {
            Tree.erase(Tree.to_iterator(itBlock));
        }
        return ans;
    }

    void
    ReplaceIncludes(PropertyTree& Tree, const CommonBlockMap& Common, uint32_t Lvl) const
    {
        // Local keyword for the include nodes to specify at which path one has to locate the file
        // to include
        constexpr auto PathKeyword = "path";

        // Local keyword for the include nodes to specify the format to use for the decoding
        constexpr auto FormatKeyword = "format";

        // Local keyword to identify the common block to search for
        constexpr auto NameKeyword = "name";

        // Local keyword to identify the part of the block to replace
        constexpr auto ReplaceKeyword = "replace";

        if (Lvl > s_MaxNestingLevel)
        {
            Log::Framework::Warn(
                "Maximum nesting level ({}) reached while expanding the nested include blocks",
                s_MaxNestingLevel);
            return;
        }

        auto itNode = Tree.begin();
        while (itNode != Tree.end())
        {
            if (itNode->first == s_Include_Keyword)
            {
                auto OptPath = itNode->second.get_optional<std::string>(PathKeyword);
                auto OptName = itNode->second.get_optional<std::string>(NameKeyword);
                auto& Data = itNode->second.data();
                PropertyTree IncludedTree;
                bool ValidBlock = true;

                if (OptName)
                {
                    if (OptPath || !Data.empty())
                    {
                        Log::Framework::Warn(
                            "A nested include block has both a name element ({}) and either a path "
                            "element or a valid data string.",
                            *OptName);
                        ValidBlock = false;
                    }

                    auto itBlock = Common.find(*OptName);
                    if (ValidBlock && itBlock == Common.end())
                    {
                        Log::Framework::Warn("An include block with the name {} doesn't match any "
                                              "of the loaded common blocks",
                            *OptName);
                        ValidBlock = false;
                    }

                    if (ValidBlock)
                    {
                        IncludedTree = itBlock->second;
                    }
                }
                else
                {
                    if (!OptPath && Data.empty())
                    {
                        Log::Framework::Warn("A nested include block has neither a name nor a "
                                              "path element nor a valid data string.");
                        ValidBlock = false;
                    }

                    if (ValidBlock && OptPath && !Data.empty())
                    {
                        Log::Framework::Warn("A nested include block has both a path element and "
                                              "a valid data string.");
                        ValidBlock = false;
                    }

                    auto Path = bfs::path{ OptPath ? (*OptPath) : Data };
                    if (ValidBlock && !bfs::is_regular_file(Path))
                    {
                        Log::Framework::Error("The specified include path:\n{}\n"
                                               "is not valid or does not match a regular file",
                            Path.string());
                        ValidBlock = false;
                    }

                    if (ValidBlock)
                    {
                        std::ifstream InputFile{ Path.native() };
                        if (InputFile.good())
                        {
                            auto OptFormat =
                                itNode->second.get_optional<std::string>(FormatKeyword);
                            this->SimpleRead(
                                InputFile, IncludedTree, OptFormat.get_value_or(XMLFormatTag));
                        }
                        else
                        {
                            Log::Framework::Error(
                                "Unable to open the file at the path:\n{}", Path.string());
                            ValidBlock = false;
                        }
                    }
                }

                if (ValidBlock)
                {
                    ReplaceIncludes(IncludedTree, Common, Lvl + 1);
                    auto itReplacementNode = itNode->second.find(ReplaceKeyword);
                    if (itReplacementNode != itNode->second.not_found())
                    {
                        MergeConfigurationTrees(IncludedTree, itReplacementNode->second, true);
                    }
                }

                itNode = Tree.erase(itNode);

                if (ValidBlock)
                {
                    for (auto& Node : IncludedTree)
                    {
                        Tree.insert(itNode, Node);
                    }
                }
            }
            else
            {
                ReplaceIncludes(itNode->second, Common, Lvl + 1);
                ++itNode;
            }
        }
    }
};

// DEVNOTE I need anyway a separate declaration for the static constexpr member even if it is
// already initialized in the header
constexpr uint32_t ConfigFileParser::Impl::s_MaxNestingLevel;
constexpr char ConfigFileParser::Impl::s_Include_Keyword[];
constexpr char ConfigFileParser::Impl::s_CommonBlock_Keyword[];


ConfigFileParser::ConfigFileParser() : m_impl{ new ConfigFileParser::Impl{} } {}

ConfigFileParser::~ConfigFileParser() = default;

ConfigFileParser::ConfigFileParser(const ConfigFileParser& other) :
    m_impl{ new ConfigFileParser::Impl{ *other.m_impl } }
{}

ConfigFileParser&
ConfigFileParser::operator=(ConfigFileParser other)
{
    swap(*this, other);
    return *this;
}

ConfigFileParser::ConfigFileParser(ConfigFileParser&& other) : ConfigFileParser()
{
    swap(*this, other);
}

void
swap(ConfigFileParser& lhs, ConfigFileParser& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_impl, rhs.m_impl);
}

std::istream&
ConfigFileParser::Read(std::istream& In, PropertyTree& Tree, const std::string FormatTag) const
{
    return m_impl->Read(In, Tree, FormatTag);
    ;
}

std::istream&
ConfigFileParser::Read(std::istream& In, PropertyTree& Tree) const
{
    return m_impl->Read(In, Tree, XMLFormatTag);
}

std::ostream&
ConfigFileParser::Write(
    std::ostream& Out, const PropertyTree& Tree, const std::string& FormatTag) const
{
    return m_impl->Write(Out, Tree, FormatTag);
}

std::ostream&
ConfigFileParser::Write(std::ostream& Out, const PropertyTree& Tree) const
{
    return m_impl->Write(Out, Tree, XMLFormatTag);
}
} // namespace MXWare::Configuration
