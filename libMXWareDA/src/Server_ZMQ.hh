/*
 * Server_ZMQ.hh
 *
 *  Created on: Nov 26, 2019
 *      Author: caiazza
 */

#ifndef LIBMXWAREDA_SRC_SERVER_ZMQ_HH_
#define LIBMXWAREDA_SRC_SERVER_ZMQ_HH_
#pragma once

#include "MXCourier_ZMQ.hh"
#include "MXWare/Signals/Signal.hh"
#include <vector>
#include <string>
#include <shared_mutex>
#include <atomic>
#include <thread>
#include <future>
#include <exception>
#include <chrono>
#include <deque>
#include <unordered_map>
#include <boost/optional.hpp>

namespace MXWare
{
namespace Core
{
namespace Courier
{
class Server_ZMQ : public Server
{
public:
    /// Creates a new server socket connected to the ZMQ IO Context
    /// The context MUST be a valid pointer
    Server_ZMQ(std::shared_ptr<MXCourier_ZMQ> Context);

    /// Destructor
    ~Server_ZMQ();

    ///@{
    /// Copy not allowed
    Server_ZMQ(const Server_ZMQ& other) = delete;

    Server_ZMQ&
    operator=(const Server_ZMQ& other) = delete;
    ///@}

    /// Swap
    /// TODO Check the actual functionality of the swap when the processing
    /// is running
    //    friend void
    //    swap(Server_ZMQ& lhs, Server_ZMQ& rhs) noexcept;

    ///@{
    /// Move and move-assign
    Server_ZMQ(Server_ZMQ&& other) = delete;

    Server_ZMQ&
    operator=(Server_ZMQ&& other) = delete;
    ///@}

    /// Binds the server to an internal endpoint identified by a string.
    /// This is to be used to connect the server to clients which reside
    /// in the same process. A server can be bound to multiple endpoints
    bool
    BindInternal(const std::string& Identifier) override;

    /// Opens the port specified by the @par Port on the network interface
    /// @par interface and starts listening
    bool
    BindNetworkInterface(uint16_t Port, const std::string& Interface) override;

    /// Retrieves the number of endpoints to which the server is currently bound
    std::size_t
    GetNBoundEndpoints() const noexcept override;

    /// Disconnects the server from all the endpoints to which is connected
    void
    UnbindAll() override;

    /// Implementing the function from the interface
    Signals::Connection
    ProcessRequestCategory(const std::string& Cat, std::function<Reply(const Request&)> F) override;


private:
    /// The ZMQ context managing the IO services
    /// Using a shared_ptr to make sure the context will be alive
    /// as long as the client
    std::shared_ptr<MXCourier_ZMQ> m_Context;

    /// The ZMQ socket to perform async replies
    zmq::socket_t m_Router;

    /// Contains a list of the endpoints which are currently bound
    std::vector<std::string> m_BoundEndpoints;

    /// A mutex to sync the access to the socket
    mutable std::mutex m_RouterAccess;

    /// A flag which indicates that the server should stop the processing loop
    std::atomic<bool> m_StopProcessing;

    /// The future of the processing loop
    std::future<void> m_ProcessingHandle;

    /// A structure to store the data of a rceived request.
    /// Due to the promise a ReceivedRequest cannot be copied, only moved
    struct ReceivedRequest
    {
        /// The category of the received request
        std::string Category{ "" };

        /// The Id of the message source to which send the message back
        zmq::message_t SourceId{};

        /// The id of the request that was sent
        uint64_t ReqId{ 0 };

        /// The body of the request to be able to retry
        Request Req{};

        /// The promise for a reply for which we sent the future
        Reply Rep{};

        /// Time of the received request to calculate the timeout (if necessary)
        std::chrono::system_clock::time_point TS = std::chrono::system_clock::now();
    };

    /// Container where to store the queue of a single category of requests, connected to
    /// a dedicated mutex for each category
    using RequestQueue = std::pair<std::deque<ReceivedRequest>, std::shared_timed_mutex>;

    /// Container where to store all requests
    std::unordered_map<std::string, RequestQueue> m_WaitingQueues;

    /// A mutex to access all queues
    std::shared_timed_mutex m_QueuesAccess;

    /// A map of the function registered to process the different request categories
    std::unordered_map<std::string, Signals::Signal<Reply(const Request&)>> m_ProcessingFunctions;

    /// Binds the ZMQ router socket to the required endpoint
    void
    BindRouter(const std::string& Endpoint);

    /// Starts the task processing the send and receive queues
    void
    StartProcessing();

    /// Stops the processing task
    void
    StopProcessing();

    /// The function which performs the listening loop and processes the received messages
    void
    ProcessMessages();

    /// Dequeues all the existing messages from the router socket
    void
    DequeueAllFromRouter();

    /// Fills a Received Request object from the data of a message sequence
    /// If the set of messages cannot be converted to a valid request returns
    /// a MessageDecodingError
    ReceivedRequest
    DecodeRequest(std::vector<zmq::message_t>& MsgFrames);

    /// Send the reply stored in the received request
    bool
    ReplyRequest(ReceivedRequest& R);
};


} // namespace Courier


} // namespace Core


} // namespace MXWare




#endif /* LIBMXWAREDA_SRC_SERVER_ZMQ_HH_ */
