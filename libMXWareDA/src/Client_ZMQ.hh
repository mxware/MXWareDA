#ifndef LIBMXWARE_SRC_CLIENT_ZMQ_HH_
#define LIBMXWARE_SRC_CLIENT_ZMQ_HH_
#pragma once

#include "MXCourier_ZMQ.hh"
#include <thread>
#include <atomic>
#include <deque>
#include <mutex>
#include <future>
#include <unordered_map>
#include <chrono>

namespace MXWare
{
namespace Core
{
namespace Courier
{
class Client_ZMQ : public Client
{
public:
    /// Constructor. Requires a connection to a ZMQ context.
    /// The parameter MUST be a valid pointer
    /// The default parameter allows to build a client with its own standalone context.
    // DEVNOTE At the moment the default version is only useful to make the move working
    // properly with the swap so that the swapped object can still be valid
    Client_ZMQ(std::shared_ptr<MXCourier_ZMQ> Context = std::make_shared<MXCourier_ZMQ>(1));

    /// Destructor
    ~Client_ZMQ();

    ///@{
    /// No copy allowed as the sockect cannot be copied as they manage
    /// potentially uncopiable OS resource
    Client_ZMQ(const Client_ZMQ& other) = delete;

    Client_ZMQ&
    operator=(const Client_ZMQ& other) = delete;
    ///@}

    /// Swapping two clients
    friend void
    swap(Client_ZMQ& lhs, Client_ZMQ& rhs) noexcept;

    ///@{
    /// Move and move assign
    Client_ZMQ(Client_ZMQ&& other);

    Client_ZMQ&
    operator=(Client_ZMQ&& other);
    ///@}

    /// Connects the client through the inproc transport
    /// Starts listening for replies on the open socket
    /// For now a client can be connected only to 1 server at the time
    void
    ConnectInternal(const std::string& Identifier) override;

    /// Connects the client through the tpc transport
    /// Starts listening for replies on the open socket
    /// For now a client can be connected only to 1 server at the time
    void
    ConnectRemote(const std::string Hostname, uint16_t Port) override;

    /// Checks if the client is connected to any endpoint
    bool
    IsConnected() const override;

    /// Disconnects the connected endpoints
    void
    Disconnect() override;

    /// Sends a request to the connected remote
    /// If the Client is not connected to any remote it will throw an exception of type
    /// EndpointNotConnected.
    /// It returns a future to the expected reply
    /// TODO Devise a method to cancel or timeout a request
    std::future<Reply>
    SendRequest(Request&& Req) override;

private:
    /// A container for the data necessary to process a request
    struct ProcRequest
    {
        /// The id of the request that was sent
        uint64_t m_Id;
        /// The body of the request to be able to retry
        Request m_Req;
        /// The promise for a reply for which we sent the future
        std::promise<Reply> m_Rep;
    };

    /// The ZMQ based courier service managing the IO services
    /// Using a shared_ptr to make sure the context will be alive
    /// as long as the client
    // DEV NOTE  The courier is the sole owner of the zmq context which
    // will always be alive but can also contains other services, namely
    // the logger
    std::shared_ptr<MXCourier_ZMQ> m_Context;

    /// The ZMQ socket to perform async requests
    zmq::socket_t m_Dealer;

    /// The endpoint currently connected
    std::string m_CurrentEndpoint;

    /// Flag starting or stopping the listening for messages
    std::atomic<bool> m_ListenStop;

    /// Future for the reply listening thread
    std::future<void> m_FListen;

    /// A queue of the requests waiting to be processed
    std::deque<ProcRequest> m_Outstanding;

    /// Mutex to sync the access to the outstanding queue
    mutable std::mutex m_OutstandingAccess;

    /// A list of the Requests currently waiting for an answer,
    /// mapped to the id which we used in the request
    std::unordered_map<uint64_t, ProcRequest> m_Processing;

    /// The ID to assign to the next message
    std::atomic<uint64_t> m_NextMessageId;

    /// Connects the ZMQ dealer to the given endpoint
    void
    ConnectDealer(const std::string& Endpoint);

    /// This function is the main task to execute to send and receive messages
    void
    MessageDealing();

    /// Converts a set of messages in a reply
    std::pair<Reply, uint64_t>
    DecodeReply(std::vector<zmq::message_t>& MsgFrames);

    /// Starts the task processing the send and receive queues
    void
    StartProcessing();

    /// Stops the processing task
    void
    StopProcessing();
};


} // namespace Courier
} // namespace Core
} // namespace MXWare

#endif /*LIBMXWARE_SRC_CLIENT_ZMQ_HH_*/
