#include "MXWare/Extensions/Registry.hh"

#include <new>
#include <type_traits>
#include <algorithm>
#include <numeric>

namespace MXWare::Extensions
{
Registry::Registry(const Registry& other) : m_Registry{}, m_RegistryAccess{}
{
    std::shared_lock Lock{ other.m_RegistryAccess };
    std::for_each(other.m_Registry.begin(), other.m_Registry.end(), [this](auto& OriginPair) {
        m_Registry.insert(std::make_pair(OriginPair.first, OriginPair.second->Clone()));
    });
}

void
Registry::swap(Registry& other) noexcept
{
    if (this == &other)
    {
        return;
    }

    using std::swap;
    std::scoped_lock Lock{ m_RegistryAccess, other.m_RegistryAccess };
    swap(m_Registry, other.m_Registry);
}

Registry&
Registry::operator=(Registry other) noexcept
{
    swap(other);
    return *this;
}

Registry::Registry(Registry&& other) noexcept : Registry{} { swap(other); }

bool
Registry::empty() const noexcept
{
    std::shared_lock Lock{ m_RegistryAccess };
    return m_Registry.empty();
}

Registry::size_type
Registry::CountRegisteredExtensions() const noexcept
{
    std::shared_lock Lock{ m_RegistryAccess };
    return std::accumulate(
        m_Registry.begin(), m_Registry.end(), size_type{ 0 },
        [](auto& Last, auto& Item) -> auto { return Last += Item.second->size(); });
}

void
Registry::clear() noexcept
{
    std::unique_lock Lock{ m_RegistryAccess };
    return m_Registry.clear();
}

void
Registry::Merge(const Registry& other)
{
    if(this == &other)
        return;

    std::shared_lock ReadLock(other.m_RegistryAccess, std::defer_lock);
    std::unique_lock WriteLock(m_RegistryAccess, std::defer_lock);
    std::lock(WriteLock, ReadLock);

    std::for_each(other.m_Registry.begin(), other.m_Registry.end(), [&](auto& In_Pair) {
        if (auto it_Reg = m_Registry.find(In_Pair.first); it_Reg != m_Registry.end())
        {
            it_Reg->second->Merge(*In_Pair.second);
        }
        else
        {
            // auto& Inserted = m_Registry.insert(In_Pair).first->second;
            m_Registry.insert(std::make_pair(In_Pair.first, In_Pair.second->Clone()));
        }
    });
}

} // namespace MXWare::Extensions::
