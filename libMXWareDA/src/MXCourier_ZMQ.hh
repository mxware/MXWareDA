#ifndef LIBMXWARE_SRC_MXCOURIER_ZMQ_HH_
#define LIBMXWARE_SRC_MXCOURIER_ZMQ_HH_
#pragma once

#include "MXWare/Core/MXCourier.hh"
#include <zmq.hpp>

namespace MXWare
{
namespace Core
{

/// ZMQ implementation of the MXCourier
/// The class manages the interface with the IO services using the zmq context class
// DEVNOTE The class is derived from enable_shared_from_this to allow to forward
// the shared_ptr storing the object safely. This implies that it can only be used
// through a shared_ptr
class MXCourier_ZMQ : public MXCourier, public std::enable_shared_from_this<MXCourier_ZMQ>
{
public:
    /// Creates the Courier services with a user defined number of io_threads
    /// The second argument allows to assign a logging service to the Courier at the construction
    /// time If the LogService pointer os not valid a new default one will be created
    MXCourier_ZMQ(int io_threads = 1);

    ///@{ Copy and assignment are not allowed
    MXCourier_ZMQ(const MXCourier_ZMQ& other) = delete;

    MXCourier&
    operator=(const MXCourier_ZMQ& other) = delete;
    ///@}

    /// Swaps the content
    friend void
    swap(MXCourier_ZMQ& lhs, MXCourier_ZMQ& rhs) noexcept;

    ///@{
    /// Move and move assignment
    MXCourier_ZMQ(MXCourier_ZMQ&& other) noexcept;

    MXCourier_ZMQ&
    operator=(MXCourier_ZMQ&& other) noexcept;
    ///@}

    /// Obtains a reference to the ZMQ context managed by the class
    zmq::context_t&
    ZMQ_Context();

    /// Creates a new ZMQ based courier client
    std::unique_ptr<Courier::Client>
    CreateClient() override;

    /// Creates a new server connected to this Courier service
    std::unique_ptr<Courier::Server>
    CreateServer() override;

    /// Obtains a shared reference to the default logger of the courier service
    Log::Logger&
    Log() override;

    /// Configures the courier using a configuration tree
    void
    Configure(const Configuration::PropertyTree& Tree) override;

    /// Retrieves the current status of the courier
    Configuration::PropertyTree
    CurrentConfiguration() const override;

private:
    /// The zmq context used to manage the IO Services
    std::unique_ptr<zmq::context_t> m_Context;

    /// The default logger used by the courier service
    Log::Logger& m_DefaultCourierLogger;
};

} // namespace Core

} // namespace MXWare

#endif /*LIBMXWARE_SRC_MXCOURIER_ZMQ_HH_*/
