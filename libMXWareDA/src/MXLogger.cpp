/*
 * MXLogger.cpp
 *
 *  Created on: Aug 16, 2019
 *      Author: caiazza
 */

#include "MXWare/Log.hh"
#include "MXWare/Exceptions.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"
#include "MXWare/Log/Sinks/StdoutColor.hh"
#include "MXWare/Log/Sinks/SingleFileSink.hh"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/details/registry.h"

#include "fmt/format.h"

#include <utility> //<- for swap
#include <algorithm>
#include <functional>
#include <new> //<- Placement new
#include <filesystem>

#include <vector>
#include <memory>
#include <optional>
#include <shared_mutex>

#include <boost/current_function.hpp>

namespace MXWare::Log
{
bool
Logger::AddSink(ISink& InSink)
{
    auto SpdlogSink = InSink.ShareSpdlogSink();
    if (!SpdlogSink)
        return false;

    auto& Sinks = m_Logger.sinks();
    if (std::find(Sinks.begin(), Sinks.end(), SpdlogSink) == Sinks.end())
    {
        Sinks.push_back(SpdlogSink);
        return true;
    }
    else
    {
        return false;
    }
}

void
Logger::Configure(const Configuration::PropertyTree& ConfTree)
{
    std::string DesiredLevel;
    try
    {
        if (Configuration::UpdateParameter(DesiredLevel, ConfTree, "level"))
        {
            SetLevel(NamedLevels.at(DesiredLevel));
        }
    }
    catch (const ObjectNotFound& e)
    {
        Warn( "Invalid value desired for the log level of logger {}: {}", GetName(), DesiredLevel);
    }
    Debug("Logger {} configured", GetName());
}

Configuration::PropertyTree
Logger::CurrentConfiguration() const
{
    Configuration::PropertyTree ans{};
    Configuration::WriteToTree(GetName(), ans, "name");
    auto itFound = std::find_if(NamedLevels.begin(), NamedLevels.end(),
        [&](const auto& Pair) -> bool { return Pair.second == GetLevel(); });
    if (itFound != NamedLevels.end())
    {
        Configuration::WriteToTree(itFound->first, ans, "level");
    }
    return ans;
}

/* *********************************************************************************
 * 
 *          Sinks
 * 
 * *********************************************************************************/

namespace Sinks
{
std::shared_ptr<spdlog::sinks::basic_file_sink_mt>
SingleFileSink::CreateSink(std::string_view FilePath)
{
    // if (!std::filesystem::is_regular_file(FilePath))
    // {
    //     throw FileError{ "SingleFileSink", BOOST_CURRENT_FUNCTION,
    //         fmt::format("The path {} is not valid", FilePath) };
    // }

    return std::make_shared<spdlog::sinks::basic_file_sink_mt>(std::string{FilePath});
}


void
SingleFileSink::Configure(const Configuration::PropertyTree& Tree)
{
    if (Configuration::UpdateParameter(m_LogFilePath, Tree, "path"))
    {
        ResetSpdlogSink(CreateSink(m_LogFilePath));
        Framework::Debug("Single file sink configured.\nLog file: {}", m_LogFilePath);
    }
}

} // namespace Sinks



/// A container for a set of logger
class LoggerRegistry
{
public:
    void
    swap(LoggerRegistry& other) noexcept
    {
        using std::swap;
        swap(m_Loggers, other.m_Loggers);
    }

    std::optional<std::reference_wrapper<Logger>>
    FindLogger(std::string_view LName) noexcept
    {
        std::shared_lock LockMe{ m_Access };
        if (auto it = std::find_if(m_Loggers.begin(), m_Loggers.end(),
                [&](auto& LogPtr) { return LogPtr->GetName() == LName; });
            it != m_Loggers.end())
        {
            return std::optional<std::reference_wrapper<Logger>>{ std::ref(*(*it)) };
        }
        else
        {
            return std::optional<std::reference_wrapper<Logger>>{};
        }
    }

    Logger&
    AddLogger(std::unique_ptr<Logger> MyLogger)
    {
        if (!MyLogger)
        {
            NullPointerException e{ "LoggerRegistry", BOOST_CURRENT_FUNCTION,
                "Attempting to store an invalid logger" };
            e.raise();
        }

        std::lock_guard LockMe{ m_Access };
        if (auto itPosition =
                std::lower_bound(m_Loggers.begin(), m_Loggers.end(), MyLogger->GetName(),
                    [&](auto& LogPtr, auto& Name) { return LogPtr->GetName() < Name; });
            itPosition == m_Loggers.end() || (*itPosition)->GetName() != MyLogger->GetName())
        {
            auto Insertion = m_Loggers.insert(itPosition, std::move(MyLogger));
            return *(*Insertion);
        }
        else
        {
            DuplicateObject e{ "LoggerRegistry", BOOST_CURRENT_FUNCTION,
                "Attempting to create a logger of the same name of an existing one" };
            e.raise();
        }
    }

    /// Applies a function to each logger which is stored
    template <typename UnaryFunction>
    UnaryFunction
    ForEachLogger(UnaryFunction F)
    {
        std::shared_lock LockMe{ m_Access };
        std::for_each(m_Loggers.begin(), m_Loggers.end(), [&F](auto& LogPtr) { F(*LogPtr); });
        return F;
    }

    template <typename UnaryFunction>
    UnaryFunction
    ForEachLogger(UnaryFunction F) const
    {
        std::shared_lock LockMe{ m_Access };
        std::for_each(m_Loggers.cbegin(), m_Loggers.cend(), [&F](auto& LogPtr) { F(*LogPtr); });
        return F;
    }

private:
    // DEVNOTE Having the loggers stored on the heap makes it
    // safe to sort without risking to invalidate references
    // It may also allow to convert a unique_ptr to a shared one in
    // case I decided to do the sharing of the loggers and allows logger
    // to be an abstract or base class
    // Having a redundant string stored in the pair should allow for a quick lookout
    // but most of the time is actually spent in the string comparison so I can store
    // just the pointer and make it simpler and safer. In fact storing a pair or storing
    // just the pointer are practically equivalent from the performance point-of-view
    using container_t = std::vector<std::unique_ptr<Logger>>;

    /// A list of the registered loggers ordered by name.
    container_t m_Loggers;

    /// Mutex to sync the registry access
    mutable std::shared_mutex m_Access;
};

class SinkRegistry
{
public:
    void
    swap(SinkRegistry& other) noexcept
    {
        using std::swap;
        swap(m_Sinks, other.m_Sinks);
    }

    ISink&
    AddSink(std::unique_ptr<ISink> MySink)
    {
        if (!MySink)
        {
            NullPointerException e{ "SinkRegistry", BOOST_CURRENT_FUNCTION,
                "Attempting to store an invalid sink" };
            e.raise();
        }

        std::lock_guard LockMe{ m_Access };
        // DEVNOTE I'm storing a unique_ptr so the same pointer cannot be
        // already in the container. I could in theory extract the shared_ptr
        // from one ISink to store it in another unique ISink and then add it
        // to this registry but there is no natural way I can think of to make
        // that happen. Even if that happens, if there is a duplicate sink in
        // the registry I will only log the message twice which is not a
        // critical failure
        // auto itFound = std::find(m_Sinks.begin(), m_Sinks.end(), MySink)
        m_Sinks.push_back(std::move(MySink));
        return *m_Sinks.back();
    }

    void
    ConfigureSinks(const Configuration::PropertyTree& Tree, std::string_view NodeName)
    {
        auto itSinksConfig = Tree.equal_range(NodeName.data());
        auto itCurrentConfig = itSinksConfig.first;
        while(itCurrentConfig != itSinksConfig.second)
        {
            auto& CurrentTree = itCurrentConfig->second;
            std::unique_ptr<ISink> TheSink{};
            try
            {
                Configuration::UpdateParameter(TheSink, CurrentTree, "");
                if(TheSink && TheSink->IsValid())
                {
                    AddSink(std::move(TheSink));
                }
            }
            catch(const std::exception& e)
            { 
                Framework::Error(fmt::format("The sink cannot be correctly configured.\n Error: {}", e.what()));
            }
            
            ++itCurrentConfig;
        }
    }

    /// Add all the registered sinks that are not already stored in the logger
    /// to the input logger
    void
    SetLoggerSinks(Logger& L) const
    {
        auto& LoggerSinks = L.AccessSpdlogLogger().sinks();
        std::shared_lock LockMe{ m_Access };
        for (auto& Sink : m_Sinks)
        {
            if (auto SinkToShare = Sink->ShareSpdlogSink();
                std::find(LoggerSinks.begin(), LoggerSinks.end(), SinkToShare) == LoggerSinks.end())
            {
                LoggerSinks.push_back(SinkToShare);
            }
        }
    }

private:
    /// The wrappers are stored as unique_ptr as the shared component is the
    /// wrapped spdlog sink
    using container_t = std::vector<std::unique_ptr<ISink>>;

    /// The list of registered sinks
    container_t m_Sinks;

    /// Mutex to sync the registry access
    mutable std::shared_mutex m_Access;
};

// The implementation class is simply a container of the private members to which the
// mother class has friend access
class LoggingService::Impl
{
    friend class LoggingService;

    // Stores the current log level
    Level m_CurrentLevel;

    /// The registry of the sinks
    SinkRegistry m_Sinks;

    /// The registry of the registered loggers
    LoggerRegistry m_Loggers;

    /// The general logger which is always created
    Logger& m_GeneralLogger;

    /// The framework logger, which is always created
    Logger& m_FrameworkLogger;

public:
    /// Creates a new logging service
    Impl() :
        m_CurrentLevel{ Level::Info },
        m_Sinks{},
        m_Loggers{},
        m_GeneralLogger{ m_Loggers.AddLogger(std::make_unique<Logger>(s_GeneralLoggerName)) },
        m_FrameworkLogger{ m_Loggers.AddLogger(std::make_unique<Logger>(s_FrameworkLoggerName)) }
    {
        m_Sinks.AddSink(std::make_unique<Sinks::StdoutColor>());
        m_Loggers.ForEachLogger([this](auto& L) { SetCommonConfiguration(L); });
    }

    std::optional<std::reference_wrapper<Logger>>
    GetLoggerIfExists(std::string_view Name)
    {
        return m_Loggers.FindLogger(Name);
    }

    Logger&
    GetOrCreateLogger(std::string_view Name)
    {
        if (auto OptLogger = m_Loggers.FindLogger(Name))
        {
            return OptLogger.value();
        }

        auto& ans = m_Loggers.AddLogger(std::make_unique<Logger>(Name));
        SetCommonConfiguration(ans);
        return ans;
    }

    void
    SetCommonConfiguration(Logger& InLogger)
    {
        InLogger.SetLevel(m_CurrentLevel);
        m_Sinks.SetLoggerSinks(InLogger);
    }

    void
    SetLevel(Level LogLevel)
    {
        if (LogLevel != m_CurrentLevel)
        {
            m_Loggers.ForEachLogger([&](auto& L) { L.SetLevel(LogLevel); });
        }
    }

    Level
    CurrentLevel() const
    {
        return m_CurrentLevel;
    }

    void
    Configure(const Configuration::PropertyTree& ConfTree)
    {
        // Adds the sink
        m_Sinks.ConfigureSinks(ConfTree, "sink");

        // Sets the global level of logging
        std::string DesiredLevel;
        try
        {
            if(Configuration::UpdateParameter(DesiredLevel, ConfTree, "level"))
            {
                SetLevel(NamedLevels.at(DesiredLevel));
            }
        }
        catch (const ObjectNotFound& e)
        {
            m_FrameworkLogger.Warn(
                "Invalid value desired for the global log level: {}", DesiredLevel);
        }

        // Configures the existing loggers with the new parameters
        m_Loggers.ForEachLogger([this](auto& L) { SetCommonConfiguration(L); });

        auto itLoggersConfigs = ConfTree.equal_range("logger");
        auto itCurrent = itLoggersConfigs.first;
        while(itCurrent != itLoggersConfigs.second)
        {
            auto& CurrentTree = itCurrent->second;
            auto LoggerName = itCurrent->second.get_optional<std::string>("name");
            if(LoggerName)
            {
                auto& Logger = GetOrCreateLogger(LoggerName.get());
                SetCommonConfiguration(Logger);
                Logger.Configure(CurrentTree);
            }
            ++itCurrent;
        }
    }

    Configuration::PropertyTree
    CurrentConfiguration() const
    {
        Configuration::PropertyTree ans;
        auto itFound = std::find_if(NamedLevels.begin(), NamedLevels.end(),
            [&](const auto& Pair) -> bool { return Pair.second == CurrentLevel(); });
        if (itFound != NamedLevels.end())
        {
            Configuration::WriteToTree(itFound->first, ans, "level");
        }

        m_Loggers.ForEachLogger([&](auto& L) { ans.add_child("logger", L.CurrentConfiguration()); });

        return ans;
    }
};

/* ************************************************
 *
 ************************************************** */

LoggingService::LoggingService() : m_impl{ std::make_unique<Impl>() } {}

LoggingService::~LoggingService() noexcept = default;

LoggingService::LoggingService(LoggingService&& other) noexcept = default;

LoggingService&
LoggingService::operator=(LoggingService&& other) noexcept = default;

void
swap(LoggingService& lhs, LoggingService& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_impl, rhs.m_impl);
}

std::optional<std::reference_wrapper<Logger>>
LoggingService::GetLoggerIfExists(std::string_view Name)
{
    return m_impl->GetLoggerIfExists(Name);
}

Logger&
LoggingService::GetOrCreateLogger(std::string_view Name)
{
    return m_impl->GetOrCreateLogger(Name);
}

void
LoggingService::SetCommonConfiguration(Logger& InLogger)
{
    m_impl->SetCommonConfiguration(InLogger);
}

void
LoggingService::SetLevel(Level log_level)
{
    m_impl->SetLevel(log_level);
}

Level
LoggingService::CurrentLevel() const
{
    return m_impl->CurrentLevel();
}

void
LoggingService::Configure(const Configuration::PropertyTree& ConfTree)
{
    m_impl->Configure(ConfTree);
}

Configuration::PropertyTree
LoggingService::CurrentConfiguration() const
{
    return m_impl->CurrentConfiguration();
}

Logger&
LoggingService::GeneralLogger()
{
    return m_impl->m_GeneralLogger;
}

Logger&
LoggingService::FrameworkLogger()
{
    return m_impl->m_FrameworkLogger;
}

static std::aligned_storage_t<sizeof(LoggingService), alignof(LoggingService)> LogServiceBuffer{};
LoggingService& GlobalService = reinterpret_cast<LoggingService&>(LogServiceBuffer);

namespace detail
{
static int NiftyCounter;

LogServiceInitializer::LogServiceInitializer()
{
    if (NiftyCounter++ == 0)
    {
        new (&GlobalService) LoggingService();
    }
}

LogServiceInitializer::~LogServiceInitializer()
{
    if (--NiftyCounter == 0)
    {
        (&GlobalService)->~LoggingService();
    }
}

} // namespace detail

} // namespace MXWare::Log
