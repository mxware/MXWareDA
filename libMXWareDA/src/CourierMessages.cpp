#include "MXWare/Courier/IMessage.hh"
#include "MXWare/Courier/Replies.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"

namespace MXWare::Courier::Reply
{
Empty::Empty(const IMessage& Request):
    m_Id{},
    m_RequestId{Request.GetId()}
{

}

const std::string&
Empty::GetId() const noexcept
{
    return m_Id;
}

std::string
Empty::GetTypeId() const noexcept
{
    return Empty::TypeId();
}

Configuration::PropertyTree
Empty::GetBody() const
{
    return Configuration::PropertyTree{};
}

std::string
Empty::TypeId()
{
    return "Empty";
}

void
Empty::Configure(const Configuration::PropertyTree& ConfTree)
{
    Configuration::UpdateParameter(m_Id, ConfTree, "id");
    Configuration::UpdateParameter(m_RequestId, ConfTree, "request_id");
}

Configuration::PropertyTree
Empty::CurrentConfiguration() const
{
    Configuration::PropertyTree ans;
    ans.put(Configuration::detail::ObjectTypeTag, GetTypeId());
    ans.put("id", GetId());
    ans.put_child("body", GetBody());
    ans.put("request_id", GetRequestId());
    return ans;
}

const std::string&
Empty::GetRequestId() const noexcept
{
    return m_RequestId;
}

} // namespace MXWare::Courier

namespace MXWare::Courier::detail
{
Configuration::PropertyTree
Message_ConfigWriterImpl::CurrentConfiguration() const
{
    Configuration::PropertyTree ans;
    ans.put(Configuration::detail::ObjectTypeTag, GetTypeId());
    ans.put("id", GetId());
    ans.put_child("body", GetBody());
    return ans;
}

}