/*
 * NodeClient.cpp
 *
 *  Created on: Nov 7, 2019
 *      Author: caiazza
 */

#include "MXWare/Core/NodeServices.hh"
#include "MXWare/Exceptions.hh"
#include <cassert>

namespace MXWare
{
namespace Core
{
class NodeClient::Impl
{
public:
    Impl(std::shared_ptr<MXCourier> Context) : m_Courier{ Context }, m_Client{}
    {
        assert(static_cast<bool>(m_Courier));
        m_Client = m_Courier->CreateClient();
    }

    Impl(const Impl& other) : m_Courier{ other.m_Courier } {}

    friend void
    swap(Impl& lhs, Impl& rhs) noexcept
    {
        using std::swap;
        swap(lhs.m_Courier, rhs.m_Courier);
        swap(lhs.m_Client, rhs.m_Client);
    }

    Impl(Impl&& other) : m_Courier{} { swap(*this, other); }

    Impl&
    operator=(Impl other)
    {
        swap(*this, other);
        return *this;
    }

    bool
    Connect(const std::string&)
    {
        return true;
    }

private:
    /// Handle to the common context class
    std::shared_ptr<MXCourier> m_Courier;

    /// The messaging client actually sending the messages
    std::unique_ptr<Courier::Client> m_Client;
};






NodeClient::NodeClient(std::shared_ptr<MXCourier> Context) :
    m_impl{ std::make_unique<Impl>(Context) }
{}

NodeClient::~NodeClient() = default;

NodeClient::NodeClient(const NodeClient& other) : m_impl{ std::make_unique<Impl>(*other.m_impl) } {}

MXAPI void
swap(NodeClient& lhs, NodeClient& rhs) noexcept
{
    using std::swap;
    swap(lhs.m_impl, rhs.m_impl);
}

NodeClient::NodeClient(NodeClient&& other) { swap(*this, other); }

NodeClient&
NodeClient::operator=(NodeClient other)
{
    swap(*this, other);
    return *this;
}

bool
NodeClient::Connect(const std::string& Endpoint)
{
    return m_impl->Connect(Endpoint);
}

} // namespace Core


} // namespace MXWare
