/**
 * \file MXExceptions.hh
 * File containing all the framework general exceptions
 *
 *  Created on: Mar 2, 2015
 *      Author: caiazza
 */

#ifndef SRC_MXEXCEPTIONS_HH_
#define SRC_MXEXCEPTIONS_HH_
#pragma once

#include <string>
#include <stdint.h>

#include "MXWare/Exceptions.hh"


namespace MXWare
{
/**
 * To be thrown when there is an error in the parsing of a configuration file
 */
struct /*MXAPI*/ ConfigParsingError : public GeneralException
{
    ConfigParsingError(const std::string& Message);

    ConfigParsingError(
        const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~ConfigParsingError() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/**
 * Special case of the ConfigParsingError to be used when a mandatory node is
 * missing from the configuration file
 */
struct /*MXAPI*/ MissingConfigNode : public ConfigParsingError
{
    MissingConfigNode(const std::string& Message);

    MissingConfigNode(
        const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~MissingConfigNode() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

using InvalidParameter = InvalidArgument;

/**
 * Special case of InvalidParameter when the object parameter overflows the
 * integer range of a given size
 */
struct /*MXAPI*/ IntegerOverflow : public InvalidParameter
{
    IntegerOverflow(const std::string& Message);

    IntegerOverflow(
        const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~IntegerOverflow() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/**
 * Parameter is invalid because it's of a different type than the required one
 */
struct /*MXAPI*/ TypeMismatch : public InvalidParameter
{
    TypeMismatch(const std::string& Message);

    TypeMismatch(const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~TypeMismatch() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/**
 * General error related to DLL loading and handling
 */
struct /*MXAPI*/ DLLError : public GeneralException
{
    DLLError(const std::string& Message);

    DLLError(const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~DLLError() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/**
 * General error related to the plugin handling
 */
struct /*MXAPI*/ PluginError : public GeneralException
{
    PluginError(const std::string& Message);

    PluginError(const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~PluginError() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/**
 * The required version of a package or a plugin is different than what is
 * expected
 */
struct /*MXAPI*/ VersionMismatch : public GeneralException
{
    VersionMismatch(const std::string& Message);

    VersionMismatch(
        const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~VersionMismatch() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/**
 * Error during the streaming to or from a data stream
 */
struct /*MXAPI*/ StreamingError : public GeneralException
{
    StreamingError(const std::string& Message);

    StreamingError(
        const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~StreamingError() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};


/**
 * Error during the handling of a data object
 */
struct /*MXAPI*/ DataError : public GeneralException
{
    DataError(const std::string& Message);

    DataError(const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~DataError() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};


/**
 * Error during the handling of a data object
 */
struct /*MXAPI*/ ParsingError : public GeneralException
{
    ParsingError(const std::string& Message);

    ParsingError(const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~ParsingError() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

/// Exception to be thrown in case of errors within the MXWare IO system,
/// including the messenger system
/// Generic errors in the handling of the basic C++ IO are handled by the
/// specific Core exceptions
struct /*MXAPI*/ IOException : public GeneralException
{
    /// Constructs it setting the exception message
    IOException(const std::string& Message);

    /// Constructs the exception composing the message with the three strings
    /// like in the General exceptions
    IOException(const std::string& Class, const std::string& Function, const std::string& Message);

    /// Polymorphic destructor
    virtual ~IOException() = default;

    /// Polymorphical throw clause
    virtual void
    raise();
};

} // namespace MXWare


#endif /* MXWARE_CORE_MXEXCEPTIONS_HH_ */
