#include "MXExceptions_internal.hh"

namespace MXWare
{

/* **********************************************
 *              GeneralException              *
 ********************************************** */

GeneralException::GeneralException(const std::string& Message) : runtime_error{ Message } {}

GeneralException::GeneralException(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class + "::" + Function + " : " + Message }
{}


/* **********************************************
 *             Other core exceptions            *
 ********************************************** */

MXWARE_IMPLEMENT_EXCEPTION(NullPointerException, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(ObjectNotFound, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(DuplicateObject, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(InvalidArgument, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(ConfigurationError, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(OutOfRange, InvalidParameter)

MXWARE_IMPLEMENT_EXCEPTION(RequirementFailure, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(FileError, GeneralException)

MXWARE_IMPLEMENT_EXCEPTION(FileNotFound, FileError)

MXWARE_IMPLEMENT_EXCEPTION(FileAccessError, FileError)

MXWARE_IMPLEMENT_EXCEPTION(ExtensionNotFound, GeneralException)


/* **********************************************
 *              ConfigParsingError              *
 ********************************************** */

ConfigParsingError::ConfigParsingError(const std::string& Message) : GeneralException(Message) {}

ConfigParsingError::ConfigParsingError(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
ConfigParsingError::raise()
{
    throw *this;
}

/* **********************************************
 *              MissingConfigNode               *
 ********************************************** */

MissingConfigNode::MissingConfigNode(const std::string& Message) : ConfigParsingError(Message) {}

MissingConfigNode::MissingConfigNode(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    ConfigParsingError{ Class, Function, Message }
{}

void
MissingConfigNode::raise()
{
    throw *this;
}

/* **********************************************
 *              IntegerOverflow                 *
 ********************************************** */

IntegerOverflow::IntegerOverflow(const std::string& Message) : InvalidParameter(Message) {}

IntegerOverflow::IntegerOverflow(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    InvalidParameter(Class, Function, Message)
{}

void
IntegerOverflow::raise()
{
    throw *this;
}

/* **********************************************
 *              TypeMismatch                    *
 ********************************************** */
TypeMismatch::TypeMismatch(const std::string& Message) : InvalidParameter{ Message } {}

TypeMismatch::TypeMismatch(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    InvalidParameter{ Class, Function, Message }
{}

void
TypeMismatch::raise()
{
    throw *this;
}

/* **********************************************
 *              DLLError                        *
 ********************************************** */
DLLError::DLLError(const std::string& Message) : GeneralException{ Message } {}

DLLError::DLLError(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException(Class, Function, Message)
{}

void
DLLError::raise()
{
    throw *this;
}

/* **********************************************
 *              PluginError                 *
 ********************************************** */
PluginError::PluginError(const std::string& Message) : GeneralException{ Message } {}

PluginError::PluginError(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
PluginError::raise()
{
    throw *this;
}

/* **********************************************
 *              VersionMismatch                 *
 ********************************************** */
VersionMismatch::VersionMismatch(const std::string& Message) : GeneralException{ Message } {}

VersionMismatch::VersionMismatch(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
VersionMismatch::raise()
{
    throw *this;
}

/* **********************************************
 *              StreamingError                 *
 ********************************************** */
StreamingError::StreamingError(const std::string& Message) : GeneralException{ Message } {}

StreamingError::StreamingError(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
StreamingError::raise()
{
    throw *this;
}

/* **********************************************
 *              DataError                 *
 ********************************************** */
DataError::DataError(const std::string& Message) : GeneralException{ Message } {}

DataError::DataError(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
DataError::raise()
{
    throw *this;
}

/* **********************************************
 *              ParsingError                 *
 ********************************************** */
ParsingError::ParsingError(const std::string& Message) : GeneralException{ Message } {}

ParsingError::ParsingError(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
ParsingError::raise()
{
    throw *this;
}

/* **********************************************
 *              IOException                 *
 ********************************************** */
IOException::IOException(const std::string& Message) : GeneralException{ Message } {}

IOException::IOException(
    const std::string& Class, const std::string& Function, const std::string& Message) :
    GeneralException{ Class, Function, Message }
{}

void
IOException::raise()
{
    throw *this;
}

} // namespace MXWare
