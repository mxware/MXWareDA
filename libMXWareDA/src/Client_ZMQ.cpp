#include "Client_ZMQ.hh"
#include "MXWare/Utilities/real_async.hh"

#include <cassert>
#include <sstream>
#include <boost/current_function.hpp>
#include <random>
#include <chrono>

#include "fmt/ostream.h"

namespace MXWare
{
namespace Core
{
namespace Courier
{

Client_ZMQ::Client_ZMQ(std::shared_ptr<MXCourier_ZMQ> Context) :
    m_Context{ Context },
    m_Dealer{},
    m_CurrentEndpoint{ "" },
    m_ListenStop{ true },
    m_FListen{},
    m_Outstanding{},
    m_OutstandingAccess{},
    m_Processing{},
    m_NextMessageId{ std::random_device{}() }
{
    assert(static_cast<bool>(m_Context));
    try
    {
        m_Dealer = zmq::socket_t{ m_Context->ZMQ_Context(), zmq::socket_type::dealer };
        m_Context->Log().Debug("New ZMQ dealer socket created");
    }
    catch (zmq::error_t& e)
    {
        std::stringstream mstream;
        mstream << "ZMQ error: " << e.num() << "\nUnable to create the required socket";
        const std::string msg = mstream.str();
        switch (e.num())
        {
        case EINVAL:
            throw InvalidEndpoint{ msg };
        case ETERM:
            throw IOServiceTerminated{ msg };
        case EFAULT:
            throw IOServiceInvalid{ msg };
        case EMFILE:
            throw MaxSupportedSockets{ msg };
        default:
            throw MXCourierException{ msg };
        }
    }
}

Client_ZMQ::~Client_ZMQ()
{
    try
    {
        Disconnect();
    }
    catch (std::exception& e)
    {
        m_Context->Log().Error(
            "Unhandled exception detected while closing the client:\n{}", e.what());
    }
    // The close function is no except
    m_Dealer.close();
}

void
swap(Client_ZMQ& lhs, Client_ZMQ& rhs) noexcept
{
    std::unique_lock<std::mutex> lhs_Out{ lhs.m_OutstandingAccess, std::defer_lock };
    std::unique_lock<std::mutex> rhs_Out{ rhs.m_OutstandingAccess, std::defer_lock };
    std::lock(lhs_Out, rhs_Out);

    using std::swap;
    swap(lhs.m_Context, rhs.m_Context);
    swap(lhs.m_Dealer, rhs.m_Dealer);
    swap(lhs.m_CurrentEndpoint, rhs.m_CurrentEndpoint);
    lhs.m_ListenStop.store(rhs.m_ListenStop.exchange(lhs.m_ListenStop.load()));
    swap(lhs.m_FListen, rhs.m_FListen);
    swap(lhs.m_Outstanding, rhs.m_Outstanding);
    swap(lhs.m_Processing, rhs.m_Processing);
}

Client_ZMQ::Client_ZMQ(Client_ZMQ&& other) : Client_ZMQ() { swap(*this, other); }

Client_ZMQ&
Client_ZMQ::operator=(Client_ZMQ&& other)
{
    swap(*this, other);
    return *this;
}

void
Client_ZMQ::ConnectDealer(const std::string& Endpoint)
{
    try
    {
        if (Endpoint != m_CurrentEndpoint)
        {
            Disconnect();
        }
        m_Dealer.connect(Endpoint);
        m_CurrentEndpoint = Endpoint;

        m_Context->Log().Debug("Dealer socket connected to the {} endpoint", Endpoint);
    }
    catch (zmq::error_t& e)
    {
        std::string msg =
            fmt::format("ZMQ error: {}\nUnable to connect the Courier client to the endpoint {}",
                e.num(), Endpoint);

        switch (e.num())
        {
        case EINVAL:
            throw InvalidEndpoint{ msg };
        case EPROTONOSUPPORT:
            throw UnsupportedProtocol{ msg };
        case ENOCOMPATPROTO:
            throw IncompatibleProtocol{ msg };
        case ETERM:
            throw IOServiceTerminated{ msg };
        case ENOTSOCK:
            throw InvalidSocket{ msg };
        case EMTHREAD:
            throw MissingThread{ msg };
        default:
            throw MXCourierException{ msg };
        }
    }
}

void
Client_ZMQ::ConnectInternal(const std::string& Identifier)
{
    std::stringstream tmp;
    tmp << "inproc://" << Identifier;
    ConnectDealer(tmp.str());

    StartProcessing();
}

void
Client_ZMQ::ConnectRemote(const std::string Hostname, uint16_t Port)
{
    std::stringstream Endpoint;
    Endpoint << "tcp://" << Hostname << ":" << Port;
    ConnectDealer(Endpoint.str());

    StartProcessing();
}

bool
Client_ZMQ::IsConnected() const
{
    return !m_CurrentEndpoint.empty();
}

void
Client_ZMQ::Disconnect()
{
    try
    {
        if (!m_CurrentEndpoint.empty())
        {
            StopProcessing();

            m_Dealer.disconnect(m_CurrentEndpoint);
            m_CurrentEndpoint.clear();
            for (auto& Req : m_Processing)
            {
                Req.second.m_Rep.set_exception(std::make_exception_ptr(SocketDisconnected{
                    "The client was disconnected before fulfilling the request" }));
            }
        }
    }
    catch (zmq::error_t& e)
    {
        std::stringstream mstream;
        mstream << "ZMQ error: " << e.num()
                << "\nUnable to disconnect the Courier client to the current endpoint "
                << m_CurrentEndpoint;
        const std::string msg = mstream.str();
        switch (e.num())
        {
        case EINVAL:
            throw InvalidEndpoint{ msg };
        case ETERM:
            throw IOServiceTerminated{ msg };
        case ENOTSOCK:
            throw InvalidSocket{ msg };
        case ENOENT:
            throw EndpointNotConnected{ msg };
        default:
            throw MXCourierException{ msg };
        }
    }
}

std::future<Reply>
Client_ZMQ::SendRequest(Request&& Req)
{
    if (!IsConnected())
    {
        Courier::EndpointNotConnected e{ "Client_ZMQ", BOOST_CURRENT_FUNCTION,
            "The client is not currently connected" };
        e.raise();
    }

    ProcRequest P{ m_NextMessageId.fetch_add(1), std::move(Req), std::promise<Reply>{} };

    auto ans = P.m_Rep.get_future();
    m_Context->Log().Debug("Request with ID {} queued", P.m_Id);

    std::lock_guard<std::mutex> Lock{ m_OutstandingAccess };
    m_Outstanding.push_back(std::move(P));

    return ans;
}

void
Client_ZMQ::MessageDealing()
{
    // DEVNOTE The send and recv must be done within the same thread because
    // the dealer socket is not thread safe
    // Therefore I should enter in a loop, connected to a queue of requests
    // to be processed. The request to be processed is transformed in an
    // appropriate message and moved to the list of the processing requests
    // waiting for replies. A request should also be associated with a flag
    // defining: retry policy, timeout policy and potentially more
    // Once I sent a request I check if there are replies
    // waiting and continue sending until the send queue is empty.
    // If the send queue is empty and there are no replies I can wait and poll
    // for a while and then check again

    // Checks that the socket is connected?

    // The length of time to wait in the poll loop before trying again
    constexpr auto PollTimeout = std::chrono::milliseconds{ 1 };

    // This is a messy interface of cppzmq. pollitem_t is the same as the zmq_pollitem
    // which takes a void pointer to the socket as argument. The cppzmq socket as an
    // overloaded casting to void* which returns the raw zmq socket that one can put into
    // the poller. At one point I may want to use a better interface but this works now and
    // polls correctly
    // Note that the zmq API specifies that one has to cast the concrete object to a void *
    std::vector<zmq::pollitem_t> Sockets2Poll{ { zmq::pollitem_t{
        static_cast<void*>(m_Dealer), 0, ZMQ_POLLIN, 0 } } };

    // Sockets2Poll.push_back(zmq::pollitem_t{ static_cast<void*>(m_Dealer), 0, ZMQ_POLLIN, 0 });

    // Starts listening for replies until we decide to stop
    while (!m_ListenStop)
    {
        try
        {
            if (!m_Outstanding.empty())
            {
                m_Context->Log().Debug("Sending all outstanding messages");
                // Sends all outstanding requests
                // Blocks the two queues
                std::unique_lock<std::mutex> OutLock{ m_OutstandingAccess };

                for (auto& it : m_Outstanding)
                {
                    auto ID = it.m_Id;
                    auto Header = GenerateRequestHeader(it.m_Req, ID);
                    m_Context->Log().Debug("Header {}", Header);
                    // Sending an empty message to simulate the req envelope
                    m_Dealer.send(zmq::message_t{}, zmq::send_flags::sndmore);
                    m_Dealer.send(zmq::message_t{ Header }, zmq::send_flags::sndmore);
                    auto& Payload = it.m_Req.GetPayload();
                    m_Dealer.send(
                        zmq::message_t{ Payload.begin(), Payload.end() }, zmq::send_flags::none);
                    /*auto res = */ m_Processing.insert(std::make_pair(ID, std::move(it)));
                }

                m_Outstanding.clear();
            }

            // This will poll the listed socket up to a timeout of 1
            // millisecond It will come out of the loop as soon as one
            // of the socket receives data and returns the events
            // received by the attached sockets or 0 if no event has
            // been received. In the first case we will proceed emptying
            // the message queues, otherwise we will loop again and check
            // if in the meantime we were required to stop
            // auto PollResult =  zmq::poll(Sockets2Poll, PollTimeout);
            if (zmq::poll(Sockets2Poll, PollTimeout) != 0)
            // if (Sockets2Poll[0].revents & (ZMQ_POLLIN))
            {
                m_Context->Log().Trace("Replies received");
                std::vector<zmq::message_t> MsgFrames{};
                zmq::message_t ReceivedFrame{};

                auto Received = m_Dealer.recv(ReceivedFrame, zmq::recv_flags::dontwait);
                while (Received)
                {
                    m_Context->Log().Trace("Received reply: {}", ReceivedFrame);
                    // Read all frames and put them in the vector until we don't receive more
                    MsgFrames.push_back(zmq::message_t{});
                    MsgFrames.back().copy(ReceivedFrame);

                    // If we don't receive more messages create a Request and store it then clear
                    // the vector
                    if (!ReceivedFrame.more())
                    {
                        auto Rep = DecodeReply(MsgFrames);
                        if (Rep.first.Valid())
                        {
                            auto itRep = m_Processing.find(Rep.second);
                            if (itRep != m_Processing.end())
                            {
                                itRep->second.m_Rep.set_value(std::move(Rep.first));
                                m_Processing.erase(itRep);
                            }
                        }
                        MsgFrames.clear();
                    }
                    Received = m_Dealer.recv(ReceivedFrame, zmq::recv_flags::dontwait);
                }
            }
        }
        catch (zmq::error_t& e)
        {
            m_Context->Log().Error(
                "ZMQ error {} caught: {}. Attempting to recover", e.num(), zmq_strerror(e.num()));
            continue;
        }
        catch (MessageDecodingError& e)
        {
            m_Context->Log().Warn(e.what());
        }
    }
}

std::pair<Reply, uint64_t>
Client_ZMQ::DecodeReply(std::vector<zmq::message_t>& MsgFrames)
{
    std::pair<Reply, uint64_t> ans{};

    if (MsgFrames.size() > 3)
    {
        m_Context->Log().Debug(
            "Received message with {} frames when no more than 3 were expected. Ignoring it");
    }
    else if (MsgFrames.size() <= 1)
    {
        m_Context->Log().Debug("Received an empty message. Ignoring it");
    }
    else
    {
        auto& HeaderFrame = MsgFrames[1];
        const std::string ExpHeaderHead =
            fmt::format("MXP {} {} REP ", ProtocolVersion.Major, ProtocolVersion.Minor);

        if (HeaderFrame.size() < ExpHeaderHead.size())
        {
            throw MessageDecodingError{ fmt::format("Received reply with incomplete header: {}",
                std::string{ static_cast<const char*>(HeaderFrame.data()), HeaderFrame.size() }) };
        }
        // Checks if the beginning of the header is what we expect
        else if (ExpHeaderHead.compare(0, ExpHeaderHead.size(),
                     static_cast<const char*>(HeaderFrame.data()), ExpHeaderHead.size()) != 0)
        {
            throw MessageDecodingError{ fmt::format("Received reply with unrecognized header: {}",
                std::string{ static_cast<const char*>(HeaderFrame.data()), HeaderFrame.size() }) };
        }
        else
        {
            std::stringstream HeaderTail{ std::string{
                static_cast<const char*>(HeaderFrame.data()) + ExpHeaderHead.size(),
                HeaderFrame.size() - ExpHeaderHead.size() } };
            std::string Category{};
            HeaderTail >> ans.second >> Category;
            if (HeaderTail.fail())
            {
                throw MessageDecodingError{ fmt::format(
                    "Received message with unrecognized header: {}",
                    std::string{
                        static_cast<const char*>(HeaderFrame.data()), HeaderFrame.size() }) };
            }
            ans.first.SetCategory(Category);

            if (MsgFrames.size() == 3)
            {
                auto& PayloadFrame = MsgFrames[2];
                RawPayload P{ static_cast<const char*>(PayloadFrame.data()),
                    static_cast<const char*>(PayloadFrame.data()) + PayloadFrame.size() };
                ans.first.SetPayload(std::move(P));
            }

            m_Context->Log().Trace(
                "Reply Id: {}\nReply Category: {}", ans.second, ans.first.GetCategory());
        }
    }

    return ans;
}

void
Client_ZMQ::StartProcessing()
{
    // If the thread is already running do nothing
    if (m_FListen.valid())
    {
        m_Context->Log().Debug("The client is already running");
        return;
    }

    m_ListenStop.store(false);
    m_FListen = real_async([&]() { this->MessageDealing(); });
}

void
Client_ZMQ::StopProcessing()
{
    m_ListenStop.store(true);

    if (m_FListen.valid())
        m_FListen.get();
}


} // namespace Courier
} // namespace Core
} // namespace MXWare
