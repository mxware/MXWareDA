/*
 * ControlMessage.hh
 *
 *  Created on: Aug 19, 2019
 *      Author: caiazza
 */

#ifndef LIBMXWAREDA_SRC_CONTROLMESSAGE_HH_
#define LIBMXWAREDA_SRC_CONTROLMESSAGE_HH_

#include "MXWare/Configuration.hh"
#include <string>

namespace MXWare
{
namespace Core
{
/// Structure to store the details of a control message for the request/reply exchange
/// Each exchange requires:
/// * The connection ID of the requesting client as provided to the router
/// * The message ID as set by the client that would allow an async client
/// to match requests and replies
/// * The format used to encode the body which should possibly used to encode
/// the reply as well
/// * The body of the message structured in a Configuration::PropertyTree object
/// NOTE that this class is not intrinsically thread safe
class ControlMessage
{
public:
    /// Creates an empty message
    ControlMessage() = default;

    /// Full constructor to set all items
    ControlMessage(const std::string& Connection, const std::string& MessageID,
        const std::string& Format, const Configuration::PropertyTree& Body) :
        m_Connection{ Connection }, m_MessageID{ MessageID }, m_FormatTag{ Format }, m_Body{ Body }
    {}

    /// Creates a new empty control message, allowing to make a parameter creation chain
    static ControlMessage
    Create()
    {
        return ControlMessage();
    }

    ///@{
    /// Getters and setters.
    /// Note that the setters allow the usage of the named parameter idiom
    const Configuration::PropertyTree&
    GetBody() const noexcept
    {
        return m_Body;
    }

    ControlMessage&
    SetBody(const Configuration::PropertyTree& body)
    {
        m_Body = body;
        return *this;
    }

    ControlMessage&
    SetBody(Configuration::PropertyTree&& body) noexcept
    {
        std::swap(m_Body, body);
        return *this;
    }

    const std::string&
    GetConnection() const noexcept
    {
        return m_Connection;
    }

    ControlMessage&
    SetConnection(const std::string& connection)
    {
        m_Connection = connection;
        return *this;
    }

    ControlMessage&
    SetConnection(std::string&& connection) noexcept
    {
        m_Connection = std::move(connection);
        return *this;
    }

    const std::string&
    GetFormatTag() const noexcept
    {
        return m_FormatTag;
    }

    ControlMessage&
    SetFormatTag(const std::string& formatTag)
    {
        m_FormatTag = formatTag;
        return *this;
    }

    ControlMessage&
    SetFormatTag(std::string&& formatTag) noexcept
    {
        m_FormatTag = std::move(formatTag);
        return *this;
    }

    const std::string&
    GetMessageId() const noexcept
    {
        return m_MessageID;
    }

    ControlMessage&
    SetMessageId(const std::string& messageId)
    {
        m_MessageID = messageId;
        return *this;
    }

    ControlMessage&
    SetMessageId(std::string&& messageId) noexcept
    {
        m_MessageID = std::move(messageId);
        return *this;
    }
    ///@}

private:
    /// The connection identifier
    std::string m_Connection;

    /// The identifier of the message as provided by the client
    std::string m_MessageID;

    /// The tag identifying the format used to encode the message
    std::string m_FormatTag;

    /// The body of the message
    Configuration::PropertyTree m_Body;
};


} // namespace Core
} // namespace MXWare





#endif /* LIBMXWAREDA_SRC_CONTROLMESSAGE_HH_ */
