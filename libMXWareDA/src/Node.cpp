#include "MXWare/Execution/Node.hh"

#include "MXWare/Core/Utilities/ReaderWriterQueue.hh"
#include "MXWare/Execution/INodeCommand.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"
#include "MXWare/Execution/ElementList.hh"
#include "MXWare/Core/MXCourier.hh"
#include "MXWare/Utilities/real_async.hh"
#include "MXWare/Signals.hh"

#include <cassert>
#include <atomic>

#include <chrono>


namespace MXWare::Execution
{
/// The implementation class of the node
class Node::Impl
{
    friend class Node;

    /// A plain structure to store the Node commands and the promise
    /// connected to that command
    class AsyncNodeCommand
    {
    public:
        AsyncNodeCommand() : m_Command{}, m_ResultPromise{} {}

        AsyncNodeCommand(std::unique_ptr<INodeCommand>&& Cmd) :
            m_Command{ std::move(Cmd) }, m_ResultPromise{}
        {}

        auto
        GetFuture()
        {
            return m_ResultPromise.get_future();
        }

        void
        Execute(Node& N)
        {
            try
            {
                m_ResultPromise.set_value(m_Command->Execute(N));
            }
            catch (...)
            {
                // store anything thrown in the promise
                m_ResultPromise.set_exception(std::current_exception());
            }
        }

        const INodeCommand&
        GetCommand() const
        {
            return *m_Command;
        }

    private:
        /// The command to execute
        std::unique_ptr<INodeCommand> m_Command;

        /// The promise of the result
        std::promise<std::unique_ptr<Courier::IReply>> m_ResultPromise;
    };

public:
    Impl(const std::string& Name, std::shared_ptr<Core::MXCourier> C, Node& N) :
        m_CurrentState{ Node::State::Ready },
        m_Name{ Name },
        m_CommandQueue{},
        m_Courier{ C },
        m_Elements{},
        m_StopExecution{ true },
        m_StopNode{ false },
        m_StopMonitoring{ false },
        m_ExecutionStarted{ false },
        m_MonitoringHandle{ real_async([&]() { MonitorNode(N); }) },
        m_WhenExecutionComplete{}
    {

        // Initialize the plugin manager
        // Loads the default configuration
        // Creates the execution control with its control queue
    }

    Impl(Node& N) :
        Impl("", Core::MXCourier::NewCourier(), N)
    {}

    ~Impl()
    {
        StopProcessing();
        m_StopMonitoring.store(true);
        assert(m_MonitoringHandle.valid());
        m_MonitoringHandle.get();
    }

    Impl(const Impl& other) = delete;

    Impl&
    operator=(const Impl& other) = delete;

    void
    Start(Node& N)
    {
        m_StopNode.store(false);
        StartProcessing(N);
        while (!m_StopNode)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        StopProcessing();
    }

    void
    Stop()
    {
        m_StopNode.store(true);
    }

    Node::State
    GetState() const noexcept
    {
        return m_CurrentState.load();
    }

    std::future<std::unique_ptr<Courier::IReply>>
    EnqueueCommand(std::unique_ptr<INodeCommand> Command)
    {
        AsyncNodeCommand ToEnqueue{ std::move(Command) };
        auto ans = ToEnqueue.GetFuture();
        m_CommandQueue.enqueue(std::move(ToEnqueue));
        return ans;
    }

    void
    Configure(const Configuration::PropertyTree& Tree, Node& Parent)
    {
        Configuration::UpdateParameter(m_Name, Tree, "name");
        Configuration::UpdateParameter(*m_Courier, Tree, Core::MXCourier::s_CourierService_NN);
        Configuration::UpdateParameter(m_Elements, Tree, "elements", Parent);
    }

    Configuration::PropertyTree
    CurrentConfiguration() const
    {
        Configuration::PropertyTree ans{};
        Configuration::WriteToTree(m_Name, ans, "name");
        Configuration::WriteToTree(*m_Courier, ans, Core::MXCourier::s_CourierService_NN);
        Configuration::WriteToTree(m_Elements, ans, "elements");
        return ans;
    }

    void
    StartProcessing(Node& N)
    {
        if (!m_CommandProcessingHandle.valid())
        {
            m_StopExecution.store(false);
            m_CommandProcessingHandle = real_async([&]() { ProcessCommandQueue(N); });
            m_CurrentState.store(Node::State::Processing);
        }
    }

    void
    StopProcessing() noexcept
    {
        if (m_CommandProcessingHandle.valid())
        {
            m_StopExecution.store(true);
            try
            {
                m_CommandProcessingHandle.get();
            }
            catch (const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }

            m_CurrentState.store(Node::State::Ready);
        }
    }

    void
    StartAll()
    {
        for (auto& E : m_Elements)
        {
            Log::GlobalService.FrameworkLogger().Debug("Starting element {}", E.GetName());
            E.Start();
            Log::GlobalService.FrameworkLogger().Debug("Element {} started", E.GetName());
        }
        m_ExecutionStarted.store(true);
    }

private:
    /// The current state of the node
    std::atomic<Node::State> m_CurrentState;

    /// The identification name of the name
    std::string m_Name;

    /// Lock free queue to manage the commands to execute
    ReaderWriterQueue<AsyncNodeCommand> m_CommandQueue;

    /// Handle to the background command processing function
    std::future<void> m_CommandProcessingHandle;

    /// The shareable courier service
    std::shared_ptr<Core::MXCourier> m_Courier;

    /// A list of the registered executable elements
    Execution::ElementList m_Elements;

    /// Atomic flag to stop the node execution
    std::atomic<bool> m_StopExecution;

    /// Stop the running of the node
    std::atomic<bool> m_StopNode;

    /// Atomic flag to stop monitoring
    std::atomic<bool> m_StopMonitoring;

    /// Atomic flag signalling the element execution is started
    std::atomic<bool> m_ExecutionStarted;

    /// Handle to the background process that monitors the node execution
    std::future<void> m_MonitoringHandle;

    /// Signals the completion of the execution
    Signals::Signal<void()> m_WhenExecutionComplete;

    void
    ProcessCommandQueue(Node& N)
    {
        while (!m_StopExecution)
        {
            if (AsyncNodeCommand NextCommand{}; m_CommandQueue.try_dequeue(NextCommand))
            {
                Log::GlobalService.FrameworkLogger().Debug(
                    "Executing command: {}", NextCommand.GetCommand().GetTypeId());
                NextCommand.Execute(N);
            }
            else
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }
        }
    }

    void
    MonitorNode(Node& N) noexcept
    {
        while (!m_StopMonitoring)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds{ 100 });
            if (m_ExecutionStarted)
            {
                bool StillExecutingSome = false;
                for (auto& E : m_Elements)
                {
                    if (auto EState = E.GetState();
                        EState == IElement::State::Running || EState == IElement::State::Suspended)
                    {
                        StillExecutingSome = true;
                        break;
                    }
                }
                if (!StillExecutingSome)
                {
                    m_ExecutionStarted.store(false);
                    // m_WhenExecutionComplete.emit();
                    N.Stop();
                }
            }
        }
    }
};

/* **************************************************************************
 * **************************************************************************
 *
 * Definition of the node class
 *
 ****************************************************************************
 ************************************************************************** */
Node::Node() : m_impl{ new Node::Impl(*this) } {}

Node::Node(const std::string& Name,
    std::shared_ptr<Core::MXCourier> C) :
    m_impl{ new Node::Impl(Name, C, *this) }
{}

Node::~Node() = default;

Node::Node(Node&& other) noexcept = default;

Node&
Node::operator=(Node&& other) noexcept = default;

void
Node::Start()
{
    m_impl->Start(*this);
}

void
Node::Stop()
{
    m_impl->Stop();
}

auto
Node::GetState() const noexcept -> State
{
    return m_impl->GetState();
}

std::future<std::unique_ptr<Courier::IReply>>
Node::EnqueueCommand(std::unique_ptr<INodeCommand> Command)
{
    return m_impl->EnqueueCommand(std::move(Command));
}

void
Node::Configure(const Configuration::PropertyTree& T)
{
    return m_impl->Configure(T, *this);
}

Configuration::PropertyTree
Node::CurrentConfiguration() const
{
    return m_impl->CurrentConfiguration();
}

Signals::Connection
Node::WhenExecutionComplete(const Signals::Signal<void()>::slot_type& Slot)
{
    return m_impl->m_WhenExecutionComplete.connect(Slot);
}

Log::LoggingService&
Node::GetLoggingService()
{
    return Log::GlobalService;
}

const Log::LoggingService&
Node::GetLoggingService() const
{
    return Log::GlobalService;
}

Core::MXCourier&
Node::GetCourier()
{
    return *(m_impl->m_Courier);
}

const Core::MXCourier&
Node::GetCourier() const
{
    return *(m_impl->m_Courier);
}

std::shared_ptr<Core::MXCourier>
Node::ShareCourier()
{
    return m_impl->m_Courier;
}

void
Node::SetCourier(std::shared_ptr<Core::MXCourier> C)
{
    m_impl->m_Courier = C;
}

const std::string&
Node::GetName() const
{
    return m_impl->m_Name;
}

void
Node::SetName(const std::string& Name)
{
    m_impl->m_Name = Name;
}

Execution::ElementList&
Node::GetElementList()
{
    return m_impl->m_Elements;
}

const Execution::ElementList&
Node::GetElementList() const
{
    return m_impl->m_Elements;
}
void
Node::StartProcessing()
{
    m_impl->StartProcessing(*this);
}

void
Node::StopProcessing() noexcept
{
    m_impl->StopProcessing();
}

void
Node::StartAll()
{
    m_impl->StartAll();
}
// void
// Node::PauseProcessing()
// {}

// void
// Node::ContinueProcessing()
// {}

} // namespace MXWare::Execution
