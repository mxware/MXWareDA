#include "MXWare/Execution/Commands.hh"
#include "MXWare/Courier/Replies.hh"
#include "MXWare/Execution/Node.hh"
#include "MXWare/Execution/ElementList.hh"
#include "MXWare/Log.hh"

namespace MXWare::Execution::Command
{
namespace detail
{
void
CommandBase::Configure(const Configuration::PropertyTree& ConfTree)
{
    Configuration::UpdateParameter(m_Id, ConfTree, "id");
}

Configuration::PropertyTree
CommandBase::CurrentConfiguration() const
{
    Configuration::PropertyTree ans;
    ans.put(Configuration::detail::ObjectTypeTag, GetTypeId());
    ans.put("id", GetId());
    ans.put_child("body", GetBody());
    return ans;
}

Configuration::PropertyTree
CommandBase::GetBody() const
{
    return Configuration::PropertyTree{};
}

} // namespace detail

auto
StartAll::Execute(Node& N) const -> std::unique_ptr<Courier::IReply>
{
    auto& NodeElements = N.GetElementList();
    N.GetLoggingService().FrameworkLogger().Debug("Starting {} elements", NodeElements.size());
    N.StartAll();
    return std::make_unique<Courier::Reply::Empty>(*this);
}


auto
StopWhenFinished::Execute(Node&) const -> std::unique_ptr<Courier::IReply>
{
    // N.WhenExecutionComplete([&](){N.Stop();});
    return std::make_unique<Courier::Reply::Empty>(*this);
}

} // namespace MXWare::Execution::Command