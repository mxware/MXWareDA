#ifndef MXWARE_SRC_PROPERTYTREECODECS_HH_
#define MXWARE_SRC_PROPERTYTREECODECS_HH_
#pragma once

#include "MXWare/Configuration/ICodec.hh"

namespace MXWare::Configuration
{
class XML_ConfTreeCodec : public ICodec
{
public:
    /// No configuration necessary
    void
    Configure(const PropertyTree&) override
    {}

    /// No configuration
    PropertyTree
    CurrentConfiguration() const override
    { return PropertyTree{};}

    /// Reads the input stream and formats it into configuration tree which should be
    /// already allocated
    /// Returns the input stream after all the necessary characters have been extracted
    /// If the input stream cannot be decoded the tree will not be changed and the input stream
    /// failbit will be set to true.
    std::istream&
    Read(std::istream& In, PropertyTree& Tree) const override;

    /// Writes the content of an existing configuration tree to an output stream
    virtual std::ostream&
    Write(std::ostream& Out, const PropertyTree& Tree) const override;

    /// Removed the XML comment trees, labelled <xmlcomment> from a property tree
    static void
    RemoveXMLComments(PropertyTree& Tree);

    /// Converts the attributes contained in the <xmlattr> fields of the
    /// property tree in normal elements
    static void
    FlattenXMLAttributes(PropertyTree& Tree);
};
} // namespace MXWare::Configuration

#endif // MXWARE_SRC_PROPERTYTREECODECS_HH_