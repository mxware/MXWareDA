/*
 * NodeServices.cpp
 *
 *  Created on: Nov 7, 2019
 *      Author: caiazza
 */

#include "MXWare/Core/NodeServices.hh"

namespace MXWare
{
namespace Core
{
MXWARE_IMPLEMENT_EXCEPTION(NodeConnectionException, GeneralException)

} // namespace Core


} // namespace MXWare
