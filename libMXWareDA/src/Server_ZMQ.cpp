/*
 * Server_ZMQ.cpp
 *
 *  Created on: Nov 26, 2019
 *      Author: caiazza
 */

#include "Server_ZMQ.hh"
#include "fmt/format.h"
#include <sstream>
#include <cassert>
#include <algorithm>
#include <array>
#include "MXWare/Utilities/real_async.hh"

namespace MXWare
{
namespace Core
{
namespace Courier
{
Server_ZMQ::Server_ZMQ(std::shared_ptr<MXCourier_ZMQ> Context) :
    m_Context{ Context },
    m_Router{},
    m_BoundEndpoints{},
    m_RouterAccess{},
    m_StopProcessing{ true },
    m_ProcessingHandle{},
    m_WaitingQueues{},
    m_QueuesAccess{},
    m_ProcessingFunctions{}
{
    assert(static_cast<bool>(m_Context));
    m_Router = zmq::socket_t{ m_Context->ZMQ_Context(), zmq::socket_type::router };
    StartProcessing();
}

Server_ZMQ::~Server_ZMQ()
{
    try
    {
        StopProcessing();
        UnbindAll();
    }
    catch (std::exception& e)
    {
        m_Context->Log().Error(
            "Unhandled exception detected while closing the server:\n{}", e.what());
    }
    m_Router.close();
}

// Server_ZMQ::Server_ZMQ(Server_ZMQ&& other) :
//    m_Context{ std::move(other.m_Context) },
//    m_Router{ std::move(other.m_Router) },
//    m_BoundEndpoints{std::move(other.m_BoundEndpoints)},
//    m_RouterAccess{},
//    m_StopProcessing{other.m_StopProcessing.load()}
//{}
//
// void
// swap(Server_ZMQ& lhs, Server_ZMQ& rhs) noexcept
//{
//    using std::swap;
//    std::unique_lock<std::mutex> L_Lock(lhs.m_RouterAccess, std::defer_lock);
//    std::unique_lock<std::mutex> R_Lock(rhs.m_RouterAccess, std::defer_lock);
//    std::lock(L_Lock, R_Lock);
//    swap(lhs.m_Context, rhs.m_Context);
//    swap(lhs.m_Router, rhs.m_Router);
//    swap(lhs.m_BoundEndpoints, rhs.m_BoundEndpoints);
//    lhs.m_StopProcessing.store(rhs.m_StopProcessing.exchange(lhs.m_StopProcessing.load()));
//}
//
// Server_ZMQ&
// Server_ZMQ::operator=(Server_ZMQ&& other)
//{
//    swap(*this, other);
//    return *this;
//}

bool
Server_ZMQ::BindInternal(const std::string& Identifier)
{
    BindRouter(fmt::format("inproc://{}", Identifier));
    return true;
}

bool
Server_ZMQ::BindNetworkInterface(uint16_t Port, const std::string& Interface)
{
    if (Interface == "*")
    {
        BindRouter(fmt::format("tcp://0.0.0.0:{}", Port));
    }
    else
    {
        BindRouter(fmt::format("tcp://{}:{}", Interface, Port));
    }
    return true;
}

void
Server_ZMQ::BindRouter(const std::string& Endpoint)
{
    try
    {
        std::lock_guard<std::mutex> Lock(m_RouterAccess);
        auto it = std::find(m_BoundEndpoints.begin(), m_BoundEndpoints.end(), Endpoint);
        if (it == m_BoundEndpoints.end())
        {
            m_Router.bind(Endpoint);

            m_BoundEndpoints.push_back(Endpoint);
            m_Context->Log().Debug("Router socket bound to the endpoint {}", Endpoint);
        }
    }
    catch (zmq::error_t& e)
    {
        auto msg =
            fmt::format("ZMQ error: {}\nUnable to bind the Courier server to the endpoint {}",
                e.num(), Endpoint);

        switch (e.num())
        {
        case EINVAL:
            throw InvalidEndpoint{ msg };
        case EPROTONOSUPPORT:
            throw UnsupportedProtocol{ msg };
        case ENOCOMPATPROTO:
            throw IncompatibleProtocol{ msg };
        case EADDRINUSE:
            throw AddressInUse{ msg };
        case EADDRNOTAVAIL:
            throw AddressNotAvailable{ msg };
        case ENODEV:
            throw NoDevices{ msg };
        case ETERM:
            throw IOServiceTerminated{ msg };
        case ENOTSOCK:
            throw InvalidSocket{ msg };
        case EMTHREAD:
            throw MissingThread{ msg };
        default:
            throw MXCourierException{ msg };
        }
    }
}

std::size_t
Server_ZMQ::GetNBoundEndpoints() const noexcept
{
    std::lock_guard<std::mutex> Lock(m_RouterAccess);
    return m_BoundEndpoints.size();
}

void
Server_ZMQ::UnbindAll()
{
    std::lock_guard<std::mutex> Lock(m_RouterAccess);
    for (auto& Endpoint : m_BoundEndpoints)
    {
        try
        {
            m_Router.unbind(Endpoint);
        }
        catch (zmq::error_t& e)
        {
            auto msg = fmt::format(
                "ZMQ error: {}\nUnable to disconnect the Courier client to the current endpoint {}",
                e.num(), Endpoint);
            switch (e.num())
            {
            case EINVAL:
            case ENOENT:
                m_Context->Log().Error("Unexpected error encountered while unbounding the server"
                                       " from the endpoint {}. Invalid or unbound endpoint",
                    Endpoint);
                break;
            case ETERM:
                throw IOServiceTerminated{ msg };
            case ENOTSOCK:
                throw InvalidSocket{ msg };
            default:
                throw MXCourierException{ msg };
            }
        }
    }
    m_BoundEndpoints.clear();
}

Signals::Connection
Server_ZMQ::ProcessRequestCategory(const std::string&, std::function<Reply(const Request&)>)
{
    return Signals::Connection{};
}

void
Server_ZMQ::StartProcessing()
{
    // The thread is already running
    if (m_ProcessingHandle.valid())
        return;

    m_StopProcessing.store(false);
    m_ProcessingHandle = MXWare::real_async([&]() { this->ProcessMessages(); });
}

void
Server_ZMQ::StopProcessing()
{
    m_StopProcessing.store(true);
    if (m_ProcessingHandle.valid())
    {
        m_ProcessingHandle.get();
    }
}

Server_ZMQ::ReceivedRequest
Server_ZMQ::DecodeRequest(std::vector<zmq::message_t>& MsgFrames)
{
    // The first frame contains the source identifier as for the ZMQ socket specifications
    auto& IDFrame = MsgFrames[0];

    // Ignoring the Frame[1] as that is the empty delimiter
    auto& HeaderFrame = MsgFrames[2];
    const std::string ExpHeaderHead =
        fmt::format("MXP {} {} REQ ", ProtocolVersion.Major, ProtocolVersion.Minor);

    if (HeaderFrame.size() < ExpHeaderHead.size())
    {
        throw MessageDecodingError{ fmt::format("Received message with incomplete header: {}",
            std::string{ static_cast<const char*>(HeaderFrame.data()), HeaderFrame.size() }) };
    }
    // Checks if the beginning of the header is what we expect
    else if (ExpHeaderHead.compare(0, ExpHeaderHead.size(),
                 static_cast<const char*>(HeaderFrame.data()), ExpHeaderHead.size()) != 0)
    {
        throw MessageDecodingError{ fmt::format("Received message with unrecognized header: {}",
            std::string{ static_cast<const char*>(HeaderFrame.data()), HeaderFrame.size() }) };
    }
    else
    {
        ReceivedRequest Req{};
        // Req.SourceId.assign(static_cast<const char*>(IDFrame.data()), IDFrame.size());
        Req.SourceId.move(IDFrame);

        std::stringstream HeaderTail{ std::string{
            static_cast<const char*>(HeaderFrame.data()) + ExpHeaderHead.size(),
            HeaderFrame.size() - ExpHeaderHead.size() } };

        HeaderTail >> Req.ReqId >> Req.Category;
        if (HeaderTail.fail())
        {
            throw MessageDecodingError{ fmt::format("Received message with unrecognized header: {}",
                std::string{ static_cast<const char*>(HeaderFrame.data()), HeaderFrame.size() }) };
        }

        Req.Req.SetCategory(Req.Category);

        if (MsgFrames.size() == 4)
        {
            auto& PayloadFrame = MsgFrames[3];
            RawPayload P{ static_cast<const char*>(PayloadFrame.data()),
                static_cast<const char*>(PayloadFrame.data()) + PayloadFrame.size() };
            Req.Req.SetPayload(std::move(P));
        }

        m_Context->Log().Trace("Request Id: {}\nRequest Category: {}\nRequest Payload: {}",
            Req.ReqId, Req.Category,
            std::string{ Req.Req.GetPayload().data(), Req.Req.GetPayload().size() });
        return Req;
    }
}

void
Server_ZMQ::DequeueAllFromRouter()
{
    // A vector of char vectors where to store the received messages
    // We expect up to 4 frames: Id, Empty delimiter, Header, Payload
    constexpr auto MaxExpectedFrames = 4;
    std::vector<zmq::message_t> MsgFrames{};
    MsgFrames.reserve(MaxExpectedFrames);

    zmq::message_t ReceivedFrame{};

    // Receives all messages currently queued in the router socket
    // std::lock_guard<std::mutex> R_Lock(m_RouterAccess);

    bool Received;
    while ((Received =
                   static_cast<bool>(m_Router.recv(ReceivedFrame, zmq::recv_flags::dontwait))) != 0)
    {
        // Read all frames and put them in the vector until we don't receive more
        m_Context->Log().Trace("Received frame \n{}", ReceivedFrame.str());

        MsgFrames.push_back(zmq::message_t{});
        MsgFrames.back().copy(ReceivedFrame);

        // If we don't receive more messages create a Request and store it then clear the vector
        if (!ReceivedFrame.more())
        {
            if (MsgFrames.size() > 4)
            {
                m_Context->Log().Debug("Received message with {} frames when no more than 3 were "
                                       "expected. Ignoring it");
            }
            else if (MsgFrames.size() <= 2)
            {
                m_Context->Log().Debug("Received an empty message. Ignoring it");
            }
            else
            {
                try
                {
                    auto Req = DecodeRequest(MsgFrames);
                    std::shared_lock<std::shared_timed_mutex> ReadLock{ m_QueuesAccess };
                    auto itQueue = m_WaitingQueues.find(Req.Category);
                    // If there is no queue I will have to reply with a "request denied" error 501
                    if (itQueue == m_WaitingQueues.end())
                    {
                        ReadLock.unlock();
                        Req.Rep = Reply{ "501" };
                        ReplyRequest(Req);
                    }
                    else
                    {
                        std::unique_lock<std::shared_timed_mutex> QueueWrite{
                            itQueue->second.second
                        };
                        itQueue->second.first.push_back(std::move(Req));
                    }
                }
                catch (MessageDecodingError& e)
                {
                    m_Context->Log().Warn(e.what());
                }
            }

            MsgFrames.clear();
        }
    }
}

bool
Server_ZMQ::ReplyRequest(ReceivedRequest& R)
{
    // zmq::message_t Src{ R.SourceId.data(), R.SourceId.size() };
    const std::string Header = GenerateReplyHeader(R.Rep, R.ReqId);
    auto& Payload = R.Rep.GetPayload();

    m_Context->Log().Trace(
        "Replying to the message with ID {} to source {}", R.ReqId, R.SourceId.str());
    // Sends the destination identity first
    m_Router.send(R.SourceId, zmq::send_flags::sndmore);
    // Sending an empty message to simulate the REQ/REP protocol
    m_Router.send(zmq::message_t{}, zmq::send_flags::sndmore);
    // Send the rest of the message
    m_Router.send(zmq::message_t{ Header }, zmq::send_flags::sndmore);
    m_Router.send(zmq::message_t{ Payload.begin(), Payload.end() }, zmq::send_flags::none);

    return true;
}

void
Server_ZMQ::ProcessMessages()
{
    // DEVNOTE The server needs to wait for the reception of any messages on the bound connections
    // those connection may come and go and the server must lock when it receives the message
    // but not while I poll. I rely on ZMQ non doing things weirdly in that case
    // If the poll tells me I have a new message I received I should check if the request
    // belongs to one of the accepted categories

    // The length of time to wait in the poll loop before trying again
    constexpr auto PollTimeout = std::chrono::milliseconds{ 1 };

    std::vector<zmq::pollitem_t> Sockets2Poll{};

    // This is a messy interface of cppzmq. pollitem_t is the same as the zmq_pollitem
    // which takes a void pointer to the socket as argument. The cppzmq socket as an
    // overloaded casting to void* which returns the raw zmq socket that one can put into
    // the poller. At one point I may want to use a better interface but this works now and
    // polls correctly
    // Note that the zmq API specifies that one has to cast the concrete object to a void *
    Sockets2Poll.push_back(zmq::pollitem_t{ static_cast<void*>(m_Router), 0, ZMQ_POLLIN, 0 });

    while (!m_StopProcessing)
    {
        try
        {

            // This will poll the listed socket up to a timeout of 1
            // millisecond It will come out of the loop as soon as one
            // of the socket receives data and returns the events
            // received by the attached sockets or 0 if no event has
            // been received. In the first case we will proceed emptying
            // the message queues, otherwise we will loop again and check
            // if in the meantime we were required to stop
            auto PollResult = zmq::poll(Sockets2Poll, PollTimeout);
            if (PollResult != 0)
            {
                DequeueAllFromRouter();
            }
        }
        catch (zmq::error_t& e)
        {
            m_Context->Log().Error(
                "ZMQ error {} caught: {}. Attempting to recover", e.num(), zmq_strerror(e.num()));
            continue;
        }
    }
}

} // namespace Courier
} // namespace Core
} // namespace MXWare
