#include "MXWare/Extensions/PluginManager.hh"
#include "MXWare/Extensions.hh"
#include "MXWare/Exceptions.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"
#include "MXWare/Configuration/Specializations/StdVector.hh"
#include "MXWare/Extensions/Plugins.hh"
#include "MXWare/Log.hh"

#include <vector>
#include <algorithm>
#include <shared_mutex>
#include <functional>
#include <filesystem>
#include <memory>
#include <cassert>
#include <utility>
#include <new>
#include <numeric>

#include <boost/current_function.hpp>
#include <boost/dll.hpp>
#include <fmt/format.h>
#include <fmt/ostream.h>

namespace MXWare::Extensions
{

/// An internal structure to store the data correlated to an existing plugin
/// in the system. It stores the path, the metadata, a pointer to the loaded
/// library, if loaded
struct ActualPluginData
{
    /// The path where the plugin is located
    std::filesystem::path Path;
    /// The metadata of the plugin at a given path
    boost::shared_ptr<Metadata> PluginMeta;
    /// The pointer to the loaded plugin class
    std::unique_ptr<IPlugin> LoadedPlugin;
    /// The extensions loaded from this plugin
    Registry LoadedExtensions = Registry{};

    /// Compares an instance of this structure to a path returning true
    /// if the path in the instance is less than the input path
    static auto
    OrderByPath(const ActualPluginData& data, const std::filesystem::path& InPath)
    {
        return data.Path < InPath;
    };

    static auto
    CompareByName(const ActualPluginData& lhs, const ActualPluginData& rhs)
    {
        return lhs.PluginMeta && rhs.PluginMeta && lhs.PluginMeta->Name == rhs.PluginMeta->Name;
    };
};
class PluginManager::Impl
{
    friend class PluginManager;

public:
    Impl() noexcept = default;

    ~Impl() noexcept = default;

    Impl(const Impl& other) = delete;

    Impl&
    operator=(const Impl& other) = delete;

    Impl(Impl&& other) noexcept = default;

    Impl&
    operator=(Impl&& other) noexcept = default;

    bool
    AddSearchFolder(const std::filesystem::path& Folder)
    {
        CheckFolderValidity(Folder);

        // Then add it to the list of the folders to search
        auto CanonicalPath = std::filesystem::canonical(Folder);
        if (auto itSearch = std::lower_bound(
                std::begin(m_SearchFolders), std::end(m_SearchFolders), CanonicalPath);
            itSearch == std::end(m_SearchFolders) || *itSearch != CanonicalPath)
        {
            m_SearchFolders.insert(itSearch, std::move(CanonicalPath));
            return true;
        }
        else
        {
            return false;
        }
    }

    void
    LoadValidPlugins()
    {
        std::for_each(std::begin(m_SearchFolders), std::end(m_SearchFolders),
            [&](const auto& Path) { SearchFolderForPlugins(Path); });
        RebuildLoadedExtensions();
    }

    std::size_t
    CountLoadedPlugins() const noexcept
    {
        return m_ValidPlugins.size();
    }

    const std::vector<LoadedExtension>&
    GetLoadedExtensions() const noexcept
    {
        return m_LoadedExtensions;
    }

    std::size_t
    CountRegisteredExtensions() const noexcept
    {
        auto& Extensions = GetLoadedExtensions();
        return std::accumulate(begin(Extensions), end(Extensions), std::size_t{ 0UL },
            [](std::size_t In, const LoadedExtension& Ext) {
                In += Ext.RegistryRef.get().CountRegisteredExtensions();
                return In;
            });
    }

    void
    Configure(const Configuration::PropertyTree& Tree)
    {
        Configuration::UpdateParameter(m_SearchFolders, Tree, "search_folder");
    }

    Configuration::PropertyTree
    CurrentConfiguration() const
    {
        Configuration::PropertyTree ans{};

        // Configuration::WriteToTree(m_SearchFolders, ans, "search_folder");
        auto AddSearchFolder2Tree = [&ans](const auto& Path) {
            ans.add("search_folder.val", Path.string());
        };
        std::for_each(std::begin(m_SearchFolders), std::end(m_SearchFolders), AddSearchFolder2Tree);

        return ans;
    }

private:
    /// A vector containing the folders to search for plugins
    /// The data are always sorted
    std::vector<std::filesystem::path> m_SearchFolders;

    /// Stores the details of valid plugins the data are sorted by the path value
    std::vector<ActualPluginData> m_ValidPlugins;

    /// The list of the currently loaded extensions
    std::vector<LoadedExtension> m_LoadedExtensions;

    /// Verifies the validity of the folder
    static void
    CheckFolderValidity(const std::filesystem::path& Folder)
    {
        // Check if path is valid
        if (!std::filesystem::exists(Folder))
        {
            throw FileNotFound{ "PluginManager", BOOST_CURRENT_FUNCTION,
                fmt::format("The path {} does not exist", Folder.string()) };
        }
        if (!std::filesystem::is_directory(Folder))
        {
            throw FileNotFound{ "PluginManager", BOOST_CURRENT_FUNCTION,
                fmt::format("The path {} is not a directory", Folder.string()) };
        }
    }

    /// Searches the input folder for valid plugin files
    uint32_t
    SearchFolderForPlugins(const std::filesystem::path& Folder)
    {
        namespace fs = std::filesystem;
        CheckFolderValidity(Folder);
        uint32_t Count = 0;

        std::for_each(
            fs::directory_iterator{ Folder }, fs::directory_iterator{}, [&](const auto& Entry) {
                if (TryLoadPluginDetails(Entry))
                    ++Count;
            });
        return Count;
    }

    /// Check if the file specified by the directory entry contains valid
    /// plugin metadata. If it does stores the pathe and the plugin details
    /// in a data structure
    bool
    TryLoadPluginDetails(const std::filesystem::directory_entry& Entry)
    {
        namespace fs = std::filesystem;
        auto CanonicalPath = fs::canonical(Entry.path());

        // Checks if the file at the given path has been already loaded
        auto itSearch = std::lower_bound(std::begin(m_ValidPlugins), std::end(m_ValidPlugins),
            CanonicalPath, ActualPluginData::OrderByPath);

        // A loaded plugin already exists at a given path
        if (itSearch != std::end(m_ValidPlugins) && itSearch->Path == CanonicalPath)
        {
            Log::Framework::Debug(
                "The library at the path {} has been already loaded", CanonicalPath);
            return false;
        }

        auto IsSharedLibraryCandidate = [](const fs::path& Path) -> bool {
            return fs::is_regular_file(Path) && Path.has_extension() &&
                   (Path.extension() == ".so" || Path.extension() == ".dylib" ||
                       Path.extension() == ".dll");
        };

        if (IsSharedLibraryCandidate(CanonicalPath))
        {
            try
            {
                boost::dll::shared_library CandidateLib{ CanonicalPath.native() };
                if (!CandidateLib.has(DetailsExportName))
                {
                    Log::Framework::Debug(
                        "The library at the path {} do not contain a plugin symbol", CanonicalPath);
                    return false;
                }

                auto PluginDetails =
                    boost::dll::import<Metadata>(std::move(CandidateLib), DetailsExportName);
                assert(PluginDetails);

                if (!IsPluginSupported(*PluginDetails))
                {
                    Log::Framework::Warn(
                        "The plugin found at {} is not supported.\nPlugin API: {}\nPlugin Name: {}",
                        CanonicalPath, PluginDetails->APIVersion, PluginDetails->Name);
                    return false;
                }

                if (!IsPluginUnique(*PluginDetails))
                {
                    Log::Framework::Warn(
                        "A plugin named {} has been already loaded", PluginDetails->Name);
                    return false;
                }

                std::unique_ptr<IPlugin> LoadedPlugin{ PluginDetails->LoadPlugin() };
                if (!LoadedPlugin)
                {
                    throw NullPointerException{ "PluginManager", BOOST_CURRENT_FUNCTION,
                        fmt::format(
                            "The plugin identified in file {} cannot be loaded.", CanonicalPath) };
                }

                auto Extensions = LoadPluginExtensions(*LoadedPlugin);

                Log::Framework::Info(
                    "Plugin named {} found at {}", PluginDetails->Name, CanonicalPath);
                m_ValidPlugins.insert(
                    itSearch, ActualPluginData{ std::move(CanonicalPath), std::move(PluginDetails),
                                  std::move(LoadedPlugin), std::move(Extensions) });
                return true;
            }
            catch (const boost::system::system_error& e)
            {
                Log::Framework::Info(
                    "Unable to load the plugin at: {}\n{}", CanonicalPath, e.what());
            }
        }

        return false;
    }

    /// Checks if the plugin is supported by this version of the software
    static bool
    IsPluginSupported(const Metadata& Meta)
    {
        /// For the moment only version 1 of the API is supported
        return (strcmp(Meta.Name, "") != 0) && Meta.APIVersion == 1;
    }

    /// Checks whether a plugin with the same name has been already loaded from another path.
    /// It doesn't matter if the plugin is actually different or it is a different version.
    /// Plugins should be unique
    bool
    IsPluginUnique(const Metadata& Meta) const
    {
        auto itFind = std::find_if(std::begin(m_ValidPlugins), std::end(m_ValidPlugins),
            [&](const ActualPluginData& Data) { return Data.PluginMeta->Name == Meta.Name; });
        return itFind == std::end(m_ValidPlugins);
    }

    Extensions::Registry
    LoadPluginExtensions(const IPlugin& P)
    {
        auto& LoadedPlugin = static_cast<const Versions::API1&>(P);
        return LoadedPlugin.GetExtensions();
    }

    void
    RebuildLoadedExtensions()
    {
        m_LoadedExtensions.clear();
        std::for_each(std::begin(m_ValidPlugins), std::end(m_ValidPlugins),
            [&](const ActualPluginData& PluginData) {
                m_LoadedExtensions.push_back(
                    LoadedExtension{ std::string{ PluginData.PluginMeta->Name },
                        std::cref(PluginData.LoadedExtensions) });
            });
    }
};

PluginManager::PluginManager() noexcept : m_Impl{ new PluginManager::Impl{} } {}

PluginManager::~PluginManager() noexcept = default;

PluginManager::PluginManager(PluginManager&& other) noexcept = default;

PluginManager&
PluginManager::operator=(PluginManager&& other) noexcept = default;

void
PluginManager::swap(PluginManager& other) noexcept
{
    using std::swap;
    swap(m_Impl, other.m_Impl);
}

bool
PluginManager::AddSearchFolder(const std::filesystem::path& Folder)
{
    return m_Impl->AddSearchFolder(Folder);
}

void
PluginManager::LoadValidPlugins()
{
    m_Impl->LoadValidPlugins();
}

std::size_t
PluginManager::CountLoadedPlugins() const noexcept
{
    return m_Impl->CountLoadedPlugins();
}

const std::vector<PluginManager::LoadedExtension>&
PluginManager::GetLoadedExtensions() const noexcept
{
    return m_Impl->GetLoadedExtensions();
}

std::size_t
PluginManager::CountRegisteredExtensions() const noexcept
{
    return m_Impl->CountRegisteredExtensions();
}

void
PluginManager::Configure(const Configuration::PropertyTree& Tree)
{
    return m_Impl->Configure(Tree);
}

Configuration::PropertyTree
PluginManager::CurrentConfiguration() const
{
    return m_Impl->CurrentConfiguration();
}

static std::aligned_storage_t<sizeof(PluginManager), alignof(PluginManager)> PMBuffer{};
PluginManager& GlobalPluginManager = reinterpret_cast<PluginManager&>(PMBuffer);

namespace detail
{
static int NiftyCounter;

PluginManagerInitializer::PluginManagerInitializer()
{
    if (NiftyCounter++ == 0)
    {
        new (&GlobalPluginManager) PluginManager();
    }
}

PluginManagerInitializer::~PluginManagerInitializer()
{
    if (--NiftyCounter == 0)
    {
        (&GlobalPluginManager)->~PluginManager();
    }
}

} // namespace detail

} // namespace MXWare::Extensions