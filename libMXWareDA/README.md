# The Core Library

This library contains the core components of the framework.

## Folder structure
* `include` contains all header files that will be installed with the project. The internal directory structure should match the expected directory structure in the install folder. 
* `src` contains all the source files and those headers which should not be installed
* `test` contains the test units of the library

## Components and file locations
Each major component of the library should have his files placed in a suitable directory with a single header file in the base folder `include/MXWare` which will include all the relevant headers to allow a user to include all the pieces of a component with a single file

## The configuration tree (PropertyTree)

### Configuration tree parsers
 * XML
 * JSON