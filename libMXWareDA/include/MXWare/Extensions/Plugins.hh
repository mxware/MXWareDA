
#ifndef MXWARE_EXTENSIONS_PLUGINS_HH_
#define MXWARE_EXTENSIONS_PLUGINS_HH_
#pragma once

#include <type_traits>
#include "MXWare/Definitions.hh"
#include "MXWare/Extensions/IPlugin_API1.hh"

namespace MXWare::Extensions
{
/// Alias for the function pointer to load
using LoaderFunction = std::add_pointer_t<IPlugin*()>;

/// The name of the exported symbol for the plugin details
constexpr auto DetailsExportName = "PluginMetadata";

/// Declaring the current plugin version
using PluginAPI = Versions::API1;

/// Returns the current Plugin API
constexpr uint32_t
GetCurrentAPIVersion() noexcept
{
    return PluginAPI::APIVersion;
}

/// A structure containing the details characterizing the plugin
struct Metadata
{
    /// The version of the plugin API
    uint32_t APIVersion;
    /// The name of the plugin
    const char* Name;
    /// The version of the plugin
    uint32_t PluginVersion;
    /// The function to load the plugin class
    LoaderFunction LoadPlugin;
    /// The Major version number of MXWare with which the plugin was compiled
    // int MXWareVersion_Major;
    /// The Minor version number of MXWare with which the plugin was compiled
    // int MXWareVersion_Minor;
};

} // namespace MXWare::Extensions

#define MXWARE_DEFINE_PLUGIN(PLUGINNAME, PLUGINVERSION)                       \
    class PLUGINNAME : public MXWare::Extensions::PluginAPI                          \
    {                                                                                   \
    public:                                                                             \
        const char* GetName() const noexcept override {return #PLUGINNAME;}             \
        uint32_t GetPluginVersion() const noexcept override {return PLUGINVERSION;}     \
        MXWare::Extensions::Registry GetExtensions() const override;                \
    };                                                                                  \
    MXAPI MXWare::Extensions::IPlugin* CreatePlugin(){return new PLUGINNAME;}        \
    extern "C" MXAPI MXWare::Extensions::Metadata PluginMetadata;                    \
    MXWare::Extensions::Metadata PluginMetadata{                                     \
        MXWare::Extensions::GetCurrentAPIVersion() ,                                 \
        #PLUGINNAME , PLUGINVERSION , CreatePlugin };



#endif /*MXWARE_EXTENSIONS_PLUGINS_HH_*/
