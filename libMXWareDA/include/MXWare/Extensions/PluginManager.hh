#ifndef MXWARE_EXTENSIONS_PLUGINMANAGER_HH_
#define MXWARE_EXTENSIONS_PLUGINMANAGER_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Utilities/propagate_const.hh"
#include "MXWare/Configuration/Configurable.hh"
#include "MXWare/Extensions/Registry.hh"

#include <memory>
#include <filesystem>
#include <string>
#include <string_view>
#include <functional>

namespace MXWare::Extensions
{
/// A class managing the plugin system.
/// The user can add folders where to search plugins which
/// will then be loaded if they are compatible with the current runtime
/// system and functions can be called on this plugin set
/// This class is not thread safe, should only be used in a
/// single thread or locked with an external mutex
class MXAPI PluginManager : public Configuration::Configurable
{
public:
    ///@{
    /// The gang of five + swap
    PluginManager() noexcept;

    ~PluginManager() noexcept;

    PluginManager(const PluginManager& other) = delete;

    PluginManager&
    operator=(PluginManager& other) = delete;

    PluginManager(PluginManager&& other) noexcept;

    PluginManager&
    operator=(PluginManager&& other) noexcept;

    void
    swap(PluginManager& other) noexcept;
    ///@}

    /// Adds a new folder to the list of those to be searched.
    /// The path must be that of an existing and accessible directory
    /// The function will throw a FileNotFound exception if those
    /// requirements are not fulfilled
    /// In case of underlying OS API error the function will throw a
    /// filesystem_error exception
    /// The function will return true if the search folder was inserted
    /// in the list of search folders, otherwise it will return false
    /// No logging is performed directly in this function
    bool
    AddSearchFolder(const std::filesystem::path& Folder);

    /// Searches among the search folders all the valid plugins
    /// Returns the number of plugins that were loaded
    void
    LoadValidPlugins();

    /// Returns the number of plugins currently loaded
    std::size_t
    CountLoadedPlugins() const noexcept;

    template <typename Interface, typename... Args>
    std::unique_ptr<Interface>
    CreateExtensionObject(std::string_view Key, Args&&... args) const
    {
        auto& PluginExtension = GetLoadedExtensions();
        std::unique_ptr<Interface> ans{};
        std::find_if(std::begin(PluginExtension), std::end(PluginExtension),
            [&](const LoadedExtension& Ext) {
                ans = Ext.RegistryRef.get().CreateObject<Interface, Args...>(
                    Key, std::forward<Args>(args)...);
                return static_cast<bool>(ans);
            });
        return ans;
    }

    /// Retrieves the registration key of a given object, if existing
    template <typename T>
    std::optional<std::string>
    GetExtensionRegistration(const T& Obj) const
    {
        auto& PluginExtension = GetLoadedExtensions();
        std::optional<std::string> ans;
        std::find_if(std::begin(PluginExtension), std::end(PluginExtension),
            [&](const LoadedExtension& Ext) {
                ans = Ext.RegistryRef.get().GetExtensionRegistration(Obj);
                return ans.has_value();
            });
        return ans;
    }

    /// Counts all the registered extensions
    std::size_t
    CountRegisteredExtensions() const noexcept;

    /// Counts the number of registrations of a given constructor type
    /// with a given name which may be stored in the plugin manager
    /// There may be more than one extension under the same name for the
    /// same interface if multiple plugins loaded extensions with the same name
    template <typename Interface, typename... Args>
    std::size_t
    CountRegisteredExtensions(const std::string& key) const noexcept
    {
        auto& PluginExtension = GetLoadedExtensions();
        std::size_t Count = 0UL;
        for (auto& LoadedExt : PluginExtension)
        {
            Count += LoadedExt.RegistryRef.get().CountRegistrations<Interface, Args...>(key);
        }
        return Count;
    }

    /// Configure the object with a configuration tree
    void
    Configure(const Configuration::PropertyTree& ConfTree) override;

    /// Serialize the current configuration to a configuration tree
    Configuration::PropertyTree
    CurrentConfiguration() const override;

private:
    /// Private implementation
    class Impl;

    /// Pointer to the implementation
    std::experimental::propagate_const<std::unique_ptr<Impl>> m_Impl;

    /// A structure storing the extension registry loaded with a matching
    /// name of the plugin from which was loaded
    struct LoadedExtension
    {
        /// The name of the plugin from which the registry was loaded
        /// If empty the extensions are builtin
        std::string PluginName;

        /// The registry containing the loaded extensions
        std::reference_wrapper<const Registry> RegistryRef;
    };

    /// Retrieves a Read only list of the registered extensions
    const std::vector<LoadedExtension>&
    GetLoadedExtensions() const noexcept;
};

MXAPI extern PluginManager& GlobalPluginManager;

namespace detail
{
static class MXAPI PluginManagerInitializer
{
public:
    PluginManagerInitializer();
    ~PluginManagerInitializer();

    PluginManagerInitializer(const PluginManagerInitializer&) = delete;
    PluginManagerInitializer(PluginManagerInitializer&&) = delete;
    PluginManagerInitializer&
    operator=(const PluginManagerInitializer&) = delete;
    PluginManagerInitializer&
    operator=(PluginManagerInitializer&&) = delete;
} G_PMInit;
} // namespace detail
} // namespace MXWare::Extensions

#endif // MXWARE_EXTENSIONS_PLUGINMANAGER_HH_