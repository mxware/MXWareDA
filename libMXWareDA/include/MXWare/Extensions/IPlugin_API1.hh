#ifndef MXWARE_EXTENSIONS_IPLUGIN_API1_HH_
#define MXWARE_EXTENSIONS_IPLUGIN_API1_HH_
#pragma once

#include "MXWare/Extensions/IPlugin.hh"
#include "MXWare/Extensions/Registry.hh"

namespace MXWare::Extensions::Versions
{

class API1 : public IPlugin
{
public:
    virtual ~API1() noexcept = default;

    uint32_t
    GetAPIVersion() const noexcept override
    {
        return APIVersion;
    }

    /// Returns the extensions defined by this plugin
    virtual Registry
    GetExtensions() const = 0;

    static constexpr uint32_t APIVersion = 1;
};


} // namespace MXWare::Extensions::Versions

#endif // MXWARE_EXTENSIONS_IPLUGIN_API1_HH_