#ifndef MXWARE_EXTENSIONS_IPLUGIN_HH_
#define MXWARE_EXTENSIONS_IPLUGIN_HH_
#pragma once

#include <stdint.h>

namespace MXWare::Extensions
{
/// Abstract class defining the basic plugin interface
class IPlugin
{
public:
    /// Polymorhic destructor
    virtual ~IPlugin() noexcept = default;

    /// Returns the API version of the plugin
    /// This allows the evolution of the plugin API and allows the
    /// plugin manager to properly manage multiple plugin versions
    virtual uint32_t
    GetAPIVersion() const noexcept = 0;

    /// Gets an identifying name for the plugin
    /// This identifier should be unique among all loaded plugins.
    virtual const char*
    GetName() const noexcept = 0;

    /// Returns the version of the current plugin.
    virtual uint32_t
    GetPluginVersion() const noexcept = 0;
};
} // namespace MXWare::Extensions

#endif // MXWARE_EXTENSIONS_IPLUGIN_HH_