#ifndef MXWARE_EXTENSIONS_BUILTINS_HH_
#define MXWARE_EXTENSIONS_BUILTINS_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Extensions/Registry.hh"

namespace MXWare::Extensions
{
    /// Creates a new registry containing the builtins
    MXAPI Registry
    CreateBuiltinsRegistry() noexcept;

    /// Retrieves the Extensions registry containing the Builtin extensions
    // MXAPI const Registry&
    // GetBuiltinsRegistry() noexcept;

    inline const Registry Builtins = CreateBuiltinsRegistry();

    
}


#endif // MXWARE_EXTENSIONS_BUILTINS_HH_