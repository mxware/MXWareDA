#ifndef MXWARE_EXTENSIONS_REGISTRY_HH_
#define MXWARE_EXTENSIONS_REGISTRY_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Extensions/GeneralizedFactory.hh"

#include <memory>
#include <unordered_map>
#include <string>
#include <string_view>
#include <shared_mutex>
#include <functional>
#include <algorithm>
#include <optional>
#include <utility>

namespace MXWare::Extensions
{
/// Stores the extensions for multiple type of interfaces
class MXAPI Registry
{
    /// The container for the factories
    /// The factories are indexed by their unique identifier
    using container_t = std::unordered_map<std::string, std::unique_ptr<IFactory<std::string>>>;

public:
    ///@{ STL compatible typedefs
    using size_type = container_t::size_type;

    ///@}

    /// @{
    /// The gang of five
    Registry() noexcept = default;

    ~Registry() noexcept = default;

    Registry(const Registry& other);

    Registry(Registry&&) noexcept;

    Registry&
    operator=(Registry other) noexcept;
    ///@}

    /// Swapping
    void
    swap(Registry& other) noexcept;

    /// Checks the container is empty
    bool
    empty() const noexcept;

    /// Returns the number of extensions registered.
    size_type
    CountRegisteredExtensions() const noexcept;

    /// Clears the container
    void
    clear() noexcept;

    template <typename T, typename Interface, typename... Args>
    bool
    RegisterExtension(std::string_view Key,
        std::function<std::unique_ptr<Interface>(Args...)> F = std::make_unique<T, Args...>)
    {
        using factory_t = GeneralizedFactory<std::unique_ptr<Interface>(Args...), std::string>;
        auto ID = factory_t::UniqueFactoryId();

        std::lock_guard Lock{ m_RegistryAccess };
        auto res = m_Registry.find(ID);
        if (res == m_Registry.end())
        {
            res = m_Registry.insert(std::make_pair(ID, std::make_unique<factory_t>())).first;
        }
        return dynamic_cast<factory_t&>(*(res->second))
            .RegisterImplementation(std::move(F), std::string{Key}, typeid(T));
    }

    template <typename Interface, typename... Args>
    bool
    UnregisterExtension(std::string_view key)
    {
        using factory_t = GeneralizedFactory<std::unique_ptr<Interface>(Args...), std::string>;
        auto ID = factory_t::UniqueFactoryId();

        std::lock_guard Lock{ m_RegistryAccess };
        if (auto Factory = m_Registry.find(ID); Factory != std::end(m_Registry))
        {
            return Factory->second->UnregisterImplementation(std::string{key});
        }
        else
        {
            return false;
        }
    }

    /// Counts the number of registrations of a given constructor type
    /// with a given name which are stored in the registry
    template <typename Interface, typename... Args>
    std::size_t
    CountRegistrations(std::string_view key) const noexcept
    {
        using factory_t = GeneralizedFactory<std::unique_ptr<Interface>(Args...), std::string>;
        auto ID = factory_t::UniqueFactoryId();
        std::shared_lock Lock{ m_RegistryAccess };
        if (auto search_it = m_Registry.find(ID); search_it == m_Registry.end())
        {
            return 0UL;
        }
        else
        {
            return search_it->second->CountRegistrations(std::string{key});
        }
    }

    template <typename Interface, typename... Args>
    std::unique_ptr<Interface>
    CreateObject(std::string_view key, Args&&... args) const
    {
        using factory_t = GeneralizedFactory<std::unique_ptr<Interface>(Args...), std::string>;
        auto ID = factory_t::UniqueFactoryId();
        std::shared_lock Lock{ m_RegistryAccess };
        auto search_it = m_Registry.find(ID);
        if (search_it == m_Registry.end())
        {
            return std::unique_ptr<Interface>{};
        }

        return dynamic_cast<const factory_t&>(*search_it->second)
            .CreateObject(std::string{key}, std::forward<Args>(args)...);
    }

    template <typename T>
    std::optional<std::string>
    GetExtensionRegistration(const T& Obj) const
    {
        std::type_index ID{ typeid(Obj) };
        std::shared_lock Lock{ m_RegistryAccess };
        auto res = std::find_if(m_Registry.begin(), m_Registry.end(),
            [&](auto& Record) { return Record.second->CountRegistrations(ID) != 0; });
        if (res != m_Registry.end())
        {
            return res->second->FindRegistrationKey(ID);
        }
        else
        {
            return std::optional<std::string>{};
        }
    }

    /// Merges the content of the two extension registry in this.
    void
    Merge(const Registry& other);

private:
    /// The factory container
    container_t m_Registry;

    /// Manages the access to the registry
    mutable std::shared_mutex m_RegistryAccess;
};

/// Free swap function
inline void
swap(Registry& lhs, Registry& rhs) noexcept
{
    return lhs.swap(rhs);
}

/// copy and merge
inline Registry
Merge(Registry lhs, const Registry& rhs)
{
    lhs.Merge(rhs);
    return lhs;
}
} // namespace MXWare::Extensions

#endif // MXWARE_EXTENSIONS_REGISTRY_HH_