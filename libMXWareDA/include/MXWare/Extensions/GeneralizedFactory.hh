#ifndef MXWARE_EXTENSIONS_GENERALIZEDFACTORY_HH_
#define MXWARE_EXTENSIONS_GENERALIZEDFACTORY_HH_
#pragma once

#include <string>
#include <typeinfo>
#include <memory>
#include <optional>
#include <functional>
#include <mutex> // for scoped_lock
#include <shared_mutex>
#include <stdexcept>
#include <unordered_map>
#include <typeindex>
#include <cassert>
#include <vector>
#include <algorithm>

#include <boost/iterator/transform_iterator.hpp>
#include <boost/core/demangle.hpp>

namespace MXWare::Extensions
{
/// Base class for the generalized factory allowing to store them in a container
/// identify them.
template <typename K = std::string>
class IFactory
{
public:
    /// Type representing the size of the container
    using size_type = std::size_t;

    /// Polymorphic destructor
    virtual ~IFactory() = default;

    /// Empties the internal registry from all the content
    virtual void
    clear() noexcept = 0;

    /// Queries the number of registered implementations
    virtual size_type
    size() const noexcept = 0;

    /// Checks if the factory is empty
    virtual bool
    empty() const noexcept = 0;

    /// Returns a unique name identifying the factory
    virtual std::string
    GetUniqueFactoryId() const noexcept = 0;

    /// Counts the number of constructors registered for classes
    /// with the given type id
    virtual size_type
    CountRegistrations(const std::type_index& id) const noexcept = 0;

    /// Counts the number of constructors registered for classes
    /// with the given type id
    virtual size_type
    CountRegistrations(const K& key) const noexcept = 0;

    /// Returns the registration ID, if available, of the type
    /// with a given type index. The returned optional will be set to
    /// null if the key is not found
    virtual std::optional<K>
    FindRegistrationKey(const std::type_index& id) const noexcept = 0;

    /// Removes an implementation registered in the factory at a given name.
    /// Returns false if no such implementation is registered
    virtual bool
    UnregisterImplementation(const K&) noexcept = 0;

    /// Makes a copy of the derived type
    virtual std::unique_ptr<IFactory<K>>
    Clone() const = 0;

    /// Copies the implementation of the other factory into the current one
    /// if not yet existing. If an implementation with the same identifier is
    /// present, no copy is made. Only factories of the same concrete type can
    /// be merged. If the concrete type of the input parameter does not match
    /// that of the concrete derived class, the function will throw a bad_cast
    /// exception
    /// It returns a vector containing the list of the keys that were
    /// merged in the factory
    virtual std::vector<K>
    Merge(const IFactory<K>& Other) = 0;

    /// Returns the list of the keys of the registered implementations
    virtual std::vector<K>
    GetRegisteredKeys() const = 0;
};

/// Simplified signature of the generalized factory, useful for the users of the template
/// This allows to declare the factory using a function signature as its first template
/// parameter
template <typename ConstructorSignature, typename Key = std::string>
class GeneralizedFactory;

/// A generic factory class which stores function objects with a predefined signature
/// under a unique key. The typical usage is to store the constructors of the implementations
/// of a certain type that could be declared at runtime.
/// The factory uses a locking system to ensure its thread safety
// TODO Switch to shared_mutex and read/write locks
// Decide whether to keep the current containers (PMR vectors?)
// Check a better way to register in the factory (avoid explictly inputting the typeid)
template <typename Key, typename Ret, typename... Args>
class GeneralizedFactory<Ret(Args...), Key> : public IFactory<Key>
{
    /// The base type of the factory
    using factory_base = IFactory<Key>;

    /// The complete templated type
    using self_t = GeneralizedFactory<Ret(Args...), Key>;

public:
    /// The key for indexing the factory
    using key_type = Key;

    /// The type used to represent the size of the container
    using size_type = typename factory_base::size_type;

    /// Alias to the constructor functions the class stores
    using constructor_type = std::function<Ret(Args...)>;

    /// Alias to the type of object the class produces
    using produced_type = Ret;

private:
    /// The container of the factory functions for the registered
    /// implementations The container is a unordered map as we want to be
    /// optimized for fast look up rather than navigation
    using Registry = std::unordered_map<key_type, constructor_type>;

    /// The container use for the inverse lookup of the type of objects
    /// registered This contaienr allows to lookup the key at which the
    /// constructor of a specific implementation is stored
    using InverseRegistry = std::unordered_map<std::type_index, key_type>;

    /// The type of iterator used to navigate the factory
    /// This is a forward iterator because the factory is unordered
    /// There is only the constant iterator because the user shouldn't
    /// modify the factory content through the iterator Iterator for STL
    /// navigation among the registered factory functions NOTE That the use
    /// of iterators is not intrinsically thread safe
    typedef typename Registry::const_iterator const_iterator;

public:
    /// Default constructor.
    /// By default a factory is constructed using the mangled construction
    /// function type id name as identifier
    GeneralizedFactory();

    /// Destructor. Polymorphic to allow for further specializations
    ~GeneralizedFactory() = default;

    /// Copy constructor
    GeneralizedFactory(const GeneralizedFactory<Ret(Args...), Key>& other);

    /// Move constructor
    GeneralizedFactory(GeneralizedFactory<Ret(Args...), Key>&& other);

    /// Assignment
    GeneralizedFactory<Ret(Args...), Key>&
    operator=(GeneralizedFactory<Ret(Args...), Key> other);

    /// Makes a copy of the derived type
    std::unique_ptr<factory_base>
    Clone() const override;

    /// Static factory Id
    static std::string
    UniqueFactoryId() noexcept;

    /// Returns a unique name identifying the factory
    std::string
    GetUniqueFactoryId() const noexcept override;

    /// Counts the number of constructors registered for classes
    /// with the given type id
    size_type
    CountRegistrations(const std::type_index& id) const noexcept override;

    /// Counts the number of constructors registered for classes
    /// with the given type id
    size_type
    CountRegistrations(const key_type& key) const noexcept override;

    /// Returns the registration ID, if available, of the type
    /// with a given type index. The returned optional will be set to
    /// null if the key is not found
    std::optional<Key>
    FindRegistrationKey(const std::type_index& id) const noexcept override;

    /// Registers an implementation in the factory
    /// If another implementation is already registered at the same name or if
    /// an implementation with the same index is already registered, the
    /// function will return false. In that case and in case of other exceptions
    /// the containers will not be modified
    bool
    RegisterImplementation(
        const constructor_type& ClassCreator, const key_type& RegistrationName, std::type_index id);

    /// Removes the registration of an implementation with a given name
    /// from the factory. Returns false if there wasn't an implementation with
    /// that name registered, true otherwise
    /**
     * Unregisters a class from the factory
     * @param RegistrationName
     * @return
     */
    bool
    UnregisterImplementation(const key_type& RegistrationName) noexcept override;

    /// Remove the class identified by the typeid from the registries
    bool
    UnregisterImplementation(const std::type_index& id);

    /// Creates a new object using the factory function registered with the
    /// given key It returns a null pointer in case no constructor is registered
    /// with the given key The function does not throw exceptions directly but
    /// the constructor might so it cannot be noexcept
    produced_type
    CreateObject(const key_type& K, Args&&... args) const;

    ///@{
    /// Finds the registration record of an implementation possibly stored in
    /// the factory. This can be done using the templated function given the
    /// implementation class to look for as a template parameter or providing a
    /// referene to an existing object of the
    template <class T>
    const_iterator
    FindRegistration() const noexcept;

    const_iterator
    FindRegistration(
        const typename std::pointer_traits<produced_type>::element_type& Obj) const noexcept;
    ///@}

    /// Merges the content of the other factory into this. It returns a
    /// reference to this object after the merge so that several merges can be
    /// chained
    std::vector<Key>
    Merge(const GeneralizedFactory<Ret(Args...), Key>& other);

    std::vector<Key>
    Merge(const factory_base& Other) override
    {
        return Merge(dynamic_cast<const self_t&>(Other));
    }

    /// Returns a ist of the keys of the registered implementations
    std::vector<Key>
    GetRegisteredKeys() const override
    {
        std::vector<Key> ans;
        std::for_each(std::begin(m_Registry), std::end(m_Registry),
            [&](const auto& Pair) { ans.push_back(Pair.first); });
        return ans;
    }

    /// Empties the internal registry from all the content
    virtual void
    clear() noexcept override
    {
        std::unique_lock access{ m_RegistryAccess };
        m_Registry.clear();
        m_InverseRegistry.clear();
    }

    /// Queries the number of registered implementations
    virtual size_type
    size() const noexcept override
    {
        std::shared_lock access{ m_RegistryAccess };
        return m_Registry.size();
    }

    /// Checks if the factory is empty
    virtual bool
    empty() const noexcept override
    {
        std::shared_lock access{ m_RegistryAccess };
        return m_Registry.empty();
    }

    /// Locates the implementation mapped to the given key (if existing)
    const_iterator
    find(const key_type& key) const
    {
        std::shared_lock access{ m_RegistryAccess };
        return m_Registry.find(key);
    }

    /// Counts the number of elements at the given key (0 or 1)
    size_type
    count(const key_type& key) const
    {
        std::shared_lock access{ m_RegistryAccess };
        return m_Registry.count(key);
    }

    ///@{
    /// Iterators
    /// NOTE That the use of the iterator needs external synchronization for
    /// thread safety
    const_iterator
    begin() const noexcept
    {
        return m_Registry.begin();
    }

    const_iterator
    cbegin() const noexcept
    {
        return m_Registry.cbegin();
    }

    const_iterator
    end() const noexcept
    {
        return m_Registry.end();
    }

    const_iterator
    cend() const noexcept
    {
        return m_Registry.cend();
    }
    ///@}

    ///@{
    /// Comparison operator.
    /// Two factories are equal if they store the same implementations
    /// The optional factory name is not taken into account when performing the
    /// comparison
    friend bool
    operator==(const GeneralizedFactory<Ret(Args...), Key>& lhs,
        const GeneralizedFactory<Ret(Args...), Key>& rhs)
    {
        std::shared_lock lhsAccess{ lhs.m_RegistryAccess, std::defer_lock };
        std::shared_lock rhsAccess{ rhs.m_RegistryAccess, std::defer_lock };
        std::scoped_lock lockboth{ lhsAccess, rhsAccess };

        // DEV NOTE The function objects cannot be compared but we assume that
        // registry and inverse registry are always sinchronized so we can do
        // the comparison by just comparing the inverse registry
        return lhs.m_InverseRegistry == rhs.m_InverseRegistry;
    }

    friend bool
    operator!=(const GeneralizedFactory<Ret(Args...), Key>& lhs,
        const GeneralizedFactory<Ret(Args...), Key>& rhs)
    {
        return !(lhs == rhs);
    }
    ///@}

    /// Swaps the content of two factories
    friend void
    swap(GeneralizedFactory<Ret(Args...), Key>& First,
        GeneralizedFactory<Ret(Args...), Key>& Second) noexcept
    {
        std::scoped_lock LockBoth(First.m_RegistryAccess, Second.m_RegistryAccess);

        std::swap(First.m_Registry, Second.m_Registry);
        std::swap(First.m_InverseRegistry, Second.m_InverseRegistry);
    }

private:
    /// Containers to store the constructors of the registered implementations
    Registry m_Registry;

    /// Container to store the
    InverseRegistry m_InverseRegistry;

    /// Mutex to access the registry
    mutable std::shared_mutex m_RegistryAccess;
};




/* *****************************************************
 * *****************************************************
 *              Template implementation
 ***************************************************** */

template <typename Key, typename Ret, typename... Args>
GeneralizedFactory<Ret(Args...), Key>::GeneralizedFactory() :
    m_Registry{}, m_InverseRegistry{}, m_RegistryAccess{}
{}

template <typename Key, typename Ret, typename... Args>
inline GeneralizedFactory<Ret(Args...), Key>::GeneralizedFactory(
    const GeneralizedFactory<Ret(Args...), Key>& other) :
    m_Registry{}, m_InverseRegistry{}, m_RegistryAccess{}
{
    // DEV NOTE Only the other registry must be blocked as we are still
    // constructing this class
    std::shared_lock access{ other.m_RegistryAccess };
    this->m_Registry = other.m_Registry;
    this->m_InverseRegistry = other.m_InverseRegistry;
}

template <typename Key, typename Ret, typename... Args>
inline GeneralizedFactory<Ret(Args...), Key>::GeneralizedFactory(
    GeneralizedFactory<Ret(Args...), Key>&& other) :
    GeneralizedFactory<Ret(Args...), Key>{}
{
    swap(*this, other);
}

template <typename Key, typename Ret, typename... Args>
inline GeneralizedFactory<Ret(Args...), Key>&
GeneralizedFactory<Ret(Args...), Key>::operator=(GeneralizedFactory<Ret(Args...), Key> other)
{
    // DEVNOTE Using copy and swap to define both assignments The copy is
    // done in the function parameters. This provides a strong exception
    // guarantee for the copy
    swap(*this, other);
    return *this;
}

template <typename Key, typename Ret, typename... Args>
inline std::string
GeneralizedFactory<Ret(Args...), Key>::UniqueFactoryId() noexcept
{
    return boost::core::demangle(typeid(self_t).name());
}

template <typename Key, typename Ret, typename... Args>
inline std::string
GeneralizedFactory<Ret(Args...), Key>::GetUniqueFactoryId() const noexcept
{
    return self_t::UniqueFactoryId();
}

template <typename Key, typename Ret, typename... Args>
inline typename GeneralizedFactory<Ret(Args...), Key>::size_type
GeneralizedFactory<Ret(Args...), Key>::CountRegistrations(const std::type_index& id) const noexcept
{
    std::shared_lock Access{ m_RegistryAccess };
    return m_InverseRegistry.count(id);
}

template <typename Key, typename Ret, typename... Args>
inline typename GeneralizedFactory<Ret(Args...), Key>::size_type
GeneralizedFactory<Ret(Args...), Key>::CountRegistrations(const Key& key) const noexcept
{
    std::shared_lock Access{ m_RegistryAccess };
    return m_Registry.count(key);
}

template <typename Key, typename Ret, typename... Args>
inline std::optional<Key>
GeneralizedFactory<Ret(Args...), Key>::FindRegistrationKey(const std::type_index& id) const noexcept
{
    std::shared_lock access{ m_RegistryAccess };
    if (auto itIndex = m_InverseRegistry.find(id); itIndex == m_InverseRegistry.end())
    {
        return std::optional<key_type>{};
    }
    else
    {
        return std::optional<key_type>{ itIndex->second };
    }
}

template <typename Key, typename Ret, typename... Args>
inline bool
GeneralizedFactory<Ret(Args...), Key>::RegisterImplementation(
    const constructor_type& ClassCreator, const key_type& RegistrationName, std::type_index id)
{
    std::unique_lock access{ m_RegistryAccess };
    if (m_Registry.count(RegistrationName) || m_InverseRegistry.count(id))
    {
        return false;
    }

    auto InsertResult = m_Registry.insert(std::make_pair(RegistrationName, ClassCreator));
    if (InsertResult.second)
    {
        m_InverseRegistry.insert(std::make_pair(id, RegistrationName));
    }
    return InsertResult.second;
}

template <typename Key, typename Ret, typename... Args>
inline bool
GeneralizedFactory<Ret(Args...), Key>::UnregisterImplementation(
    const key_type& RegistrationName) noexcept
{
    std::unique_lock access{ m_RegistryAccess };

    auto location = m_Registry.find(RegistrationName);
    if (location != m_Registry.end())
    {
        auto itInverse = std::find_if(m_InverseRegistry.begin(), m_InverseRegistry.end(),
            [&](const std::pair<std::type_index, std::string>& P) {
                return P.second == RegistrationName;
            });
        assert(itInverse != m_InverseRegistry.end());

        m_Registry.erase(location);
        m_InverseRegistry.erase(itInverse);
        return true;
    }
    else
    {
        return false;
    }
}

template <typename Key, typename Ret, typename... Args>
inline bool
GeneralizedFactory<Ret(Args...), Key>::UnregisterImplementation(const std::type_index& id)
{
    std::unique_lock access{ m_RegistryAccess };
    auto itType = m_InverseRegistry.find(id);
    if (itType != m_InverseRegistry.end())
    {
        auto itName = std::find_if(
            m_Registry.begin(), m_Registry.end(), [&](typename Registry::const_reference& P) {
                // Locate the matching registration in the register
                return P.first == itType->second;
            });

        // DEV NOTE This should be a logic error. The class is designed so
        // that the two registry are always in sync. This is the only
        // exceptional scenario to be dealt with. Adding a dependency to boost,
        // the logger and to the common exceptions it's going to mess everything
        // up for little gain, especially considering that this is a template
        // which can potentially be used in other libraries and adding a
        // reference to the compiled exceptions of the framework library will
        // require linking, ABI compatibilioty checks and introduce all the
        // compatibility problems which are typically involved in that. At most
        // I could use the std logic_error which may introduce compatibility
        // problems only if the ABI of the std library is changed which is much
        // less probable
        assert(itName != m_Registry.end());

        m_InverseRegistry.erase(itType);
        m_Registry.erase(itName);
        return true;
    }
    else
    {
        return false;
    }
}

template <typename Key, typename Ret, typename... Args>
inline typename GeneralizedFactory<Ret(Args...), Key>::produced_type
GeneralizedFactory<Ret(Args...), Key>::CreateObject(const key_type& K, Args&&... args) const
{
    produced_type result{};
    std::shared_lock access{ m_RegistryAccess };

    auto location = m_Registry.find(K);
    if (location != m_Registry.end())
    {
        result = location->second(std::forward<Args>(args)...);
    }

    return result;
}

template <typename Key, typename Ret, typename... Args>
template <class T>
inline typename GeneralizedFactory<Ret(Args...), Key>::const_iterator
GeneralizedFactory<Ret(Args...), Key>::FindRegistration() const noexcept
{
    std::shared_lock access{ m_RegistryAccess };
    auto itIndex = m_InverseRegistry.find(typeid(T));
    if (itIndex == m_InverseRegistry.end())
    {
        return this->end();
    }
    else
    {
        return m_Registry.find(itIndex->second);
    }
}

template <typename Key, typename Ret, typename... Args>
inline typename GeneralizedFactory<Ret(Args...), Key>::const_iterator
GeneralizedFactory<Ret(Args...), Key>::FindRegistration(
    const typename std::pointer_traits<produced_type>::element_type& Obj) const noexcept
{
    std::shared_lock access{ m_RegistryAccess };
    auto itIndex = m_InverseRegistry.find(typeid(Obj));
    if (itIndex == m_InverseRegistry.end())
    {
        return this->end();
    }
    else
    {
        return m_Registry.find(itIndex->second);
    }
}

template <typename Key, typename Ret, typename... Args>
inline std::unique_ptr<IFactory<Key>>
GeneralizedFactory<Ret(Args...), Key>::Clone() const
{
    return std::make_unique<GeneralizedFactory<Ret(Args...), Key>>(*this);
}

template <typename Key, typename Ret, typename... Args>
inline std::vector<Key>
GeneralizedFactory<Ret(Args...), Key>::Merge(const GeneralizedFactory<Ret(Args...), Key>& other)
{
    std::vector<Key> ans{};

    if (this == &other)
        return ans;

    ans.reserve(other.size());

    std::unique_lock WriteAccess{ m_RegistryAccess, std::defer_lock };
    std::shared_lock ReadAccess{ other.m_RegistryAccess, std::defer_lock };
    std::scoped_lock LockBoth{ WriteAccess, ReadAccess };

    // I'm going to do two rounds to make sure that the two containers
    // can be succesfully merged before changing any of them to guarantee
    // strongly in case of exceptions
    std::vector<typename InverseRegistry::const_iterator> ItemsToMerge;
    auto OtherInvIndex = other.m_InverseRegistry.cbegin();
    for (; OtherInvIndex != other.m_InverseRegistry.cend(); ++OtherInvIndex)
    {
        auto& KeyVal = *OtherInvIndex;
        if (auto InvIndex = this->m_InverseRegistry.find(KeyVal.first);
            InvIndex == m_InverseRegistry.end())
        {
            // there is no implementation registered under the same index
            // there could be the possibility that another class is registered
            // under the name for which this implementation is registered in
            // other In this case I cannot merge has it would be a cascading
            // effect which cannot be controlled automatically. I can throw a
            // runtime_error in this case
            if (m_Registry.count(KeyVal.second) > 0)
            {
                std::runtime_error e{ "Conflicting implementation naming in the "
                                      "source and in the sink. Unable to merge the "
                                      "factories" };
                throw e;
            }
            ItemsToMerge.push_back(OtherInvIndex);
        }
    }

    // Now I'm sure I can correctly merge all the items which are marked in the
    // vector
    for (auto& it : ItemsToMerge)
    {
        // This is the ref to the pair stored in the inverse registry of "other"
        auto& KeyVal = *it;

        // This is the ref to the pair stored in the direct registry of "other"
        // This must exist if the two registry are correctly synced
        auto& OtherRecord = *(other.m_Registry.find(KeyVal.second));

        // Now I copy them in the registry of this.
        // I'm already sure that I want to insert them so I can use the direct
        // access operator to do the assignment
        m_InverseRegistry[KeyVal.first] = KeyVal.second;
        m_Registry[OtherRecord.first] = OtherRecord.second;
        ans.push_back(KeyVal.second);
    }
    return ans;
}
} // namespace MXWare::Extensions

#endif /*MXWARE_EXTENSIONS_GENERALIZEDFACTORY_HH_*/
