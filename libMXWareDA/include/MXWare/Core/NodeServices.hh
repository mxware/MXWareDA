/*
 * NodeServices.hh
 *
 * Defines the node services
 *
 *  Created on: Nov 7, 2019
 *      Author: caiazza
 */

#ifndef MXWARE_CORE_NODESERVICES_HH_
#define MXWARE_CORE_NODESERVICES_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Core/MXCourier.hh"
#include "MXWare/Exceptions.hh"

#include "MXWare/Utilities/propagate_const.hh"

#include <memory>
#include <string>

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(push)
#pragma warning(disable : 4251)

#endif

namespace MXWare
{
namespace Core
{
namespace detail
{
/// Forward declaration of the implementation class on which the node client is based
// DEVNOTE This allows to keep the implementation segregated from the interface.
// It is not declared as a private member of the class so that it can be used internally
// by the other implementations
class NodeClient_Impl;
} // namespace detail

MXWARE_DECLARE_EXCEPTION(MXAPI, NodeConnectionException, GeneralException)

/// This class represents the interface to a single remote node
class MXAPI NodeClient
{
public:
    /// Builds a new Node Client connecting it to the MXCourier service
    /// The MXCourier is distributed as a shared pointer to make sure it
    /// is not destroyed if there are some of the subservices depending on
    /// it that are still alive
    NodeClient(std::shared_ptr<MXCourier> Context);

    /// Destructor (non polymorphic)
    ~NodeClient();

    /// Copies the connection handle to the remote node
    NodeClient(const NodeClient& other);

    /// Swap function
    friend MXAPI void
    swap(NodeClient& lhs, NodeClient& rhs) noexcept;

    /// Move constructor
    NodeClient(NodeClient&& other);

    /// Assignment operator
    NodeClient&
    operator=(NodeClient other);

    /// Attempts to connect to the provided endpoint where to locate the remote node
    /// The endpoint syntax MUST be of the form <transport>://<transport dependent URL>
    bool
    Connect(const std::string& Endpoint);

    /// Retrieves the state of the connected node.
    /// It returns an exception if the client is not connected
    //    INode::State
    //    GetState() const;

private:
    /// Implementation class
    class Impl;
    /// Pointer to the implementation
    std::experimental::propagate_const<std::unique_ptr<Impl>> m_impl;
};

class MXAPI NodeServer
{
public:
};

} // namespace Core
} // namespace MXWare

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(pop)

#endif


#endif /* MXWARE_CORE_NODESERVICES_HH_ */
