#ifndef MXWARE_CORE_MXCOURIER_HH
#define MXWARE_CORE_MXCOURIER_HH
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Utilities/ComponentVersion.hh"
#include "MXWare/Exceptions.hh"
#include "MXWare/Log.hh"
#include "MXWare/Signals/Connection.hh"
#include <memory>
#include <string>
#include <future>
#include <vector>
#include <functional>
#include <fmt/format.h>

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(push)
#pragma warning(disable : 4251)

#endif

namespace MXWare
{
namespace Core
{
/// Generic exception of the Courier service
MXWARE_DECLARE_EXCEPTION(MXAPI, MXCourierException, GeneralException)

namespace Courier
{
/// Version of the Courier protocol
constexpr ComponentVersion ProtocolVersion{ 1, 0, 0 };

/// The provided sockect is invalid
MXWARE_DECLARE_EXCEPTION(MXAPI, InvalidSocket, MXCourierException)
/// The endpoint supplied for the connection is not valid
MXWARE_DECLARE_EXCEPTION(MXAPI, InvalidEndpoint, MXCourierException)
/// The desired endpoint is not connected
MXWARE_DECLARE_EXCEPTION(MXAPI, EndpointNotConnected, MXCourierException)
/// The requested protocol is not supported
MXWARE_DECLARE_EXCEPTION(MXAPI, UnsupportedProtocol, MXCourierException)
/// The requested protocol is not compatible with the socket used
MXWARE_DECLARE_EXCEPTION(MXAPI, IncompatibleProtocol, MXCourierException)
/// The requested address is already in use
MXWARE_DECLARE_EXCEPTION(MXAPI, AddressInUse, MXCourierException)
/// The requested address is not available
MXWARE_DECLARE_EXCEPTION(MXAPI, AddressNotAvailable, MXCourierException)
/// The requested address specifies a non existent interface
MXWARE_DECLARE_EXCEPTION(MXAPI, NoDevices, MXCourierException)
/// The io service associated with the socket was terminated
MXWARE_DECLARE_EXCEPTION(MXAPI, IOServiceTerminated, MXCourierException)
/// The io service used is not valid
MXWARE_DECLARE_EXCEPTION(MXAPI, IOServiceInvalid, MXCourierException)
/// No I/O thread available for the task
MXWARE_DECLARE_EXCEPTION(MXAPI, MissingThread, MXCourierException)
/// The maximum amount of sockets supported by the system has been reached
MXWARE_DECLARE_EXCEPTION(MXAPI, MaxSupportedSockets, MXCourierException)
/// Unable to decode a received message in what it was expected
MXWARE_DECLARE_EXCEPTION(MXAPI, MessageDecodingError, MXCourierException)
/// The socket was disconnected before it could fulfill the request
MXWARE_DECLARE_EXCEPTION(MXAPI, SocketDisconnected, InvalidSocket)

/// Definition of the payload
using RawPayload = std::vector<char>;

/// Definition of the object representing a message identifier
using MessageId = uint64_t;

/// An object collecting the information required to issue a request to the client
/// A request contains a category which must be non-empty and a data represented
/// by a generic byte buffer. The Courier will not modify the payload
// TODO Additional optional fields: retry policy, max retries, timeout
// DEVNOTE Having a poly destructor allows inheritance but than I will always have to
// pass the request by ptr or ref and build it on the heap. I'd rather have a fixed
// structure for the request which allows to pass by value and build on the stack
// and have functions which create specialized type of requests with different
// hardcoded parameters. Those subclasses just return a request object
class MXAPI Request
{
public:
    /// Empty request
    Request();

    /// Creates a request without the optional payload
    Request(const std::string& Category);

    /// Issues a request with a given category and payload
    Request(const std::string& Category, RawPayload&& Payload);

    /// Issues a request receiving the payload as a string
    Request(const std::string& Category, const std::string& Payload);

    /// Destructor
    ~Request() = default;

    ///@{
    /// Copy, move assign through copy and swap
    Request(const Request& other) = default;

    friend void
    swap(Request& lhs, Request& rhs) noexcept;

    Request(Request&& other);

    Request&
    operator=(Request other);
    ///@}

    ///@{
    /// Checks if the request is valid,
    /// A valid request has a non empty category
    /// The payload is optional
    bool
    Valid() const;

    inline operator bool() const { return Valid(); }
    ///@}

    ///@{
    /// Setters and getters
    /// The setters enable the named parameter idiom by returning a reference to the
    /// object after theparameter has been set
    inline Request&
    SetCategory(const std::string& C) noexcept
    {
        m_Category = C;
        return *this;
    }

    inline const std::string&
    GetCategory() const noexcept
    {
        return m_Category;
    }

    inline Request&
    SetPayload(RawPayload&& P)
    {
        m_Payload = std::move(P);
        return *this;
    }

    inline Request&
    SetPayload(const std::string& P)
    {
        m_Payload.clear();
        m_Payload.reserve(P.size());
        m_Payload.assign(P.begin(), P.end());
        return *this;
    }

    inline const RawPayload&
    GetPayload() const
    {
        return m_Payload;
    }
    ///@}

private:
    /// The request category
    std::string m_Category;

    /// The payload of the request
    RawPayload m_Payload;
};

/// Generates the string a client should send to identify a request
inline std::string
GenerateRequestHeader(const Request& R, const MessageId& Id)
{
    return fmt::format(
        "MXP {} {} REQ {} {}", ProtocolVersion.Major, ProtocolVersion.Minor, Id, R.GetCategory());
}

/// An object storing the information for a Client reply.
/// Each reply should contain a string representing the generic category
/// and an optional payload
class MXAPI Reply
{
public:
    /// Empty reply
    Reply();

    /// Creates a reply without the optional payload
    Reply(const std::string& Category);

    /// Issues a reply with a given category and payload
    Reply(const std::string& Category, RawPayload&& Payload);

    /// Destructor
    ~Reply() = default;

    ///@{
    /// Copy, move assign through copy and swap
    Reply(const Reply& other) = default;

    friend void
    swap(Reply& lhs, Reply& rhs) noexcept;

    Reply(Reply&& other);

    Reply&
    operator=(Reply other);
    ///@}

    ///@{
    /// Checks if the reply is valid,
    /// A valid reply has a non empty category
    /// The payload is optional
    bool
    Valid() const;

    inline operator bool() const { return Valid(); }
    ///@}

    ///@{
    /// Setters and getters
    /// The setters enable the named parameter idiom by returning a reference to the
    /// object after theparameter has been set
    inline Reply&
    SetCategory(const std::string& C) noexcept
    {
        m_Category = C;
        return *this;
    }

    inline const std::string&
    GetCategory() const noexcept
    {
        return m_Category;
    }

    inline Reply&
    SetPayload(RawPayload&& P)
    {
        m_Payload = std::move(P);
        return *this;
    }

    inline const RawPayload&
    GetPayload() const
    {
        return m_Payload;
    }
    ///@}

private:
    /// The reply category
    std::string m_Category;

    /// The optional payload of the reply
    RawPayload m_Payload;
};

inline std::string
GenerateReplyHeader(const Reply& R, const MessageId& Id)
{
    return fmt::format(
        "MXP {} {} REP {} {}", ProtocolVersion.Major, ProtocolVersion.Minor, Id, R.GetCategory());
}


/// A courier client is the interface to the messaging service used to send messages
/// to a matching server.
/// A client can be connected to a single endpoint at the time.
/// If a connect command is executed to connect on a different endpoint, the client
/// will disconnect from the previously open connection, if any
class Client
{
public:
    /// Polymorphic constructor
    virtual ~Client() = default;

    /// Connects the client to a server bound to the internal endpoint
    /// identified by the argument string. This is to be used to connect
    /// the client with server residing in the same process
    /// If the connection fails one of the exceptions of the MXCourierException
    /// hierarchy will be thrown
    virtual void
    ConnectInternal(const std::string& Identifier) = 0;

    /// Connects to a remote endpoint at a given point.
    /// By default the destination port is 1717
    /// If the connection fails one of the exceptions of the MXCourierException
    /// hierarchy will be thrown
    virtual void
    ConnectRemote(const std::string Hostname, uint16_t Port = 1717) = 0;

    /// Checks if the client is connected to any endpoint
    virtual bool
    IsConnected() const = 0;

    /// Disconnects the connected endpoints
    virtual void
    Disconnect() = 0;

    /// Sends a request to the connected remote
    /// If the Client is not connected to any remote it will throw an exception of type
    /// EndpointNotConnected.
    /// It returns a future to the expected reply
    // TODO Devise a method to cancel or timeout a request
    virtual std::future<Reply>
    SendRequest(Request&& Req) = 0;
};

/// A courier server is the receiving interface of the messaging system of the courier network
/// Each server can be bound to several endpoints at the same time and listen from all
/// of them.
class Server
{
public:
    /// Polymorphic destructor
    virtual ~Server() = default;

    /// Binds the server to an internal endpoint identified by a string.
    /// This is to be used to connect the server to clients which reside
    /// in the same process
    /// If the binding fails the function SHOULD throw one of the exceptions
    /// derived from MXCourierException
    virtual bool
    BindInternal(const std::string& Identifier) = 0;

    /// Opens the port specified by the @par Port on the network interface
    /// @par interface and starts listening
    virtual bool
    BindNetworkInterface(uint16_t Port = 1717, const std::string& Interface = "*") = 0;

    /// Retrieves the number of endpoints to which the server is currently bound
    virtual std::size_t
    GetNBoundEndpoints() const noexcept = 0;

    /// Disconnects the server from all the endpoints to which is connected
    virtual void
    UnbindAll() = 0;

    /// Associate a function that processes a request into a reply to a message category
    /// The returned object is necessary to signal if the connected function is not available
    /// anymore. Therefore the caller should keep that object alive and disconnect or let it go
    /// out of scope when it's not going to process requests anymore.
    /// Each category must be connected to a single processing function. If a category was
    /// previously associated to another function, the previous one will be disconnected
    /// Note that through the Signals::Connection object it os possible to ascertain whether
    /// a connection is still active
    virtual Signals::Connection
    ProcessRequestCategory(const std::string& Cat, std::function<Reply(const Request&)> F) = 0;
};

} // namespace Courier


class MXCourier
{
public:
    /// Polymorphic destructor
    virtual ~MXCourier() = default;

    /// Creates a new client connected to this Courier service
    virtual std::unique_ptr<Courier::Client>
    CreateClient() = 0;

    /// Creates a new server connected to this Courier service
    virtual std::unique_ptr<Courier::Server>
    CreateServer() = 0;

    /// Replaces the existing logging service with a new one
    /// Note that this will not modify any existing logger created
    /// through the previous logging service.
    /// The input parameter should point to a valid Logging Service
    /// If a logger with the default name for the default courier logger
    /// is already stored inside the service the current DefaultLogger
    /// is replaced with the one stored in the service.
    /// If the new logging service does not contain a logger with the default name
    /// the current default logger is registered in the new logging service
    // virtual void SetLoggingService(std::shared_ptr<Log::LoggingService>) = 0;

    /// Obtains a shared reference to the default logger of the courier service
    /// The logger is stored in a shared_ptr, therefore it will be available
    /// even after the LoggingService and the Courier has been deactivated
    /// The default logger may be created the first time this function is called
    /// or it may already be available
    virtual Log::Logger&
    Log() = 0;

    /// Configures the courier using a configuration tree
    virtual void
    Configure(const Configuration::PropertyTree& Tree) = 0;

    /// Retrieves the current status of the courier
    virtual Configuration::PropertyTree
    CurrentConfiguration() const = 0;

    /// String defining the name of the default courier logger
    static constexpr auto DefaultLoggerName = "courier" ;

    /// The default name for the node where to find the courier configuration
    static constexpr auto s_CourierService_NN = "courier";

    ///@{
    /// Factory function to create the default Courier implementation
    /// The service is returned as a shared_ptr because this should be
    /// shared/shareable between the different socket it creates to avoid
    /// killing the service if any user still needs it
    /// This function provides two overloads, one which allows to provide a
    /// custom, already initialized logging service
    MXAPI static std::shared_ptr<MXCourier>
    NewCourier();
    ///@}
};

} // namespace Core

} // namespace MXWare

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(pop)

#endif

#endif /* MXWARE_CORE_MXCOURIER_HH */
