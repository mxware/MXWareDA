#ifndef MXWARE_SIGNALS_CONNECTION_HH_
#define MXWARE_SIGNALS_CONNECTION_HH_
#pragma once

#include <boost/signals2/connection.hpp>
#include <boost/signals2/shared_connection_block.hpp>

namespace MXWare::Signals
{
class ConnectionBlock;

/// The class managing the connection between a signal and a slot.
/// The connection will be active as long as the Connection object is in scope.
/// Because this class automatically manages the underlying connection and disconnects the
/// connection on destruction it cannot be copied as there would be two conflicting objects
/// managing the same connection.
/// It is based on the boost scoped connection which has the same features
class Connection
{
    using base_connection = boost::signals2::scoped_connection;

public:
    /// Default construction
    Connection() : m_Conn{} {}

    Connection(base_connection&& c) : m_Conn{ std::move(c) } {}

    ~Connection() = default;

    Connection(const Connection& other) = delete;

    Connection&
    operator=(const Connection& other) = delete;

    Connection(Connection&& other) = default;

    Connection&
    operator=(Connection&& other) = default;

    /// Manually disconnects the managed connection
    void
    Disconnect() const noexcept
    {
        m_Conn.disconnect();
    }

    /// Checks whether the connection is active
    bool
    Connected() const noexcept
    {
        return m_Conn.connected();
    }

    /// Creates a signal connection block which will keep the connection blocked
    /// until the returned object is in scope
    /// NOTE Needs to be implemented out of line to avoid circular dependencies
    inline ConnectionBlock
    GetBlock() const;

    /// Checks if the connection was blocked
    bool
    Blocked() const noexcept
    {
        return m_Conn.blocked();
    }

private:
    base_connection m_Conn;

    friend class ConnectionBlock;
};

/// This class is used to block the connection between a signal and its slot.
/// It is useful to break iterative infinite loops where a signal would attivate a slot
/// that calls the signal again
class ConnectionBlock
{
    using base_block = boost::signals2::shared_connection_block;

public:
    ConnectionBlock(const Connection& Conn = Connection{}) :
        m_Block{ Conn.m_Conn, true }
    {}

    /// Blocks a signal connection
    void
    Block()
    {
        m_Block.block();
    }

    /// Unblocks a signal connection
    void
    Unblock()
    {
        m_Block.unblock();
    }

    /// Queries whether the connection is blocking
    bool
    Blocking()
    {
        return m_Block.blocking();
    }

private:
    base_block m_Block;
};


inline ConnectionBlock
Connection::GetBlock() const
{
    return ConnectionBlock(*this);
}
}

#endif //MXWARE_SIGNALS_CONNECTION_HH_