#ifndef MXWARE_SIGNALS_SIGNAL_HH_
#define MXWARE_SIGNALS_SIGNAL_HH_
#pragma once

#include <boost/signals2.hpp>
#include "MXWare/Signals/Connection.hh"

namespace MXWare::Signals
{
/// Forward declared template signature
template <typename Signature,
    typename Combiner = boost::signals2::optional_last_value<
        typename boost::function_traits<Signature>::result_type>,
    typename Group = int, typename GroupCompare = std::less<Group>>
class Signal;

/// The class to manage the signaling between different components of the MXWare framework
/// This is currently implemented through the boost signal2 library
/// A signal is a sequential container of callbacks (slots) which are attached to the Signal using
/// its "connect" function. When the owner of the signal wants to trigger the slots he will use the
/// "emit" function This signal class supports only the basic functionalities of the signal2 library
template <typename Combiner, typename Group, typename GroupCompare, typename Ret, typename... Args>
class Signal<Ret(Args...), Combiner, Group, GroupCompare>
{
    using base_signal = boost::signals2::signal<Ret(Args...), Combiner>;

public:
    using slot_type = typename base_signal::slot_type;

    using result_type = typename base_signal::result_type;

    using SlotPosition = boost::signals2::connect_position;

    ///@{
    /// The gang of five
    /// Copy is not allowed for signals or it will result in a duplication of the
    /// signals emitted
    Signal() = default;

    ~Signal() = default;

    Signal(const Signal& other) = delete;

    Signal&
    operator=(const Signal& other) = delete;

    Signal(Signal&& other) = default;

    Signal&
    operator=(Signal&& other) = default;
    ///@}

    /// Connects a new slot to the signal.
    /// Returns a Connection object which provides a RAII implementation of the connection
    /// between signal and slots avoiding a connection to become orphan if the observer forgets
    /// to disconnect
    Connection
    connect(const slot_type& S)
    {
        return Connection{ m_Sig.connect(S, boost::signals2::connect_position::at_back) };
    }

    /// Connects a new slot to the beginning of the slot list
    Connection
    connect_front(const slot_type& S)
    {
        return Connection{ m_Sig.connect(S, boost::signals2::connect_position::at_front) };
    }

    ///@{
    /// Emits the signal, calling executing all the connected slots
    result_type
    emit(Args... args)
    {
        return m_Sig(std::forward<Args>(args)...);
    }

    result_type
    emit(Args... args) const
    {
        return m_Sig(std::forward<Args>(args)...);
    }
    ///@}

    /// returns the number of slots connected to this signal
    /// The Boost implementation is no-throw guarantee
    std::size_t
    NSlots() const noexcept
    {
        return m_Sig.num_slots();
    }

    /// Returns whether there are any connected slots
    /// The Boost implementation is no-throw guarantee
    bool
    empty() const noexcept
    {
        return m_Sig.empty();
    }


private:
    /// The signal object managed by the class
    base_signal m_Sig;
};

}

#endif //MXWARE_SIGNALS_SIGNAL_HH_