#ifndef MXWARE_DATA_IMXDATA_HH_
#define MXWARE_DATA_IMXDATA_HH_
#pragma once

#include "MXWare/Utilities/PolymorphicConstructible.hh"
#include "MXWare/Utilities/Cloneable.hh"
#include "MXWare/Utilities/Comparable.hh"

namespace MXWare::Data
{
/**
 * Base class for data objects which need to be stored in containers, passed between
 * processing algorithms and serialized to a file
 * The data objects have the following  requirements:
 * * They have to be default constructible
 * * They can be copied
 * * They can be compared
 *
 * @author caiazza
 * @version Feb 18, 2015A
 */
class IMXData :
    public Utilities::Cloneable<IMXData>,
    public Utilities::DefaultConstructible<IMXData>,
    public Utilities::Comparable<IMXData>
{
public:
    /**
     * Virtual destructor
     */
    virtual ~IMXData() = default;

    ///@{
    /// Forwarding of base class methods
    using Utilities::DefaultConstructible<IMXData>::Create;
    using Utilities::Cloneable<IMXData>::Clone;
    ///@}
};

/// A simple wrapper around a generic object which complies with the three requirements of the
/// IMXData type This allows to use any compatible object within the MXWare data hierarchy, for
/// messaging and so on The wrapped type must be a concrete object, stored by value in the wrapper
template <typename T>
class MXDataWrapper : public IMXData
{
    /// Alias to self
    using self_t = MXDataWrapper<T>;

public:
    /// Default constructor, requires the wrapped object to be default constructible
    MXDataWrapper() : m_Data{ T{} } {}

    /// Setting constructor
    MXDataWrapper(const T& Data) : m_Data{ Data } {}

    /// Polymorphic destructor
    virtual ~MXDataWrapper() = default;

    ///@{
    /// Compiler defined copy and move
    MXDataWrapper(const self_t&) = default;

    self_t&
    operator=(const self_t&) = default;

    MXDataWrapper(self_t&&) = default;

    self_t&
    operator=(self_t&& other) = default;
    ///@}

    /// Conversion operator
    explicit operator T() const { return m_Data; }

    ///@{
    /// Setters and getters
    void
    SetData(const T& Data)
    {
        m_Data = Data;
    }

    const T&
    GetData() const
    {
        return m_Data;
    }

    T&
    Data()
    {
        return m_Data;
    }
    ///@}


private:
    /// The wrapped data
    T m_Data;

    /// Polymorphic copy
    IMXData*
    RawClone() const override
    {
        return new self_t{ m_Data };
    }

    /// Polymorphic default construction
    IMXData*
    RawCreate() const override
    {
        return new self_t{};
    }

    ///@{
    /// Static comparison
    friend bool
    operator==(const self_t& lhs, const self_t& rhs)
    {
        return lhs.m_Data == rhs.m_Data;
    }

    friend bool
    operator!=(const self_t& lhs, const self_t& rhs)
    {
        return !(lhs == rhs);
    }
    ///@}

    /// Polymorphic comparison
    bool
    IsEqual(const compare_base& rhs) const override
    {
        return *this == static_cast<const self_t&>(rhs);
    }
};

/// An helper class based on the CRTP pattern to implement the IMXData interface in
/// a generic Derived class
template <typename Derived>
class MXDataHelper : public IMXData
{
public:
    virtual ~MXDataHelper() = default;

private:
    /// Polymorphic copy
    IMXData*
    RawClone() const override
    {
        return new Derived{ static_cast<const Derived&>(*this) };
    }

    /// Polymorphic defaul construction
    IMXData*
    RawCreate() const override
    {
        return new Derived{};
    }

    bool
    IsEqual(const compare_base& rhs) const override
    {
        return static_cast<const Derived&>(*this) == static_cast<const Derived&>(rhs);
    }
};

} // namespace MXWare


#endif /*MXWARE_DATA_IMXDATA_HH_*/
