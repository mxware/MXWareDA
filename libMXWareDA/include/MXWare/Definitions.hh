/**
 * @file Config.hh
 * @brief Configuration macros and definition
 * 
 * This header imports all the configuration macros and definitions
 * from Boost which allow to recognize special features in the build environment
 * and modify the structure of the code accordingly. Some of those macros are
 * renamed to make them consistent within the MXWare environment
 */

#ifndef MXWARE_DEFINITIONS_HH_
#define MXWARE_DEFINITIONS_HH_
#pragma once

#include <boost/config/helper_macros.hpp>

/// Defines a common symbol to be used to export the API components of the library
#if defined _WIN32 || defined __CYGWIN__
  #ifdef BUILDING_LIBRARY
    #ifdef __GNUC__
      #define MXAPI __attribute__ ((dllexport))
    #else
      #define MXAPI __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #else
    #ifdef __GNUC__
      #define MXAPI __attribute__ ((dllimport))
    #else
      #define MXAPI __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #endif
  #define MXPRIVATE
#else
  #if __GNUC__ >= 4
    #define MXAPI __attribute__ ((visibility ("default")))
    #define MXPRIVATE  __attribute__ ((visibility ("hidden")))
  #else
    #define MXAPI
    #define MXPRIVATE
  #endif
#endif

// #define MXAPI BOOST_SYMBOL_EXPORT

/**
 * The following piece of macro magic joins the two arguments together, even when one
 * of the arguments is itself a macro (see 16.3.1 in C++ standard). 
 */
#define MXWARE_JOIN(X, Y) BOOST_DO_JOIN(X, Y)

/**
 * Converts the parameter X to a string after macro replacement
 */
#define MXWARE_STRINGIZE(X) BOOST_DO_STRINGIZE(X)


#endif /* MXWARE_DEFINITIONS_HH_ */
