#ifndef MXWARE_EXECUTION_COMMANDS_HH_
#define MXWARE_EXECUTION_COMMANDS_HH_
#pragma once

#include "MXWare/Execution/Command/StartAll.hh"
#include "MXWare/Execution/Command/StopWhenFinished.hh"

#endif // MXWARE_EXECUTION_COMMANDS_HH_