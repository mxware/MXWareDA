#ifndef MXWARE_EXECUTION_COMMAND_STARTALL_HH_
#define MXWARE_EXECUTION_COMMAND_STARTALL_HH_
#pragma once

#include "MXWare/Execution/Command/CommandBase.hh"

namespace MXWare::Execution::Command
{
class MXAPI StartAll : public detail::CommandBase_CRTP<StartAll>
{
    using base_t = detail::CommandBase_CRTP<StartAll>;

public:
    using base_t::base_t;

    auto
    Execute(Node&) const -> std::unique_ptr<Courier::IReply> override;

    static auto
    TypeId()
    {
        return "StartAll";
    }
};

} // namespace MXWare::Execution::Command

#endif // MXWARE_EXECUTION_COMMAND_STARTALL_HH_