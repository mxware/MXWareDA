#ifndef MXWARE_EXECUTION_COMMAND_STOPWHENFINISHED_HH_
#define MXWARE_EXECUTION_COMMAND_STOPWHENFINISHED_HH_
#pragma once

#include "MXWare/Execution/Command/CommandBase.hh"

namespace MXWare::Execution::Command
{
class MXAPI StopWhenFinished : public detail::CommandBase_CRTP<StopWhenFinished>
{
    using base_t = detail::CommandBase_CRTP<StopWhenFinished>;

    public:
    using base_t::base_t;

    auto
    Execute(Node&) const -> std::unique_ptr<Courier::IReply> override;

    static auto
    TypeId()
    {
        return "StopWhenFinished";
    }
};
} // namespace MXWare::Execution::Command


#endif // MXWARE_EXECUTION_COMMAND_STOPWHENFINISHED_HH_
