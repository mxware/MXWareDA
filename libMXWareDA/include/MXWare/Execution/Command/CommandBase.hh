#ifndef MXWARE_EXECUTION_COMMAND_COMMANDBASE_HH_
#define MXWARE_EXECUTION_COMMAND_COMMANDBASE_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Execution/INodeCommand.hh"
#include "MXWare/Random/RandomString.hh"
#include <string_view>

namespace MXWare::Execution::Command::detail
{
/// A base class to define the common functions in all commands
class MXAPI CommandBase : public INodeCommand
{
public:
    /// Creates the Command message with a random string
    CommandBase() : m_Id{Random::GenerateRandomString(16)} {}

    /// Creates the Command message with a custom string
    CommandBase(std::string_view Id) : m_Id{ Id } {}

    const std::string&
    GetId() const noexcept final
    {
        return m_Id;
    }
    
    void
    Configure(const Configuration::PropertyTree& ConfTree) override;

    Configuration::PropertyTree
    CurrentConfiguration() const override;

    Configuration::PropertyTree
    GetBody() const override;

private:
    std::string m_Id;
};

/// CRTP base class for the commands providing function definitions
/// depending on the derived class itself
template <class Derived>
class CommandBase_CRTP : public CommandBase
{
public:
    /// Inheriting constructors
    using CommandBase::CommandBase;

    std::string
    GetTypeId() const noexcept override
    {
        return std::string{ Derived::TypeId() };
    }
};

} // namespace MXWare::Execution::Command::detail

#endif // MXWARE_EXECUTION_COMMAND_COMMANDBASE_HH_