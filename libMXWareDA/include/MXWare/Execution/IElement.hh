/*
 * IElement.hh
 *
 *  Created on: Oct 29, 2019
 *      Author: caiazza
 */

#ifndef MXWARE_EXECUTION_IELEMENT_HH_
#define MXWARE_EXECUTION_IELEMENT_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Configuration.hh"
#include "MXWare/Utilities/PolymorphicConstructible.hh"

#include <stdint.h>
#include <string>
#include <memory>

namespace MXWare::Execution
{
class Node;

/// The common interface to all the processing element of the system
/// MXWare elements are named objects that allow them to be identified if many of the same type
/// are defined in a single program.
/// All elements should be default constructible to a state where no resource is yet acquired
/// The elements behave like state machines with these states:
/// * LOADED: This is the starting state of the FSM. No resource SHALL be acquired in this
///     state if that can be done later
/// * READY: All the resources necessary for the operation of the element MUST be acquired in this
///     state so that the element is ready to start the required task
/// * RUNNING: The specific task of the element is in execution. The configuration SHOULD NOT be
///     changed while in this state. The configuration before and after the run SHOULD be the same
/// * SUSPENDED: The task is suspended but all the resources acquired, even the temporary ones,
///     should be kept
/// * ERROR: a non recoverable error happened. The element must be disabled and reenabled to
///     be reused.
///
/// An element MUST support the following transitions:
/// * Enable: which acquires all the resources the object will use in its lifetime like,
///     for example, memory, sockets or other connections, files and so on.
///     If a data payload is provided to this event it SHOULD be used to configure the
///     element in a way compatible to the call of the configure transition on a READY element
/// * Configure: Applies a custom configuration specified in the payload to the current element
/// * Start: Start executing the specific task of the element using the configured option.The
/// additional
///     payload MAY be used to apply additional configuration which are valid for this
///     execution run alone so that, at the end of the execution the configuration
///     SHOULD be the same as before the execution
/// * Pause: which temporarily stops the execution while keeping the current operational status.
///     Keeps all the resources allocated so that the task can be continued as nothing happened
/// * Continue: Continue the execution from a SUSPENDED state
/// * Stop: Interrupts the task as soon as possible cycling the element back to its
///     READY state.
/// * Reset: Applies a default configuration to the current element. This SHOULD be
///     equivalent to the state of the node after being enabled. This transition
///     does not allow a payload as it should always bring the element to the same configuration
/// * Disable: Deallocate all the resources acquired by the element. This transition
///     SHALL never fail. This transition does not allow any payload
/// Each transition returns the state of the node at the end of the transition so
/// that the caller can test whether the transition was successful. In the case of
/// the Configure and Ready transition which go back to the the same state, a failure
/// MUST transition to an ERROR state which MAY store the data necessary to debug the
/// error
class MXAPI IElement : public Utilities::PolymorphicConstructible<IElement, Node&>
{
public:
    /// An enumeration listing the available states of the Element FSM
    // DEVNOTE The names should not be all Uppercase to avoid collisions
    // with some window macro. In particular ERROR is defined as a macro in
    // windows.h which will mess up everything
    enum class State : uint32_t
    {
        Loaded = 0,
        Ready,
        Running,
        Suspended,
        Error
    };

    // TODO Implement State comparison

    /// Polymorphic destructor
    virtual ~IElement() = default;

    /// Execute the enable transition
    virtual State
    Enable() = 0;

    /// Execute the configure transition
    /// If the element is still in the loaded state this should be equivalent to a call
    /// to the Enable transition followed by the Configure from the ready state
    /// The @param Data object should be used to configure the node
    virtual State
    Configure(const Configuration::PropertyTree& Data = Configuration::PropertyTree{}) = 0;

    /// Execute the start transition
    /// The @param Data object should be used to provide additional data necessary
    /// to the operation of the element in this particular run but SHOULD NOT change the
    /// configuration of the node at the end of the run
    virtual State
    Start(const Configuration::PropertyTree& Data = Configuration::PropertyTree{}) = 0;

    /// Execute the pause transition
    virtual State
    Pause() = 0;

    /// Executes the continue transition
    virtual State
    Continue() = 0;

    /// Executes the stop transition
    /// The @param Data object should be used to provide additional data necessary
    /// which can be used in the stopping phase but SHOULD NOT change the
    /// configuration of the node at the end of the run
    virtual State
    Stop(const Configuration::PropertyTree& Data = Configuration::PropertyTree{}) = 0;

    /// Execute the reset transition
    virtual State
    Reset() = 0;

    /// Execute the disable transition
    virtual State
    Disable() noexcept = 0;

    /// Retrieves the current configuration
    virtual Configuration::PropertyTree
    CurrentConfiguration() const = 0;

    /// Retrieves the current state
    virtual State
    GetState() const = 0;

    ///@{
    /// Accessors for the Name property
    virtual const std::string&
    GetName() const = 0;

    virtual void
    SetName(const std::string& Name) = 0;
    ///@}

    ///@{
    /// Access the node to which the element is connected
    virtual const Node&
    GetNode() const = 0;

    virtual Node&
    GetNode() = 0;
    ///@}

    /// Polymorphic construction
    using Utilities::PolymorphicConstructible<IElement, Node&>::Create;

private:
    /// Polymorphic function to create a new object of the same concrete class of
    /// the one through which is called using the template arguments for the constructor
    virtual IElement*
    RawCreate(Node& N) const override = 0;
};
} // namespace MXWare::Execution


#endif /* MXWARE_EXECUTION_IELEMENT_HH_ */
