/*
 * ElementBase.hh
 *
 *  Created on: 29 Dec 2019
 *      Author: caiazza
 */

#ifndef MXWARE_EXECUTION_ELEMENTBASE_HH_
#define MXWARE_EXECUTION_ELEMENTBASE_HH_
#pragma once

#include "MXWare/Execution/IElement.hh"
#include "MXWare/Utilities/propagate_const.hh"

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(push)
#pragma warning(disable : 4251)

#endif

namespace MXWare::Configuration
{
class SmartParameterList;
}

namespace MXWare::Execution
{
namespace detail
{
/// The base class defining the algorithms common to all the elements
///
class MXAPI ElementBase_Complete : public IElement
{
protected:
    ///@{
    /// The gang of five
    /// The class is not copyable because the signals cannot be copied and
    /// because an element represents an execution task that typically cannot
    /// be copied.
    /// Nonetheless it is possible to copy the element configuration and reconfigure
    /// an element of the same type to achieve a similar effect
    ElementBase_Complete(Node& N);

    virtual ~ElementBase_Complete() noexcept;

    ElementBase_Complete(const ElementBase_Complete& other) = delete;

    ElementBase_Complete&
    operator=(const ElementBase_Complete& other) = delete;

    ElementBase_Complete(ElementBase_Complete&& other) noexcept;

    ElementBase_Complete&
    operator=(ElementBase_Complete&& other) noexcept;

public:
    friend MXAPI void
    swap(ElementBase_Complete& lhs, ElementBase_Complete& rhs) noexcept;
    ///@}

    /// Execute the enable transition
    State
    Enable() final;

    /// Execute the configure transition
    /// If the element is still in the loaded state this should be equivalent to a call
    /// to the Enable transition followed by the Configure from the ready state
    /// The @param Data object should be used to configure the node
    State
    Configure(const Configuration::PropertyTree& Data = Configuration::PropertyTree{}) final;

    /// Execute the start transition
    /// The @param Data object should be used to provide additional data necessary
    /// to the operation of the element in this particular run but SHOULD NOT change the
    /// configuration of the node at the end of the run
    State
    Start(const Configuration::PropertyTree& Data = Configuration::PropertyTree{}) final;

    /// Execute the pause transition
    State
    Pause() final;

    /// Executes the continue transition
    State
    Continue() final;

    /// Executes the stop transition
    /// The @param Data object should be used to provide additional data necessary
    /// which can be used in the stopping phase but SHOULD NOT change the
    /// configuration of the node at the end of the run
    State
    Stop(const Configuration::PropertyTree& Data = Configuration::PropertyTree{}) final;

    /// Execute the reset transition
    State
    Reset() final;

    /// Execute the disable transition
    State
    Disable() noexcept final;

    /// Retrieves the current configuration
    /// This action is available only when the element is in a well defined state,
    /// therefore not during a transition
    virtual Configuration::PropertyTree
    CurrentConfiguration() const override;

    /// Retrieves the current state
    State
    GetState() const final;

    ///@{
    /// Accessors for the Name property
    const std::string&
    GetName() const final;

    void
    SetName(const std::string& Name) final;
    ///@}

    ///@{
    /// Access the node to which the element is connected
    const Node&
    GetNode() const final;

    Node&
    GetNode() final;
    ///@}

protected:
    /// The private implementation of the element
    class Impl;

    /// Pointer to the implementation
    std::experimental::propagate_const<std::unique_ptr<Impl>> m_impl;

    /// User transition function to enable the element
    /// It is guaranteed to be executed independently from any other transition,
    /// in a single thread
    /// By default it does nothing
    virtual void
    doEnable()
    {}

    /// Performs additional configuration steps, besides updating the values stored in the
    /// smart parameter list
    /// This function is guaranteed to be executed indipendently from any other transition
    /// in a single-thread
    virtual void
    doConfigure(const Configuration::PropertyTree&)
    {}

    /// Executes the starting transition.
    /// It can use the data from the input parameter to update further
    /// internal parameters which are required to process a single run, rather than
    /// the general configuration of the element. The two configuration sets should not be
    /// overlapping so that the general configuration remains the same at every run
    /// Note that this function blocks and it's followed by the execution of the ExecutionTask
    /// in a parallel thread
    virtual void
    doStart(const Configuration::PropertyTree&)
    {}

    /// Queries the base class whether the Execution of the background task
    /// should be stopped. It is ON when the element is being stopped
    bool
    ShouldStopExecution() const noexcept;

    /// Queries the base class whether the Execution of the background task
    /// should be paused. It is ON when the pause transition is requested
    bool
    ShouldPauseExecution() const noexcept;

    /// The task performed by the element executed in a parallel thread
    /// The caller should use the ShouldStopExecution and ShouldPauseExecution
    /// functions to control whether the task should be stopped/paused
    virtual void
    ExecutionTask(const Configuration::PropertyTree&)
    {}

    virtual void
    doPause()
    {}

    /// User algorithm to stop the execution of the task
    virtual void
    doStop(const Configuration::PropertyTree&)
    {}

    /// User algorithm to continue the execution of the task that was paused
    virtual void
    doContinue()
    {}

    /// User function for the operations to perform for the Reset transition
    /// This function is guaranteed to be executed indipendently from any other transition
    /// in a single-thread
    virtual void
    doReset()
    {}

    /// User function to perform the disabling operations which should release the
    /// resources acquired in the enable phase
    /// This function SHOULD NOT throw any exception
    /// It is guaranteed to be executed in a single-threaded environment, independently from
    /// other threads
    virtual void
    doDisable() noexcept
    {}

    /// Registers the configurable parameters in the parameter list
    virtual void
    RegisterParameters(Configuration::SmartParameterList& Pars) const;
};

MXAPI void
swap(ElementBase_Complete& lhs, ElementBase_Complete& rhs) noexcept;

} // namespace detail

/// The final class to be derived by the user, which uses CRTP techniques
/// to define a few functions which require the knowledge of the actual concrete
/// class. The first template parameter is the name of the concrete class which is
/// deriving this one, the second is the name of the base class which defines the
/// most common functionalities. By default this is the ElementBase_Complete class
template <class Derived, class Base = detail::ElementBase_Complete>
class ElementBase : public Base
{
protected:
    using base_t = Base;
    using crtp_base = ElementBase<Derived, Base>;

    ElementBase(Node& N) : Base{ N } {}

    virtual ~ElementBase() = default;

    ElementBase(const ElementBase<Derived, Base>& other) = delete;

    ElementBase<Derived, Base>&
    operator=(const ElementBase<Derived, Base>& other) = delete;

    ElementBase(ElementBase<Derived, Base>&& other) = default;

    ElementBase<Derived, Base>&
    operator=(ElementBase<Derived, Base>&& other) = default;

    IElement*
    RawCreate(Node& N) const override
    {
        return new Derived{ N };
    }
};

} // namespace MXWare::Execution


#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(pop)

#endif



#endif /* MXWARE_EXECUTION_ELEMENTBASE_HH_ */
