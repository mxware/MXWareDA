#ifndef MXWARE_EXECUTION_NODE_HH_
#define MXWARE_EXECUTION_NODE_HH_
#pragma once


#include "MXWare/Utilities/propagate_const.hh"
#include "MXWare/Definitions.hh"
#include "MXWare/Configuration.hh"
#include "MXWare/Signals.hh"

#include <memory>
#include <future>

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(push)
#pragma warning(disable : 4251)

#endif
namespace MXWare
{
namespace Log
{
/// Forward declaration of the logging service
class LoggingService;
} // namespace Log

namespace Core
{
/// Forward declaration of the Courier service
class MXCourier;
} // namespace Core

namespace Courier
{
class IReply;
}

namespace Execution
{
///@{ Forward declarations
class ElementList;
class INodeCommand;
///@}

/// The Node in MXWare manages all the process resources of a processing node of
/// the application.
/// A node contains a set of shareable components and a set of unique components
/// The shareable components are services that should be shared at a process level even if
/// there are other nodes or similar objects which may use them. Those services are:
/// * The logging system
/// * The Courier service
/// * The plugin manager
///
/// Other components are uniquely managed by each node and that are the element list and
/// execution control
///
/// The node contains a shareable instance of the logging service which can be shared with
/// all the other subcomponents. By default it is a  shared reference to the global logging
/// service albeit it could be set to some independent instance
class MXAPI Node
{
public:
    /// An enumeration listing the possible states of the Node FSM
    enum class State : uint32_t
    {
        Ready = 0,
        Processing
    };

    ///@{
    /// The gang of five
    /// At construction the node will initialize all the global management structure but do not
    /// open any connection
    /// The full constructor allows to initialize the node with a set of external shared services
    Node();

    Node(const std::string& Name, std::shared_ptr<Core::MXCourier>);

    ~Node();

    Node(const Node& other) = delete;

    Node&
    operator=(const Node& other) = delete;

    Node(Node&& other) noexcept;

    Node&
    operator=(Node&& other) noexcept;
    ///@}

    /// Blocking command to start the Node process
    /// It will terminate when the Stop command is issued
    void
    Start();

    /// Stops the processing in the Node.
    /// Triggers the return from the Start function
    void
    Stop();

    /// Returns the current state of the node
    State
    GetState() const noexcept;

    /// Adds a command at the end of the command queue
    std::future<std::unique_ptr<Courier::IReply>>
    EnqueueCommand(std::unique_ptr<INodeCommand> Command);

    /// Configures the current node using a configuration tree
    void
    Configure(const Configuration::PropertyTree& Tree);

    /// Retrieves the current status of the node
    Configuration::PropertyTree
    CurrentConfiguration() const;

    /// Starts processing the incoming commands
    void
    StartProcessing();

    /// Stop the processing of the commands
    /// If the Force parameter is set to true, the execution control will attempt to
    /// stop the running command, otherwise it will let the command complete and not
    /// execute the next
    void
    StopProcessing() noexcept;

    /// Signals that the execution of all elements has been completed
    Signals::Connection
    WhenExecutionComplete(const Signals::Signal<void()>::slot_type& Slot);

    /// Pause the processing of the outstanding commands
    // void
    // PauseProcessing();

    // /// Continue the processing of the commands received from where it was last stopped
    // void
    // ContinueProcessing();

    ///@{
    /// Setters and getters for the Node logging service
    Log::LoggingService&
    GetLoggingService();

    const Log::LoggingService&
    GetLoggingService() const;
    ///@}

    ///@{
    /// Accessors for the shareable Courier service
    Core::MXCourier&
    GetCourier();

    const Core::MXCourier&
    GetCourier() const;

    std::shared_ptr<Core::MXCourier>
    ShareCourier();

    void
    SetCourier(std::shared_ptr<Core::MXCourier> C);
    ///@}

    ///@{
    /// Accessors for the Name property
    const std::string&
    GetName() const;

    void
    SetName(const std::string& Name);
    ///@}

    ///@{
    /// Accessors for the list of executable elements
    ElementList&
    GetElementList();

    const ElementList&
    GetElementList() const;
    ///@}

    /// Starts all the elements which are managed
    void
    StartAll();
    // This should queue a new control message, represented by a configuration tree
    // PropertyTree&
    // QueueControlMessage(const PropertyTree&)


private:
    /// The private implementation of the node
    class Impl;

    /// Pointer to the implementation
    std::experimental::propagate_const<std::unique_ptr<Impl>> m_impl;
};
} // namespace Execution
} // namespace MXWare

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(pop)

#endif

/*
 * C interface
 * Node* NodeCreate()
 * ConfigureNode(Node*, char*)
 * NodeCommand(Node*, int) // Start, Stop ...
 */

#endif /* MXWARE_EXECUTION_NODE_HH_ */
