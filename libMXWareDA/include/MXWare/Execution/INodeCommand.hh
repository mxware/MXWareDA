#ifndef MXWARE_EXECUTION_INODECOMMAND_HH_
#define MXWARE_EXECUTION_INODECOMMAND_HH_
#pragma once

#include "MXWare/Courier/IMessage.hh"
#include "MXWare/Courier/IReply.hh"
#include <memory>

namespace MXWare::Execution
{
class Node;

/// Represents a command for the node.
/// A command is a message which can execute a task on the node.
/// The result of the command
/// must be another message, delivered in a future
class INodeCommand : public Courier::IMessage
{
public:
    virtual auto
    Execute(Node&) const -> std::unique_ptr<Courier::IReply> = 0;
};

} // namespace MXWare::Execution

#endif // MXWARE_EXECUTION_INODECOMMAND_HH_