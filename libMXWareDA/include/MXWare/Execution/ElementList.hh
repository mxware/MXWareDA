/*
 * ElementList.hh
 *
 *  Created on: Jan 9, 2020
 *      Author: caiazza
 */

#ifndef MXWARE_EXECUTION_ELEMENTLIST_HH_
#define MXWARE_EXECUTION_ELEMENTLIST_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Execution/IElement.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"
#include "MXWare/Configuration/Specializations/StdVector.hh"

#include <list>
#include <memory>
#include <shared_mutex>

#include <boost/iterator/transform_iterator.hpp>
#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(push)
#pragma warning(disable : 4251)

#endif

namespace MXWare::Execution
{
/// A class managing and owning the list of the running elements
/// It contains unique pointer to the managed elements but dereference them
/// directly on access
/// On read the container exposes the references to the owned objects
/// The class contains a mutex that should be used to lock the access to the class itself
// DEVNOTE Private inheritance allows to inherit all the functions of the list
// and declare public only those that we need to use unchanged while replacing the
// others without incurring in the risk of slicing
class ElementList : private std::vector<std::unique_ptr<IElement>>
{
    /// The container of the elements to execute
    using base_t = std::vector<std::unique_ptr<IElement>>;

public:
    ///@{
    /// Standard STL typedef
    using value_type = IElement;
    using reference = IElement&;
    using const_reference = const IElement&;
    using size_type = base_t::size_type;
    using difference_type = base_t::difference_type;
    ///@}

private:
    ///@{
    /// The signature of the function used to dereference the list elements
    using transform_t = std::function<reference(base_t::reference)>;
    using const_transform_t = std::function<const_reference(base_t::const_reference)>;
    ///@}

    /// Helper class to derefernce the elements in the list
    struct ListDerefenceValue_T
    {
        reference
        operator()(base_t::reference p)
        {
            return *(p);
        }

        const_reference
        operator()(base_t::const_reference p)
        {
            return *(p);
        }
    };

public:
    ///@{
    /// Iterators to navigate the element list
    using iterator = boost::transform_iterator<transform_t, base_t::iterator>;
    using const_iterator = boost::transform_iterator<const_transform_t, base_t::const_iterator>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    ///@}

public:
    ///@{ The gang of five
    ElementList() = default;

    ~ElementList() = default;

    ElementList(const ElementList& other) = delete;

    ElementList&
    operator=(const ElementList& other) = delete;

    ElementList(ElementList&& other) : base_t{ std::move(other) }, m_Access{} {}

    ElementList&
    operator=(ElementList&& other)
    {
        if (this != &other)
        {
            base_t::operator=(std::move(other));
        }
        return *this;
    }

    // TODO Swap function
    ///@}

    /* *******************************************************
     *  Iterators
     ******************************************************* */
    ///@{
    /// Iterator to the beginning of the container
    iterator
    begin() noexcept
    {
        return iterator{ base_t::begin(), ListDerefenceValue_T{} };
    }

    const_iterator
    begin() const noexcept
    {
        return const_iterator{ base_t::begin(), ListDerefenceValue_T{} };
    }

    const_iterator
    cbegin() const noexcept
    {
        return const_iterator{ base_t::cbegin(), ListDerefenceValue_T{} };
    }
    ///@}

    ///@{
    /// Iterator to the past-end of the container
    iterator
    end() noexcept
    {
        return iterator{ base_t::end(), ListDerefenceValue_T{} };
    }

    const_iterator
    end() const noexcept
    {
        return const_iterator{ base_t::end(), ListDerefenceValue_T{} };
    }

    const_iterator
    cend() const noexcept
    {
        return const_iterator{ base_t::cend(), ListDerefenceValue_T{} };
    }
    /// @}

    ///@{
    /// Reverse iterator to the beginning of the list
    reverse_iterator
    rbegin() noexcept
    {
        return reverse_iterator(end());
    }

    const_reverse_iterator
    rbegin() const noexcept
    {
        return const_reverse_iterator(end());
    }

    const_reverse_iterator
    crbegin() const noexcept
    {
        return const_reverse_iterator(end());
    }
    ///@}

    ///@{
    /// Reverse iterator to past-the-beginning element
    reverse_iterator
    rend() noexcept
    {
        return reverse_iterator(begin());
    }

    const_reverse_iterator
    rend() const noexcept
    {
        return const_reverse_iterator(begin());
    }

    const_reverse_iterator
    crend() const noexcept
    {
        return const_reverse_iterator(begin());
    }
    ///@}

    /* *******************************************************
     *  Capacity
     ******************************************************* */
    using base_t::empty;
    using base_t::size;
    using base_t::max_size;

    /* *******************************************************
     *  Modifiers
     ******************************************************* */
    using base_t::clear;
    using base_t::push_back;
    using base_t::emplace_back;
    using base_t::pop_back;
    // using base_t::push_front;
    // using base_t::emplace_front;
    // using base_t::pop_front;
    using base_t::resize;

    /// Insertion function
    // DEVNOTE All the other insertion function are inserting by value
    // or by iterator and in both cases it's not applicable to this type
    // of list because the unique_ptr are move-only and the IElement are
    // not cloneable
    iterator
    insert(const_iterator pos, std::unique_ptr<IElement>&& value)
    {
        return iterator(base_t::insert(pos.base(), std::move(value)));
    }

    /// Emplace function
    template <class... Args>
    iterator
    emplace(const_iterator pos, Args&&... args)
    {
        return iterator(base_t::emplace(pos.base(), std::forward<Args>(args)...));
    }

    iterator
    erase(const_iterator pos)
    {
        return iterator{ base_t::erase(pos.base()) };
    }

    iterator
    erase(const_iterator first, const_iterator last)
    {
        return iterator{ base_t::erase(first.base(), last.base()) };
    }

    // TODO Splice, remove, reverse

    /// Adds a new element at the end of the container
    //    void
    //    push_back(std::unique_ptr<IElement>&& Element)
    //    {
    //        base_t::push_back(std::move(Element));
    //    }


    /* *******************************************************
     *  Locking
     ******************************************************* */
    ///@{
    /// A set of functions providing safe access to the enclosed R/W lock
    /// Each function returns a lock, either shared or unique and each function as a standard
    /// untemplated version returning the locked lock and a second version which allows to
    /// forward the locking policy when creating the lock
    auto
    ModificationAccess() const
    {
        return std::unique_lock<std::shared_timed_mutex>{ m_Access };
    }

    template <typename Policy>
    auto
    ModificationAccess(Policy p) const
    {
        return std::unique_lock<std::shared_timed_mutex>{ m_Access, p };
    }

    auto
    ReaderAccess() const
    {
        return std::shared_lock<std::shared_timed_mutex>{ m_Access };
    }

    template <typename Policy>
    auto
    ReaderAccess(Policy p) const
    {
        return std::shared_lock<std::shared_timed_mutex>{ m_Access, p };
    }
    ///@}

    /* *******************************************************
     *  Auto configuration
     ******************************************************* */
    /// Retrieves the current status of the node
    // DEVNOTE Defining this function allows to use the nested
    Configuration::PropertyTree
    CurrentConfiguration() const
    {
        Configuration::PropertyTree ans{};
        // We are going to use the base class write function, which is
        // based on the sequential algorithms internally here because
        // for the user the output type is of type IElement which is not
        // a pointer type therefore doesn't search for the types in the
        // factory
        Configuration::WriteToTree<base_t>(static_cast<const base_t&>(*this), ans, "");
        return ans;
    }

private:
    /// Mutex to lock the access to this object
    mutable std::shared_timed_mutex m_Access;
};

} // namespace MXWare::Execution

namespace MXWare::Configuration::Traits
{
template <>
struct UpdateMode<Execution::ElementList>
{
    using tag = Tags::SequentialContainer;
    using value = std::unique_ptr<Execution::IElement>;
};

template <>
struct WriteMode<Execution::ElementList>
{
    using tag = Tags::Configurable;
};



// template <typename Parameter>
// struct Parameter_Traits;

// template <>
// struct Parameter_Traits<Execution::ElementList>
// {
//     using Read_Parameter_Tag = detail::NestedParTag;
//     using Write_Parameter_Tag = detail::SequentialContainerParTag;

//     using container_type = Execution::ElementList;
//     using Read_Value_Type = Execution::IElement;
//     using Write_Value_Type = std::unique_ptr<Execution::IElement>;
// };
} // namespace MXWare::Configuration::Traits



#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(pop)

#endif




#endif /* MXWARE_EXECUTION_ELEMENTLIST_HH_ */
