#ifndef MXWARE_COURIER_REPLY_EMPTY_HH_
#define MXWARE_COURIER_REPLY_EMPTY_HH_
#pragma once

#include "MXWare/Courier/IReply.hh"
#include "MXWare/Definitions.hh"

namespace MXWare::Courier::Reply
{

class MXAPI Empty : public IReply
{
public:
    Empty(const IMessage& Request);

    const std::string&
    GetId() const noexcept override;

    std::string
    GetTypeId() const noexcept override;

    Configuration::PropertyTree
    GetBody() const override;

    static std::string
    TypeId();

    void
    Configure(const Configuration::PropertyTree& ConfTree) override;

    Configuration::PropertyTree
    CurrentConfiguration() const override;

    const std::string&
    GetRequestId() const noexcept override;

private:
    std::string m_Id;

    std::string m_RequestId;
};


}


#endif // MXWARE_COURIER_REPLY_EMPTYREPLY_HH_