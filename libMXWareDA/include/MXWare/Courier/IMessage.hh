#ifndef MXWARE_COURIER_IMESSAGE_HH_
#define MXWARE_COURIER_IMESSAGE_HH_
#pragma once

#include "MXWare/Definitions.hh"

#include "MXWare/Configuration/Configurable.hh"
#include <string>

namespace MXWare::Courier
{
/// Represents a generic message between the system components
/// A message is an object whose content can be represented in a
/// property tree.
/// A message can also have a string identifier to distinguish it
/// from other message. Whether the identifier is unique depends on the
/// message type and the context where it is used
// DEVNOTE The content of the message may be cloned through the configuration
// tree but the message itself should be unique so the class is not cloneable
class IMessage : public Configuration::Configurable
{
public:
    /// Polymorphic destructor
    virtual ~IMessage() = default;

    /// Returns the identifier of the message
    /// This string will be stored in the id field of the configuration tree
    virtual const std::string&
    GetId() const noexcept = 0;

    /// Returns a string identifying the type of message
    /// This string will be stored in the type attribute of the configuration tree
    virtual std::string
    GetTypeId() const noexcept = 0;

    /// Return a property tree containing the body of the message
    virtual Configuration::PropertyTree
    GetBody() const = 0;
};

namespace detail
{
/// An implementation of the configuration methods of the message based on
/// the remaining virtual methods
class MXAPI Message_ConfigWriterImpl : public IMessage
{
public:
    /// Defines the proper serialization of the parts accessible through
    /// the message interface
    Configuration::PropertyTree
    CurrentConfiguration() const override;
};
} // namespace detail
} // namespace MXWare::Courier

#endif // MXWARE_COURIER_IMESSAGE_HH_