#ifndef MXWARE_COURIER_IREPLY_HH_
#define MXWARE_COURIER_IREPLY_HH_
#pragma once

#include "MXWare/Courier/IMessage.hh"

namespace MXWare::Courier
{
/// A reply is a message which stores a reference to the ID of the message
/// to which it's replying, which is called a request
class IReply : public IMessage
{
public:
    /// Gets the idea of the matching request
    virtual const std::string&
    GetRequestId() const noexcept = 0;
};

}

#endif //MXWARE_COURIER_IREPLY_HH_