#ifndef MXWARE_RANDOM_RANDOMSTRING_HH_
#define MXWARE_RANDOM_RANDOMSTRING_HH_
#pragma once

#include "MXWare/Utilities/CharacterSets.hh"

#include <string>
#include <random>
#include <algorithm>


namespace MXWare::Random
{
/// Generates a string of a given length of random characters
/// extracted from a custom character set, using a custom random engine.
template <typename CS, typename E>
std::string
GenerateRandomString(std::size_t N, const CS& Chars, E& Engine)
{
    std::string ans(N, '\0');
    std::uniform_int_distribution<std::size_t> Distribution{ 0, Chars.size() - 1 };
    std::generate(ans.begin(), ans.end(), [&]() { return Chars[Distribution(Engine)]; });
    return ans;
}

/// Generates a string of a given length of random characters
/// extracted from a custom character set.
/// This overloads uses by default the Mersenne Twister random engine
template <typename CS = std::array<char, 62>>
std::string
GenerateRandomString(std::size_t N, const CS& Chars = Utilities::CharacterSets::Alphanumeric)
{
    std::mt19937_64 Engine{ std::random_device{}() };
    return GenerateRandomString(N, Chars, Engine);
}

} // namespace MXWare::Utilities

#endif // MXWARE_UTILITIES_RANDOMSTRING_HH_