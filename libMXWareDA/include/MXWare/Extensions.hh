#ifndef MXWARE_EXTENSIONS_HH_
#define MXWARE_EXTENSIONS_HH_
#pragma once

#include "MXWare/Extensions/PluginManager.hh"
#include "MXWare/Extensions/Builtins.hh"

namespace MXWare::Extensions
{
/// Creates a new extension object of a given interface from the globally registered extensions
template <typename Interface, typename... Args>
std::unique_ptr<Interface>
CreateObject(std::string_view key, Args&&... args)
{
    if(Builtins.CountRegistrations<Interface, Args...>(key) > 0)
    {
        return Builtins.CreateObject<Interface>(key, std::forward<Args>(args)...);
    }
    else
    {
        return GlobalPluginManager.CreateExtensionObject<Interface>(key, std::forward<Args>(args)...);
    }
    
}

/// Returns the registration name of a given object if registered in the global registry
template <typename T>
std::optional<std::string>
GetRegistration(const T& Obj)
{
    auto ans = Extensions::Builtins.GetExtensionRegistration(Obj);
    if(ans)
    {
        return ans;
    }
    else
    {
        return Extensions::GlobalPluginManager.GetExtensionRegistration(Obj);
    }
    
}

} // namespace MXWare::Core


#endif // MXWARE_EXTENSIONS_HH_