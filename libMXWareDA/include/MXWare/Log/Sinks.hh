#ifndef MXWARE_LOG_SINKS_HH_
#define MXWARE_LOG_SINKS_HH_
#pragma once

#include "MXWare/Log/Sinks/StdoutColor.hh"
#include "MXWare/Log/Sinks/SingleFileSink.hh"

#endif //MXWARE_LOG_SINKS_HH_