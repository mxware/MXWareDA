#ifndef MXWARE_LOG_LEVEL_HH
#define MXWARE_LOG_LEVEL_HH
#pragma once

#include "spdlog/common.h"
#include "MXWare/Utilities/FlatLookupTable.hh"
#include <string_view>

namespace MXWare::Log
{

/// Enumeration of the predefined log levels
enum class Level : uint32_t
{
    Trace = spdlog::level::trace,
    Debug = spdlog::level::debug,
    Info = spdlog::level::info,
    Warning = spdlog::level::warn,
    Error = spdlog::level::err,
    Critical = spdlog::level::critical,
    Off = spdlog::level::off
};

using namespace std::literals::string_view_literals;
constexpr Utilities::FlatLookupTable<std::string_view, Level, 7> NamedLevels{
    std::array<std::pair<std::string_view, Level>, 7>{ { { "trace"sv, Level::Trace },
        { "debug"sv, Level::Debug }, { "info"sv, Level::Info }, { "warning"sv, Level::Warning },
        { "error"sv, Level::Error }, { "critical"sv, Level::Critical }, { "off"sv, Level::Off } } }
};
} // namespace MXWare::Log

#endif // MXWARE_LOG_LEVEL_HH