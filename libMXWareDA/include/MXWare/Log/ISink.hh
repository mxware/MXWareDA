#ifndef MXWARE_LOG_ISINK_HH_
#define MXWARE_LOG_ISINK_HH_
#pragma once

#include "spdlog/sinks/sink.h"
#include <memory>
#include "MXWare/Configuration/Configurable.hh"
#include "MXWare/Utilities/Comparable.hh"

namespace MXWare::Log
{
/// Wrapper around a spdlog sink to enable the property tree
/// configuration
// DEVNOTE Using an external configuration function would 
// not work because the spdlog sink is an abstract class so I need
// a virtual dispatch to be able to configure appropriately the
// different sinks
class ISink: public Utilities::Comparable<ISink>
{
public:
    virtual ~ISink() = default;

    std::shared_ptr<spdlog::sinks::sink>
    ShareSpdlogSink() noexcept
    {
        return m_Sink;
    }

    std::shared_ptr<const spdlog::sinks::sink>
    ShareSpdlogSink() const noexcept
    {
        return std::const_pointer_cast<const spdlog::sinks::sink>(m_Sink);
    }

    /// Configures the sink through a property tree
    virtual void
    Configure(const Configuration::PropertyTree& ConfTree) = 0;

    /// Retrieves the sink configuration
    virtual Configuration::PropertyTree
    CurrentConfiguration() const = 0;

    bool
    IsValid() const noexcept
    {
        return static_cast<bool>(m_Sink);
    }

protected:
    /// Default construction
    ISink(): m_Sink{}
    {}

    ISink(std::shared_ptr<spdlog::sinks::sink> S):m_Sink{S}
    {}
    
    void
    ResetSpdlogSink(std::shared_ptr<spdlog::sinks::sink> S)
    {
        m_Sink = S;
    }

private:
    std::shared_ptr<spdlog::sinks::sink> m_Sink;

    bool
    IsEqual(const compare_base& rhs) const override
    {
        return m_Sink == rhs.ShareSpdlogSink();
    }
};
} // namespace MXWare::Log

#endif // MXWARE_LOG_ISINK_HH_