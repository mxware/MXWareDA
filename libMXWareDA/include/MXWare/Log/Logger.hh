#ifndef MXWARE_LOG_LOGGER_HH
#define MXWARE_LOG_LOGGER_HH
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Log/Level.hh"
#include "MXWare/Log/ISink.hh"
#include "MXWare/Configuration/PropertyTree.hh"
#include "spdlog/logger.h"
#include <string_view>

namespace MXWare::Log
{
/// The class used to log messages.
/// The logging system of MXWare is based on spdlog
/// This class is a wrapper around the spdlog logger
class Logger
{
public:
    /// Creates a new logger with the given name
    Logger(std::string_view Name): m_Logger{std::string{Name}}
    {}
    ///@{
    /// Forwarding constructors
    // using spdlog::logger::logger;
    ///@}

    const std::string&
    GetName() const noexcept
    {
        return m_Logger.name();
    }

    void
    SetLevel(Level LogLevel) noexcept
    {
        m_Logger.set_level(static_cast<spdlog::level::level_enum>(LogLevel));
    }

    Level
    GetLevel() const noexcept
    {
        return static_cast<Level>(m_Logger.level());
    }

    template <typename FormatString, typename... Args>
    void
    Trace(const FormatString& fmt, const Args&... args)
    {
        m_Logger.trace(fmt, args...);
    }

    template <typename FormatString, typename... Args>
    void
    Debug(const FormatString& fmt, const Args&... args)
    {
        m_Logger.debug(fmt, args...);
    }

    template <typename FormatString, typename... Args>
    void
    Info(const FormatString& fmt, const Args&... args)
    {
        m_Logger.info(fmt, args...);
    }

    template <typename FormatString, typename... Args>
    void
    Warn(const FormatString& fmt, const Args&... args)
    {
        m_Logger.warn(fmt, args...);
    }

    template <typename FormatString, typename... Args>
    void
    Error(const FormatString& fmt, const Args&... args)
    {
        m_Logger.error(fmt, args...);
    }

    template <typename FormatString, typename... Args>
    void
    Critical(const FormatString& fmt, const Args&... args)
    {
        m_Logger.critical(fmt, args...);
    }

    /// Direct access to the spdlog logger.
    /// NOTE This is for development only. Do not use if you don't know
    /// what you're doing as it will be eliminated without notice
    spdlog::logger&
    AccessSpdlogLogger() noexcept
    {
        return m_Logger;
    }

    /// Configures the logger
    MXAPI void
    Configure(const Configuration::PropertyTree&);

    /// Extracts the logger configuration
    MXAPI Configuration::PropertyTree
    CurrentConfiguration() const;

    /// Adds a sink to the current logger unless the same sink is already
    /// connected to the logger
    MXAPI bool
    AddSink(ISink& InSink);

private:
    /// The spdlog logger managed by the object
    spdlog::logger m_Logger;
};
} // namespace MXWare::Log

#endif // MXWARE_LOG_LOGGER_HH