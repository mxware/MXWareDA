#ifndef MXWARE_LOG_SERVICE_HH_
#define MXWARE_LOG_SERVICE_HH_
#pragma once

#include "MXWare/Definitions.hh"
#include "MXWare/Utilities/propagate_const.hh"
#include "MXWare/Configuration/PropertyTree.hh"

#include "MXWare/Log/Level.hh"
#include "MXWare/Log/Logger.hh"

#include <memory>
#include <string>
#include <optional>
#include <functional>
#include <string_view>

namespace MXWare::Log
{
/// Forward declarations
class Logger;

// The simple logging system should only include the framework and general/user logger
// initialized with some defaults.
// The user should be able to access the predefined loggers globally
// Logging through the stream operator could be added but I should maintain it personally and
//   I don't want to spend time on that
// The settings of all loggers should be adjustable with a configuration tree
// The conftree based configuration could be done using templated functions in a separate header
//   which will not mix up the definition of the logging system and its custom configuration
// The users should be allowed to create new loggers and configure them at will but should only
//   change the configuration of the framework and general loggers as well as the global
//   configuration of the logging system from the node interface
// To do this last thing I should not allow the user to access the whole logger with all its
//   functionalities but only the log functions themselves

/// Public interface to manage and customize the logging service
/// This should give the possibility to create custom loggers, configure them,
/// configure the global features of the logging system.
class MXAPI LoggingService
{
public:
    /// Creates a new logging service
    LoggingService();
    
    /// Destructor
    ~LoggingService() noexcept;

    ///@{
    /// Copy not allowed
    LoggingService(const LoggingService& other) = delete;

    LoggingService&
    operator=(const LoggingService& other) = delete;
    ///@}
    
    ///@{
    /// Move and move assign through copy and swap
    friend void
    swap(LoggingService& lhs, LoggingService& rhs) noexcept;

    LoggingService(LoggingService&& other) noexcept;

    LoggingService&
    operator=(LoggingService&& other) noexcept;
    ///@}

    /// Gets a logger of a given name if it is already registered in
    /// in the logging service
    std::optional<std::reference_wrapper<Logger>>
    GetLoggerIfExists(std::string_view Name);

    /// Gets a logger of the given name or creates a new one initialized
    /// with the common options if it doesn't
    Logger&
    GetOrCreateLogger(std::string_view Name);
    
    /// Sets the common parameters for the input logger
    void
    SetCommonConfiguration(Logger& InLogger);

    /// Configure the logging service from a configuration tree
    void
    Configure(const Configuration::PropertyTree& ConfTree);

    /// Outputs the current configuration
    Configuration::PropertyTree
    CurrentConfiguration() const;
    
    /// Sets the level assigned to all loggers which are registered in the registry
    /// It also assigns this level to all the loggers already registered
    void
    SetLevel(Level LogLevel);

    /// Returns the current global log level
    Level
    CurrentLevel() const;

    /// Access the builtin logger named "general"
    /// If such a logger does not exist yet it will be created using the standard settings
    Logger&
    GeneralLogger();

    /// Access the builtin "framework" logger
    /// If such a logger does not exist yet it will be created using the standard settings
    Logger&
    FrameworkLogger();

private:
    /// The private implementation of the node
    class Impl;

    /// Pointer to the implementation
    std::experimental::propagate_const<std::unique_ptr<Impl>> m_impl;

public:
    /// The default name to be used to look for the logging service configuration node
    static constexpr auto s_LoggingService_NN = "logging";

    ///@{
    /// The default names of the standard loggers
    static constexpr auto s_GeneralLoggerName = "general";
    static constexpr auto s_FrameworkLoggerName = "framework";
    ///@}
};

/// A reference to the global logging service
MXAPI extern LoggingService& GlobalService;

namespace detail
{
    static class MXAPI LogServiceInitializer
    {
        public:
    LogServiceInitializer();
    ~LogServiceInitializer();

    LogServiceInitializer(const LogServiceInitializer&) = delete;
    LogServiceInitializer(LogServiceInitializer&&) = delete;
    LogServiceInitializer&
    operator=(const LogServiceInitializer&) = delete;
    LogServiceInitializer&
    operator=(LogServiceInitializer&&) = delete;
    } G_LogInit;
}

} // namespace MXWare::Log


#endif // MXWARE_LOG_SERVICE_HH_