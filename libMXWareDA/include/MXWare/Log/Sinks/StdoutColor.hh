#ifndef MXWARE_LOG_SINKS_STDOUTCOLOR_HH_
#define MXWARE_LOG_SINKS_STDOUTCOLOR_HH_
#pragma once

#include "MXWare/Log/ISink.hh"
#include "spdlog/sinks/stdout_color_sinks.h"
#include <memory>

namespace MXWare::Log::Sinks
{
class StdoutColor : public ISink
{
public:
    StdoutColor(): ISink{std::make_shared<spdlog::sinks::stdout_color_sink_mt>()}
    {}

    /// Configures the sink through a property tree
    void
    Configure(const Configuration::PropertyTree&) override
    {}

    /// Retrieves the sink configuration
    Configuration::PropertyTree
    CurrentConfiguration() const override
    {
        return Configuration::PropertyTree{};
    }

    static auto
    TypeId()
    {
        return "stdout_color";
    }
};
} // namespace MXWare::Log::Sinks

#endif // MXWARE_LOG_SINKS_STDOUTCOLOR_HH_