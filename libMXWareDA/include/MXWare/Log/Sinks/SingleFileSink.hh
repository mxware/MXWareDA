#ifndef MXWARE_LOG_SINKS_SINGLEFILESINK_HH_
#define MXWARE_LOG_SINKS_SINGLEFILESINK_HH_
#pragma once

#include "MXWare/Log/ISink.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"
#include "spdlog/sinks/basic_file_sink.h"
#include <memory>
#include <filesystem>
#include <string_view>
#include <string>

namespace MXWare::Log::Sinks
{
class SingleFileSink : public ISink
{
public:
    SingleFileSink() : ISink{}, m_LogFilePath{} {}

    SingleFileSink(std::string_view Path) :
        ISink{ CreateSink( Path ) }, m_LogFilePath{ Path }
    {}

    /// Configures the sink through a property tree
    void
    Configure(const Configuration::PropertyTree& Tree) override;

    /// Retrieves the sink configuration
    Configuration::PropertyTree
    CurrentConfiguration() const override
    {
        Configuration::PropertyTree ans{};
        Configuration::WriteToTree(m_LogFilePath, ans, "path");
        return ans;
    }

    static auto
    TypeId()
    {
        return "single_file_sink";
    }

private:
    std::string m_LogFilePath;

    static std::shared_ptr<spdlog::sinks::basic_file_sink_mt>
    CreateSink(std::string_view FilePath);
};
} // namespace MXWare::Log::Sinks

#endif // MXWARE_LOG_SINKS_SINGLEFILESINK_HH_