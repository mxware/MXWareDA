/*
 * MXLogger.hh
 *
 * This header contains the public API of the MXWare logging system.
 * The logging system is based on the spdlog library.
 *
 *  Created on: Aug 16, 2019
 *      Author: caiazza
 */

#ifndef MXWARE_LOG_HH_
#define MXWARE_LOG_HH_
#pragma once

#include <memory>

#include "MXWare/Definitions.hh"
#include "MXWare/Log/Level.hh"
#include "MXWare/Log/Logger.hh"
#include "MXWare/Log/Service.hh"
#include "MXWare/Log/Sinks.hh"

namespace MXWare::Log
{
/// Logs a trace message in the general logger
template <typename FormatString, typename... Args>
void
Trace(const FormatString& str, Args&&... args)
{
    GlobalService.GeneralLogger().Trace(str, std::forward<Args>(args)...);
}

/// Logs a debug message in the general logger
template <typename FormatString, typename... Args>
void
Debug(const FormatString& str, Args&&... args)
{
    GlobalService.GeneralLogger().Debug(str, std::forward<Args>(args)...);
}

/// Logs an information message in the general logger
template <typename FormatString, typename... Args>
void
Info(const FormatString& str, Args&&... args)
{
    GlobalService.GeneralLogger().Info(str, std::forward<Args>(args)...);
}

/// Logs an warning message in the general logger
template <typename FormatString, typename... Args>
void
Warn(const FormatString& str, Args&&... args)
{
    GlobalService.GeneralLogger().Warn(str, std::forward<Args>(args)...);
}

/// Logs an error message in the general logger
template <typename FormatString, typename... Args>
void
Error(const FormatString& str, Args&&... args)
{
    GlobalService.GeneralLogger().Error(str, std::forward<Args>(args)...);
}

/// Logs an error message in the general logger
template <typename FormatString, typename... Args>
void
Critical(const FormatString& str, Args&&... args)
{
    GlobalService.GeneralLogger().Critical(str, std::forward<Args>(args)...);
}

namespace Framework
{
/// Logs a trace message in the framework logger
template <typename FormatString, typename... Args>
void
Trace(const FormatString& str, Args&&... args)
{
    GlobalService.FrameworkLogger().Trace(str, std::forward<Args>(args)...);
}

/// Logs a debug message in the framework logger
template <typename FormatString, typename... Args>
void
Debug(const FormatString& str, Args&&... args)
{
    GlobalService.FrameworkLogger().Debug(str, std::forward<Args>(args)...);
}

/// Logs an information message in the framework logger
template <typename FormatString, typename... Args>
void
Info(const FormatString& str, Args&&... args)
{
    GlobalService.FrameworkLogger().Info(str, std::forward<Args>(args)...);
}

/// Logs an warning message in the framework logger
template <typename FormatString, typename... Args>
void
Warn(const FormatString& str, Args&&... args)
{
    GlobalService.FrameworkLogger().Warn(str, std::forward<Args>(args)...);
}

/// Logs an error message in the framework logger
template <typename FormatString, typename... Args>
void
Error(const FormatString& str, Args&&... args)
{
    GlobalService.FrameworkLogger().Error(str, std::forward<Args>(args)...);
}

/// Logs an error message in the framework logger
template <typename FormatString, typename... Args>
void
Critical(const FormatString& str, Args&&... args)
{
    GlobalService.FrameworkLogger().Critical(str, std::forward<Args>(args)...);
}

} // namespace Framework

} // namespace MXWare::Log


#endif /* MXWARE_CORE_MXLOGGER_HH_ */
