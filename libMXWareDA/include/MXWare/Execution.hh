#ifndef MXWARE_EXECUTION_HH_
#define MXWARE_EXECUTION_HH_
#pragma once

#include "MXWare/Execution/IElement.hh"
#include "MXWare/Execution/ElementList.hh"
#include "MXWare/Execution/Node.hh"

#endif // MXWARE_CORE_EXECUTION_HH_