#ifndef MXWARE_EXCEPTIONS_HH_
#define MXWARE_EXCEPTIONS_HH_
#pragma once

#include <stdexcept>

#include "MXWare/Definitions.hh"

// This is necessary because by default the STL classes are not exported symbols. MSVC warns about
// this and maybe could also be a problem in GCC
#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
class MXAPI std::exception;
class MXAPI std::runtime_error;
#endif

/// Declares a new exception to be added to the enclosing namespace
/// The default MXWare exception can be constructed with a simple message or
/// with a triplet of Class name, function name and Message
/// The exception supports the polymorphic exception handling system with the raise function
/// @param API The visibility/export attribute of the function
/// @param EXC The name of the declared exception
/// @param BASE The base class of the declared exception
#define MXWARE_DECLARE_EXCEPTION(API, EXC, BASE)                                                   \
    class API EXC : public BASE                                                                    \
    {                                                                                              \
    public:                                                                                        \
        EXC(const std::string& Message);                                                           \
        EXC(const std::string& Class, const std::string& Function, const std::string& Message);    \
        virtual ~EXC() = default;                                                                  \
        [[noreturn]] virtual void                                                                  \
        raise() override                                                                           \
        {                                                                                          \
            throw *this;                                                                           \
        };                                                                                         \
    };

/// Implements an exception in the enclosing namespace
/// @param
#define MXWARE_IMPLEMENT_EXCEPTION(EXC, BASE)                                                      \
    EXC::EXC(const std::string& Message) : BASE(Message) {}                                        \
    EXC::EXC(const std::string& Class, const std::string& Function, const std::string& Message) :  \
        BASE(Class, Function, Message)                                                             \
    {}

namespace MXWare
{
/*
 * NOTE
 * The problem is that exporting the exceptions it's useful in case those exceptions are
 * going to be thrown outside the library boundary which is may introduce compatibility
 * problems. If we are going to have a clean API the exceptions should be all managed within
 * the library. On the other hand it could be useful to have them available to specify
 * failure of the framework components in a good C++ way
 *
 * PS I remember there was a reason why it was better to have the exceptions compiled and
 * not inlined but I do not remember why. I think it was because you need them in the library
 * if you want to be able to pass them reliably between libraries
 */

/**
 * Generic exception class of the framework
 */
struct MXAPI GeneralException : public std::runtime_error
{
    explicit GeneralException(const std::string& Message);

    GeneralException(
        const std::string& Class, const std::string& Function, const std::string& Message);

    virtual ~GeneralException() = default;

    /// Prints the error message
    virtual const char*
    what() const noexcept
    {
        return runtime_error::what();
    }

    /// Polymorphical throw clause
    [[noreturn]] virtual void
    raise()
    {
        throw *this;
    }
};

/**
 * Exception to be thrown when a function is attempting to dereference an invalid pointer
 * or set a pointer to null when this is not allowed
 */
MXWARE_DECLARE_EXCEPTION(MXAPI, NullPointerException, GeneralException)

/// A required object was not found in the resource where it was supposed to be
MXWARE_DECLARE_EXCEPTION(MXAPI, ObjectNotFound, GeneralException)

/// Attempting to store an object in a resource where it is supposed to be unique
MXWARE_DECLARE_EXCEPTION(MXAPI, DuplicateObject, GeneralException)

/// The argument evaluated is not valid in the evaluation context
MXWARE_DECLARE_EXCEPTION(MXAPI, InvalidArgument, GeneralException)

/// The argument evaluated is not valid in the evaluation context
MXWARE_DECLARE_EXCEPTION(MXAPI, ConfigurationError, GeneralException)

/// The argument evaluated is out of the allowed range for that type
MXWARE_DECLARE_EXCEPTION(MXAPI, OutOfRange, InvalidArgument)

/// A prerequisite for the execution of a function is not fulfilled
MXWARE_DECLARE_EXCEPTION(MXAPI, RequirementFailure, GeneralException)

/// Generic error in the handling of a file
MXWARE_DECLARE_EXCEPTION(MXAPI, FileError, GeneralException)

/// A file was not found at the expected path
MXWARE_DECLARE_EXCEPTION(MXAPI, FileNotFound, FileError)

/// Error detected when accessing a file
MXWARE_DECLARE_EXCEPTION(MXAPI, FileAccessError, FileError)

/// Extension with the required identifier not found
MXWARE_DECLARE_EXCEPTION(MXAPI, ExtensionNotFound, GeneralException)

}

#endif // MXWARE_EXCEPTIONS_HH_