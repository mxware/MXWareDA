#ifndef MXWARE_CONFIGURATION_SPECIALIZATIONS_HH_
#define MXWARE_CONFIGURATION_SPECIALIZATIONS_HH_
#pragma once

#include "MXWare/Configuration/Specializations/StdVector.hh"
#include "MXWare/Configuration/Specializations/StdList.hh"
#include "MXWare/Configuration/Specializations/StdDeque.hh"
#include "MXWare/Configuration/Specializations/StdSet.hh"
#include "MXWare/Configuration/Specializations/StdMap.hh"

#endif // MXWARE_CONFIGURATION_SPECIALIZATIONS_HH_