/*
 * SmartConfigurable.hh
 *
 *  Created on: 29 Dec 2019
 *      Author: caiazza
 */

#ifndef MXWARE_CONFIGURATION_SMARTCONFIGURABLE_HH_
#define MXWARE_CONFIGURATION_SMARTCONFIGURABLE_HH_

#include "MXWare/Configuration/Configurable.hh"
#include "MXWare/Configuration/SmartParameterList.hh"

#include <type_traits>

namespace MXWare::Configuration
{
/// Template to extend an existing class, which may or may not define already
/// the configurable interface, with the smart configuration system based on the
/// smart parameter list. This class is designed to be an intermediate component
/// in an inheritance hierarchy. Whether the resulting class derives from the
/// Configurable interface is defined by the caller
/// NOTE the Configure and CurrentConfiguration methods override any existing
/// definition in the base class. This class is designed to be the first in the
/// hierarchy to implement those two methods
template <class T>
class SmartConfigurable_T : public T
{
public:
    /// Forwarding constructor. Just prepares an empty list
    /// NOTE I cannot call the Register function here because it's virtual and the derived
    /// class is not yet available in the constructor
    template <typename... Args>
    SmartConfigurable_T(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args...>) :
        T{ std::forward<Args>(args)... }, m_Pars{}
    {}

    /// Virtual destructor
    virtual ~SmartConfigurable_T() noexcept(std::is_nothrow_destructible_v<T>) = default;

    ///@{
    /// Copy move and assignment
    /// The class copy and move is allowed but there is no actual copy of the parameter list
    /// because the parameter list is just a set of links to the concrete objects. It's
    /// a task of the derived class to actually copy the connected objects properly.
    /// Moreover it cannot be defaulted because the smart parameter list itself is not
    /// copyable
    SmartConfigurable_T(const SmartConfigurable_T& other) noexcept(
        std::is_nothrow_copy_constructible_v<T>) :
        T{ other }, m_Pars{}
    {}

    SmartConfigurable_T&
    operator=(const SmartConfigurable_T& other) noexcept(std::is_nothrow_copy_assignable_v<T>)
    {
        if (this != &other)
        {
            T::operator=(other);
        }
        return *this;
    }

    SmartConfigurable_T(SmartConfigurable_T&& other) noexcept(std::is_nothrow_move_constructible_v<T>) : T{ other }, m_Pars{} {}

    SmartConfigurable_T&
    operator=(SmartConfigurable_T&& other) noexcept(std::is_nothrow_move_assignable_v<T>)
    {
        if (this != &other)
        {
            T::operator=(other);
        }
        return *this;
    }
    ///@}

    /// Configures the concrete class through a property tree
    virtual void
    Configure(const PropertyTree& ConfTree)
    {
        if (!m_Pars.IsInitialized())
        {
            RegisterParameters(m_Pars);
            m_Pars.MarkInitialized();
        }

        m_Pars.UpdateAll(ConfTree);
    }

    /**
     * Serialization of the current configuration status in a configuration tree
     * @return
     */
    virtual PropertyTree
    CurrentConfiguration() const
    {
        if (!m_Pars.IsInitialized())
        {
            RegisterParameters(m_Pars);
            m_Pars.MarkInitialized();
        }

        return m_Pars.CreateTree();
    }

protected:
    /// Registers the configurable parameters in the parameter list
    virtual void
    RegisterParameters(SmartParameterList& Pars) const = 0;

    ///@{
    /// Direct access to the parameter list
    const SmartParameterList&
    GetParameterList() const
    {
        return m_Pars;
    }

    SmartParameterList&
    GetParameterList()
    {
        return m_Pars;
    }
    ///@}

private:
    /// The list of the configurable parameters of the class
    /// This member is mutable to be initialized also in the CurrentConfiguration function
    mutable SmartParameterList m_Pars;
};

/// Specialization of the SmartConfigurable template to be used as root
/// of class hierarchy.
using SmartConfigurable = SmartConfigurable_T<Configurable>;

} // namespace MXWare




#endif /* MXWARE_CONFIGURATION_SMARTCONFIGURABLE_HH_ */
