#ifndef MXWARE_CONFIGURATION_PROPERTYTREE_HH_
#define MXWARE_CONFIGURATION_PROPERTYTREE_HH_
#pragma once

#include "MXWare/Definitions.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <sstream>
#include <istream>
#include <ostream>
#include <chrono>
#include <string_view>

namespace MXWare::Configuration
{
using PropertyTree = boost::property_tree::ptree;

///@{
/// Hardcoded value for some of the configuration parameters
constexpr auto XMLFormatTag = "application/mxware+xml";
///@}

/// Formats a property tree using a given format and outputs it on an existing ostream object
/// If the codec is not recognized it returns an ExtensionNotFound exception
MXAPI std::ostream&
StreamTree(std::ostream& Out, const PropertyTree& Tree, std::string_view FormatTag = XMLFormatTag);

/// Streams the tree on a new stream
MXAPI std::stringstream
StreamTree(const PropertyTree& Tree, std::string_view FormatTag = XMLFormatTag);

/// Parses a tree from an input stream using the provided format
/// If the codec is not recognized it returns an ExtensionNotFound exception
/// New items are added to the existing tree.
MXAPI std::istream&
ParseTree(std::istream& In, PropertyTree& Tree, std::string_view FormatTag = XMLFormatTag);

/// Creates a new tree parsing the input stream using the given format
MXAPI PropertyTree
ParseTree(std::istream& In, std::string_view FormatTag = XMLFormatTag);


/**
 * @brief  Merges a property tree, named source in another named sink.
 * 
 * @param Sink The tree to which the new parameters will be added
 * @param Source The tree which will be merged into the sink
 * @param Overwrite Whether the parameters in the source will overwrite existing one in the sink
 * 
 * 
 */
MXAPI void
MergeConfigurationTrees(PropertyTree& Sink, const PropertyTree& Source, bool Overwrite = true);

namespace detail
{
/// Custom translator for bool (only supports std::string as internal type)
struct MXAPI BoolTranslator
{
    using internal_type = PropertyTree::data_type;
    using external_type = bool;

    /// Converts a string to bool
    boost::optional<external_type>
    get_value(const internal_type& str);

    /// Converts a bool to string
    boost::optional<internal_type>
    put_value(const external_type& b)
    {
        return boost::optional<internal_type>(b ? "true" : "false");
    }
};

template <class Clock, class Duration>
struct MXAPI TimePointTranslator
{
    using internal_type = PropertyTree::data_type;
    using external_type = typename std::chrono::time_point<Clock, Duration>;

    /// Converts a string to time point
    boost::optional<external_type>
    get_value(const internal_type& str)
    {
        typedef typename external_type::duration duration_t;
        if (!str.empty())
        {
            external_type ans;
            if (str.find_first_not_of("0123456789.") == internal_type::npos)
            {
                typename external_type::duration::rep tmp;
                std::stringstream stream{ str };
                stream >> tmp;
                ans = external_type{ duration_t{ tmp } };
            }
            else
            {
                auto PosixTime = boost::posix_time::from_iso_string(str);
                auto TimeSinceEpoch = PosixTime - boost::posix_time::from_time_t(0);

#ifdef BOOST_DATE_TIME_HAS_NANOSECONDS
                std::chrono::nanoseconds nsec{ TimeSinceEpoch.total_nanoseconds() };
#else
                std::chrono::nanoseconds nsec { TimeSinceEpoch.total_microseconds() * 1000 }
#endif
                duration_t d = std::chrono::duration_cast<duration_t>(nsec);


                /*std::chrono::nanoseconds nsec{ TimeSinceEpoch.total_seconds()
                * 1000000000 + TimeSinceEpoch.fractional_seconds() * (1000000000
                / TimeSinceEpoch.ticks_per_second())}; auto d =
                std::chrono::duration_cast<duration_t>{ nsec };*/

                /*ans = std::chrono::system_clock::from_time_t(
                    TimeSinceEpoch.total_seconds());
                int64_t nsec = TimeSinceEpoch.fractional_seconds() *
                            (1000000000 / TimeSinceEpoch.ticks_per_second());
                ans += std::chrono::nanoseconds { nsec };*/
                ans = external_type(d);
            }

            return boost::optional<external_type>{ ans };
        }
        else
            return boost::optional<external_type>(boost::none);
    }

    /// Converts a time point to string
    boost::optional<internal_type>
    put_value(const external_type& from)
    {
        // typedef std::chrono::time_point<Clock, Duration> time_point_t;
        typedef std::chrono::nanoseconds duration_t;
        typedef duration_t::rep rep_t;
        rep_t d = std::chrono::duration_cast<duration_t>(from.time_since_epoch()).count();
        rep_t musec = d / 1000;
        rep_t nsec = d % 1000;

        auto PosixTime = boost::posix_time::from_time_t(0) +
                         boost::posix_time::microseconds(musec) +
#ifdef BOOST_DATE_TIME_HAS_NANOSECONDS
                         boost::posix_time::nanoseconds(nsec);
#else
                         boost::posix_time::microseconds((nsec + 500L) / 1000L);
#endif

        return boost::optional<internal_type>{ boost::posix_time::to_iso_string(PosixTime) };
    }
};
} // namespace detail
} // namespace MXWare::Configuration

/* ******************************************************
 *
 *      Custom translators for a property tree
 *
 ******************************************************** */
namespace boost::property_tree
{
/**
 * Specialize translator_between so that it uses our custom translator for
 * bool value types. Specialization must be in boost::property_tree
 * namespace.
 */
template <typename Ch, typename Traits, typename Alloc>
struct translator_between<std::basic_string<Ch, Traits, Alloc>, bool>
{
    typedef MXWare::Configuration::detail::BoolTranslator type;
};

/**
 * Specialize translator_between so that it uses our custom translator for
 * timepoint value types. Specialization must be in boost::property_tree
 * namespace.
 */
template <typename Ch, typename Traits, typename Alloc, class Clock, class Duration>
struct translator_between<std::basic_string<Ch, Traits, Alloc>,
    std::chrono::time_point<Clock, Duration>>
{
    typedef MXWare::Configuration::detail::TimePointTranslator<Clock, Duration> type;
};

/**
 * Prints the content of the given property tree to a stream in the XML format
 * NOTE This overload of the operator<< must be in the boost::property_tree
 * namespace to allow for the ADL to pick it up correctly. Cannot be in the
 * MXWare namespace because there the PropertyTree class is only an alias to the
 * boost property tree
 * @param Tree
 * @return
 */
MXAPI std::ostream&
operator<<(std::ostream& os, const ptree& Tree);
} // namespace boost::property_tree


#endif