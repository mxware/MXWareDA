#ifndef MXWARE_CONFIGURATION_TRAITS_HH_
#define MXWARE_CONFIGURATION_TRAITS_HH_
#pragma once

#include <type_traits>
#include "MXWare/Configuration/PropertyTree.hh"

namespace MXWare::Configuration::Tags
{
/// Tag identifying configuration parameters of simple type.
/// This type of parameter can be directly converted to and from a string
struct Simple
{};

/// Tag identifying configuration parameters of nested type.
/// A nested parameter is an object which contains internal data members
/// which should be recursively update from the tree subnodes. A parameter of this type
/// is of non-pointer type and it's not a container and must define a function with the
/// Configure signature or a Configure template for that object is defined
struct Configurable
{};

/// Tag identifying pointer-like configuration parameters
/// Those parameters should be dereferenced to be configured and may require the
/// usage of a factory and the extensions system to be generated
/// if part of a hierarchy system
struct Pointer
{};

/// Tag identifying configuration parameters of sequential container type
/// Those parameters are those like the STL vector, list and deque
/// Those three are the only sequential containers supported by default
/// In a sequential parameter new objects are by default to be added to the end
struct SequentialContainer
{};

/// Tag identifying configuration parameters that are associative containers
/// An associative container needs both a key and a value to insert a parameter
struct AssociativeContainer
{};
} // namespace MXWare::Configuration::Tags

namespace MXWare::Configuration::detail
{
/**
 * Trait class to check the compliance with the Configure signature for classes which
 * are not derictly derived form the Configurable interface but have anyway the Configure method
 * Template that checks, at compile time, through the SFINAE technique, that the
 * required function has the configure method.
 */
template <typename C>
class has_configure
{
    template <class T>
    static auto
    test(MXWare::Configuration::PropertyTree Tree)
        -> decltype(std::declval<T>().Configure(Tree), std::true_type{});

    template <class T>
    static std::false_type
    test(...);

public:
    // using type = decltype(test<C>(MXWare::Configuration::PropertyTree{} ));
    // static const bool value = type::value;
    static constexpr bool value =
        std::is_same<decltype(test<C>(MXWare::Configuration::PropertyTree{})),
            std::true_type>::value;
};

/**
 * Trait class to check the compliance with the CurrentConfiguration signature .
 * DEVNOTE This uses the same SFINAE technique has the one with Configure but applies the check to
 * the template parameter of the embedded struct rather than the functional parameter of the
 * testSignature function. I added this one as well for reference and for testing. The results
 * are actually the same
 */
template <typename C>
class has_currentconfiguration
{
    typedef char (&Yes)[1];
    typedef char (&No)[2];

    template <class T>
    static Yes
    test(T const* data, typename std::enable_if<std::is_same<MXWare::Configuration::PropertyTree,
                            decltype(data->CurrentConfiguration())>::value>::type* = 0);
    static No
    test(...);

public:
    static const bool value = sizeof(Yes) == sizeof(has_currentconfiguration::test(
                                                 (typename std::remove_reference<C>::type*)0));
};

}

namespace MXWare::Configuration::Traits
{
/// The trait class to define the update mode for a custom class
/// It should define the tag alias which specifies how to update the class
/// and, depending on the tag some other aliases
template<typename T, typename Enable = void>
struct UpdateMode
{
    // static_assert(sizeof(T), "Update mode undefined for this type");
    using tag = Tags::Simple;
};

/// A class that has the Configure method is defined as Configurable
template<typename T>
struct UpdateMode<T, std::enable_if_t<detail::has_configure<T>::value>>
{
    using tag = Tags::Configurable;
};

/// The trait class to define the write mode for a custom class
/// It should define the tag alias which specifies how to write the class
/// in a property tree and depending on the tag, some other aliases
/// By default it uses the same traits as the Update Mode
template<typename T, typename Enable = void>
struct WriteMode: public UpdateMode<T>
{ };

/// A class with the CurrentConfiguration method is configurable
template<typename T>
struct WriteMode<T, std::enable_if_t<detail::has_currentconfiguration<T>::value>>
{
    using tag = Tags::Configurable;
};

/// Alias for simple access to the tag
template <typename T>
using UpdateTag_T = typename UpdateMode<T>::tag;

/// Alias for the type to update and insert in the container
template<typename T>
using UpdateContainedValue_T = typename UpdateMode<T>::value;

/// Alias for the type to use as key
template<typename T>
using UpdateAssociativeKey_T = typename UpdateMode<T>::key;

/// Alias for simple access to the tag
template <typename T>
using WriteTag_T = typename WriteMode<T>::tag;

/// Alias for the value to write from a container
template<typename T>
using WriteContainedValue_T = typename WriteMode<T>::value;

/// Alias for the key type of the container
template<typename T>
using WriteAssociativeKey_T = typename WriteMode<T>::key;

/// @{
/// Utility functions to query if the object is of a certain type
template <typename T>
inline constexpr auto IsUpdateConfigurable_V = std::is_same_v<UpdateTag_T<T>, Tags::Configurable>;

template <typename T>
inline constexpr auto IsUpdatePointer_V = std::is_same_v<UpdateTag_T<T>, Tags::Pointer>;

template <typename T>
inline constexpr auto IsUpdateSequentialContainer_V = std::is_same_v<UpdateTag_T<T>, Tags::SequentialContainer>;

template <typename T>
inline constexpr auto IsUpdateAssociativeContainer_V = std::is_same_v<UpdateTag_T<T>, Tags::AssociativeContainer>;

template <typename T>
inline constexpr auto IsWriteConfigurable_V = std::is_same_v<WriteTag_T<T>, Tags::Configurable>;

template <typename T>
inline constexpr auto IsWritePointer_V = std::is_same_v<WriteTag_T<T>, Tags::Pointer>;

template <typename T>
inline constexpr auto IsWriteSequentialContainer_V = std::is_same_v<WriteTag_T<T>, Tags::SequentialContainer>;

template <typename T>
inline constexpr auto IsWriteAssociativeContainer_V = std::is_same_v<WriteTag_T<T>, Tags::AssociativeContainer>;

///@}


// /* *****************************************************
//  *       Tag specializations
//  * *****************************************************/
/// Specialization for raw pointers
template <typename T>
struct UpdateMode<T*>
{
    using tag = Tags::Pointer;
};

/// Specialization for unique pointer
template <typename T, typename Deleter>
struct UpdateMode<std::unique_ptr<T, Deleter>>
{
    using tag = Tags::Pointer;
};

/// Specialization for shared pointer
template <typename T>
struct UpdateMode<std::shared_ptr<T>>
{
    using tag = Tags::Pointer;
};

} // namespace MXWare::Configuration::Traits

#endif // MXWARE_CONFIGURATION_TRAITS_HH_