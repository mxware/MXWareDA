#ifndef MXWARE_CONFIGURATION_SMARTPARAMETERLIST_HH_
#define MXWARE_CONFIGURATION_SMARTPARAMETERLIST_HH_

#include <string>
#include <functional>
#include <set>
#include <memory>
#include <type_traits> //<- to remove after reviewing iterator
#include "boost/current_function.hpp"
#include "fmt/format.h"
#include "MXWare/Exceptions.hh"
#include "MXWare/Configuration/AutoConfiguration.hh"

namespace MXWare::Configuration
{
namespace detail
{
/**
 * Structure defining the metadata of the configuration parameters.
 * Each parameter is defined by a name, a description and a type category.
 * Additionally each parameter can be enabled or disabled
 */
class ParameterMetadata
{
public:
    /// Full constructor
    ParameterMetadata(const std::string& Name, const std::string& Description) :
        m_Name{ Name }, m_Description{ Description }, m_Enable{ true }
    {}

    /// Returns the parameter description
    const std::string&
    GetDescription() const noexcept
    {
        return m_Description;
    }

    /// Specifies if a parameter is enabled
    bool
    IsEnable() const noexcept
    {
        return m_Enable;
    }

    /// Enables or disable a parameter
    void
    Enable(bool enable) noexcept
    {
        m_Enable = enable;
    }

    /// returns the parameter identifier
    const std::string&
    GetName() const noexcept
    {
        return m_Name;
    }

private:
    /// The name of the parameter.
    /// It corresponds to the name of the node where it is to be stored
    std::string m_Name;

    /// The parameter description for documentation purposes
    std::string m_Description;

    /// Specifies whether the parameter is enabled
    bool m_Enable;

    /// Defines the strict ordering of the parameter metadata.
    /// Two metadata are only ordered based on their name so that
    /// they can be used directly in ordered containers like sets
    friend bool
    operator<(const ParameterMetadata& lhs, const ParameterMetadata& rhs) noexcept
    {
        return lhs.m_Name < rhs.m_Name;
    }

    /// Equality operator
    /// Note that the equality depends on all the data members while the ordering depends
    /// only on the parameter name
    friend bool
    operator==(const ParameterMetadata& lhs, const ParameterMetadata& rhs) noexcept
    {
        return (lhs.m_Name == rhs.m_Name) && (lhs.m_Description == rhs.m_Description) &&
               (lhs.m_Enable == rhs.m_Enable);
    }
};

/**
 * Interface for the smart, managed configuration parameters.
 * The interface is needed so that the concrete templated objects can be stored
 * together in the same container.
 * Note that these objects are not copyable.
 * NOTE Because the objects are not copyable the IsSet parameter is not brought along
 * when the containing object is copied so it can only be used to verify that the parameter
 * was set in the lifetime of the current object. USE WITH CARE.
 * If some check like that is necessary, it should be performed manually.
 * In general is better to rely on a specific boolean switch.
 *
 * Some types of parameters can also be connected to a physical unit which allows an automatic
 * conversion from the conventional unit specified in the configuration file to the internal
 * unit of the system of unit. Automatically the populate procedure will convert it back to the
 * input value
 *
 * It is also possible to specify that a parameter is dependant from another. This is useful when
 * one wants to give the possibility to set a certain variable using different units (say radians
 * and degrees). In this case the convention is to define one of the two as the primary parameter
 * and the other as the dependant parameter. The dependant parameter will never be used in the
 * populate phase so that only the primary one will be present in the output configuration tree.
 * Not that the user should make sure not to have circular dependencies among parameters or none
 * will be output correctly. In the update phase the dependant parameter will be updated only if the
 * primary one is not defined in the same configuration file. This means that it is possible to
 * connect two different parameters to the same variable but using different unit multipliers to
 * allow different input units connected to the same physical value. Remember that than only the
 * primary will be used to populate
 */
class ISmartParameter
{
public:
    /// Virtual destructor
    virtual ~ISmartParameter() noexcept = default;

    /// Get the metadata of the parameter
    virtual const ParameterMetadata&
    GetMetadata() const noexcept = 0;

    /// Access the metadata of the parameter
    virtual ParameterMetadata&
    Metadata() noexcept = 0;

    /// Sets the metadata of the connected parameter
    virtual void
    SetMetadata(const ParameterMetadata& Meta) noexcept = 0;

    /// Updates the parameter value
    virtual void
    Update(const PropertyTree& ConfTree) = 0;

    /// Writes the parameter configuration to a tree
    virtual void
    Write(PropertyTree& RootNode) const = 0;

    /// Returns a boolean specifying whether the connected parameter was contained
    /// in the input configuration tree and successfully configured
    virtual bool
    IsSet() const noexcept = 0;

    /// Defines whether a parameter was correctly configured in the last configuration run
    virtual void
    Set(bool) noexcept = 0;

    /**
     * Retrieves the physical unit multiplier for the parameter.
     * If the unit is 0, it implies no unit multiplier was set and needs to be used
     * @return
     */
    virtual double
    GetUnitMultiplier() const noexcept = 0;

    /**
     * Sets the unit physical multiplier for the parameter.
     * If this value is set to 0 then no unit is implied and no parameter adjustment will
     * be performed
     * @param Unit
     */
    virtual void
    SetUnitMultiplier(double Unit) noexcept = 0;

    /**
     * Specifies whether this variable depends on a second one
     * @return
     */
    virtual bool
    IsDependant() const noexcept = 0;

    /**
     * Specifies the name of the primary parameter on which this depends
     * @return
     */
    virtual const std::string&
    GetPrimaryParameterName() const noexcept = 0;

    /**
     * Sets the name of the primary parameter on which this depends.
     * If the argument is an empty string it unset the dependency
     * @param Name
     */
    virtual void
    SetPrimaryParameterName(const std::string& Name) noexcept = 0;

    /// Defines the strict ordering of the parameter metadata.
    /// Two metadata are only ordered based on their name so that
    /// they can be used directly in ordered containers like sets
    friend bool
    operator<(const ISmartParameter& lhs, const ISmartParameter& rhs) noexcept
    {
        return lhs.GetMetadata().GetName() < rhs.GetMetadata().GetName();
    }
};

/**
 * A templated smart parameter which connects one metadata to a specific object
 * of a class.
 * The template then simply calls the appropriate templated update and write functions
 * to update the connected parameter or write its value in a tree
 */
template <typename T>
class SmartParameter : public ISmartParameter
{
public:
    /// Full constructor
    /// Connects a metadata object to a specific object
    SmartParameter(const ParameterMetadata& Metadata, T& Obj) :
        m_Metadata{ Metadata },
        m_Val{ Obj },
        m_Set{ false },
        m_IsDependant{ false },
        m_PrimaryParName{ "" }
    {}

    /// Destructor
    ~SmartParameter() noexcept = default;

    /// No copy allowed
    SmartParameter(const SmartParameter<T>& other) = delete;

    /// No assignment allowed
    SmartParameter<T>&
    operator=(const SmartParameter<T>& other) = delete;

    /// No movement allowed
    SmartParameter(SmartParameter<T>&& other) = delete;

    /// No move assignment allowed
    SmartParameter<T>&
    operator=(SmartParameter<T>&& other) = delete;

    /// Retrieves the parameter metadata
    const ParameterMetadata&
    GetMetadata() const noexcept override
    {
        return m_Metadata;
    }

    /// Access the metadata of the parameter
    ParameterMetadata&
    Metadata() noexcept override
    {
        return m_Metadata;
    }

    /// Sets the metadata of the connected parameter
    void
    SetMetadata(const ParameterMetadata& Meta) noexcept override
    {
        m_Metadata = Meta;
    }

    /// Updates the connected object from a tree
    void
    Update(const PropertyTree& ConfTree) override
    {
        m_Set = UpdateParameter(m_Val.get(), ConfTree, m_Metadata.GetName());
    }

    /// Writes the connected object to a Tree at the name recorded in the metadata
    void
    Write(PropertyTree& RootNode) const override
    {
        WriteToTree(m_Val.get(), RootNode, m_Metadata.GetName());
    }

    /// Returns a boolean specifying whether the connected parameter was contained
    /// in the input configuration tree and successfully configured
    bool
    IsSet() const noexcept override
    {
        return m_Set;
    }

    /// Defines whether a parameter was correctly configured in the last configuration run
    void
    Set(bool Set) noexcept override
    {
        m_Set = Set;
    }

    /**
     * Retrieves the physical unit multiplier for the parameter.
     * If the unit is 0, it implies no unit multiplier was set and needs to be used
     * @return
     */
    double
    GetUnitMultiplier() const noexcept override
    {
        return 0.0;
    }

    /**
     * Sets the unit physical multiplier for the parameter.
     * If this value is set to 0 then no unit is implied and no parameter adjustment will
     * be performed
     * @param Unit
     */
    void
    SetUnitMultiplier(double) noexcept override
    {}

    /**
     * Specifies whether this variable depends on a second one
     * @return
     */
    bool
    IsDependant() const noexcept override
    {
        return m_IsDependant;
    }

    /**
     * Specifies the name of the primary parameter on which this depends
     * @return
     */
    const std::string&
    GetPrimaryParameterName() const noexcept override
    {
        return m_PrimaryParName;
    }

    /**
     * Sets the name of the primary parameter on which this depends.
     * If the argument is an empty string it unset the dependency
     * @param Name
     */
    void
    SetPrimaryParameterName(const std::string& Name) noexcept override
    {
        m_PrimaryParName = Name;
        if (m_PrimaryParName.empty())
        {
            m_IsDependant = false;
        }
        else
        {
            m_IsDependant = true;
        }
    }

private:
    /// Metadata of the parameter
    ParameterMetadata m_Metadata;

    /// Pointer to the parameter to modify
    std::reference_wrapper<T> m_Val;

    /// Sets to true if configured correctly in the last run
    bool m_Set;

    /// Switch defining if a parameter is dependant
    bool m_IsDependant;

    /// Name of the parameter on the primary parameter of this one
    std::string m_PrimaryParName;
};

/**
 * A templated smart parameter which connects one metadata to a specific object
 * of a class.
 * The template then simply calls the appropriate templated update and write functions
 * to update the connected parameter or write its value in a tree
 */
template <typename T>
class SmartPhysicalParameter : public ISmartParameter
{
public:
    /// Full constructor
    /// Connects a metadata object to a specific object
    SmartPhysicalParameter(const ParameterMetadata& Metadata, T& Obj, double Unit) :
        m_Metadata{ Metadata },
        m_Val{ Obj },
        m_Set{ false },
        m_Unit{ Unit },
        m_IsDependant{ false },
        m_PrimaryParName{ "" }
    {}

    /// Destructor
    ~SmartPhysicalParameter() noexcept = default;

    /// No copy allowed
    SmartPhysicalParameter(const SmartPhysicalParameter<T>& other) = delete;

    /// No assignment allowed
    SmartPhysicalParameter<T>&
    operator=(const SmartPhysicalParameter<T>& other) = delete;

    /// No movement allowed
    SmartPhysicalParameter(SmartPhysicalParameter<T>&& other) = delete;

    /// No move assignment allowed
    SmartPhysicalParameter<T>&
    operator=(SmartPhysicalParameter<T>&& other) = delete;

    /// Retrieves the parameter metadata
    const ParameterMetadata&
    GetMetadata() const noexcept override
    {
        return m_Metadata;
    }

    /// Access the metadata of the parameter
    ParameterMetadata&
    Metadata() noexcept override
    {
        return m_Metadata;
    }

    /// Sets the metadata of the connected parameter
    void
    SetMetadata(const ParameterMetadata& Meta) noexcept override
    {
        m_Metadata = Meta;
    }

    /// Updates the connected object from a tree
    void
    Update(const PropertyTree& ConfTree) override
    {
        m_Set = UpdateParameter(m_Val.get(), ConfTree, m_Metadata.GetName());
        if (m_Set && m_Unit != 0.0)
        {
            (m_Val.get()) *= m_Unit;
        }
    }

    /// Writes the connected object to a Tree at the name recorded in the metadata
    void
    Write(PropertyTree& RootNode) const override
    {
        if (m_Unit != 0.0)
        {
            auto tmp = (m_Val.get()) / m_Unit;
            Configuration::WriteToTree(tmp, RootNode, m_Metadata.GetName());
        }
        else
            Configuration::WriteToTree(m_Val.get(), RootNode, m_Metadata.GetName());
    }

    /// Returns a boolean specifying whether the connected parameter was contained
    /// in the input configuration tree and successfully configured
    bool
    IsSet() const noexcept override
    {
        return m_Set;
    }

    /// Defines whether a parameter was correctly configured in the last configuration run
    void
    Set(bool Set) noexcept override
    {
        m_Set = Set;
    }

    /**
     * Retrieves the physical unit multiplier for the parameter.
     * If the unit is 0, it implies no unit multiplier was set and needs to be used
     * @return
     */
    double
    GetUnitMultiplier() const noexcept override
    {
        return m_Unit;
    }

    /**
     * Sets the unit physical multiplier for the parameter.
     * If this value is set to 0 then no unit is implied and no parameter adjustment will
     * be performed
     * @param Unit
     */
    void
    SetUnitMultiplier(double Unit) noexcept override
    {
        m_Unit = Unit;
    }

    /**
     * Specifies whether this variable depends on a second one
     * @return
     */
    bool
    IsDependant() const noexcept override
    {
        return m_IsDependant;
    }

    /**
     * Specifies the name of the primary parameter on which this depends
     * @return
     */
    const std::string&
    GetPrimaryParameterName() const noexcept override
    {
        return m_PrimaryParName;
    }

    /**
     * Sets the name of the primary parameter on which this depends.
     * If the argument is an empty string it unset the dependency
     * @param Name
     */
    void
    SetPrimaryParameterName(const std::string& Name) noexcept override
    {
        m_PrimaryParName = Name;
        if (m_PrimaryParName.empty())
        {
            m_IsDependant = false;
        }
        else
        {
            m_IsDependant = true;
        }
    }

private:
    /// Metadata of the parameter
    ParameterMetadata m_Metadata;

    /// Pointer to the parameter to modify
    std::reference_wrapper<T> m_Val;

    /// Sets to true if configured correctly in the last run
    bool m_Set;

    /// Physical unit multiplier
    double m_Unit;

    /// Switch defining if a parameter is dependant
    bool m_IsDependant;

    /// Name of the parameter on the primary parameter of this one
    std::string m_PrimaryParName;
};
} // namespace detail

/// This class stores a list of parameters to configure through a property tree and
/// can automatically perform such a configuration.
/// It is sufficient to initialize the list before updating the configuration of the
/// containing class with a set of configurable parameters, each one defined by the name of
/// the node where the value of the parameter should be searched, a description for
/// documentation purposes, and a reference to the object that should be updated
// DEVNOTE Currently this class behaves like a set to simply make sure that the parameters
// names are unique but that also means that the parameters are ordered by their name, rather
// than their insertion order. It may be useful to keep the ordering switching to a list and
// assert the uniqueness manually
class SmartParameterList
{
    /// The type of object actually contained in the comparison
    typedef std::unique_ptr<detail::ISmartParameter> contained_type;

    /// The prototype of the comparison function for the container
    typedef std::function<bool(const contained_type& lhs, const contained_type& rhs)> Compare;

    /// The actual comparison function
    static bool
    SetCompare(const contained_type& lhs, const contained_type& rhs)
    {
        return lhs->GetMetadata() < rhs->GetMetadata();
    }

    /// Aliasing of the internal container
    typedef std::set<contained_type, Compare> ParList;

    /// The iterator adaptor which automatically dereference the pointer of the parameter list
    template <bool is_const_iterator>
    class SmartParameterList_iterator
    {
    private:
        typedef ParList container_type;

        typedef
            typename std::conditional<is_const_iterator, typename container_type::const_iterator,
                typename container_type::iterator>::type InnerIterator;

        /// The internal iterator
        InnerIterator m_it;

        /// Declaring friend the other type of iterator
        friend SmartParameterList_iterator<!is_const_iterator>;

    public:
        typedef SmartParameterList_iterator<is_const_iterator> self_type;

        // Iterator typedefs, defining the iterator traits
        /// Iterator category
        typedef typename InnerIterator::iterator_category iterator_category;

        /// The type the iterator dereference to, that is the type managed by the smart pointer
        typedef typename InnerIterator::value_type::element_type value_type;

        /// The reference type of the iterated object
        typedef typename std::conditional<is_const_iterator, const value_type&, value_type&>::type
            reference;

        /// The pointer type of the iterated object
        typedef typename std::conditional<is_const_iterator, const value_type*, value_type*>::type
            pointer;

        /// Distance between iterators represented by a signed integer
        typedef typename InnerIterator::difference_type difference_type;

    public:
        /// Default constructor
        SmartParameterList_iterator() = default;

        /// Constructed with the inner iterator type
        SmartParameterList_iterator(InnerIterator It) : m_it{ It } {}

        /// Constructs a constant iterator with a non const one
        SmartParameterList_iterator(const SmartParameterList_iterator<false>& other) :
            m_it{ other.m_it }
        {}

        /// Destructor
        ~SmartParameterList_iterator() = default;

        /**
         * Returns the base iterator
         * @return
         */
        InnerIterator
        BaseIterator() const
        {
            return m_it;
        }

        /// Dereferencing to a reference to the managed object
        reference
        operator*() const
        {
            return (*(*m_it));
        }

        /// Indirection
        pointer
        operator->() const
        {
            return m_it->get();
        }

        /// Pre-increment operator
        self_type&
        operator++()
        {
            ++m_it;
            return *this;
        }

        /// Post-increment operator
        self_type
        operator++(int)
        {
            auto tmp = *this;
            ++m_it;
            return tmp;
        }

        /// Equality
        bool
        operator==(const self_type& R) const
        {
            return (m_it == R.m_it);
        }

        /// Diversity
        bool
        operator!=(const self_type& R) const
        {
            return (m_it != R.m_it);
        }

        /// Pre-decrement operator (for bidirectional iterators)
        self_type&
        operator--()
        {
            --m_it;
            return *this;
        }

        /// Post-decrement operator (for bidirectional iterators)
        self_type
        operator--(int)
        {
            auto tmp = *this;
            --m_it;
            return tmp;
        }

        /// Swap 2 iterators
        friend void
        swap(self_type& L, self_type& R)
        {
            std::swap(L.m_it, R.m_it);
        }
    };

public:
    // STL like aliases
    // typedef ParList::value_type             value_type;
    typedef contained_type::element_type value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef ParList::size_type size_type;
    typedef ParList::difference_type difference_type;

    typedef SmartParameterList_iterator<false> iterator;
    typedef SmartParameterList_iterator<true> const_iterator;
    /// Reverse iterator
    typedef std::reverse_iterator<iterator> reverse_iterator;
    /// Constant reverse iterator
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

public:
    /**
     * Default constructor.
     * It creates an empty list.
     */
    SmartParameterList() noexcept : m_Par{ SmartParameterList::SetCompare }, m_Initialized{ false }
    {}

    /// Destructor (class is not further extensible)
    ~SmartParameterList() noexcept = default;

    /**
     * Copy construction.
     * NOTE The list is not copied but a new empty one is created.
     * The smart parameter list is directly connected to the members of a specific
     * objects so it cannot be copied but should rather be reinitialized by relinking
     * it to the new objects.
     * To avoid having all classes manually defining the copy and assignment operators
     * only because of this object, the copy is not strictly forbidden but the user of
     * the class should reinitialize the list after construction or before its usage.
     * Follow the instruction on the wiki for more details
     * @param other
     */
    SmartParameterList(const SmartParameterList& other) = delete;

    /**
     * Assignment not allowed
     */
    SmartParameterList&
    operator=(const SmartParameterList& other) = delete;

    /**
     * Movement not allowed
     */
    SmartParameterList(SmartParameterList&& other) = delete;

    /**
     * Move assignment not allowed
     */
    SmartParameterList&
    operator=(SmartParameterList&& other) = delete;

    /**
     * Adding new connected parameters to the list
     * DEVNOTE To be able to use the AddParameter in a const function I need to eliminate
     * the CV qualifiers of the incoming object. This is ok because in the registration phase
     * we are not going to modify the parameters anyway, only connect them.
     * On the other hand it's important that the original object which is connected is not
     * constant in all contexts otherwise the behavior is undefined
     *
     * @param Name
     * @param Description
     * @param Obj
     * @return
     */
    template <typename T>
    bool
    AddParameter(const std::string& Name, const std::string& Description, T& Obj)
    {
        using AdaptedT = typename std::remove_cv<T>::type;
        m_Initialized = true;
        return m_Par
            .insert(contained_type{ new detail::SmartParameter<AdaptedT>(
                detail::ParameterMetadata{ Name, Description }, const_cast<AdaptedT&>(Obj)) })
            .second;
    }

    /**
     * Adds a new parameter to the list connected to a fix unit multiplier
     * NOTE To be able to use the AddParameter in a const function I need to eliminate
     * the CV qualifiers of the incoming object
     * @param Name
     * @param Description
     * @param Obj
     * @return
     */
    template <typename T>
    bool
    AddPhysicalParameter(
        const std::string& Name, const std::string& Description, T& Obj, double Unit)
    {
        using AdaptedT = typename std::remove_cv<T>::type;
        m_Initialized = true;
        return m_Par
            .insert(contained_type{ new detail::SmartPhysicalParameter<AdaptedT>{
                detail::ParameterMetadata{ Name, Description }, const_cast<AdaptedT&>(Obj), Unit } })
            .second;
    }

    /**
     * Sets two parameters to depend from each other.
     * If either of them is not already registered will throw an exception
     * @param Primary
     * @param Dependant
     */
    void
    SetParameterDependency(const std::string& Primary, const std::string& Dependant)
    {
        if (find(Primary) == end())
        {
            InvalidArgument e{ "SmartParameterList", BOOST_CURRENT_FUNCTION,
                fmt::format("No parameter registered with the name {}."
                            " The parameter dependency cannot be set.",
                    Primary) };
            e.raise();
        }

        auto itPar = find(Dependant);
        if (itPar != end())
        {
            itPar->SetPrimaryParameterName(Primary);
        }
        else
        {
            InvalidArgument e{ "SmartParameterList", BOOST_CURRENT_FUNCTION,
                fmt::format("No parameter registered with the name {}."
                            " The parameter dependency cannot be set.",
                    Dependant) };
            e.raise();
        }
    }

    /**
     * Automatically update all the registered parameters with the contents of the
     * configuration tree
     * @param ConfTree
     */
    void
    UpdateAll(const Configuration::PropertyTree& ConfTree)
    {
        if (!m_Initialized)
        {
            RequirementFailure e{ "SmartParameterList", BOOST_CURRENT_FUNCTION,
                "The smart parameter list was not initialized in the "
                "constructor of the containing class. The updating of the configuration "
                "parameters through it is not possible" };
            e.raise();
        }

        for (auto& Par : m_Par)
        {
            if (!Par->IsDependant())
                Par->Update(ConfTree);
            else
            {
                bool PrimaryExisting = ConfTree.count(Par->GetPrimaryParameterName());
                if (!PrimaryExisting)
                    Par->Update(ConfTree);
            }
        }
    }

    /**
     * Fills a property tree with the data of the registered parameters
     * @return
     */
    PropertyTree
    CreateTree() const
    {
        if (!m_Initialized)
        {
            RequirementFailure e{ "SmartParameterList", BOOST_CURRENT_FUNCTION,
                "The smart parameter list was not initialized in the "
                "constructor. The populated tree will be empty and not reflect the current "
                "status of the class instance" };
            e.raise();
        }

        Configuration::PropertyTree ans;
        for (auto& Par : m_Par)
        {
            if (!Par->IsDependant())
                Par->Write(ans);
        }

        return ans;
    }

    /// Marks the parameter list as initialized even if empty
    void
    MarkInitialized() noexcept
    {
        m_Initialized = true;
    }

    /// Checks if the list was marked as initialized
    bool
    IsInitialized() const noexcept
    {
        return m_Initialized;
    }

    // STL-Style function

    /// Iterators to the beginning of the list
    iterator
    begin() noexcept
    {
        return iterator{ m_Par.begin() };
    }

    const_iterator
    begin() const noexcept
    {
        return const_iterator{ m_Par.begin() };
    }

    const_iterator
    cbegin() const noexcept
    {
        return const_iterator{ m_Par.cbegin() };
    }

    /// Iterator past the end of the list
    iterator
    end() noexcept
    {
        return iterator{ m_Par.end() };
    }

    const_iterator
    end() const noexcept
    {
        return const_iterator{ m_Par.end() };
    }

    const_iterator
    cend() const noexcept
    {
        return const_iterator{ m_Par.cend() };
    }

    /// reverse iterator to the back of the list
    reverse_iterator
    rbegin() noexcept
    {
        return reverse_iterator{ end() };
    }

    const_reverse_iterator
    rbegin() const noexcept
    {
        return const_reverse_iterator{ end() };
    }

    const_reverse_iterator
    crbegin() const noexcept
    {
        return const_reverse_iterator{ cend() };
    }

    /// Reverse iterators past the beginning of the list
    reverse_iterator
    rend() noexcept
    {
        return reverse_iterator{ begin() };
    }

    const_reverse_iterator
    rend() const noexcept
    {
        return const_reverse_iterator{ begin() };
    }

    const_reverse_iterator
    crend() const noexcept
    {
        return const_reverse_iterator{ cbegin() };
    }

    /// Returns the size of the list
    size_t
    size() const noexcept
    {
        return m_Par.size();
    }

    /// Check if the parameter list contains anything
    bool
    empty() const noexcept
    {
        return m_Par.empty();
    }

    /// Empties the parameter list
    void
    clear() noexcept
    {
        m_Par.clear();
        m_Initialized = false;
    }

    /// Finds a parameter with the given name if existing. Returns the end iterator otherwise
    const_iterator
    find(const std::string& Name) const
    {
        auto it = const_iterator{ std::find_if(
            m_Par.begin(), m_Par.end(), [&Name](const ParList::value_type& Par) {
                return (Par->GetMetadata().GetName() == Name);
            }) };
        return it;
    }

    /// Finds a parameter with the given name if existing. Returns the end iterator otherwise
    iterator
    find(const std::string& Name)
    {
        auto it = iterator{ std::find_if(
            m_Par.begin(), m_Par.end(), [&Name](const ParList::value_type& Par) {
                return (Par->GetMetadata().GetName() == Name);
            }) };
        return it;
    }

    /// Access a parameter in the list. Throws an exception if not found
    const_reference
    at(const std::string& Name) const
    {
        auto it = find(Name);
        if (it == end())
        {
            ObjectNotFound e{ "SmartParameterList", BOOST_CURRENT_FUNCTION,
                fmt::format("Object named {} not registered in the smart parameter list", Name) };
            e.raise();
        }
        return *it;
    }

    /// Access a parameter in the list. Throws an exception if not found
    reference
    at(const std::string& Name)
    {
        auto it = find(Name);
        if (it == end())
        {
            ObjectNotFound e{ "SmartParameterList", BOOST_CURRENT_FUNCTION,
                fmt::format("Object named {} not registered in the smart parameter list", Name) };
            e.raise();
        }
        return *it;
    }



private:
    /// The internal container of the parameters
    ParList m_Par;

    /// Marks the list as initialized.
    /// This means that at least one parameter was registered or that it was manually
    /// set as initialized (in those cases where the list is inherited but should be
    /// kept empty). When the parameter is true some of the warning messages are deactivated
    bool m_Initialized;
};


} // namespace MXWare::Configuration

#endif // MXWARE_CONFIGURATION_SMARTPARAMETERLIST_HH_