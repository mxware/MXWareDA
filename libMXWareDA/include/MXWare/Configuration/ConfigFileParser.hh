/*
 * MessageSerializer.hh
 *
 *  Created on: Sep 2, 2019
 *      Author: caiazza
 */

#ifndef MXWARE_CORE_CONFIGFILEPARSER_HH_
#define MXWARE_CORE_CONFIGFILEPARSER_HH_
#pragma once

#include <iostream>
#include <string>
#include <memory>
#include <functional>

#include "MXWare/Definitions.hh"
#include "MXWare/Configuration.hh"
#include "MXWare/Utilities/propagate_const.hh"


#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(push)
#pragma warning(disable : 4251)

#endif

namespace MXWare::Configuration
{
/// This class provides the functionality to serialize and deserialize a configuration tree
/// to and from an input stream depending on the specified input/output format
/// The format is specified by a string tag.
/// There is always at least one (de)serialization option which is the XML.
/// The user can add more (de)serializer at runtime
/// ??? It is guaranteed that for each tag there is both a serializer and deserializer ???
/// The (de)serializer function are supposed to be stateless and acts uniformily on all messages
/// That also means that we assume the serializing functions are intrinsically thread safe
class MXAPI ConfigFileParser
{
public:
    ///@{
    /// The gang of five and swap
    ConfigFileParser();

    ~ConfigFileParser();

    ConfigFileParser(const ConfigFileParser& other);

    ConfigFileParser&
    operator=(ConfigFileParser other);

    ConfigFileParser(ConfigFileParser&& other);

    friend MXAPI void
    swap(ConfigFileParser& lhs, ConfigFileParser& rhs) noexcept;
    ///@}

    /// Reads the input stream and formats it into configuration tree using the selected format
    /// Returns the input stream after all the necessary characters have been extracted
    /// If the input stream cannot be decoded the tree will not be changed and the input stream
    /// failbit will be set to true.
    /// If the format required is not registered in the object, the stream will not be decoded
    /// and will be returned untouched
    std::istream&
    Read(std::istream& In, PropertyTree& Tree, const std::string FormatTag) const;

    /// Reads a stream using the default parser (XML)
    std::istream&
    Read(std::istream& In, PropertyTree& Tree) const;

    /// Writes the content of the configuration stream to the output
    /// Returns the output stream
    std::ostream&
    Write(std::ostream& Out, const PropertyTree& Tree, const std::string& FormatTag) const;

    /// Writes the stream using the default parse
    std::ostream&
    Write(std::ostream& Out, const PropertyTree& Tree) const;

private:
    // DEVNOTE This class uses the Pimpl idiom rather than an abstract interface because I want the
    // serializer to be a concrete object which manages the list of serialization functions. The
    // task of this class is to dispatch the right function call given the format tag. One advantage
    // of this approach is that the serializer can be copied around and whatever we do in the
    // implementation won't break the ABI. For the core classes of the framework I'm trying to stick
    // to this last point as much as possible. Another option could be to have an abstract interface
    // with an associated factory which constructs the required implementations based on the format
    // tag Further reading:
    // https://softwareengineering.stackexchange.com/questions/213259/whats-is-the-point-of-pimpl-pattern-while-we-can-use-interface-for-the-same-pur

    /// Private implementation of the class
    class Impl;

    /// Pointer to the implementation
    std::experimental::propagate_const<std::unique_ptr<Impl>> m_impl;
};

MXAPI void
swap(ConfigFileParser& lhs, ConfigFileParser& rhs) noexcept;
} // namespace MXWare::Configuration

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#pragma warning(pop)

#endif


#endif /* MXWARE_CORE_CONFIGFILEPARSER_HH_ */
