#ifndef MXWARE_CONFIGURATION_CONFIGURABLE_HH_
#define MXWARE_CONFIGURATION_CONFIGURABLE_HH_
#pragma once

#include "MXWare/Configuration/PropertyTree.hh"

namespace MXWare::Configuration
{
/**
 * Basic interface to serialize the configuration state of a class to a configuration tree.
 * This is the most simple case which makes the class developer responsible for the
 * definition of all the necessary code for the input and output.
 * All the classes in this hierarchy can be configured with a property tree, defined by the
 * class @class PropertyTree and through the same type of objects they can return their
 * configuration status.
 * Additionally every class of this type has to provide a list of the parameter metadata
 * which helps any program using those object to handle the configuration parameters.
 */
class Configurable
{
public:
    /// Default virtual destructor
    virtual ~Configurable() noexcept = default;

    /// Configures the concrete class through a property tree
    virtual void
    Configure(const PropertyTree& ConfTree) = 0;

    /**
     * The concrete class should populate a property tree with its internal configuration
     * parameters
     * @return
     */
    virtual PropertyTree
    CurrentConfiguration() const = 0;
};
} // namespace MXWare::Configuration

#endif /*MXWARE_CORE_UTILITIES_CONFIGURABLE_HH_*/
