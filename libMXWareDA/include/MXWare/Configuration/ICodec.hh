#ifndef MXWARE_CONFIGURATION_ICODEC_HH_
#define MXWARE_CONFIGURATION_ICODEC_HH_
#pragma once

#include "MXWare/Configuration/PropertyTree.hh"
#include "MXWare/Configuration/Configurable.hh"
#include <istream>
#include <ostream>

namespace MXWare::Configuration
{
/// Interface for classes providing an encoder and decoder 
/// for the configuration tree
class ICodec: public Configurable
{
public:
    virtual ~ICodec() = default;

    /// Reads the input stream and formats it into configuration tree which should be
    /// already allocated
    /// Returns the input stream after all the necessary characters have been extracted
    /// If the input stream cannot be decoded the tree will not be changed and the input stream
    /// failbit will be set to true.
    virtual std::istream&
    Read(std::istream& In, PropertyTree& Tree) const = 0;

    /// Writes the content of an existing configuration tree to an output stream
    virtual std::ostream&
    Write(std::ostream& Out, const PropertyTree& Tree) const = 0;
};

}

#endif // MXWARE_CONFIGURATION_ICODEC_HH_