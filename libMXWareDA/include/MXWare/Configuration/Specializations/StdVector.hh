#ifndef MXWARE_CONFIGURATION_SPECIALIZATIONS_STDVECTOR_HH_
#define MXWARE_CONFIGURATION_SPECIALIZATIONS_STDVECTOR_HH_
#pragma once

#include "MXWare/Configuration/Traits.hh"

#include <vector>

namespace MXWare::Configuration::Traits
{
template <typename T, class Allocator>
struct UpdateMode<std::vector<T, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::vector<T, Allocator>::value_type;
};

} // namespace MXWare::Configuration::Traits

#endif // MXWARE_CONFIGURATION_SPECIALIZATIONS_STDVECTOR_HH_
