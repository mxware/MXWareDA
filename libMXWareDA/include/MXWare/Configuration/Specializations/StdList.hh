#ifndef MXWARE_CONFIGURATION_SPECIALIZATIONS_STDLIST_HH_
#define MXWARE_CONFIGURATION_SPECIALIZATIONS_STDLIST_HH_
#pragma once

#include "MXWare/Configuration/Traits.hh"

#include <list>

namespace MXWare::Configuration::Traits
{

template <typename T, class Allocator>
struct UpdateMode<std::list<T, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::list<T, Allocator>::value_type;
};
} // namespace MXWare::Configuration::Traits

#endif // MXWARE_CONFIGURATION_SPECIALIZATIONS_STDLIST_HH_