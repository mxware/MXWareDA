#ifndef MXWARE_CONFIGURATION_SPECIALIZATIONS_STDMAP_HH_
#define MXWARE_CONFIGURATION_SPECIALIZATIONS_STDMAP_HH_
#pragma once

#include "MXWare/Configuration/Traits.hh"

namespace MXWare::Configuration::Traits
{
template <typename Key, typename T, class Compare, class Allocator>
struct UpdateMode<std::map<Key, T, Compare, Allocator>>
{
    using tag = Tags::AssociativeContainer;
    using value = typename std::map<Key, T, Compare, Allocator>::mapped_type;
    using key = typename std::map<Key, T, Compare, Allocator>::key_type;
};

template <typename Key, typename T, class Compare, class Allocator>
struct UpdateMode<std::multimap<Key, T, Compare, Allocator>>
{
    using tag = Tags::AssociativeContainer;
    using value = typename std::multimap<Key, T, Compare, Allocator>::mapped_type;
    using key = typename std::multimap<Key, T, Compare, Allocator>::key_type;
};

template <typename Key, typename T, class Hash, class KeyEqual, class Allocator>
struct UpdateMode<std::unordered_map<Key, T, Hash, KeyEqual, Allocator>>
{
    using tag = Tags::AssociativeContainer;
    using value = typename std::unordered_map<Key, T, Hash, KeyEqual, Allocator>::mapped_type;
    using key = typename std::unordered_map<Key, T, Hash, KeyEqual, Allocator>::key_type;
};

template <typename Key, typename T, class Hash, class KeyEqual, class Allocator>
struct UpdateMode<std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>>
{
    using tag = Tags::AssociativeContainer;
    using value = typename std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>::mapped_type;
    using key = typename std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>::key_type;
};
}
#endif // MXWARE_CONFIGURATION_SPECIALIZATIONS_STDMAP_HH_