#ifndef MXWARE_CONFIGURATION_SPECIALIZATIONS_STDSET_HH_
#define MXWARE_CONFIGURATION_SPECIALIZATIONS_STDSET_HH_
#pragma once

#include "MXWare/Configuration/Traits.hh"

#include <set>
#include <unordered_set>

namespace MXWare::Configuration::Traits
{
template <typename T, class Compare, class Allocator>
struct UpdateMode<std::set<T, Compare, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::set<T, Compare, Allocator>::value_type;
};

template <typename T, class Compare, class Allocator>
struct UpdateMode<std::multiset<T, Compare, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::multiset<T, Compare, Allocator>::value_type;
};

template <typename T, class Hash, class KeyEqual, class Allocator>
struct UpdateMode<std::unordered_set<T, Hash, KeyEqual, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::unordered_set<T, Hash, KeyEqual, Allocator>::value_type;
};

template <typename T, class Hash, class KeyEqual, class Allocator>
struct UpdateMode<std::unordered_multiset<T, Hash, KeyEqual, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::unordered_multiset<T, Hash, KeyEqual, Allocator>::value_type;
};

}

#endif //MXWARE_CONFIGURATION_SPECIALIZATIONS_STDSET_HH_