#ifndef MXWARE_CONFIGURATION_SPECIALIZATIONS_STDDEQUE_HH_
#define MXWARE_CONFIGURATION_SPECIALIZATIONS_STDDEQUE_HH_
#pragma once

#include "MXWare/Configuration/Traits.hh"

#include <deque>

namespace MXWare::Configuration::Traits
{
template <typename T, class Allocator>
struct UpdateMode<std::deque<T, Allocator>>
{
    using tag = Tags::SequentialContainer;
    using value = typename std::deque<T, Allocator>::value_type;
};
}

#endif // MXWARE_CONFIGURATION_SPECIALIZATIONS_STDDEQUE_HH_
