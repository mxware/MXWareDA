#ifndef MXWARE_CONFIGURATION_AUTOCONFIGURATION_HH_
#define MXWARE_CONFIGURATION_AUTOCONFIGURATION_HH_
#pragma once

#include "MXWare/Configuration/PropertyTree.hh"
#include "MXWare/Configuration/Traits.hh"
#include "MXWare/Extensions.hh"

namespace MXWare::Configuration
{
/* ***********************************************************
 *
 *  Automatic variable update functions
 *  The updating system is based on a common function called
 *  UpdateParameter which is templated on the type of object
 *  to update. The type of object determines the parameter tag
 *  which, in turn, determine the dispatch function to call
 *
 *********************************************************** */

/**
 * Forward declaration of the parameter update function
 * If the @param NodeName is empty the current tree will be used for the configuration,
 * otherwise it will look for a child with the give name to be used for the configuration,
 * if that child exists.
 * The users can overload this function for specific types when a special behaviour
 * is required, in particular when an additional parameter other than the configuration
 * tree must be passed to the object to be updated
 * @param Obj The object to update
 * @param Tree The parent tree containing the node where to look for the data
 * @param NodeName The name of the node to query
 */
template <typename T, typename... Args>
bool
UpdateParameter(T& Obj, const PropertyTree& Tree, const std::string& NodeName, Args&&... args);

/**
 * Forward declaration of the generic function which writes the configuration tree for an object
 * If the @param NodeName is left empty the system will replace or update the @param Tree object
 * otherwise will add a new child with the provided name where to store the data
 * @param Obj
 * @param Tree
 * @param NodeName
 * @param Overwrite
 */
template <typename T>
void
WriteToTree(const T& Obj, PropertyTree& Tree, const std::string& NodeName = std::string{},
    bool Overwrite = true);

namespace detail
{
constexpr auto ObjectTypeTag = "type";
constexpr auto ObjectNameTag = "name";
constexpr auto ContainerValueTag = "val";
constexpr auto ContainerKeyTag = "key";
constexpr auto ContainerRecordTag = "rec";

/**
 * Update function for simple parameter types
 * The object is directly streamed from a tree.
 * The current value of the object is used as default
 * The boolean parameter specifies whether one should look for the node name or
 * update from the input tree itself. This is necessary to use the system with the
 * container types.
 * By default the system checks for the presence of the value to configure among the tree
 * elements. If the value is not find there it will also search among the xml attributes
 *
 * DEV NOTE I think the default value is never usable in this context
 * because I check if I can find the node anyway but maybe, in case the data
 * string is empty or the conversion fails it is used as a backup. In any case it's
 * not harmful so it's better to keep it for safety
 *
 * @param Obj
 * @param Tree
 * @param NodeName
 * @param Tag
 * @param FromChild Specifies whether one has to look from the node name or update from the tree
 * itself
 */
template <typename T>
bool
UpdateSimpleParameter(T& Obj, const PropertyTree& Tree, const std::string& NodeName)
{
    if (!NodeName.empty())
    {
        if (auto OptVal = Tree.get_optional<T>(NodeName); OptVal)
        {
            Obj = OptVal.get();
            return true;
        }
        return false;
    }
    else
    {
        Obj = Tree.get_value(Obj);
        return true;
    }
}

/**
 * @brief Writes the configuration of a simple object to a property tree
 *
 * Writes the configuration of a simple object, that is an object for which
 * there is already a translator to the property tree internal type, to a child
 * of the tree labelled by NodeName. If that label is empty the data will be
 * written in the tree itself
 */
template <typename T>
void
WriteSimpleParameter(const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
{
    if (NodeName.empty())
    {
        // In the case of an empty node name I can only overwrite
        Tree.put_value(Obj);
    }
    else
    {
        if (Overwrite)
            Tree.put(NodeName, Obj);
        else
            Tree.add(NodeName, Obj);
    }
}

/**
 * @brief Updates a parameter which has the correct Configure signature
 */
template <typename T>
bool
UpdateConfigurableParameter(T& Obj, const PropertyTree& Tree, const std::string& NodeName)
{
    if (!NodeName.empty())
    {
        if (auto OptTree = Tree.get_child_optional(NodeName); OptTree)
        {
            Obj.Configure(OptTree.get());
            return true;
        }
        return false;
    }
    else
    {
        Obj.Configure(Tree);
        return true;
    }
}

/**
 * @brief Writes a parameter with the Configurable signature
 */
template <typename T>
void
WriteConfigurableParameter(
    const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
{
    if (NodeName.empty())
    {
        // In case of no nodename we always overwrite the whole tree
        Tree = Obj.CurrentConfiguration();
    }
    else
    {
        if (Overwrite)
            Tree.put_child(NodeName, Obj.CurrentConfiguration());
        else
            Tree.add_child(NodeName, Obj.CurrentConfiguration());
    }
}

/**
 * @brief Update a parameter of pointer type
 */
template <typename T, typename... Args>
bool
UpdatePointerParameter(
    T& Obj, const PropertyTree& Tree, const std::string& NodeName, Args&&... args)
{
    const PropertyTree* TreeRef = nullptr;
    if (!NodeName.empty())
    {
        if (auto OptTree = Tree.get_child_optional(NodeName); OptTree)
        {
            TreeRef = &OptTree.get();
        }
        else
        {
            return false;
        }
    }
    else
    {
        TreeRef = &Tree;
    }

    using PointedType = typename std::pointer_traits<T>::element_type;
    if (auto OptType = TreeRef->get_optional<std::string>(detail::ObjectTypeTag); OptType)
    {
        auto NewUnique = Extensions::CreateObject<PointedType>(
            *OptType, std::forward<Args>(args)...);
        // No object of the required type in the factory
        if (!NewUnique)
        {
            return false;
        }
        UpdateParameter(*NewUnique, *TreeRef, "");

        // If what I need is not a unique_ptr I have to convert it
        if constexpr (!std::is_same<T, std::unique_ptr<PointedType>>::value)
        {
            Obj = T{NewUnique.release()};
            return true;
        }
        else
        {
            Obj = std::move(NewUnique);
            return true;
        }
    }
    else
    {
        // Cannot configure a null pointer without a factory function
        if (!Obj)
        {
            return false;
        }
        return UpdateParameter(*Obj, *TreeRef, "");
    }
}

/**
 * @brief Writes the configuration of a pointer-like object.
 *
 * If the tyoe is registered in the extensions system it will store the
 * registration name under the type attribute
 */
template <typename T>
void
WritePointerParameter(const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
{
    if (Obj)
    {
        PropertyTree SubTree{};
        WriteToTree(*Obj, SubTree, "");
        if(auto TypeName = Extensions::GetRegistration(*Obj); 
            TypeName.has_value() && SubTree.count(detail::ObjectTypeTag) == 0)
        {
            SubTree.put(detail::ObjectTypeTag, *std::move(TypeName));
        }        

        if (NodeName.empty())
        {
            // In case of no nodename we always overwrite the whole tree
            Tree = SubTree;
        }
        else
        {
            if (Overwrite)
                Tree.put_child(NodeName, SubTree);
            else
                Tree.add_child(NodeName, SubTree);
        }
    }
}

///@{
/// Update and write functions for sequential types
/// In the update mode, if a NodeName is provided, the algorithm will look for all
/// the nodes with that name in the tree and, in each of those, either look for
/// all the attributes named "val" or use the entire subnode to create the
/// object to fill in the container. If no NodeName is provided the "val" subnodes
/// are looked for in the current tree. Note that the input container is always
/// cleared, even if no objects to include are found, therefore this always returns true
/// In the write mode, if a Nodename is given, a single node with the given name
/// is created and filled with as many subnodes tagged "val" as there are elements
/// in the container. If the overwrite parameter is set to true, all the other nodes
/// in the tree with the same name are removed. If no NodeName is provided the provided
/// tree is replaced with one containing all the "val" subnodes

template <typename T, typename... Args>
bool
UpdateSequentialContainerParameter(
    T& Obj, const PropertyTree& Tree, const std::string& NodeName, Args&&... args)
{
    // Lambda to fill an element from the content of a tree
    auto FillUnnamedTreeEntry = [&](const PropertyTree& SubTree, T& Container) -> bool {
        Traits::UpdateContainedValue_T<T> Element{};
        auto res = UpdateParameter(Element, SubTree, "", std::forward<Args>(args)...);
        if(res){Container.insert(Container.cend(), std::move(Element));}
        return res;
    };

    // Fills the content of the container from the subelements named "val"
    auto FillValEntries = [&](const PropertyTree& SubTree, T& Container) -> bool {
        auto ValRange = SubTree.equal_range(detail::ContainerValueTag);
        auto res = false;
        for (auto it = ValRange.first; it != ValRange.second; ++it)
        {
            res = FillUnnamedTreeEntry(it->second, Container) || res;
        }
        return res;
    };

    // Recursive lambda to fill the container of a generic tree path
    std::function<bool(const PropertyTree&, const std::string&, T&)> FillNodeIterative =
        [&](const PropertyTree& Subtree, const std::string& ReducedName, T& Container) -> bool{
            PropertyTree::path_type Path{ ReducedName };
            bool res = false;
            if (Path.single())
            {
                auto NodeRange = Subtree.equal_range(ReducedName);
                for (auto it = NodeRange.first; it != NodeRange.second; ++it)
                {
                    if (it->second.count(detail::ContainerValueTag) > 0)
                    {
                        res = FillValEntries(it->second, Container) || res;
                    }
                    else
                    {
                        res = FillUnnamedTreeEntry(it->second, Container) || res;
                    }
                }
            }
            else
            {
                auto PathBase = Path.reduce();
                auto PathRest = ReducedName.substr(ReducedName.find_first_of(Path.separator()) + 1);
                auto BaseRange = Subtree.equal_range(PathBase);
                for (auto it = BaseRange.first; it != BaseRange.second; ++it)
                {
                    res = FillNodeIterative(it->second, PathRest, Container) || res;
                }
            }
            return res;
        };

    if (Tree.empty() && Tree.data().empty())
    {
        return false;
    }

    T NewContainer{};
    bool res = false;
    // The actual function calls
    if (NodeName.empty())
    {
        if (Tree.count(detail::ContainerValueTag) > 0)
        {
            res = FillValEntries(Tree, NewContainer);
        }
        else
        {
            res = FillUnnamedTreeEntry(Tree, NewContainer);
        }
    }
    else
    {
        if (Tree.get_child_optional(NodeName))
        {
            res = FillNodeIterative(Tree, NodeName, NewContainer);
        }
    }
    if(res)
    {
        using std::swap;
        swap(NewContainer, Obj);
    }
    return res;
}

template <typename T>
void
WriteSequentialContainerParameter(
    const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
{
    PropertyTree Out{};
    for (auto& Element : Obj)
    {
        WriteToTree(Element, Out, detail::ContainerValueTag, false);
    }

    if (NodeName.empty())
    {
        Tree = Out;
    }
    else
    {
        if (Overwrite)
        {
            Tree.put_child(NodeName, Out);
        }
        else
        {
            Tree.add_child(NodeName, Out);
        }
    }
}
///@}

///@{
/// Update and write functions for associative containers
/// An associative container is composed of a sequence of records, each of
/// them represented by a key and a matching object.
/// The syntax to represent them in the configuration tree is similar to that
/// defined for the sequential containers but for the fact that each record
/// to be configured there must be an element in the same tree labeled "key"
/// which will be used to create the specific key for the record.
/// As in the case of the sequential container each element in the configuration
/// tree labeled by the name @param NodeName can contain a single record,
/// in which case the whole body of the element will be used to configure the
/// record, or multiple records, in which case each record must be enclosed in
/// an element labeled "val"
/// Note that if at least one "val" element exists in a tree, the system will
/// only insert those elements and the rest of the data in the tree will not be
/// considered.
/// The write function always encodes the data in the multi-element form.
/// A minor constraint is that the Key element must be default constructible
/// because the additional construction arguments are only forwarded to the
/// mapped value.
/// Moreover the parameter trait must define a value type which can be constructed
/// as an std pair and that can be used as argument to the insert function,
/// like for the STL associative containers
template <typename T, typename... Args>
bool
UpdateAssociativeContainerParameter(
    T& Container, const PropertyTree& Tree, const std::string& NodeName, Args&&... args)
{
    auto FillUnnamedEntry = [&](const PropertyTree& SubTree, T& Out) -> bool {
        Traits::UpdateAssociativeKey_T<T> theKey{};
        if (!UpdateParameter(theKey, SubTree, detail::ContainerKeyTag))
        {
            return false;
        }

        Traits::UpdateContainedValue_T<T> theElement;
        bool Updated;
        if (SubTree.count(detail::ContainerValueTag) > 0)
        {
            Updated = UpdateParameter(
                theElement, SubTree, detail::ContainerValueTag, std::forward<Args>(args)...);
        }
        else
        {
            Updated = UpdateParameter(theElement, SubTree, "", std::forward<Args>(args)...);
        }
        if (Updated)
        {
            // TODO Check for duplicates
            Out.insert(std::pair{ theKey, theElement });
        }
        return Updated;
    };

    auto FillRecordEntries = [&](const PropertyTree& SubTree, T& Out) -> bool {
        bool ans = false;
        auto RecRange = SubTree.equal_range(detail::ContainerRecordTag);
        for (auto it = RecRange.first; it != RecRange.second; ++it)
        {
            ans |= FillUnnamedEntry(it->second, Out);
        }
        return ans;
    };

    std::function<bool(const PropertyTree&, T&, const std::string&)> FillNamedIterative =
        [&](const PropertyTree& Subtree, T& Out, const std::string& ReducedName) {
            bool ans = false;
            PropertyTree::path_type Path{ ReducedName };
            if (Path.single())
            {
                auto NodeRange = Tree.equal_range(ReducedName);
                for (auto it = NodeRange.first; it != NodeRange.second; ++it)
                {
                    if (it->second.count(detail::ContainerRecordTag) > 0)
                    {
                        ans |= FillRecordEntries(it->second, Out);
                    }
                    else
                    {
                        ans |= FillUnnamedEntry(it->second, Out);
                    }
                }
            }
            else
            {
                auto PathBase = Path.reduce();
                auto PathRest = ReducedName.substr(ReducedName.find_first_of(Path.separator()) + 1);
                auto BaseRange = Subtree.equal_range(PathBase);
                for (auto it = BaseRange.first; it != BaseRange.second; ++it)
                {
                    return FillNamedIterative(it->second, Out, PathRest);
                }
            }
            return ans;
        };

    // If the input tree is empty there is nothing to update
    if (Tree.empty() && Tree.data().empty())
    {
        return false;
    }

    T tmp;
    bool ans;
    if (NodeName.empty())
    {
        // No nodename means that we are going to interpret
        // the content of the input tree directly
        if (Tree.count(detail::ContainerRecordTag) > 0)
        {
            ans = FillRecordEntries(Tree, tmp);
        }
        else
        {
            ans = FillUnnamedEntry(Tree, tmp);
        }
    }
    else
    {
        ans = FillNamedIterative(Tree, tmp, NodeName);
    }

    if (ans)
    {
        std::swap(tmp, Container);
    }
    return ans;
}

template <typename T>
void
WriteAssociativeContainerParameter(
    const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
{
    PropertyTree Out{};
    for (auto& Element : Obj)
    {
        PropertyTree ElementTree{};
        WriteToTree(Element.first, ElementTree, detail::ContainerKeyTag, false);
        WriteToTree(Element.second, ElementTree, detail::ContainerValueTag, false);
        Out.add_child(detail::ContainerRecordTag, std::move(ElementTree));
    }

    if (NodeName.empty())
    {
        Tree = std::move(Out);
    }
    else
    {
        if (Overwrite)
        {
            Tree.put_child(NodeName, Out);
        }
        else
        {
            Tree.add_child(NodeName, Out);
        }
    }
}

///@}
} // namespace detail

namespace Strategy
{
template <typename T, typename Enable = void>
struct Update
{
    using target_t = T;

    template <typename... Args>
    static bool
    apply(T& Obj, const PropertyTree& Tree, const std::string& NodeName, Args&&... args)
    {
        if constexpr (Traits::IsUpdateConfigurable_V<T>)
        {
            return detail::UpdateConfigurableParameter(Obj, Tree, NodeName);
        }
        else if constexpr (Traits::IsUpdatePointer_V<T>)
        {
            return detail::UpdatePointerParameter(Obj, Tree, NodeName, std::forward<Args>(args)...);
        }
        else if constexpr (Traits::IsUpdateSequentialContainer_V<T>)
        {
            return detail::UpdateSequentialContainerParameter(
                Obj, Tree, NodeName, std::forward<Args>(args)...);
        }
        else if constexpr (Traits::IsUpdateAssociativeContainer_V<T>)
        {
            return detail::UpdateAssociativeContainerParameter(
                Obj, Tree, NodeName, std::forward<Args>(args)...);
        }
        else
        {
            return detail::UpdateSimpleParameter(Obj, Tree, NodeName);
        }
    }
};

template <typename T, typename Enable = void>
struct Write
{
    using destination_t = T;
    
    static void apply(const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
    {
        if constexpr (Traits::IsWriteConfigurable_V<T>)
        {
            detail::WriteConfigurableParameter(Obj, Tree, NodeName, Overwrite);
        }
        else if constexpr (Traits::IsWritePointer_V<T>)
        {
            detail::WritePointerParameter(Obj, Tree, NodeName, Overwrite);
        }
        else if constexpr (Traits::IsWriteSequentialContainer_V<T>)
        {
            detail::WriteSequentialContainerParameter(Obj, Tree, NodeName, Overwrite);
        }
        else if constexpr (Traits::IsWriteAssociativeContainer_V<T>)
        {
            detail::WriteAssociativeContainerParameter(Obj, Tree, NodeName, Overwrite);
        }
        else
        {
            detail::WriteSimpleParameter(Obj, Tree, NodeName, Overwrite);
        }
    }
};

} // namespace Strategy

template <typename T, typename... Args>
bool
UpdateParameter(T& Obj, const PropertyTree& Tree, const std::string& NodeName, Args&&... args)
{
    return Strategy::Update<T>::template apply<Args...>(Obj, Tree, NodeName, std::forward<Args>(args)...);
}

template <typename T>
void
WriteToTree(const T& Obj, PropertyTree& Tree, const std::string& NodeName, bool Overwrite)
{
    Strategy::Write<T>::apply(Obj, Tree, NodeName, Overwrite);
}



} // namespace MXWare::Configuration

#endif // MXWARE_CONFIGURATION_AUTOCONFIGURATION_HH_