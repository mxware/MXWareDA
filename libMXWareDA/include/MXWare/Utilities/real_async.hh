/*
 * real_async.hh
 * From Scott Meyers Effective Modern C++
 *
 *  Created on: 22 Nov 2019
 *      Author: caiazza
 */

#ifndef MXWARE_UTILITIES_REAL_ASYNC_HH_
#define MXWARE_UTILITIES_REAL_ASYNC_HH_
#pragma once

#include <future>

namespace MXWare
{
template <typename F, typename... Ts>
inline auto
real_async(F&& f, Ts&&... params)
{
    return std::async(std::launch::async, std::forward<F>(f), std::forward<Ts>(params)...);
}


} // namespace MXWare



#endif /* MXWARE_CORE_UTILITIES_REAL_ASYNC_HH_ */
