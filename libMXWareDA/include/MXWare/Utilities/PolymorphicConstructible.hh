#ifndef MXWARE_UTILITIES_POLYMORPHICCONSTRUCTIBLE_HH_
#define MXWARE_UTILITIES_POLYMORPHICCONSTRUCTIBLE_HH_

#pragma once

#include <memory>

namespace MXWare::Utilities
{
/// An helper class that defines the interface for the polymorphic construction of an object.
/// Because the unique pointers of the derived classes are not covariant types of the unique
/// pointers to the base like the standard pointers are, the Create function is not virtual
/// and a virtual counterpart named RawCreate is provided to perform the virtual dispatching
/// to the necessary derived class while the Create function is not virtual so it can be
/// overridden in a derived class with a different return type, thus emulating a covariant
/// return
template <typename Base, typename... Args>
class PolymorphicConstructible
{
public:
    /// Polymorphic destructor
    virtual ~PolymorphicConstructible() = default;

    /// Creates a new object of the concrete class returning it as a base class pointer
    std::unique_ptr<Base>
    Create(Args&&... args) const
    {
        return std::unique_ptr<Base>(this->RawCreate(std::forward<Args>(args)...));
    }

    /// Creates a new object of the concrete class attempting to cast it to a pointer of a derived
    /// class
    template <class Derived>
    auto
    Create(Args&&... args) const
    {
        return std::unique_ptr<Derived>(
            dynamic_cast<Derived*>(this->RawCreate(std::forward<Args>(args)...)));
    }

private:
    /// Polymorphic function to create a new object of the same concrete class of
    /// the one through which is called using the template arguments for the constructor
    virtual Base*
    RawCreate(Args&&... args) const = 0;
};

template <class Base>
using DefaultConstructible = PolymorphicConstructible<Base>;

} // namespace MXWare::Utilities

#endif /*MXWARE_CORE_UTILITIES_POLYMORPHICCONSTRUCTIBLE_HH_*/
