#ifndef MXWARE_UTILITIES_COMPARABLE_HH_
#define MXWARE_UTILITIES_COMPARABLE_HH_
#pragma once

#include <type_traits>

namespace MXWare::Utilities
{
/// Interface defining a function signature for polymorphic comparison.
/// A class deriving from this interface will implement a comparison operator
/// through the IsEqual function so that a comparison through the == operator can be
/// performed using the base class references
/// Therefore the interface already implements an equality and difference operator for
/// those base interface.
/// Additionally the concrete class can implement a comparison operator using the concrete
/// class reference. In fact the IsEqual should be defined in term of the concrete equality
/// operator.
/// The IsEqual is declared protected because it should not be called directly but through the
/// standard comparison operator
template <typename Base>
class Comparable
{
public:
    /// Alias to the base class used for cloning
    typedef Base compare_base;

    /// Polymorphic destruction
    virtual ~Comparable() = default;

private:
    /// Polymorphic equality
    virtual bool
    IsEqual(const compare_base& rhs) const = 0;

    /// Comparison
    friend bool
    operator==(const compare_base& lhs, const compare_base& rhs)
    {
        if (typeid(lhs) == typeid(rhs))
        {
            return lhs.IsEqual(rhs);
        }
        else
        {
            return false;
        }
    }

    friend bool
    operator!=(const compare_base& lhs, const compare_base& rhs)
    {
        return !(lhs == rhs);
    }
};

namespace detail
{
/**
 * Trait class to verify through SFINAE that a class has the equality operator defined.
 * This is the default version for those for which the check fail
 */
template <typename T, typename = void>
struct is_equality_comparable : std::false_type
{};

/**
 * Trait class to verify through SFINAE that a class has the equality operator defined.
 * This is the version for those classes for which the operator is defined
 * , (void)0
 */
template <typename T>
struct is_equality_comparable<T,
    typename std::enable_if<true, decltype(std::declval<T&>() == std::declval<T&>())>::type> :
    std::true_type
{};
} // namespace detail
} // namespace MXWare

#endif /*MXWARE_UTILITIES_COMPARABLE_HH_*/
