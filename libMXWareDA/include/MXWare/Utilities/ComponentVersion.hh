#ifndef MXWARE_UTILITIES_COMPONENTVERSION_HH_
#define MXWARE_UTILITIES_COMPONENTVERSION_HH_
#pragma once

namespace MXWare
{

/// Simple structure to package the version informations
struct ComponentVersion
{
    int Major;
    int Minor;
    int Patch;

    constexpr ComponentVersion(int Maj, int Min, int Pat) : Major{ Maj }, Minor{ Min }, Patch{ Pat }
    {}
};

/// Comparison operator
inline constexpr bool
operator==(const ComponentVersion& L, const ComponentVersion& R)
{
    return (L.Major == R.Major && L.Minor == R.Minor && L.Patch == R.Patch);
}

constexpr ComponentVersion Version{ 3, 0, 0 };


} // namespace MXWare

#endif // MXWARE_CORE_UTILITIES_COMPONENTVERSION_HH_