#ifndef MXWARE_UTILITIES_CHARACTERSETS_HH_
#define MXWARE_UTILITIES_CHARACTERSETS_HH_
#pragma once

#include <array>

/// A namespace containing containers of different characters sets
namespace MXWare::Utilities::CharacterSets
{
[[maybe_unused]] constexpr std::array Numerals{ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };

[[maybe_unused]] constexpr std::array LowercaseLatinAlphabet{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
    'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u','v', 'w', 'x', 'y', 'z' };

[[maybe_unused]] constexpr std::array UppercaseLatinAlphabet{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U','V', 'W', 'X', 'Y', 'Z' };

[[maybe_unused]] constexpr std::array Alphanumeric{ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C',
    'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
    'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u','v', 'w', 'x', 'y', 'z' };

}

#endif //MXWARE_UTILITIES_CHARACTERSETS_HH_