#ifndef MXWARE_UTILITIES_CLONEABLE_HH_
#define MXWARE_UTILITIES_CLONEABLE_HH_
#pragma once

#include <memory>

namespace MXWare::Utilities
{

/**
 * Interface template for classes that can be cloned.
 * They define a standard clone function which returns a unique_ptr to the newly created
 * object. The automated lifetime management is the default behaviour.
 * Because the unique pointers of the derived classes are not covariant types of the unique
 * pointers to the base like the standard pointers are, the Clone function is not virtual
 * and a virtual counterpart named RawClone is provided to perform the virtual dispatching
 * to the necessary derived class while the Clone function is not virtual so it can be
 * overridden in a derived class with a different return type, thus emulating a covariant
 * return
 */
template <typename Base>
class Cloneable
{
public:
    /**
     * Destructor
     */
    virtual ~Cloneable() = default;

    /**
     * Copies the object polymorphically and returns a safe pointer to the base class
     * @return
     */
    std::unique_ptr<Base>
    Clone() const
    {
        return std::unique_ptr<Base>(this->RawClone());
    }

    /// Copies the object polymorphically and returns it in a smart pointer of some derived
    /// class
    template <class Derived>
    auto
    Clone() const
    {
        return std::unique_ptr<Derived>(dynamic_cast<Derived*>(this->RawClone()));
    }

protected:
    /**
     * Copies the object polymorphically and returns a raw pointer.
     * Using the return type covariance one can return directly the pointer to the concrete
     * class if the call is done on a pointer to the concrete class
     * @return
     */
    virtual Base*
    RawClone() const = 0;
};
} // namespace MXWare::Utilities

#endif /*MXWARE_CORE_UTILITIES_CLONEABLE_HH_*/
