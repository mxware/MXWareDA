#ifndef MXWARE_UTILITIES_FLATLLOOKUPTABLE_HH_
#define MXWARE_UTILITIES_FLATLLOOKUPTABLE_HH_
#pragma once

#include <array>
#include <algorithm>

#include "MXWare/Exceptions.hh"

namespace MXWare::Utilities
{
/// A constexpr lookup table initialize with a simple vector of pair
/// which allows fast compile time lookup of elements, for example
/// to match strings with enum types
// TODO Deduction guide and aggregate initialization
template <typename Key, typename Value, std::size_t Size>
class FlatLookupTable
{
    using container_t = std::array<std::pair<Key, Value>, Size>;

public:
    using iterator = typename container_t::iterator;
    using const_iterator = typename container_t::const_iterator;

    explicit constexpr FlatLookupTable(container_t Data) noexcept : data{ std::move(Data) } {}

    [[nodiscard]] constexpr Value
    at(const Key& key) const
    {
        const auto itr =
            std::find_if(data.begin(), data.end(), [&key](auto& v) { return v.first == key; });
        if (itr != data.end())
        {
            return itr->second;
        }
        else
        {
            throw ObjectNotFound("Data not found in the lookup table");
        }
    }

    constexpr iterator
    begin() noexcept
    {
        return data.begin();
    }

    constexpr const_iterator
    begin() const noexcept
    {
        return data.begin();
    }

    constexpr const_iterator
    cbegin() const noexcept
    {
        return data.begin();
    }

    constexpr iterator
    end() noexcept
    {
        return data.end();
    }

    constexpr const_iterator
    end() const noexcept
    {
        return data.end();
    }

    constexpr const_iterator
    cend() const noexcept
    {
        return data.end();
    }

private:
    container_t data;
};

} // namespace MXWare::Utilities

#endif // MXWARE_UTILITIES_FLATLLOOKUPTABLE_HH_