#include <benchmark/benchmark.h>
#include "MXWare/Configuration/PropertyTree.hh"

static void
BM_TreeCopy(benchmark::State& state)
{
    MXWare::Configuration::PropertyTree MyTree{};
    MyTree.add("some.int", "1");
    MyTree.add("some.string", "hello");

    for (auto _ : state)
    {
        MXWare::Configuration::PropertyTree Copy{ MyTree };
    }
}
BENCHMARK(BM_TreeCopy);

BENCHMARK_MAIN();