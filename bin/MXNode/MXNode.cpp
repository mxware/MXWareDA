/*
 * MXNode.cpp
 *
 *  Created on: Aug 12, 2019
 *      Author: caiazza
 */


#include "cxxopts.hpp"

#include "MXWare/Execution/Node.hh"
#include "MXWare/Configuration/ConfigFileParser.hh"
#include "MXWare/Log.hh"

namespace Log = MXWare::Log;

#include <filesystem>

// ASIO is used to handle the OS signals
#include <boost/asio.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION < 106600
namespace boost
{
    namespace asio
    {
        using io_context = io_service;

    }
} // namespace boost
#endif

#include <fstream>
#include <string>
#include <future>
#include <map>

#include "fmt/ostream.h"

int main(int argc, char** argv)
{
    // Initialize the logging system.
    Log::GlobalService.SetLevel(Log::Level::Info);

    cxxopts::Options cl_opt{ "MXNode",
        "" };
    cl_opt.add_options()
        ("C,config", "Path to the program configuration file", cxxopts::value<std::string>())
        ("log_level", "The default logging level of the logging service. Should be "
            "one among: off, Trace, Debug, info, warning, error, critical. The default "
            "value is info", cxxopts::value<std::string>())
        ("h,help", "Prints this help message. Ignores any other argument");
    cl_opt.parse_positional("config");

    MXWare::Configuration::PropertyTree InitialConfiguration{};

    // In this thread we run the listener for the OS signals which will trigger the switching off
    // of the node. The ASIO context for those signals will be local to this thread
    auto AsioRunHandle = std::async(std::launch::async, [&]() {
        boost::asio::io_context IOContext{ 1 };
        boost::asio::signal_set OSSignals{ IOContext };
        OSSignals.add(SIGINT);
        OSSignals.add(SIGTERM);

        // Wait for a signal to handle
//        OSSignals.async_wait(
//            [this](const boost::system::error_code&, int signal_number) {
//                Log::Framework::debug(
//                    "Received signal number {}.",
//                    signal_number);
//                this->StopProcessing(true);
//            });
        return IOContext.run();
        });

    try
    {
        auto result = cl_opt.parse(argc, argv);

        // Prints the help and exits
        if (result.count("help"))
        {
            std::cout << cl_opt.help() << std::endl;
            return 0;
        }

        // Sets the base log level
        if (result.count("log_level"))
        {
            auto DesiredLevel = result["log_level"].as<std::string>();
            try
            {
                Log::GlobalService.SetLevel(MXWare::Log::NamedLevels.at(DesiredLevel));                
                Log::GlobalService.FrameworkLogger().Debug("Log level set to {}", DesiredLevel);
            }
            catch(const std::exception& e)
            {
                std::cerr << "Logging desired level unknown (" << DesiredLevel << ")"
                    << std::endl;
                return 1002;
            }
        }

        if (result.count("config"))
        {
            auto ConfigPath = std::filesystem::path{ result["config"].as<std::string>() };
            if (std::filesystem::is_regular_file(ConfigPath))
            {
                std::ifstream ConfigFile{ ConfigPath.native() };
                if (ConfigFile.good())
                {
                    MXWare::Configuration::ConfigFileParser Reader{};
                    MXWare::Configuration::PropertyTree CLIConfigTree{};
                    Reader.Read(ConfigFile, CLIConfigTree);
                    MXWare::Configuration::MergeConfigurationTrees(InitialConfiguration, CLIConfigTree);
                }
                else
                {
                    Log::Warn("Unable to open the input configuration file\n{}",
                        ConfigPath.string());
                }
            }
            else
            {
                Log::Warn("The path for the input configuration file\n{}\n does not"
                    " match a regular file in the system. Ignoring it.", ConfigPath.string());
            }

        }

    }
    catch (cxxopts::OptionException & e)
    {
        Log::Error("Error parsing the command line : {}", e.what());
        return 1;
    }

    Log::Debug("Starting the node");
    Log::Debug("Initial node configuration:\n{}", InitialConfiguration);

    // TODO Manage exceptions
    MXWare::Execution::Node TheNode{};

    // TODO Load default configuration
    // Create the initial configuration merging available sources in this order:
    // System tree in some kind of system directory
    // Local tree defined by a standard file in the current directory
    // Other command line options
    TheNode.Configure(InitialConfiguration);

    // Binds the control node and starts listening
    TheNode.StartProcessing();

    Log::Info("Closing the node!");

    if (AsioRunHandle.valid())
    {
        AsioRunHandle.get();
    }

}
