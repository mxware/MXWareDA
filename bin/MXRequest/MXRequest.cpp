/*
 * MXSend.cpp
 *
 *  Created on: Oct 2, 2019
 *      Author: caiazza
 */

#include "cxxopts.hpp"

#include "MXWare/Log.hh"
#include "MXWare/Configuration.hh"
#include "MXWare/Configuration/ConfigFileParser.hh"
#include "MXWare/Core/MXCourier.hh"
#include "MXWare/Core/NodeServices.hh"
namespace MXCore = MXWare::Core;
namespace Log = MXWare::Log;

#include "fmt/ostream.h"

#include <filesystem>

// ASIO is used to handle the OS signals
#include <boost/asio.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION < 106600
namespace boost
{
namespace asio
{
using io_context = io_service;

}
} // namespace boost
#endif

#include <fstream>
#include <string>
#include <future>


int main(int argc, char **argv)
{
    // Defining the command line options
    cxxopts::Options cl_opt{ "MXRequest",
        "" };
    cl_opt.add_options()
        ("h,help", "")
        ("C,config", "Path to the a file containing the message set to send", cxxopts::value<std::string>());

    // Initialize the logging system.
    Log::GlobalService.SetLevel(Log::Level::Info);

    /// Create the messaging service
    auto Courier = std::shared_ptr<MXCore::MXCourier>(MXCore::MXCourier::NewCourier());
    // Create the node client to attach to the messaging service
    auto Client = MXCore::NodeClient(Courier);

    // In this thread we run the listener for the OS signals which will trigger the switching off
    // of the node. The ASIO context for those signals will be local to this thread
    auto AsioRunHandle = std::async(std::launch::async, [&](){
        boost::asio::io_context IOContext{1};
        boost::asio::signal_set OSSignals{IOContext};
        OSSignals.add(SIGINT);
        OSSignals.add(SIGTERM);

        // Wait for a signal to handle
        OSSignals.async_wait(
            [&](const boost::system::error_code&, int signal_number) {
                Log::Debug("Received signal number {}.",
                    signal_number);
//                this->StopProcessing(true);
            });
        return IOContext.run();
    });
    


    // Initialize the logging system and configure it with CL arguments

    MXWare::Configuration::PropertyTree InitialConfiguration{};

    try
    {
        auto result = cl_opt.parse(argc, argv);
        if(result.count("config"))
        {
            auto ConfigPath = std::filesystem::path{result["config"].as<std::string>()};
            if(std::filesystem::is_regular_file(ConfigPath))
            {
                std::ifstream ConfigFile{ConfigPath.native()};
                if(ConfigFile.good())
                {
                    MXWare::Configuration::ConfigFileParser Reader{};
                    MXWare::Configuration::PropertyTree CLIConfigTree{};
                    Reader.Read(ConfigFile, CLIConfigTree);
                    MXWare::Configuration::MergeConfigurationTrees(InitialConfiguration, CLIConfigTree);
                }
                else
                {
                    Log::Warn("Unable to open the input configuration file\n{}",
                                           ConfigPath.string());
                }
            }
            else
            {
                Log::Warn("The path for the input configuration file\n{}\n does not"
                    " match a regular file in the system. Ignoring it.", ConfigPath.string());
            }

        }

    }
    catch(cxxopts::OptionException& e)
    {
        Log::Error("Error parsing the command line : {}", e.what());
        return 1;
    }

    Log::Debug("Message to send:\n{}", InitialConfiguration);

    if(AsioRunHandle.valid())
    {
        AsioRunHandle.get();
    }
}

