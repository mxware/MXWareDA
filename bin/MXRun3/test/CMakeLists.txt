
AddMXWarePlugin( 	NAME TestRunPlugin
					VERSION 1
					SOURCES TestPlugin.cpp
					DEPENDENCIES MXWareDA)
					
target_compile_definitions(TestRunPlugin PRIVATE BUILDING_LIBRARY)

set( XMLScripts_Base SampleProgram)

foreach( script ${XMLScripts_Base} )
	configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/${script}.xml ${CMAKE_CURRENT_BINARY_DIR}/${script}.xml @ONLY )
	add_test( NAME MXRun_${script}
          COMMAND MXRun3 ${CMAKE_CURRENT_BINARY_DIR}/${script}.xml)
endforeach()

