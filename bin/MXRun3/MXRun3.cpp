/*
 * MXRun3.cpp
 *
 *  Created on: Oct 2, 2019
 *      Author: caiazza
 */

#include "cxxopts.hpp"

#include "MXWare/Core/MXCourier.hh"
#include "MXWare/Execution/Node.hh"
#include "MXWare/Execution/Commands.hh"
#include "MXWare/Utilities/real_async.hh"
#include "MXWare/Configuration/ConfigFileParser.hh"

namespace Log = MXWare::Log;

// ASIO is used to handle the OS signals
#include <boost/asio.hpp>
#include <boost/version.hpp>

#include "fmt/ostream.h"

#include <memory>
#include <vector>
#include <thread>
#include <filesystem>

#if BOOST_VERSION < 106600
namespace boost
{
    namespace asio
    {
        using io_context = io_service;

    }
} // namespace boost
#endif

#include <fstream>
#include <string>
#include <future>
#include <map>
#include <atomic>

int main(int argc, char** argv)
{
    // Initialize the logging system.
    auto& FWLog = Log::GlobalService.FrameworkLogger();
    Log::GlobalService.SetLevel(Log::Level::Info);

    // Now I can load in the initial configuration, the static set of configuration files
    MXWare::Configuration::PropertyTree InitialConfiguration{};
    bool PrintConf = false;

    // Load system. Located in $MXWARE_DIR/etc/mxware-conf.xml
    // Load user Located in $HOME/.mxware/mxware-conf.xml
    // Load local Located in $(cwd)/mxware-start.xml

    // Configuring the node using the command line inputs
    try
    {
        cxxopts::Options cl_opt{ "MXRun3",
            "" };
        cl_opt.add_options()
            ("C,config", "Path to the program configuration file", cxxopts::value<std::string>())
            ("log_level", "The default logging level of the logging service. Should be "
                "one among: off, trace, debug, info, warning, error, critical. The default "
                "value is info", cxxopts::value<std::string>())
            ("print_conf", "Prints the input configuration but do not execute any command")
            ("h,help", "Prints this help message. Ignores any other argument");
        cl_opt.parse_positional("config");

        auto result = cl_opt.parse(argc, argv);

        // Prints the help and exits
        if (result.count("help"))
        {
            std::cout << cl_opt.help() << std::endl;
            return 0;
        }

        // Sets the base log level
        if (result.count("log_level"))
        {
            auto DesiredLevel = result["log_level"].as<std::string>();
            try
            {
                Log::GlobalService.SetLevel(MXWare::Log::NamedLevels.at(DesiredLevel));                
                FWLog.Debug("Log level set to {}", DesiredLevel);
            }
            catch(const std::exception& e)
            {
                std::cerr << "Logging desired level unknown (" << DesiredLevel << ")"
                    << std::endl;
                return 1002;
            }
        }
        
        if (result.count("config"))
        {
            auto ConfigPath = std::filesystem::path{ result["config"].as<std::string>() };
            if (std::filesystem::is_regular_file(ConfigPath))
            {
                std::ifstream ConfigFile{ ConfigPath.native() };
                if (ConfigFile.good())
                {
                    MXWare::Configuration::ConfigFileParser Reader{};
                    MXWare::Configuration::PropertyTree CLIConfigTree{};
                    Reader.Read(ConfigFile, CLIConfigTree);
                    MXWare::Configuration::MergeConfigurationTrees(InitialConfiguration, CLIConfigTree);
                }
                else
                {
                    FWLog.Critical("Unable to open the input configuration file\n{}",
                        ConfigPath.string());
                    return 1001;
                }
            }
            else
            {
                FWLog.Critical("The path for the input configuration file\n{}\n does not"
                    " match a regular file in the system. Aborting.", ConfigPath.string());
                return 1001;
            }
        }

        if (result.count("print_conf"))
        {
            PrintConf = true;
        }
        
        if(auto PluginConfTree = InitialConfiguration.get_child_optional("plugins"))
        {
            MXWare::Extensions::GlobalPluginManager.Configure(PluginConfTree.get());
            MXWare::Extensions::GlobalPluginManager.LoadValidPlugins();
        }

        // if(auto itPluginConf = InitialConfiguration.find("plugins");
        //     itPluginConf != InitialConfiguration.not_found())
        // {
        //     MXWare::Extensions::GlobalPluginManager.Configure(itPluginConf->second);
        //     MXWare::Extensions::GlobalPluginManager.LoadValidPlugins();
        // }

        if (auto LoggingConfTree = InitialConfiguration.get_child_optional("logging"))
        {
            MXWare::Log::GlobalService.Configure(LoggingConfTree.get());
        }

    }
    catch (cxxopts::OptionException & e)
    {
        FWLog.Error("Error parsing the command line : {}", e.what());
        return 1000;
    }
    
    // Start the Courier
    auto Courier = MXWare::Core::MXCourier::NewCourier();

    // The node which will be runnning in the program
    auto MyNode = std::make_unique<MXWare::Execution::Node>("Node", Courier);
    if(InitialConfiguration.count("node") > 1)
    {
        FWLog.Warn("More than one element labelled \"node\" found in the program configuration."
            "Only one node will be configured and executed");
    }

    auto itNode = InitialConfiguration.find("node");
    if(itNode != InitialConfiguration.not_found())
    {
        MyNode->Configure(itNode->second);
    }

    if(PrintConf)
    {
        MXWare::Configuration::PropertyTree out;
        out.put_child("plugins", MXWare::Extensions::GlobalPluginManager.CurrentConfiguration());
        out.put_child("logging", MXWare::Log::GlobalService.CurrentConfiguration());
        out.put_child("node", MyNode->CurrentConfiguration());
        std::cout << out << std::endl;
        return 0;
    }

    // In this thread we run the listener for the OS signals which will trigger the switching off
    // of the node. The ASIO context for those signals will be local to this thread
    boost::asio::io_context IOContext{ 1 };
    auto AsioRunHandle = std::async(std::launch::async, [&]() {
        boost::asio::signal_set OSSignals{ IOContext };
        OSSignals.add(SIGINT);
        OSSignals.add(SIGTERM);

        // Wait for a signal to handle
        OSSignals.async_wait(
            [&](const boost::system::error_code& err, int signal_number) 
            {
                if (!err)
                {
                    FWLog.Info("Received signal number {}, stopping the program execution "
                        "gracefully", signal_number);
                    MyNode->Stop();
                }
            });
        return IOContext.run();
        });

    FWLog.Debug("Current node configuration\n{}\n", MyNode->CurrentConfiguration());

    MyNode->EnqueueCommand(std::make_unique<MXWare::Execution::Command::StartAll>());
    MyNode->EnqueueCommand(std::make_unique<MXWare::Execution::Command::StopWhenFinished>());
    MyNode->Start();

    FWLog.Debug("Node stopped\n");
    // Now that the node is executing I can send it the messages sequentially.

    // std::this_thread::sleep_for(std::chrono::milliseconds(100));

    // MyNode->StopProcessing();
    IOContext.stop();
    AsioRunHandle.get();
}

