set(exename "MXRun3")
set(fullver 1.0.0)

if(NOT TARGET spdlog)
  # Stand-alone build
  find_package(spdlog 1.4.0 CONFIG REQUIRED)
endif()

# if( NOT TARGET fmt-header-only ) find_package( fmt 6.0 CONFIG REQUIRED )
# endif()

# Until CMake 3.11 it is necessary to give at least a source
add_executable(${exename} "MXRun3.cpp")

set_property(TARGET ${exename} PROPERTY VERSION ${fullver})
# target_sources( MXNode PRIVATE MXNode.cpp )

mxware_set_all_default_properties(TARGET ${exename})
target_link_libraries(
  ${exename} PRIVATE MXWareDA cxxopts spdlog::spdlog_header_only
                     fmt::fmt-header-only)

install(
  TARGETS ${exename}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

if(BUILD_TESTING)
  add_subdirectory(test)
endif()
