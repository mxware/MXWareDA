#!/bin/bash

set -e

CHANGED=$(git diff-index --name-only HEAD --)
if [ -n "$CHANGED" ]; then
	echo "The current repository has changes $CHANGED"
	exit 1
fi 

git subtree pull --prefix cxxopts cxxopts mxware-default --squash -m "Merging with the latest MXWare Default branch"
git reset --hard
git subtree pull --prefix cppzmq cppzmq mxware-default --squash -m "Merging with the latest MXWare Default branch"
git reset --hard

