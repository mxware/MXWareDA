# The MXWare Extension system

MXWare is designed as an extensible framework to share tools and utilities in an otherwise customizable execution environment. The core infrastracture to allow this functionality is the extension system. The main components of the extension system are the Plugin manager and the extension registry.

## Component Factories and registry

The basic infrastructure that allows to customize the functionality of a program is a combination of an interface, a set of private implementations and a factory system to create the right implementation at runtime. MXWare includes a templated factory which is a container of factory function keyed to unique identifiers to select the implementation to use. For each interface and for each constructor signature there is a unique factory.

Multiple factories can be contained in an extension registry that will select the right one to create the required object. A set of builtin implementations of the core framework components are included in a builtin registry available as a global object named `MXWare::Extensions::Builtins`

## Plugins and the plugin manager

To allow developers to extend the functionalities of the core framework making new components available at runtime without recompiling the complete framework, MXWare includes a plugin system. A plugin can be created using the `MXWARE_DEFINE_PLUGIN` macro and defining the `GetExtensions` function for the plugin class which is generated through that macro. This function should register the new component in an extension registry which can then be used by the main system.

A Plugin Manager takes charge of loading those plugin libraries at runtime, searching in a set of user-defined folders, making the new components available to the system


## Using the extension components

An object of a given extension type can be created using the `CreateObject` templated function, specifying the registration name under which the object is registered in the plugin manager.

To get the registration name of an existing object the user can use the `GetRegistration` templated function.

All these functions are defined in the `MXWare/Extensions.hh` header file.