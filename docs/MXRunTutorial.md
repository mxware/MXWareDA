# The MXRun3 program

Configures a single node and sends a set of commands sequentially. If more than one node is configured in the file, only the first will be created, and configured and only to that the internal commands will be sent. If no node is configured to be run the program will terminate.