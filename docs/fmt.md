<a id="top"></a>

# The {fmt} library

MXWare depends on the **{fmt}** library. {fmt} is an open-source formatting library for C++. It can be used as a safe and fast alternative to (s)printf and iostreams. It allows to format strings with a syntax similar to the printf family of functions and the Python str.format function while keeping the safety, versatility and robustness of the standard c++ streams. 

Detailed information about this library can be found in its [documentation](https://fmt.dev/6.1.2/)

## Format API
The format API is similar in spirit to the C `printf` family of function but is safer, simpler and [several times faster](http://www.zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html) than common standard library implementations. The [format string syntax](https://fmt.dev/6.1.2/syntax.html) is similar to the one used by [str.format](https://docs.python.org/3/library/stdtypes.html#str.format) in Python:

	fmt::format("The answer is {}.", 42);

The `fmt::format` function returns a string “The answer is 42.”. You can use `fmt::memory_buffer` to avoid constructing `std::string`:

	fmt::memory_buffer out;
	format_to(out, "For a moment, {} happened.", "nothing");
	out.data(); // returns a pointer to the formatted data

The `fmt::print` function performs formatting and writes the result to a stream:

	fmt::print(stderr, "System error code = {}\n", errno);

The file argument can be omitted in which case the function prints to `stdout`:

	fmt::print("Don't {}\n", "panic");

The Format API also supports positional arguments useful for localization:

	fmt::print("I'd rather be {1} than {0}.", "right", "happy");

Named arguments can be created with `fmt::arg`. This makes it easier to track what goes where when multiple arguments are being formatted:

	fmt::print("Hello, {name}! The answer is {number}. Goodbye, {name}.",
           fmt::arg("name", "World"), fmt::arg("number", 42));

If your compiler supports C++11 user-defined literals, the suffix `_a` offers an alternative, slightly terser syntax for named arguments:

	using namespace fmt::literals;
	fmt::print("Hello, {name}! The answer is {number}. Goodbye, {name}.",
           "name"_a="World", "number"_a=42);

## Hints and tips

* By default {fmt} converts to string all builtin and standard types. It is possible to convert custom types with a defined streaming operator including the additional header `fmt/ostream.h`