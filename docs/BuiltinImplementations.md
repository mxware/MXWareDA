<a id="top"></a>

# List of the builtin implementations

## Element types

| `MXWare::Core::IElement` | |
| --- | --- |
| `DummyElement` | DummyElement |

## Configuration tree codecs

| `MXWare::Core::Conf::ICodec` | |
| --- | --- |
| `XML_ConfTreeCodec` | application/mxware+xml |