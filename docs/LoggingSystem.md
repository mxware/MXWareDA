<a id="top"></a>

# The MXWare Logging service

## Logging levels

The logging levels are:

* **Trace**
* **debug**
* **info**
* **warning**
* **error**
* **critical**
* **off**

The configuration file allows to set a single global logging level which can be overriden by the command line argument `log_level`

Additionally each individual logger can have its own logging level which overrides the global one

To format the log strings it is possible to use the [{fmt} library](fmt.md#top) which is included in MXWare 

## Development notes
