<a id="top"></a>

# Configuration and control system

The configuration and control system of MXWare is designed to steer the operation of all the components of the Magix software in a coherent way, allowing at the same time a high degree of versatility to reduce the constraints to the developers.

In all the executables a set of configuration files can be loaded at the startup of the program to define the initial state of the system and then command messages can be sent sequentially to the nodes to perform their transitions

A set of configuration files will be loaded at startup, additional commands can be sent to the nodes through the messaging system

The data in the control messages are represented through a tree structure.

In case of input configuration files it is also possible to include other files in a single configuration file.

## Parameter auto configuration

### Pointers auto configuration

When updating, if the node from which to update contains an element labelled "type", the pointer is replaced with a new object created using the global implementation registry, using the lookup name specified as value of the type element. The newly created object is the updated as simple or nested object type.

The auto configuration works with unique, shared and raw pointers with two major constraints:

* In the implementation registry it will look for factory functions creating unique_ptr, even when using other pointer types.

* The pointed object must be default constructible

For special cases one can simply define a specialization of the UpdateParameter and WriteToTree functions.

Additionally, in the case of raw pointers, the auto configuration will leak the previous pointer, therefore the user should make sure to delete it otherwise. This should not be a problem because raw pointers are really a bad idea in the first place for resources which are owned.

### Sequential containers configuration

* The elements must be default constructible and movable
