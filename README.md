# MXWare Distributed Architecture

The MX in MXWare stands for Multipurpose eXtendible. This is a framework for the distributed execution of generic C++ software components.

An MXWare distributed system is divided in processing nodes which can be configured and controlled remotely through a predefined messaging protocol. The messaging service can be bound to C++ or Python (maybe even C in the future through which one can bind anything)

## Dependencies

### Builtin dependencies

These are the library used in the project which can also be provided by the project itself. It is possible to link to a compatible library provided independently.

* [Spdlog 1.8.0](https://github.com/gabime/spdlog) - For the logging 
* [{fmt} 7.0](docs/fmt.md#top) - For string formatting
* [Catch2 2.13](https://github.com/catchorg/Catch2) - If the unit testing is enabled
* [Google Benchmark 1.5](https://github.com/google/benchmark) - If the benchmark units are enabled

### External dependencies

These are the libraries which have to be provided externally.

* Boost
* ZeroMQ
* Zyre?

## Building the project

The build system of this project is based on CMake. The project requires CMake 3.10 to be built correctly. For CMake > 3.12 the project also supports [vcpkg](https://github.com/microsoft/vcpkg).

To simplify the build process it is possible to define an environment variable within the host system named **MXWARE_DIR**, pointing it to the folder where the program is to be installed. Additionally, to simplify the finding of the packages on which this project depends, if they are outside of the default system location where they are looked for, they should either be installed in the parent folder to that of the **MXWARE_DIR** variable or define an environment variable in the host system named **MXWARE_DEP_DIR** containing a ';' separated list of the root folder containing the dependent packages.

## Testing and benchmarks

The project uses CTest to set-up the unit tests. That module provides an option named **BUILD_TESTING** to enable the build of the test units.

Some components are benchmarked with the Google Benchmark platform. To enable the building of the benchmarks one has to switch on the CMake option **BUILD_BENCHMARKS**

## Install

## Platforms

## Package managers

## Components

* [Configuration and control system](docs/ConfigurationMessages.md#top)
* [Logging service](docs/LoggingSystem.md#top)
