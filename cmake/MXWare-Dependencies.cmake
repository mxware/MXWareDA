# Sets of functions to add the builtin libraries

# Macro enabling the builtin FetchContent_MakeAvailable for Cmake version prior
# to 3.14
include(FetchContent)
if(${CMAKE_VERSION} VERSION_LESS 3.14)
  macro(FetchContent_MakeAvailable NAME)
    FetchContent_GetProperties(${NAME})
    string(TOLOWER "${NAME}" lcName)
    string(TOUPPER "${NAME}" ucName)
    if(NOT ${lcName}_POPULATED)
      FetchContent_Populate(${NAME})
      add_subdirectory(${${lcName}_SOURCE_DIR} ${${lcName}_BINARY_DIR})
      mark_as_advanced(FETCHCONTENT_SOURCE_DIR_${ucName}
                       FETCHCONTENT_UPDATES_DISCONNECTED_${ucName})
    endif()
  endmacro()
endif()

# ##############################################################################
# Fetches Catch 2 from its git repo
# ##############################################################################
function(fetch_catch2)
  message(STATUS "Downloading Catch 2")
  FetchContent_Declare(
    Catch2
    GIT_REPOSITORY https://github.com/catchorg/Catch2
    GIT_TAG v2.13.4)

  option(CATCH_USE_VALGRIND "Perform SelfTests with Valgrind" OFF)
  option(CATCH_BUILD_TESTING "Build SelfTest project" OFF)
  option(CATCH_BUILD_EXAMPLES "Build documentation examples" OFF)
  option(CATCH_BUILD_EXTRA_TESTS "Build extra tests" OFF)
  option(CATCH_ENABLE_COVERAGE "Generate coverage for codecov.io" OFF)
  option(CATCH_ENABLE_WERROR "Enable all warnings as errors" ON)
  option(CATCH_INSTALL_DOCS "Install documentation alongside library" OFF)
  option(CATCH_INSTALL_HELPERS "Install contrib alongside library" ON)
  mark_as_advanced(
    CATCH_USE_VALGRIND
    CATCH_BUILD_TESTING
    CATCH_BUILD_EXAMPLES
    CATCH_BUILD_EXTRA_TESTS
    CATCH_ENABLE_COVERAGE
    CATCH_ENABLE_WERROR
    CATCH_INSTALL_DOCS
    CATCH_INSTALL_HELPERS)

  FetchContent_MakeAvailable(Catch2)
endfunction()

# ##############################################################################
# Fetches Google Benchmark from its git repo
# ##############################################################################
function(fetch_googlebenchmark)
  message(STATUS "Downloading Google Benchmark")
  FetchContent_Declare(
    googlebenchmark
    GIT_REPOSITORY https://github.com/google/benchmark.git
    GIT_TAG v1.5.2)

  option(BENCHMARK_ENABLE_TESTING "Enable testing of the benchmark library."
         OFF)
  option(
    BENCHMARK_ENABLE_INSTALL
    "Enable installation of benchmark. (Projects embedding benchmark may want to turn this OFF.)"
    OFF)
  option(BENCHMARK_DOWNLOAD_DEPENDENCIES
         "Downloads googletest as a dependency of Google Benchmark" OFF)

  # set(BENCHMARK_ENABLE_GTEST_TESTS OFF CACHE INTERNAL "")
  FetchContent_MakeAvailable(googlebenchmark)
  mark_as_advanced(
    BENCHMARK_ENABLE_TESTING
    BENCHMARK_ENABLE_INSTALL
    BENCHMARK_DOWNLOAD_DEPENDENCIES
    BENCHMARK_ENABLE_EXCEPTIONS
    BENCHMARK_ENABLE_LTO
    BENCHMARK_USE_LIBCXX
    BENCHMARK_BUILD_32_BITS
    BENCHMARK_ENABLE_GTEST_TESTS
    BENCHMARK_ENABLE_ASSEMBLY_TESTS)
endfunction()

# ##############################################################################
# Fetches cxxopts from its git repo
# ##############################################################################
function(fetch_cxxopts)
  message(STATUS "Downloading cxxopts")
  FetchContent_Declare(
    cxxopts
    GIT_REPOSITORY https://github.com/jarro2783/cxxopts.git
    GIT_TAG v2.2.1)

  # Installing cxxopts to manage Command Line Interactions
  option(CXXOPTS_BUILD_EXAMPLES "Set to ON to built the cxxopts examples" OFF)
  option(CXXOPTS_BUILD_TESTS "Set to ON to build the cxxopts unit tests" OFF)
  option(CXXOPTS_ENABLE_INSTALL "Generate the install target" OFF)
  mark_as_advanced(CXXOPTS_BUILD_EXAMPLES CXXOPTS_BUILD_TESTS CXXOPTS_CMAKE_DIR
                   CXXOPTS_USE_UNICODE_HELP CXXOPTS_ENABLE_INSTALL)

  FetchContent_MakeAvailable(cxxopts)
endfunction()

# ##############################################################################
# Fetches fmt from its git repo
# ##############################################################################
function(fetch_libfmt)
  message(STATUS "Downloading libfmt")
  FetchContent_Declare(
    libfmt
    GIT_REPOSITORY https://github.com/fmtlib/fmt.git
    GIT_TAG 7.1.3)

  option(FMT_INSTALL "Generate the install target." ON)
  option(FMT_DOC "Generate the fmt documentation." OFF)
  option(FMT_TEST "Generate the fmt test units." OFF)
  mark_as_advanced(
    FMT_PEDANTIC
    FMT_WERROR
    FMT_DOC
    FMT_INSTALL
    FMT_TEST
    FMT_FUZZ
    FMT_CMAKE_DIR
    FMT_INC_DIR
    FMT_LIB_DIR
    FMT_CUDA_TEST
    FMT_DEBUG_POSTFIX
    FMT_OS
    FMT_PKGCONFIG_DIR)

  FetchContent_MakeAvailable(libfmt)
endfunction()

# ##############################################################################
# Fetches spdlog from its git repo
# ##############################################################################
function(fetch_spdlog)
  message(STATUS "Downloading spdlog")
  FetchContent_Declare(
    libspdlog
    GIT_REPOSITORY https://github.com/gabime/spdlog.git
    GIT_TAG v1.8.2)

  option(SPDLOG_INSTALL "Generate Spdlog install targets." ON)
  option(SPDLOG_FMT_EXTERNAL "Use an external version of the FMT library" ON)
  option(SPDLOG_FMT_EXTERNAL_HO
         "Use external fmt header-only library instead of bundled" OFF)
  option(
    SPDLOG_ENABLE_PCH
    "Build static or shared library using precompiled header to speed up compilation time"
    ON)
  mark_as_advanced(
    SPDLOG_BUILD_BENCH
    SPDLOG_BUILD_EXAMPLE
    SPDLOG_BUILD_EXAMPLE_HO
    SPDLOG_BUILD_TESTS
    SPDLOG_BUILD_TESTS_HO
    SPDLOG_BUILD_ALL
    SPDLOG_BUILD_WARNINGS
    SPDLOG_SANITIZE_ADDRESS
    SPDLOG_INSTALL
    SPDLOG_FMT_EXTERNAL
    SPDLOG_FMT_EXTERNAL_HO
    SPDLOG_WCHAR_SUPPORT
    SPDLOG_BUILD_SHARED
    SPDLOG_NO_EXCEPTIONS
    SPDLOG_CLOCK_COARSE
    SPDLOG_NO_ATOMIC_LEVELS
    SPDLOG_NO_THREAD_ID
    SPDLOG_NO_TLS
    SPDLOG_PREVENT_CHILD_FD
    SPDLOG_ENABLE_PCH
    SPDLOG_TIDY)

  FetchContent_MakeAvailable(libspdlog)
endfunction()

# ##############################################################################
# Fetches cppzmq from its git repo
# ##############################################################################
function(fetch_cppzmq)
  message(STATUS "Downloading cppzmq v4.7.1")
  FetchContent_Declare(
    libcppzmq
    GIT_REPOSITORY https://github.com/zeromq/cppzmq.git
    GIT_TAG v4.7.1)

  option(CPPZMQ_ENABLE_INSTALL "Generate the cppzmq install target" OFF)
  option(CPPZMQ_BUILD_TESTS "Builds the cppzmq tests" OFF)
  option(ENABLE_DRAFTS "Enables the zmq draft API" OFF)
  mark_as_advanced(CPPZMQ_ENABLE_INSTALL CPPZMQ_BUILD_TESTS ENABLE_DRAFTS
                   CPPZMQ_CMAKECONFIG_INSTALL_DIR)

  FetchContent_MakeAvailable(libcppzmq)
endfunction()

# ##############################################################################
# Searching for all the dependencies
# ##############################################################################
if(BUILD_TESTING AND NOT TARGET Catch2::Catch2)
  option(MXWARE_BUILTIN_CATCH2
         "Use the builtin version of Catch2. Disable to use an external version"
         ON)
  if(MXWARE_BUILTIN_CATCH2)
    fetch_catch2()
  else()
    find_package(Catch2 2.13 REQUIRED)
  endif()
endif()

if(BUILD_BENCHMARKS AND NOT TARGET benchmark::benchmark)
  option(MXWARE_BUILTIN_GOOGLEBENCHMARK
         "Use the builtin version of Google Benchmark" ON)
  if(MXWARE_BUILTIN_GOOGLEBENCHMARK)
    fetch_googlebenchmark()
  else()
    find_package(benchmark 1.5 REQUIRED)
  endif()
endif()

if(NOT TARGET fmt::fmt-header-only)
  option(MXWARE_BUILTIN_LIBFMT "Use the built-in version of the fmt library" ON)
  if(MXWARE_BUILTIN_LIBFMT)
    fetch_libfmt()
  else()
    find_package(fmt 7.0 REQUIRED)
  endif()
endif()

if(NOT TARGET spdlog::spdlog_header_only)
  option(MXWARE_BUILTIN_SPDLOG "Use the built-in version of the spdlog library"
         ON)
  if(MXWARE_BUILTIN_SPDLOG)
    fetch_spdlog()
  else()
    find_package(spdlog 1.8 REQUIRED)
  endif()
endif()

if(NOT TARGET cppzmq)
  option(MXWARE_BUILTIN_CPPZMQ "Use the builtin version of cppzmq" ON)
  if(MXWARE_BUILTIN_CPPZMQ)
    fetch_cppzmq()
  else()
    find_package(cppzmq 4.7 REQUIRED)
  endif()
endif()

if(NOT TARGET cxxopts)
  option(MXWARE_BUILTIN_CXXOPTS "Use the builtin version of cxxopts" ON)
  if(MXWARE_BUILTIN_CXXOPTS)
    fetch_cxxopts()
  else()
    find_package(cxxopts 2.2 REQUIRED)
  endif()
endif()
