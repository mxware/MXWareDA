# Installing some of the cmake files to be used by dependent projects

set(CMAKE_EXPORTED_FILES ${CMAKE_CURRENT_LIST_DIR}/MXWare-BuildUtils.cmake)

# This is a valid destination for both Win and Unix
set(CMAKE_INSTALL_DIR share/${PROJECT_NAME}/cmake)
install(FILES ${CMAKE_EXPORTED_FILES} DESTINATION ${CMAKE_INSTALL_DIR})

# The PATH_VARS argument requires the NAME of a variable, not its value so that
# that name can be forwarded to the configure_package_config_file macro
mxware_add_cmakeconfig(PATH_VARS CMAKE_INSTALL_DIR BUILD_TESTING)
