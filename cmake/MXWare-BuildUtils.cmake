
# ##############################################################################
# This module defines the following macros/functions
#
# mxware_enable_all_warnings(target_name) Enables all the warnings for a given
# target
#
# mxware_enable_default_compiler_options(target_name) Sets the default compiler
# options for a specific target
#
# force_color_output() Enables the color output for GNU and Clang also when
# using Ninja
#
# mxware_set_global_rpath_handling() Sets the global variables defining the
# RPATH behavior
#
# mxware_set_default_rpath_properties(TARGET <Target name> ) Sets the default
# rpath properties for a library, that is the no RPATH in the install target and
# no RPATH in the build targets
#
# mxware_set_full_rpath_properties(TARGET <Target name> ) Sets the RPATH
# properties with full rpath embedded in the targets
#
# mxware_set_all_default_properties( TARGET <Target name>) Sets all the default
# properties for the given target (warning, compiler, rpath)
#
# mxware_add_cmakeconfig() Adds the CMake configuration to the project
#
# AddMXWareTest(TESTNAME <test_name> TESTSRC <list of sources> DEPENDENCIES
# <list of dependencies>)
#
# AddMXWarePlugin(NAME <plugin name> VERSION <the plugin version> SOURCES <list
# of source files> DEPENDENCIES <list of dependencies> INSTALL_DIR <location
# where to install the plugin> )

# ##############################################################################
# Enables all warnings for each set of compilers PS Note that for MSVC all
# warnings are treated as errors, maybe I should remove it for consistency
function(mxware_enable_default_warnings target_name)
  target_compile_options(
    ${target_name}
    PRIVATE
      $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
      -Wall
      -Wextra
      -pedantic
      -Wfatal-errors
      -Wno-missing-braces
      -Wshadow>
      $<$<CXX_COMPILER_ID:MSVC>:/W3>)
endfunction()

# ##############################################################################
# Sets the typical compile options for a given target, besides the warnings
# which are set previously
function(mxware_enable_default_compiler_options target_name)
  target_compile_options(${target_name} PRIVATE $<$<CXX_COMPILER_ID:MSVC>:/EHsc
                                                >)
  # /MDd
endfunction()

# ##############################################################################
# Forces the color output for GNU and Clang in all cases Useful to enable that
# for Ninja build
function(force_color_output)
  option(force_colored_output
         "Always produce ANSI-colored output (GNU/Clang only)." TRUE)
  mark_as_advanced(force_colored_output)
  if(force_colored_output)
    add_compile_options(
      $<$<CXX_COMPILER_ID:GNU>:-fdiagnostics-color=always>
      $<$<AND:$<CXX_COMPILER_ID:Clang>,$<VERSION_GREATER:$<CXX_COMPILER_VERSION>,4.0.0>>:-fcolor-diagnostics>
    )
  endif()
endfunction()

# ##############################################################################
# Sets the default properties for the rpath handling. By default a library
# contains the full rpath at build but no rpath at installation so that the
# dependencies are located through the rpath
macro(mxware_set_global_rpath_handling)
  # use, i.e. don't skip the full RPATH for the build tree
  set(CMAKE_SKIP_BUILD_RPATH FALSE)

  # when building, don't use the install RPATH already (but later on when
  # installing)
  set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

  # the RPATH to be used when installing
  set(CMAKE_INSTALL_RPATH "")

  # don't add the automatically determined parts of the RPATH which point to
  # directories outside the build tree to the install RPATH
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH FALSE)

  # Whether this target on OS X or iOS is located at runtime using rpaths.
  set(CMAKE_MACOSX_RPATH TRUE)

endmacro()

# ##############################################################################
# There are two ways for a library to find its dependencies. They can be
# hardcoded in the library header or they can be relative paths the runtime
# linker should look for. More info here:
# https://gitlab.kitware.com/cmake/community/wikis/doc/cmake/RPATH-handling The
# following function allows to build relocatable libraries which do not encode
# the RPATH in the installed library so that it relies on the runtime linker for
# the linking

function(mxware_set_default_rpath_properties)
  set(options)
  set(multiValueArgs)
  set(oneValueArgs TARGET)

  cmake_parse_arguments(FVARS "${options}" "${oneValueArgs}"
                        "${multiValueArgs}" ${ARGN})

  if(NOT DEFINED FVARS_TARGET)
    message(
      FATAL_ERROR
        "Undefined name for the target for which to set the properties")
  endif(NOT DEFINED FVARS_TARGET)

  set(target_name ${FVARS_TARGET})

  set_property(TARGET ${target_name} PROPERTY SKIP_BUILD_RPATH FALSE)
  set_property(TARGET ${target_name} PROPERTY BUILD_WITH_INSTALL_RPATH FALSE)
  set_property(TARGET ${target_name} PROPERTY INSTALL_RPATH "")
  set_property(TARGET ${target_name} PROPERTY INSTALL_RPATH_USE_LINK_PATH FALSE)
  set_property(TARGET ${target_name} PROPERTY MACOSX_RPATH TRUE)
endfunction()

# ##############################################################################
# The following function sets the library properties so that the rpath is always
# written in the library, even in the install tree. This can be useful if we
# want to make a package able to find the libraries to which to link without
# setting environment variables in case those libraries are not in standard
# locations. On the other hand it makes it impossible to relocate those
# libraries without relinking
function(mxware_set_full_rpath_properties)
  set(options)
  set(multiValueArgs)
  set(oneValueArgs TARGET)

  cmake_parse_arguments(FVARS "${options}" "${oneValueArgs}"
                        "${multiValueArgs}" ${ARGN})

  if(NOT DEFINED FVARS_TARGET)
    message(
      FATAL_ERROR
        "Undefined name for the target for which to set the properties")
  endif(NOT DEFINED FVARS_TARGET)

  set(target_name ${FVARS_TARGET})

  set_property(TARGET ${target_name} PROPERTY SKIP_BUILD_RPATH FALSE)
  set_property(TARGET ${target_name} PROPERTY BUILD_WITH_INSTALL_RPATH FALSE)
  set_property(
    TARGET ${target_name} PROPERTY INSTALL_RPATH
                                   "${CMAKE_INSTALL_PREFIX}/${LIB_INSTALL_DIR}")
  set_property(TARGET ${target_name} PROPERTY INSTALL_RPATH_USE_LINK_PATH TRUE)
  set_property(TARGET ${target_name} PROPERTY MACOSX_RPATH TRUE)
endfunction()

# ##############################################################################
# The function sets all the default properties for the specified target
function(mxware_set_all_default_properties)
  set(options)
  set(multiValueArgs)
  set(oneValueArgs TARGET)

  cmake_parse_arguments(FVARS "${options}" "${oneValueArgs}"
                        "${multiValueArgs}" ${ARGN})
  if(NOT DEFINED FVARS_TARGET)
    message(
      FATAL_ERROR
        "Undefined name for the target for which to set the properties")
  endif(NOT DEFINED FVARS_TARGET)
  set(target_name ${FVARS_TARGET})

  mxware_enable_default_warnings(${target_name})
  mxware_enable_default_compiler_options(${target_name})
  mxware_set_default_rpath_properties(TARGET ${target_name})

endfunction()

# ##############################################################################
# Prepares and sets to install the CMake configuration files that allow this
# library to be discovered and the dependent libraries to be correctly
# configured It assumes the presence of a file called
# <PROJECT_NAME>Config.cmake.in in the same folder where the function is called.
# That file will be used as a template for the package configuration as
# explained in the CMake reference:
# https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html Look also at
# this reference on how to create the template file
# https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html Note
# that the configure_package_config_file does not export any variable as it
# relies on the target export system rather than the CMake 2 variable exporting
# Optionally one can provide the template configuration file at another path
# using the TEMPLATE keyword

function(mxware_add_cmakeconfig)
  set(options)
  set(multiValueArgs PATH_VARS)
  set(oneValueArgs TEMPLATE)

  cmake_parse_arguments(FVARS "${options}" "${oneValueArgs}"
                        "${multiValueArgs}" ${ARGN})

  # Configuring the package for exporting
  include(CMakePackageConfigHelpers)

  # This is a valid destination for both Win and Unix
  set(CMAKE_INSTALL_DIR share/${PROJECT_NAME}/cmake)

  # Creating the target export file
  install(
    EXPORT "${PROJECT_NAME}Targets"
    FILE "${PROJECT_NAME}Targets.cmake"
    NAMESPACE MXWare::
    DESTINATION "${CMAKE_INSTALL_DIR}")

  # Creates the versioning file
  set(VersionFilePath
      "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake")
  write_basic_package_version_file(
    ${VersionFilePath}
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY SameMajorVersion)

  # Prepares the configuration file
  set(ConfigFileName "${PROJECT_NAME}Config.cmake")
  if(NOT DEFINED FVARS_TEMPLATE)
    set(FVARS_TEMPLATE ${CMAKE_CURRENT_LIST_DIR}/${ConfigFileName}.in)
  endif()
  configure_package_config_file(
    "${FVARS_TEMPLATE}" "${CMAKE_CURRENT_BINARY_DIR}/${ConfigFileName}"
    INSTALL_DESTINATION "${CMAKE_INSTALL_DIR}"
    PATH_VARS ${FVARS_PATH_VARS})

  # Installs the configuration file
  install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${ConfigFileName}"
                "${VersionFilePath}" DESTINATION "${CMAKE_INSTALL_DIR}")

endfunction(mxware_add_cmakeconfig)

# ##############################################################################
# Defines a new test unit to be added to the project The function requires an
# option named TESTNAME to provide the name of the test to create The list of
# sources which should be used to create the test are listed after thet TESTSRC
# keyword If no source is given the function assumes that only one is necessary
# which has the same name of the test itself the list of dependencies of the
# test is listed after the DEPENDENCIES keyword

function(AddMXWareTest)

  # set(options INSTALLTEST)
  set(options)
  set(multiValueArgs DEPENDENCIES TESTSRC)
  set(oneValueArgs TESTNAME)

  cmake_parse_arguments(T "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGN})

  if(NOT DEFINED T_TESTNAME)
    message(FATAL_ERROR "Undefined name for the test configure")
  endif()

  if(NOT DEFINED T_TESTSRC)
    message(
      STATUS
        "No source set for the test named ${T_TESTNAME}, assuming a default source"
        " named like the test")
    set(T_TESTSRC "${T_TESTNAME}.cpp")
  endif()

  message(
    STATUS
      "Adding test unit named ${T_TESTNAME} dependent from ${T_DEPENDENCIES}")

  add_executable(${T_TESTNAME} ${T_TESTSRC})
  target_link_libraries(${T_TESTNAME} PRIVATE ${T_DEPENDENCIES})
  mxware_set_all_default_properties(TARGET ${T_TESTNAME})

  add_test(NAME ${T_TESTNAME} COMMAND ${T_TESTNAME})

endfunction()

# ##############################################################################
# Defines the standard folder where the plugin should be installed

# ##############################################################################
# Defines a new plugin unit to be added to the project
function(AddMXWarePlugin)
  set(options)
  set(multiValueArgs SOURCES DEPENDENCIES)
  set(oneValueArgs NAME VERSION INSTALL_DIR)

  cmake_parse_arguments(T "${options}" "${oneValueArgs}" "${multiValueArgs}"
                        ${ARGN})

  if(NOT DEFINED T_NAME)
    message(
      FATAL_ERROR "Plugin name undefined. Set the NAME parameter in this macro")
  endif()

  if(NOT DEFINED T_VERSION)
    message(
      FATAL_ERROR
        "Version undefined for the plugin ${T_NAME}. Set the VERSION parameter")
  endif()

  if(NOT DEFINED T_SOURCES)
    message(
      FATAL_ERROR
        "No sources defined for the plugin ${T_NAME}. Add at least one source")
  endif()

  add_library(${T_NAME} MODULE "")
  target_sources(${T_NAME} PRIVATE ${T_SOURCES})
  target_link_libraries(${T_NAME} PRIVATE ${T_DEPENDENCIES})
  mxware_set_all_default_properties(TARGET ${T_NAME})
  set_property(TARGET ${T_NAME} PROPERTY NO_SONAME TRUE)
  set_property(TARGET ${T_NAME} PROPERTY SOVERSION ${T_VERSION})

  if(DEFINED T_INSTALL_DIR)
    message(STATUS "Installing the plugin ${T_NAME} version ${T_VERSION}")
    install(
      TARGETS ${T_NAME}
      LIBRARY DESTINATION ${T_INSTALL_DIR}
      ARCHIVE DESTINATION ${T_INSTALL_DIR})
  else()
    message(STATUS "Plugin ${T_NAME} to be built but not installed")
  endif()

endfunction(AddMXWarePlugin)
