# - Try to find cppzmq headers and libraries
#
# Usage of this module as follows:
#
#     find_package(cppzmq)
#
# Variables used by this module, they can change the default behaviour and need
# to be set before calling find_package:
#
#  cppzmq_DIR  Set this variable to the root installation of cppzmq
#             if the module has problems finding the proper installation path.
#
# Variables defined by this module:
#
#  cppzmq_FOUND              System has ZeroMQ libs/headers
#  cppzmq_INCLUDE_DIRS       The location of ZeroMQ headers
#  cppzmq_LIBRARIES 		 The location of the cppzmq libraries (that is the zmq library)

include(LibFindMacros)

#Dependencies
libfind_package(cppzmq ZeroMQ)

find_path(cppzmq_DIR
  NAMES include/zmq.hpp
  HINTS ${ZeroMQ_DIR}
  )

find_path(cppzmq_INCLUDE_DIRS
  NAMES zmq.hpp
  HINTS ${cppzmq_DIR}/include
  )
  
list( APPEND cppzmq_INCLUDE_DIRS ${ZeroMQ_INCLUDE_DIRS} )

list( APPEND cppzmq_LIBRARIES ${ZeroMQ_LIBRARIES} )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  cppzmq
  REQUIRED_VARS cppzmq_INCLUDE_DIRS cppzmq_LIBRARIES
  )

mark_as_advanced(
  cppzmq_DIR
  cppzmq_INCLUDE_DIRS
  cppzmq_LIBRARIES
  )