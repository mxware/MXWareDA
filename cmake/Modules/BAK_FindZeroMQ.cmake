# - Try to find ZeroMQ headers and libraries
#
# Usage of this module as follows:
#
#     find_package(ZeroMQ)
#
# Variables used by this module, they can change the default behaviour and need
# to be set before calling find_package:
#
#  ZeroMQ_DIR  Set this variable to the root installation of
#                            ZeroMQ if the module has problems finding
#                            the proper installation path.
#
# Variables defined by this module:
#
#  ZeroMQ_FOUND              System has ZeroMQ libs/headers
#  ZeroMQ_LIBRARIES          The ZeroMQ libraries
#  ZeroMQ_INCLUDE_DIRS        The location of ZeroMQ headers
#  ZeroMQ_VERSION            The version of ZeroMQ

find_path(ZeroMQ_DIR
  NAMES include/zmq.h
  )
 
set( ZeroMQ_INCLUDE_DIRS ${ZeroMQ_DIR}/include )

file(READ ${ZeroMQ_INCLUDE_DIRS}/zmq.h file_contents)
	
string(REGEX MATCH "ZMQ_VERSION_MAJOR ([0-9]+)" _  "${file_contents}" )	
if(NOT CMAKE_MATCH_COUNT EQUAL 1)
	message(FATAL_ERROR "Could not extract major version number from zmq.h")
endif()	
set(ver_major ${CMAKE_MATCH_1})
	
string(REGEX MATCH "ZMQ_VERSION_MINOR ([0-9]+)" _  "${file_contents}" )	
if(NOT CMAKE_MATCH_COUNT EQUAL 1)
    message(FATAL_ERROR "Could not extract minor version number from zmq.h")
endif()
set(ver_minor ${CMAKE_MATCH_1})
	
string(REGEX MATCH "ZMQ_VERSION_PATCH ([0-9]+)" _  "${file_contents}" )	
if(NOT CMAKE_MATCH_COUNT EQUAL 1)
    message(FATAL_ERROR "Could not extract patch version number from zmq.h")
endif()
set(ver_patch ${CMAKE_MATCH_1})	

set(ZeroMQ_VERSION "${ver_major}.${ver_minor}.${ver_patch}")

# if Linux or Apple
if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux" 
	OR CMAKE_HOST_SYSTEM_NAME STREQUAL "Darwin")
	find_library(ZeroMQ_LIBRARIES
		NAMES zmq libzmq
	    HINTS ${ZeroMQ_DIR}/lib
    )
elseif(CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")	
	find_library(ZeroMQ_LIBRARY_RELEASE
		NAMES "libzmq-mt-${ver_major}_${ver_minor}_${ver_patch}" zmq libzmq
		HINTS ${ZeroMQ_DIR}/bin
              ${ZeroMQ_DIR}/lib  )

	find_library(ZeroMQ_LIBRARY_DEBUG
		NAMES "libzmq-mt-gd-${ver_major}_${ver_minor}_${ver_patch}" zmq libzmq
		HINTS ${ZeroMQ_DIR}/bin
              ${ZeroMQ_DIR}/lib  )
	if(ZeroMQ_LIBRARY_RELEASE AND ZeroMQ_LIBRARY_DEBUG)
		set(ZeroMQ_LIBRARIES
		    debug ${ZeroMQ_LIBRARY_DEBUG}
		    optimized ${ZeroMQ_LIBRARY_RELEASE}
		    )
    elseif(ZeroMQ_LIBRARY_RELEASE)
		set(ZeroMQ_LIBRARIES ${ZeroMQ_LIBRARY_RELEASE})
    elseif(ZeroMQ_LIBRARY_DEBUG)
	    set(ZeroMQ_LIBRARIES ${ZeroMQ_LIBRARY_DEBUG})
	endif()
else()
message( FATAL_ERROR "Host system name: ${CMAKE_HOST_SYSTEM_NAME}. Unable to find ZeroMQ in this system")
endif()
# endif

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  ZeroMQ
  REQUIRED_VARS ZeroMQ_LIBRARIES ZeroMQ_INCLUDE_DIRS
  VERSION_VAR ZeroMQ_VERSION
  )

mark_as_advanced( ZeroMQ_DIR
				  ZeroMQ_INCLUDE_DIRS
				  ZeroMQ_LIBRARIES
				  ZeroMQ_VERSION
				  )

  
  

#if(MSVC)
#  #add in all the names it can have on windows
#  if(CMAKE_GENERATOR_TOOLSET MATCHES "v140" OR MSVC14)
#    set(_zmq_TOOLSET "-v140")
#  elseif(CMAKE_GENERATOR_TOOLSET MATCHES "v120" OR MSVC12)
#    set(_zmq_TOOLSET "-v120")
#  elseif(CMAKE_GENERATOR_TOOLSET MATCHES "v110_xp")
#    set(_zmq_TOOLSET "-v110_xp")
#  elseif(CMAKE_GENERATOR_TOOLSET MATCHES "v110" OR MSVC11)
#    set(_zmq_TOOLSET "-v110")
#  elseif(CMAKE_GENERATOR_TOOLSET MATCHES "v100" OR MSVC10)
#    set(_zmq_TOOLSET "-v100")
#  elseif(CMAKE_GENERATOR_TOOLSET MATCHES "v90" OR MSVC90)
#    set(_zmq_TOOLSET "-v90")
#  endif()
#
#  set(_zmq_versions
#     "4_3_3" "4_1_5" "4_1_4" "4_1_3" "4_1_2" "4_1_1" "4_1_0"
#     "4_0_8" "4_0_7" "4_0_6" "4_0_5" "4_0_4" "4_0_3" "4_0_2" "4_0_1" "4_0_0"
#     "3_2_5" "3_2_4" "3_2_3" "3_2_2"  "3_2_1" "3_2_0" "3_1_0")
#
#  set(_zmq_release_names)
#  set(_zmq_debug_names)
#  foreach( ver ${_zmq_versions})
#    list(APPEND _zmq_release_names "libzmq-mt-${ver}")
#    list(APPEND _zmq_release_names "libzmq${_zmq_TOOLSET}-mt-${ver}")
#  endforeach()
#  foreach( ver ${_zmq_versions})
#    list(APPEND _zmq_debug_names "libzmq${_zmq_TOOLSET}-mt-gd-${ver}")
#  endforeach()
#
#  #now try to find the release and debug version
#  find_library(ZeroMQ_LIBRARY_RELEASE
#    NAMES ${_zmq_release_names} zmq libzmq
#    HINTS ${ZeroMQ_ROOT_DIR}/bin
#          ${ZeroMQ_ROOT_DIR}/lib
#    )
#
#  find_library(ZeroMQ_LIBRARY_DEBUG
#    NAMES ${_zmq_debug_names} zmq libzmq
#    HINTS ${ZeroMQ_ROOT_DIR}/bin
#          ${ZeroMQ_ROOT_DIR}/lib
#    )
#
#  if(ZeroMQ_LIBRARY_RELEASE AND ZeroMQ_LIBRARY_DEBUG)
#    set(ZeroMQ_LIBRARY
#        debug ${ZeroMQ_LIBRARY_DEBUG}
#        optimized ${ZeroMQ_LIBRARY_RELEASE}
#        )
#  elseif(ZeroMQ_LIBRARY_RELEASE)
#    set(ZeroMQ_LIBRARY ${ZeroMQ_LIBRARY_RELEASE})
#  elseif(ZeroMQ_LIBRARY_DEBUG)
#    set(ZeroMQ_LIBRARY ${ZeroMQ_LIBRARY_DEBUG})
#  endif()
#
#else()
#  find_library(ZeroMQ_LIBRARY
#    NAMES zmq libzmq
#    HINTS ${ZeroMQ_ROOT_DIR}/lib
#    )
#endif()


#We are using the 2.8.10 signature of find_package_handle_standard_args,
#as that is the version that ParaView 5.1 && VTK 6/7 ship, and inject
#into the CMake module path. This allows our FindModule to work with
#projects that include VTK/ParaView before searching for Remus
#include(FindPackageHandleStandardArgs)
#find_package_handle_standard_args(
#  ZeroMQ
#  REQUIRED_VARS ZeroMQ_LIBRARY ZeroMQ_INCLUDE_DIR
#  VERSION_VAR ZeroMQ_VER
#  )
#
#set(ZeroMQ_FOUND ${ZEROMQ_FOUND})
#set(ZeroMQ_INCLUDE_DIRS ${ZeroMQ_INCLUDE_DIR})
#set(ZeroMQ_LIBRARIES ${ZeroMQ_LIBRARY})
#set(ZeroMQ_VERSION ${ZeroMQ_VER})
#
#mark_as_advanced(
#  ZeroMQ_ROOT_DIR
#  ZeroMQ_LIBRARY
#  ZeroMQ_LIBRARY_DEBUG
#  ZeroMQ_LIBRARY_RELEASE
#  ZeroMQ_INCLUDE_DIR
#  ZeroMQ_VERSION
#  )