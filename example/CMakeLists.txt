set(files2configure SimpleMXRunConfig.xml)

foreach(file ${files2configure})
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${file}
                 ${CMAKE_CURRENT_BINARY_DIR}/${file} @ONLY)
endforeach()
